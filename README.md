Sheepland
=========

Sheepland is a board game invented by Cranio Creations.
You can find more details on the game itself here:
http://www.craniocreations.com/it/boardView.php?id=8

This project is creating a multi-player only computer
replica of the board game.

This project is part of the Software Engineering class
at Politecnico di Milano, academic year 2013-2014.
For this reason, it is currently licensed proprietary.
