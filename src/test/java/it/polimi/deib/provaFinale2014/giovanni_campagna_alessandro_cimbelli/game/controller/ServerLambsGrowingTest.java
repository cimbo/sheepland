package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

public class ServerLambsGrowingTest {
    public SheepTileIdentifier sentOutIdentifier;
    public Sheep[] sentOutNewAdultSheep;
    public HashMap<SheepTileIdentifier, Sheep[]> sentOutMap;

    @Before
    public void clearOutTemporaries() {
        sentOutIdentifier = null;
        sentOutNewAdultSheep = null;
        sentOutMap = new HashMap<SheepTileIdentifier, Sheep[]>();
    }

    @Test
    public void singleTileTest() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine();

        // retrieve land/1
        SheepTileIdentifier stId = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st = engine.getSheepTileFromId(stId);

        // we're using a fake random, so there will be a ram here
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));

        // remove it and replace it with a lamb that is about to grow
        st.removeSheep(st.getAnySheepOfType(SheepType.RAM));
        Lamb newLamb = new Lamb(Lamb.GROWING_AGE - 1);
        engine.getGameBoard().addSheep(newLamb);
        st.addSheep(newLamb);

        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(1, st.getNumberOfSheep(SheepType.LAMB));

        engine.growLambsOnTile(st);

        // the lamb was replaced with something else, and that something else
        // is again a ram (because our random said 1)
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));
        assertFalse(st.containsSheep(newLamb));
        Sheep newRam = st.getAnySheepOfType(SheepType.RAM);
        assertNotNull(newRam);
        assertNotNull(newRam.getId());

        // and we sent out the information on the wire
        assertEquals(stId, sentOutIdentifier);
        assertEquals(1, sentOutNewAdultSheep.length);
        assertEquals(newRam, sentOutNewAdultSheep[0]);
    }

    @Test
    public void notGrowingTest() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine();

        // retrieve land/1
        SheepTileIdentifier stId = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st = engine.getSheepTileFromId(stId);

        // we're using a fake random, so there will be a ram here
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));

        // remove it and replace it with a lamb that is not about to grow
        st.removeSheep(st.getAnySheepOfType(SheepType.RAM));
        Lamb newLamb = new Lamb(0);
        engine.getGameBoard().addSheep(newLamb);
        st.addSheep(newLamb);

        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(1, st.getNumberOfSheep(SheepType.LAMB));

        engine.growLambsOnTile(st);

        // the lamb was not replaced
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(1, st.getNumberOfSheep(SheepType.LAMB));
        assertTrue(st.containsSheep(newLamb));
        assertEquals(1, newLamb.getAge());

        // but we still sent the information on the wire
        assertEquals(stId, sentOutIdentifier);
        assertEquals(0, sentOutNewAdultSheep.length);
    }

    @Test
    public void twoLambsTest() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine();

        // retrieve land/1
        SheepTileIdentifier stId = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st = engine.getSheepTileFromId(stId);

        // we're using a fake random, so there will be a ram here
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));

        // remove it and replace it with two lambs that are about to grow
        st.removeSheep(st.getAnySheepOfType(SheepType.RAM));
        Lamb newLamb1 = new Lamb(Lamb.GROWING_AGE - 1);
        engine.getGameBoard().addSheep(newLamb1);
        st.addSheep(newLamb1);
        Lamb newLamb2 = new Lamb(Lamb.GROWING_AGE - 1);
        engine.getGameBoard().addSheep(newLamb2);
        st.addSheep(newLamb2);

        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(2, st.getNumberOfSheep(SheepType.LAMB));

        engine.growLambsOnTile(st);

        // the lamb was replaced with something else, and that something else
        // is again a ram (because our random said 1)
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(2, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));
        assertFalse(st.containsSheep(newLamb1));
        assertFalse(st.containsSheep(newLamb2));
        Sheep[] rams = st.getAllSheep();
        assertNotNull(rams[0].getId());
        assertNotNull(rams[1].getId());

        // and we sent out the information on the wire
        assertEquals(stId, sentOutIdentifier);
        assertEquals(2, sentOutNewAdultSheep.length);
        if (rams[0].equals(sentOutNewAdultSheep[0])) {
            assertEquals(rams[1], sentOutNewAdultSheep[1]);
        } else {
            assertEquals(rams[0], sentOutNewAdultSheep[1]);
            assertEquals(rams[1], sentOutNewAdultSheep[0]);
        }
    }

    @Test
    public void twoLambsOneGrowingTest() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine();

        // retrieve land/1
        SheepTileIdentifier stId = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st = engine.getSheepTileFromId(stId);

        // we're using a fake random, so there will be a ram here
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));

        // remove it and replace it with two lambs, one of which is about to
        // grow
        st.removeSheep(st.getAnySheepOfType(SheepType.RAM));
        Lamb newLamb1 = new Lamb(0);
        engine.getGameBoard().addSheep(newLamb1);
        st.addSheep(newLamb1);
        Lamb newLamb2 = new Lamb(Lamb.GROWING_AGE - 1);
        engine.getGameBoard().addSheep(newLamb2);
        st.addSheep(newLamb2);

        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(2, st.getNumberOfSheep(SheepType.LAMB));

        engine.growLambsOnTile(st);

        // the second lamb was replaced with something else, and that something
        // else
        // is again a ram (because our random said 1)
        // the first lamb is still there
        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(1, st.getNumberOfSheep(SheepType.LAMB));
        assertTrue(st.containsSheep(newLamb1));
        assertFalse(st.containsSheep(newLamb2));
        assertEquals(1, newLamb1.getAge());
        Sheep[] sheep = st.getAllSheep();

        // and we sent out the information on the wire
        assertEquals(stId, sentOutIdentifier);
        assertEquals(1, sentOutNewAdultSheep.length);
        if (sheep[0].equals(sentOutNewAdultSheep[0])) {
            assertNotNull(sheep[0].getId());
            assertEquals(sheep[1], newLamb1);
        } else {
            assertEquals(sheep[1], sentOutNewAdultSheep[0]);
            assertNotNull(sheep[1].getId());
            assertEquals(sheep[0], newLamb1);
        }
    }

    @Test
    public void allLambs() throws InvalidIdentifierException,
            RuleViolationException {
        final HashMap<SheepTileIdentifier, Sheep[]> sentOutInformation = new HashMap<SheepTileIdentifier, Sheep[]>();

        ServerGameEngine engine = getAGameEngine(new EmptyServerDelegate() {
            @Override
            public void didGrowLambs(SheepTileIdentifier tile,
                    Sheep[] newAdultSheep) {
                if (sentOutInformation.containsKey(tile)) {
                    fail("Duplicate information for tile " + tile);
                }

                sentOutInformation.put(tile, newAdultSheep);
            }
        });

        engine.growAllLambs();

        // there are no lambs on this board, but we still send out the info
        // for all tiles
        assertEquals(19, sentOutInformation.size());
    }

    private ServerGameEngine getAGameEngine() throws RuleViolationException {
        return getAGameEngine(new EmptyServerDelegate() {
            @Override
            public void didGrowLambs(SheepTileIdentifier tile,
                    Sheep[] newAdultSheep) {
                sentOutIdentifier = tile;
                sentOutNewAdultSheep = newAdultSheep;
            }
        });
    }

    private ServerGameEngine getAGameEngine(ServerDelegate delegate)
            throws RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();

        ServerGameEngine engine = new ServerGameEngine(new FakeRandom(),
                delegate, rules);
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();

        return engine;
    }
}
