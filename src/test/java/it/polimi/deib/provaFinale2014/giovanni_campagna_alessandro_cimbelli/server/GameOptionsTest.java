package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;

import org.junit.Before;
import org.junit.Test;

public class GameOptionsTest {
    private GameOptions defaults;

    @Before
    public void setUp() {
        defaults = new GameOptions();
    }

    @Test
    public void everythingCorrect() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n"
                + "GAME_TURN_TIMEOUT=102\n"
                + "GAME_TURN_DISCONNECTED_TIMEOUT=103\n" + "PING_TIMEOUT=104\n";
        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(102, options.getGameTurnTimeout());
        assertEquals(103, options.getGameTurnDisconnectedTimeout());
        assertEquals(104, options.getPingTimeout());
    }

    @Test
    public void comments() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n" + "# a comment\n"
                + "GAME_TURN_TIMEOUT=102\n";

        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(102, options.getGameTurnTimeout());
    }

    @Test
    public void invalidComment() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n"
                + "    # a comment, does not start the line\n"
                + "GAME_TURN_TIMEOUT=102\n";

        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(defaults.getGameTurnTimeout(),
                options.getGameTurnTimeout());
    }

    @Test
    public void noNewLineAtEnd() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n"
                + "GAME_TURN_TIMEOUT=102\n"
                + "GAME_TURN_DISCONNECTED_TIMEOUT=103\n" + "PING_TIMEOUT=104";
        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(102, options.getGameTurnTimeout());
        assertEquals(103, options.getGameTurnDisconnectedTimeout());
        assertEquals(104, options.getPingTimeout());
    }

    @Test
    public void unknownKey() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n"
                + "GAME_TURN_TIMEOUT=102\n" + "FOO_TURN_TIMEOUT=7\n"
                + "GAME_TURN_DISCONNECTED_TIMEOUT=103\n" + "PING_TIMEOUT=104\n";
        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(102, options.getGameTurnTimeout());
        assertEquals(103, options.getGameTurnDisconnectedTimeout());
        assertEquals(104, options.getPingTimeout());
    }

    @Test
    public void invalidInteger() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n"
                + "GAME_TURN_TIMEOUT=foo\n"
                + "GAME_TURN_DISCONNECTED_TIMEOUT=103\n";
        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(defaults.getGameTurnTimeout(),
                options.getGameTurnTimeout());
        assertEquals(defaults.getGameTurnDisconnectedTimeout(),
                options.getGameTurnDisconnectedTimeout());
    }

    @Test
    public void noEqualKey() {
        String configFile = "GAME_STARTING_TIMEOUT=101\n"
                + "GAME_TURN_TIMEOUT=foo\n"
                + "GAME_TURN_DISCONNECTED_TIMEOUT=103\n";
        GameOptions options = new GameOptions(new StringReader(configFile));

        assertEquals(101, options.getGameStartingTimeout());
        assertEquals(defaults.getGameTurnTimeout(),
                options.getGameTurnTimeout());
        assertEquals(defaults.getGameTurnDisconnectedTimeout(),
                options.getGameTurnDisconnectedTimeout());
    }
}
