package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.TwoPlayersRuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ForceEndTurnMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.InitiallyPlaceShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ServerGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerSender;

import java.io.IOException;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ServerPlayerTest {
    private GameIncubator incubator;
    private int pending;
    private boolean loginAnswer;
    private boolean closed;
    private boolean allowGetSender;
    private GameBoard sentBoard;
    private PlayerIdentifier sentPlayer;
    private RuleSet sentRules;
    private ServerConnection sentMoveOn;
    private Move sentMove;
    private ServerConnection sentAckOn;
    private Exception sentAck;
    private GameState sentState;
    private Map<PlayerIdentifier, Integer> sentScores;
    private static User marco;
    private static User mario;
    private static User[] testUsers;

    private synchronized void waitDone() {
        while (pending > 0) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    @BeforeClass
    public static void initUserDB() throws InvalidPasswordException {
        testUsers = new User[4];
        for (int i = 0; i < 4; i++) {
            testUsers[i] = UserDatabase.get().getOrCreateUser(
                    "testuser" + Integer.toString(i), Integer.toString(i));
        }
        marco = UserDatabase.get().getOrCreateUser("marco", "polo");
        mario = UserDatabase.get().getOrCreateUser("mario", "ahiahiahi");
    }

    @Before
    public void setUp() {
        incubator = new GameIncubator(new FakeGameOptions());
        pending = 0;
        loginAnswer = false;
        closed = false;
        allowGetSender = false;
        sentAck = null;
        sentAckOn = null;
        sentBoard = null;
        sentMove = null;
        sentMoveOn = null;
        sentPlayer = null;
        sentRules = null;
        sentState = null;
        sentScores = null;
        marco.detachPlayer();
        mario.detachPlayer();
        for (int i = 0; i < 4; i++) {
            testUsers[i].detachPlayer();
        }
    }

    @After
    public void tearDown() {
        incubator.close();
        assertNull(marco.getCurrentPlayer());
        assertNull(mario.getCurrentPlayer());
        for (int i = 0; i < 4; i++) {
            assertNull(testUsers[i].getCurrentPlayer());
        }
    }

    @Test(timeout = 5000)
    public void connectionAndLogin() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendLoginAnswer(boolean answer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    loginAnswer = answer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());

        pending++;
        player.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(true, loginAnswer);
        assertEquals(player, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        incubator.maybeRemoveServerPlayer(player);
        marco.detachPlayer(player);
        connection.close();
    }

    @Test(timeout = 5000)
    public void wrongLogin() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendLoginAnswer(boolean answer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    loginAnswer = answer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());

        pending++;
        player.doLoginToServer("marco", "pippo");
        waitDone();
        assertEquals(false, loginAnswer);
        assertNull(marco.getCurrentPlayer());
        assertFalse(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        connection.close();
    }

    @Test(timeout = 5000)
    public void relogin() {
        ServerConnection firstConnection = new FakeServerConnection() {
            @Override
            public void setServerReceiver(ServerReceiver receiver) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    pending--;
                    ServerPlayerTest.this.notify();
                }
                super.setServerReceiver(receiver);
            }

            @Override
            public void sendLoginAnswer(boolean answer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    loginAnswer = answer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }

            @Override
            public void close() {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    closed = true;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
                super.close();
            }
        };
        ServerConnection secondConnection = new FakeServerConnection() {
            @Override
            public ServerSender getServerSender() {
                if (!allowGetSender) {
                    fail();
                }
                return this;
            }
        };

        pending = 2; // a setServerReceiver and a sendLoginAnswer
        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());

        firstPlayer.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(true, loginAnswer);

        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());

        pending = 1; // a close
        // the login answer will happen on the second connection
        // (and now we allow the getSender() for it)
        allowGetSender = true;
        secondPlayer.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(true, closed);

        assertEquals(firstPlayer, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(firstPlayer));
        assertFalse(incubator.containsServerPlayer(secondPlayer));
        assertFalse(incubator.isTimerRunning());

        incubator.maybeRemoveServerPlayer(firstPlayer);
        marco.detachPlayer(firstPlayer);
        secondConnection.close();
    }

    @Test(timeout = 10000)
    public void pinging() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendPing() throws IOException {
                synchronized (ServerPlayerTest.this) {
                    // unlike other tests, we allow more pings than
                    // we expect here
                    if (pending > 0) {
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }
                super.sendPing();
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions() {
                    @Override
                    public int getPingTimeout() {
                        return 1;
                    }
                });

        pending = 1;
        player.doLoginToServer("marco", "polo");

        waitDone();

        connection.close();
        marco.detachPlayer(player);
    }

    @Test(timeout = 20000)
    public void pingTimeout() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendPing() {
                // look ma, no super!
            }

            @Override
            public void close() {
                simpleComplete();
                super.close();
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions() {
                    @Override
                    public int getPingTimeout() {
                        return 1;
                    }
                });

        pending = 1;
        player.doLoginToServer("marco", "polo");

        waitDone();
        // awesome, the connection was properly closed due to ping
        // timeout

        marco.detachPlayer(player);
    }

    @Test(timeout = 5000)
    public void abandonBeforeGameStart() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendLoginAnswer(boolean answer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    loginAnswer = answer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }

            @Override
            public void sendAbandonComplete() {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());

        pending++;
        player.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(true, loginAnswer);
        assertEquals(player, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        pending++;
        player.doAbandonGame();
        waitDone();
        // can't do, triggers assert fail if you don't own the
        // game lock (correctly)
        // assertTrue(player.isAbandoned());
        assertNull(marco.getCurrentPlayer());
        assertFalse(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        connection.close();
    }

    @Test(timeout = 5000)
    public void abandonBeforeLogin() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendAbandonComplete() {
                fail();
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());

        player.doAbandonGame();
        // can't do, triggers assert fail if you don't own the
        // game lock (correctly)
        // assertFalse(player.isAbandoned());

        connection.close();
    }

    @Test(timeout = 5000)
    public void disconnectBeforeGameStart() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendLoginAnswer(boolean answer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    loginAnswer = answer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());

        pending = 1;
        player.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(true, loginAnswer);
        assertEquals(player, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        connection.close();

        // the player is still canonical for marco, because he
        // did not abandon/close the connection cleanly
        assertEquals(player, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        incubator.maybeRemoveServerPlayer(player);
        marco.detachPlayer(player);
    }

    @Test(timeout = 5000)
    public void gameStartRace() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendLoginAnswer(boolean answer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    loginAnswer = answer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }

            @Override
            public void sendSetup(RuleSet rules, GameBoard board,
                    PlayerIdentifier self) {
                fail();
            }
        };
        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());

        pending = 1;
        player.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(true, loginAnswer);
        assertEquals(player, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        // even though the timer is not running, we force a starting now,
        // as if the timer fired and another player disconnected at the same
        // time
        incubator.startGameNow();

        // the player is still canonical for marco and still in the incubator,
        // because the game was not actually started
        assertEquals(player, marco.getCurrentPlayer());
        assertTrue(incubator.containsServerPlayer(player));
        assertFalse(incubator.isTimerRunning());

        connection.close();
        // don't remove the player from the incubator here (so we test
        // incubator.close() too)
        marco.detachPlayer(player);
    }

    @Test(timeout = 10000)
    public void gameStartByTimeout() {
        GameIncubator goodIncubator = new GameIncubator(new FakeGameOptions() {
            @Override
            public int getGameStartingTimeout() {
                return 1;
            }
        });

        ServerConnection firstConnection = new FakeServerConnection() {
            @Override
            public void sendSetup(RuleSet rules, GameBoard board,
                    PlayerIdentifier selfPlayer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    sentRules = rules;
                    sentBoard = board;
                    sentPlayer = selfPlayer;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerConnection secondConnection = new FakeServerConnection() {
            @Override
            public void sendSetup(RuleSet rules, GameBoard board,
                    PlayerIdentifier selfPlayer) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection,
                goodIncubator, new FakeGameOptions());
        firstPlayer.doLoginToServer("mario", "ahiahiahi");

        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                goodIncubator, new FakeGameOptions());

        pending = 2;
        secondPlayer.doLoginToServer("marco", "polo");

        waitDone();

        assertEquals(firstPlayer, mario.getCurrentPlayer());
        assertNotNull(firstPlayer.getPlayer());
        assertEquals("mario", firstPlayer.getPlayer().getName());
        assertFalse(goodIncubator.containsServerPlayer(firstPlayer));

        assertEquals(secondPlayer, marco.getCurrentPlayer());
        assertNotNull(secondPlayer.getPlayer());
        assertEquals("marco", secondPlayer.getPlayer().getName());
        assertFalse(goodIncubator.containsServerPlayer(secondPlayer));

        Game game = firstPlayer.getGame();
        assertNotNull(game);
        assertEquals(game.getEngine().getRules(), sentRules);
        assertTrue(game.getEngine().getRules() instanceof TwoPlayersRuleSet);
        assertEquals(firstPlayer.getPlayer().getId(), sentPlayer);

        assertNotEquals(game.getEngine().getGameBoard(), sentBoard);
        assertNotNull(sentBoard.getPlayingOrder());

        Player sentPlayers[] = sentBoard.getAllPlayers();
        if (sentPlayers[0].getName().equals("mario")) {
            assertEquals(firstPlayer.getPlayer().getId(),
                    sentPlayers[0].getId());
            assertEquals("marco", sentPlayers[1].getName());
            assertEquals(secondPlayer.getPlayer().getId(),
                    sentPlayers[1].getId());
        } else {
            assertEquals("marco", sentPlayers[0].getName());
            assertEquals(secondPlayer.getPlayer().getId(),
                    sentPlayers[0].getId());
            assertEquals("mario", sentPlayers[1].getName());
            assertEquals(firstPlayer.getPlayer().getId(),
                    sentPlayers[1].getId());
        }

        firstConnection.close();
        secondConnection.close();

        synchronized (game) {
            firstPlayer.setYourTurn(false);
            secondPlayer.setYourTurn(false);
        }
        mario.detachPlayer(firstPlayer);
        marco.detachPlayer(secondPlayer);
        goodIncubator.close();
    }

    private synchronized void simpleComplete() {
        if (pending <= 0) {
            fail();
        }
        pending--;
        notify();
    }

    class TestServerConnection extends FakeServerConnection {
        @Override
        public void sendSetup(RuleSet rules, GameBoard board,
                PlayerIdentifier selfPlayer) {
            simpleComplete();
        }

        @Override
        public void sendMoveAcknowledged(Exception e) {
            synchronized (ServerPlayerTest.this) {
                if (pending <= 0) {
                    fail();
                }
                sentAckOn = this;
                sentAck = e;
                pending--;
                ServerPlayerTest.this.notify();
            }
        }

        @Override
        public void sendDidExecuteMove(Move move) {
            synchronized (ServerPlayerTest.this) {
                if (pending <= 0) {
                    fail();
                }
                sentMoveOn = this;
                sentMove = move;
                pending--;
                ServerPlayerTest.this.notify();
            }
        }

        @Override
        public void sendEndGame(Map<PlayerIdentifier, Integer> scores) {
            synchronized (ServerPlayerTest.this) {
                if (pending <= 0) {
                    fail();
                }
                sentScores = scores;
                pending--;
                ServerPlayerTest.this.notify();
            }
        }

        @Override
        public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
            simpleComplete();
        }

        @Override
        public void sendBlackSheepMoved(SheepTileIdentifier destination) {
            simpleComplete();
        }

        @Override
        public void sendWolfMoved(SheepTileIdentifier destination,
                SheepIdentifier eatenSheep) {
            simpleComplete();
        }

        @Override
        public void sendLambsGrew(SheepTileIdentifier destination,
                Sheep[] newAdultSheep) {
            simpleComplete();
        }

        @Override
        public void sendAbandonComplete() {
            simpleComplete();
        }
    }

    @Test(timeout = 5000)
    public void gameStartByThreshold() throws InvalidPasswordException {
        ServerConnection[] connections = new ServerConnection[4];
        ServerPlayer[] players = new ServerPlayer[4];

        pending = 4;
        for (int i = 0; i < 4; i++) {
            connections[i] = new TestServerConnection();
            players[i] = new ServerPlayer(connections[i], incubator,
                    new FakeGameOptions());
        }

        for (int i = 0; i < 4; i++) {
            players[i].doLoginToServer("testuser" + Integer.toString(i),
                    Integer.toString(i));
        }

        waitDone();
        // Reaching this point is enough for a successful test, it
        // means the game started

        Game game = players[0].getGame();
        assertFalse(game.getEngine().getRules() instanceof TwoPlayersRuleSet);
        for (int i = 0; i < 4; i++) {
            assertNotNull(players[i].getGame());
            assertEquals(game, players[i].getGame());
            assertNotNull(players[i].getPlayer());
            assertEquals("testuser" + Integer.toString(i), players[i]
                    .getPlayer().getName());
        }

        synchronized (game) {
            for (int i = 0; i < 4; i++) {
                players[i].setYourTurn(false);
            }
        }
        for (int i = 0; i < 4; i++) {
            connections[i].close();
        }
        for (int i = 0; i < 4; i++) {
            User user = UserDatabase.get().getOrCreateUser(
                    "testuser" + Integer.toString(i), Integer.toString(i));

            user.detachPlayer(players[i]);
            assertNull(user.getCurrentPlayer());
        }
    }

    @Test(timeout = 5000)
    public void gameStartByThresholdInterleaved()
            throws InvalidPasswordException {
        ServerConnection[] connections = new ServerConnection[4];
        ServerPlayer[] players = new ServerPlayer[4];

        pending = 4;
        for (int i = 0; i < 4; i++) {
            connections[i] = new TestServerConnection();
            players[i] = new ServerPlayer(connections[i], incubator,
                    new FakeGameOptions());
            players[i].doLoginToServer("testuser" + Integer.toString(i),
                    Integer.toString(i));
        }

        waitDone();
        // Reaching this point is enough for a successful test, it
        // means the game started

        Game game = players[0].getGame();
        assertFalse(game.getEngine().getRules() instanceof TwoPlayersRuleSet);
        for (int i = 0; i < 4; i++) {
            assertNotNull(players[i].getGame());
            assertEquals(game, players[i].getGame());
            assertNotNull(players[i].getPlayer());
            assertEquals("testuser" + Integer.toString(i), players[i]
                    .getPlayer().getName());
        }

        synchronized (game) {
            for (int i = 0; i < 4; i++) {
                players[i].setYourTurn(false);
            }
        }
        for (int i = 0; i < 4; i++) {
            connections[i].close();
        }
        for (int i = 0; i < 4; i++) {
            User user = UserDatabase.get().getOrCreateUser(
                    "testuser" + Integer.toString(i), Integer.toString(i));

            user.detachPlayer(players[i]);
            assertNull(user.getCurrentPlayer());
        }
    }

    @Test(timeout = 5000)
    public void gameAbandon() throws InvalidPasswordException {
        ServerConnection[] connections = new ServerConnection[4];
        ServerPlayer[] players = new ServerPlayer[4];

        pending = 4;
        for (int i = 0; i < 4; i++) {
            connections[i] = new FakeServerConnection() {
                @Override
                public void sendSetup(RuleSet rules, GameBoard board,
                        PlayerIdentifier self) {
                    synchronized (ServerPlayerTest.this) {
                        if (pending <= 0) {
                            fail();
                        }
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendAbandonComplete() {
                    synchronized (ServerPlayerTest.this) {
                        if (pending <= 0) {
                            fail();
                        }
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }
            };
            players[i] = new ServerPlayer(connections[i], incubator,
                    new FakeGameOptions());
            players[i].doLoginToServer("testuser" + Integer.toString(i),
                    Integer.toString(i));
        }
        try {
            waitDone();

            Game game = players[0].getGame();
            assertNotNull(game);

            pending = 4;
            for (int i = 0; i < 4; i++) {
                players[i].doAbandonGame();
            }
            synchronized (game) {
                assertTrue(game.isAbandoned());
            }
            waitDone();
        } finally {
            for (int i = 0; i < 4; i++) {
                connections[i].close();
            }
        }
    }

    @Test(timeout = 5000)
    public void goodExecuteMove() throws InvalidPasswordException {
        ServerConnection firstConnection = new TestServerConnection();
        ServerConnection secondConnection = new TestServerConnection();

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            try {
                ShepherdIdentifier shepherdId = new ShepherdIdentifier(1);
                // between sheepsburg and desert/0
                RoadSquareIdentifier rsId = new RoadSquareIdentifier(0,
                        1 + 3 * TerrainType.DESERT.ordinal() + 0);
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new InitiallyPlaceShepherdMove(playerId,
                            shepherdId, rsId);

                    pending = 4; // ack + didExecuteMove + 2*advanceNextTurn
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(firstConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(secondConnection, sentMoveOn);
                } else {
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new InitiallyPlaceShepherdMove(playerId,
                            shepherdId, rsId);

                    pending = 4; // ack + didExecuteMove + 2*advanceNextTurn
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(secondConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(firstConnection, sentMoveOn);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void goodExecuteMoveWithBlackAndLambs()
            throws InvalidPasswordException, RuleViolationException,
            InvalidIdentifierException {
        ServerConnection firstConnection = new TestServerConnection();
        ServerConnection secondConnection = new TestServerConnection();

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            synchronized (game) {
                ServerGameEngine engine = game.getEngine();
                Player[] players = engine.getPlayingOrder();
                RoadSquare land1land2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 0 * 3 + 1,
                                        1 + 0 * 3 + 2));
                engine.initiallyPlaceShepherd(players[0],
                        players[0].getAllShepherds()[0], land1land2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare water1water2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 2 * 3 + 1,
                                        1 + 2 * 3 + 2));
                engine.initiallyPlaceShepherd(players[1],
                        players[1].getAllShepherds()[0], water1water2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare mountain0mountain2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 4 * 3 + 0,
                                        1 + 4 * 3 + 2));
                engine.initiallyPlaceShepherd(players[0],
                        players[0].getAllShepherds()[1], mountain0mountain2);
                engine.checkAndAdvanceNextTurn();
                assertEquals(GamePhase.INIT, engine.getGamePhase());
            }
            try {
                ShepherdIdentifier shepherdId = new ShepherdIdentifier(1);
                // between desert1 and desert2
                RoadSquareIdentifier rsId = new RoadSquareIdentifier(
                        1 + 3 * 5 + 1, 1 + 3 * 5 + 2);
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new InitiallyPlaceShepherdMove(playerId,
                            shepherdId, rsId);

                    // ack + didExecuteMove + 2*moveBlackSheep +
                    // 2*advanceNextTurn + 2*19*didGrowLambs
                    pending = 1 + 1 + 2 + 2 + 2 * 19;
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(firstConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(secondConnection, sentMoveOn);
                } else {
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new InitiallyPlaceShepherdMove(playerId,
                            shepherdId, rsId);

                    // ack + didExecuteMove + 2*moveBlackSheep +
                    // 2*advanceNextTurn + 2*19*didGrowLambs
                    pending = 1 + 1 + 2 + 2 + 2 * 19;
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(secondConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(firstConnection, sentMoveOn);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void goodExecuteMoveWithWolf() throws InvalidPasswordException,
            RuleViolationException, InvalidIdentifierException {
        ServerConnection firstConnection = new TestServerConnection();
        ServerConnection secondConnection = new TestServerConnection();

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            // lambs and black sheep caused by our funny
            // use of checkAndAdvanceNextTurn
            pending = 2 * 19 + 2 * 2;
            synchronized (game) {
                ServerGameEngine engine = game.getEngine();
                Player[] players = engine.getPlayingOrder();
                RoadSquare land1land2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 0 * 3 + 1,
                                        1 + 0 * 3 + 2));
                engine.initiallyPlaceShepherd(players[0],
                        players[0].getAllShepherds()[0], land1land2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare water1water2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 2 * 3 + 1,
                                        1 + 2 * 3 + 2));
                engine.initiallyPlaceShepherd(players[1],
                        players[1].getAllShepherds()[0], water1water2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare mountain0mountain2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 4 * 3 + 0,
                                        1 + 4 * 3 + 2));
                engine.initiallyPlaceShepherd(players[0],
                        players[0].getAllShepherds()[1], mountain0mountain2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare desert1desert2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 3 * 5 + 1,
                                        1 + 3 * 5 + 2));
                engine.initiallyPlaceShepherd(players[1],
                        players[1].getAllShepherds()[1], desert1desert2);
                engine.checkAndAdvanceNextTurn();
                assertEquals(GamePhase.PLAY, engine.getGamePhase());
                engine.forceEndTurn(players[0]);
                engine.checkAndAdvanceNextTurn();
                assertEquals(players[1], engine.getCurrentPlayer());
                assertEquals(GamePhase.PLAY, engine.getGamePhase());
            }
            waitDone(); // lambs and black sheep (see above)
            try {
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new ForceEndTurnMove(playerId, false);

                    // ack + didExecuteMove + 2*moveWolfSheep +
                    // 2*advanceNextTurn
                    pending = 6;
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(firstConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(secondConnection, sentMoveOn);
                } else {
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new ForceEndTurnMove(playerId, false);

                    // ack + didExecuteMove + 2*moveWolfSheep +
                    // 2*advanceNextTurn
                    pending = 6;
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(secondConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(firstConnection, sentMoveOn);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void goodExecuteLastMove() throws InvalidPasswordException,
            RuleViolationException, InvalidIdentifierException {
        ServerConnection firstConnection = new TestServerConnection();
        ServerConnection secondConnection = new TestServerConnection();

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            // lambs, black sheep and wolf caused by our funny
            // use of checkAndAdvanceNextTurn
            pending = 2 * 19 + 2 * 2 + 2;
            synchronized (game) {
                ServerGameEngine engine = game.getEngine();
                Player[] players = engine.getPlayingOrder();
                RoadSquare land1land2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 0 * 3 + 1,
                                        1 + 0 * 3 + 2));
                engine.initiallyPlaceShepherd(players[0],
                        players[0].getAllShepherds()[0], land1land2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare water1water2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 2 * 3 + 1,
                                        1 + 2 * 3 + 2));
                engine.initiallyPlaceShepherd(players[1],
                        players[1].getAllShepherds()[0], water1water2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare mountain0mountain2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 4 * 3 + 0,
                                        1 + 4 * 3 + 2));
                engine.initiallyPlaceShepherd(players[0],
                        players[0].getAllShepherds()[1], mountain0mountain2);
                engine.checkAndAdvanceNextTurn();
                RoadSquare desert1desert2 = engine
                        .getGameBoard()
                        .getMap()
                        .getRoadSquareFromId(
                                new RoadSquareIdentifier(1 + 3 * 5 + 1,
                                        1 + 3 * 5 + 2));
                engine.initiallyPlaceShepherd(players[1],
                        players[1].getAllShepherds()[1], desert1desert2);
                engine.checkAndAdvanceNextTurn();
                assertEquals(GamePhase.PLAY, engine.getGamePhase());
                while (engine.getGameBoard().getRemainingFences() > 0) {
                    engine.getGameBoard().markFencePlaced();
                }
                engine.forceEndTurn(players[0]);
                engine.checkAndAdvanceNextTurn();
                assertEquals(players[1], engine.getCurrentPlayer());
                // yep, still play, even if we are without fences, because
                // we're messing with the model behind the controller's back
                assertEquals(GamePhase.PLAY, engine.getGamePhase());
                engine.forceEndTurn(players[1]);
                engine.checkAndAdvanceNextTurn();
                assertEquals(players[0], engine.getCurrentPlayer());
                assertEquals(GamePhase.MARKET_SELL, engine.getGamePhase());
                engine.forceEndTurn(players[0]);
                engine.checkAndAdvanceNextTurn();
                assertEquals(GamePhase.MARKET_SELL, engine.getGamePhase());
                engine.forceEndTurn(players[1]);
                engine.checkAndAdvanceNextTurn();
                assertEquals(GamePhase.MARKET_BUY, engine.getGamePhase());
                engine.forceEndTurn(engine.getCurrentPlayer());
                engine.checkAndAdvanceNextTurn();
                assertEquals(GamePhase.MARKET_BUY, engine.getGamePhase());
            }
            waitDone(); // lambs and black sheep (see above)
            try {
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new ForceEndTurnMove(playerId, false);

                    // ack + didExecuteMove + 2*endGame
                    pending = 4;
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(firstConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(secondConnection, sentMoveOn);
                    assertNotNull(sentScores);
                } else {
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new ForceEndTurnMove(playerId, false);

                    // ack + didExecuteMove + 2*endGame
                    pending = 4;
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertNull(sentAck);
                    assertEquals(secondConnection, sentAckOn);
                    assertEquals(move, sentMove);
                    assertEquals(firstConnection, sentMoveOn);
                    assertNotNull(sentScores);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void rveExecuteMove() throws InvalidPasswordException {
        ServerConnection firstConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };
        ServerConnection secondConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            try {
                // run a move that fails because of wrong phase
                // (really, anything that triggers a RVE in the game engine is
                // ok -
                // individual RVEs are checked in the ServerGameEngine unit
                // tests)
                ShepherdIdentifier shepherdId = new ShepherdIdentifier(1);
                // between sheepsburg and desert/0
                RoadSquareIdentifier rsId = new RoadSquareIdentifier(0,
                        1 + 3 * TerrainType.DESERT.ordinal() + 0);
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new MoveShepherdMove(playerId, shepherdId, rsId);

                    pending = 1; // ack
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertTrue(sentAck instanceof RuleViolationException);
                    assertEquals(firstConnection, sentAckOn);
                    assertNull(sentMove);
                    assertNull(sentMoveOn);
                } else {
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new MoveShepherdMove(playerId, shepherdId, rsId);

                    pending = 1; // ack
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertTrue(sentAck instanceof RuleViolationException);
                    assertEquals(secondConnection, sentAckOn);
                    assertNull(sentMove);
                    assertNull(sentMoveOn);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void wrongPlayerExecuteMove() throws InvalidPasswordException {
        ServerConnection firstConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };
        ServerConnection secondConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            try {
                // run a move that fails because of wrong phase
                // (really, anything that triggers a RVE in the game engine is
                // ok -
                // individual RVEs are checked in the ServerGameEngine unit
                // tests)
                ShepherdIdentifier shepherdId = new ShepherdIdentifier(1);
                // between sheepsburg and desert/0
                RoadSquareIdentifier rsId = new RoadSquareIdentifier(0,
                        1 + 3 * TerrainType.DESERT.ordinal() + 0);
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    // wrong player!
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new MoveShepherdMove(playerId, shepherdId, rsId);

                    pending = 1; // ack
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertTrue(sentAck instanceof RuleViolationException);
                    assertEquals(firstConnection, sentAckOn);
                    assertNull(sentMove);
                    assertNull(sentMoveOn);
                } else {
                    // wrong player!
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new MoveShepherdMove(playerId, shepherdId, rsId);

                    pending = 1; // ack
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertTrue(sentAck instanceof RuleViolationException);
                    assertEquals(secondConnection, sentAckOn);
                    assertNull(sentMove);
                    assertNull(sentMoveOn);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void invalidIdExecuteMove() throws InvalidPasswordException {
        ServerConnection firstConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };
        ServerConnection secondConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            try {
                ShepherdIdentifier shepherdId = new ShepherdIdentifier(1);
                // does not exists
                RoadSquareIdentifier rsId = new RoadSquareIdentifier(42, 42);
                if (game.getEngine().getCurrentPlayer() == firstPlayer
                        .getPlayer()) {
                    PlayerIdentifier playerId = firstPlayer.getPlayer().getId();
                    Move move = new InitiallyPlaceShepherdMove(playerId,
                            shepherdId, rsId);

                    pending = 1; // ack
                    firstPlayer.doExecuteMove(move);
                    waitDone();

                    assertTrue(sentAck instanceof InvalidIdentifierException);
                    assertEquals(firstConnection, sentAckOn);
                    assertNull(sentMove);
                    assertNull(sentMoveOn);
                } else {
                    PlayerIdentifier playerId = secondPlayer.getPlayer()
                            .getId();
                    Move move = new InitiallyPlaceShepherdMove(playerId,
                            shepherdId, rsId);

                    pending = 1; // ack
                    secondPlayer.doExecuteMove(move);
                    waitDone();

                    assertTrue(sentAck instanceof InvalidIdentifierException);
                    assertEquals(secondConnection, sentAckOn);
                    assertNull(sentMove);
                    assertNull(sentMoveOn);
                }
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void moveBeforeGame() {
        ServerConnection connection = new TestServerConnection();

        ServerPlayer player = new ServerPlayer(connection, incubator,
                new FakeGameOptions());
        player.doLoginToServer("marco", "polo");

        try {
            waitDone();
            assertNull(player.getGame());

            ShepherdIdentifier shepherdId = new ShepherdIdentifier(1);
            // between sheepsburg and desert/0
            RoadSquareIdentifier rsId = new RoadSquareIdentifier(0,
                    1 + 3 * TerrainType.DESERT.ordinal() + 0);
            // made up, there is no player obviously, because there is no game
            PlayerIdentifier playerId = new PlayerIdentifier();
            Move move = new InitiallyPlaceShepherdMove(playerId, shepherdId,
                    rsId);
            pending = 1;
            player.doExecuteMove(move);

            waitDone();

            assertTrue(sentAck instanceof RuleViolationException);
        } finally {
            connection.close();
            marco.detachPlayer();
        }
    }

    @Test(timeout = 10000)
    public void forceSynchronize() {
        ServerConnection firstConnection = new TestServerConnection() {
            @Override
            public void sendForceSynchronize(RuleSet rules, GameBoard board,
                    PlayerIdentifier self, GameState state) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    sentRules = rules;
                    sentBoard = board;
                    sentPlayer = self;
                    sentState = state;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }

            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };
        ServerConnection secondConnection = new TestServerConnection() {
            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            try {
                pending = 1;
                firstPlayer.doForceSynchronizeMe();
                waitDone();

                assertNotNull(sentBoard);
                assertEquals(firstPlayer.getPlayer().getId(), sentPlayer);
                assertEquals(firstPlayer.getGame().getEngine().getRules(),
                        sentRules);
                assertEquals(firstPlayer.getGame().getEngine().getGameState(),
                        sentState);
            } finally {
                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            firstConnection.close();
            secondConnection.close();

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 10000)
    public void forceSynchronizeBeforeLogin() {
        ServerConnection connection = new TestServerConnection() {
            @Override
            public void sendForceSynchronize(RuleSet rules, GameBoard board,
                    PlayerIdentifier self, GameState state) {
                fail("message should not be sent");
            }

            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        ServerPlayer player = new ServerPlayer(connection, incubator,
                new GameOptions());

        player.doForceSynchronizeMe();

        connection.close();
    }

    @Test(timeout = 10000)
    public void forceSynchronizeBeforeGame() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendLoginAnswer(boolean answer) {
                simpleComplete();
            }

            @Override
            public void sendForceSynchronize(RuleSet rules, GameBoard board,
                    PlayerIdentifier self, GameState state) {
                fail("message should not be sent");
            }

            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        ServerPlayer player = new ServerPlayer(connection, incubator,
                new GameOptions());

        pending = 1;
        player.doLoginToServer("marco", "polo");
        waitDone();
        assertEquals(player, marco.getCurrentPlayer());

        player.doForceSynchronizeMe();

        connection.close();
        marco.detachPlayer(player);
    }

    @Test(timeout = 10000)
    public void executeMoveBeforeLogin() {
        ServerConnection connection = new FakeServerConnection() {
            @Override
            public void sendMoveAcknowledged(Exception e) {
                fail("message should not be sent");
            }

            @Override
            public void sendDidExecuteMove(Move move) {
                fail("message should not be sent");
            }

            @Override
            public void sendAdvanceNextTurn(int nextTurn, GamePhase nextPhase) {
                fail("message should not be sent");
            }
        };

        ServerPlayer player = new ServerPlayer(connection, incubator,
                new GameOptions());

        player.doExecuteMove(new ForceEndTurnMove(new PlayerIdentifier(), false));

        connection.close();
    }

    @Test(timeout = 20000)
    public void turnTimeout() {
        GameOptions fakeOptions = new FakeGameOptions() {
            @Override
            public int getGameTurnTimeout() {
                return 1;
            }
        };
        ServerConnection[] connections = new ServerConnection[2];
        for (int i = 0; i < 2; i++) {
            connections[i] = new FakeServerConnection() {
                @Override
                public void sendSetup(RuleSet rules, GameBoard board,
                        PlayerIdentifier self) {
                    synchronized (ServerPlayerTest.this) {
                        if (pending <= 0) {
                            fail();
                        }
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendMoveAcknowledged(Exception e) {
                    synchronized (ServerPlayerTest.this) {
                        // unlike other tests, we allow more messages
                        // than we expect (in case we trigger more turn
                        // timeouts)
                        if (pending <= 0) {
                            return;
                        }
                        sentAckOn = this;
                        sentAck = e;
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendDidExecuteMove(Move move) {
                    synchronized (ServerPlayerTest.this) {
                        // unlike other tests, we allow more messages
                        // than we expect (in case we trigger more turn
                        // timeouts)
                        if (pending <= 0) {
                            return;
                        }
                        sentMoveOn = this;
                        sentMove = move;
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendAdvanceNextTurn(int turn, GamePhase phase) {
                    synchronized (ServerPlayerTest.this) {
                        // unlike other tests, we allow more messages
                        // than we expect (in case we trigger more turn
                        // timeouts)
                        if (pending <= 0) {
                            return;
                        }
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }
            };
        }

        // 2*setup + 2*didExecuteMove + 2*advanceNextTurn
        pending = 6;

        ServerPlayer firstPlayer = new ServerPlayer(connections[0], incubator,
                fakeOptions);
        ServerPlayer secondPlayer = new ServerPlayer(connections[1], incubator,
                fakeOptions);
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);
            try {
                assertNull(sentAck);
                assertNull(sentAckOn);
                assertTrue(sentMove instanceof ForceEndTurnMove);
                assertTrue(((ForceEndTurnMove) sentMove).isForced());
                assertNotNull(sentMoveOn);
            } finally {

                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            for (int i = 0; i < 2; i++) {
                connections[i].close();
            }

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void turnTimeoutDisconnected() {
        GameOptions fakeOptions = new FakeGameOptions() {
            @Override
            public int getGameTurnDisconnectedTimeout() {
                return 1;
            }
        };
        ServerConnection[] connections = new ServerConnection[2];
        for (int i = 0; i < 2; i++) {
            connections[i] = new FakeServerConnection() {
                @Override
                public void sendSetup(RuleSet rules, GameBoard board,
                        PlayerIdentifier self) {
                    synchronized (ServerPlayerTest.this) {
                        if (pending <= 0) {
                            fail();
                        }
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendMoveAcknowledged(Exception e) {
                    synchronized (ServerPlayerTest.this) {
                        // unlike other tests, we allow more messages
                        // than we expect (in case we trigger more turn
                        // timeouts)
                        if (pending <= 0) {
                            return;
                        }
                        sentAckOn = this;
                        sentAck = e;
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendDidExecuteMove(Move move) {
                    synchronized (ServerPlayerTest.this) {
                        // unlike other tests, we allow more messages
                        // than we expect (in case we trigger more turn
                        // timeouts)
                        if (pending <= 0) {
                            return;
                        }
                        sentMoveOn = this;
                        sentMove = move;
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }

                @Override
                public void sendAdvanceNextTurn(int turn, GamePhase phase) {
                    synchronized (ServerPlayerTest.this) {
                        // unlike other tests, we allow more messages
                        // than we expect (in case we trigger more turn
                        // timeouts)
                        if (pending <= 0) {
                            return;
                        }
                        pending--;
                        ServerPlayerTest.this.notify();
                    }
                }
            };
        }

        // 2*setup
        pending = 2;

        ServerPlayer firstPlayer = new ServerPlayer(connections[0], incubator,
                fakeOptions);
        ServerPlayer secondPlayer = new ServerPlayer(connections[1], incubator,
                fakeOptions);
        try {
            firstPlayer.doLoginToServer("mario", "ahiahiahi");
            secondPlayer.doLoginToServer("marco", "polo");

            incubator.startGameNow();
            waitDone();
            Game game = firstPlayer.getGame();
            assertNotNull(game);

            // didExecuteMove + advanceNextTurn
            pending = 2;
            ServerConnection expectedSentMoveOn;
            if (game.getEngine().getCurrentPlayer() == firstPlayer.getPlayer()) {
                connections[0].close();
                expectedSentMoveOn = connections[1];
            } else {
                connections[1].close();
                expectedSentMoveOn = connections[0];
            }
            waitDone();

            try {
                assertNull(sentAck);
                assertNull(sentAckOn);
                assertTrue(sentMove instanceof ForceEndTurnMove);
                assertTrue(((ForceEndTurnMove) sentMove).isForced());
                assertEquals(expectedSentMoveOn, sentMoveOn);
            } finally {

                synchronized (game) {
                    firstPlayer.setYourTurn(false);
                    secondPlayer.setYourTurn(false);
                }
            }
        } finally {
            for (int i = 0; i < 2; i++) {
                connections[i].close();
            }

            mario.detachPlayer(firstPlayer);
            marco.detachPlayer(secondPlayer);
        }
    }

    @Test(timeout = 5000)
    public void playerLeftJoined() {
        ServerConnection firstConnection = new FakeServerConnection() {
            @Override
            public void sendSetup(RuleSet rules, GameBoard board,
                    PlayerIdentifier self) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerConnection secondConnection = new FakeServerConnection() {
            @Override
            public void sendSetup(RuleSet rules, GameBoard board,
                    PlayerIdentifier self) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }

            @Override
            public void sendPlayerLeft(PlayerIdentifier who, boolean clean) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    sentPlayer = who;
                    closed = clean;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }

            @Override
            public void sendPlayerJoined(PlayerIdentifier who) {
                synchronized (ServerPlayerTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    sentPlayer = who;
                    pending--;
                    ServerPlayerTest.this.notify();
                }
            }
        };
        ServerConnection thirdConnection = new FakeServerConnection();

        ServerPlayer firstPlayer = new ServerPlayer(firstConnection, incubator,
                new FakeGameOptions());
        ServerPlayer secondPlayer = new ServerPlayer(secondConnection,
                incubator, new FakeGameOptions());
        ServerPlayer thirdPlayer = new ServerPlayer(thirdConnection, incubator,
                new FakeGameOptions());

        pending = 2;
        firstPlayer.doLoginToServer("marco", "polo");
        secondPlayer.doLoginToServer("mario", "ahiahiahi");
        try {
            incubator.startGameNow();
            waitDone();

            Game game = firstPlayer.getGame();
            assertNotNull(game);

            synchronized (game) {
                assertFalse(firstPlayer.hasLeft());
            }

            pending = 1;
            firstConnection.close();
            waitDone();

            assertEquals(firstPlayer.getPlayer().getId(), sentPlayer);
            assertEquals(false, closed);
            synchronized (game) {
                assertTrue(firstPlayer.hasLeft());
            }

            sentPlayer = null;
            pending = 1;
            thirdPlayer.doLoginToServer("marco", "polo");
            waitDone();

            assertEquals(firstPlayer.getPlayer().getId(), sentPlayer);
            synchronized (game) {
                assertFalse(firstPlayer.hasLeft());
            }

            sentPlayer = null;
            pending = 1;
            // firstPlayer is now the owner of thirdConnection
            firstPlayer.doAbandonGame();
            waitDone();
            assertEquals(firstPlayer.getPlayer().getId(), sentPlayer);
            assertEquals(true, closed);
            synchronized (game) {
                assertTrue(firstPlayer.hasLeft());
                assertTrue(firstPlayer.isAbandoned());
                assertFalse(game.isAbandoned());
            }
        } finally {
            secondConnection.close();
            thirdConnection.close();

            marco.detachPlayer();
            mario.detachPlayer();
        }
    }
}
