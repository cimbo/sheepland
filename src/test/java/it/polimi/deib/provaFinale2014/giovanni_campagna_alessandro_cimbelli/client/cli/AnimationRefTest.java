package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.ServerException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnimationRefTest {
    private Player marco;
    private Player mario;
    private Player gianni;
    private ArrayLineReader input;
    private FakeCLIUIManager ui;

    @Before
    public void setUp() {
        input = new ArrayLineReader();
        ui = new FakeCLIUIManager(input);
    }

    @After
    public void tearDown() {
        if (ui.getCurrentGame() != null) {
            ui.quit();
        }
    }

    @Test
    public void testShow() {
        // username and password
        input.feed("marco");
        input.feed("polo");
        ui.show();

        assertEquals("Welcome to Sheepland!\n"
                + "You are a proud farm owner in Sheepland, a joyful land"
                + " of promiscuous shepherds and silent sheep.\n"
                + "But even though you're rich, powerful and respected "
                + "it is only today that you will be given a name. What "
                + "will it be?\n"
                + "Please wait until I establish a connection...\n",
                ui.getOutput());
    }

    @Test
    public void testLoginOk() {
        ui.loginSuccess();
        assertEquals("And now please wait until the game starts...\n",
                ui.getOutput());
    }

    @Test
    public void testLoginFail() {
        // username and password
        input.feed("marco", "pippo", "marco", "polo");
        ui.show();
        ui.clearOutput();

        ui.getCurrentGame().loginAnswer(false);
        assertEquals(
                "Unfortunately, the gods did not recognize your name. You decide to try again.\n"
                        + "Please wait until I establish a connection...\n",
                ui.getOutput());
    }

    @Test
    public void startGame() throws ServerException {
        input.feed("marco", "polo");
        ui.show();
        ui.clearOutput();

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { mario, marco });
        board.addSheep(new Sheep(SheepType.BLACK_SHEEP));
        board.getMap().moveBlackSheep(board.getBlackSheep(),
                board.getMap().getSheepsburg());
        board.getMap().moveWolf(board.getMap().getSheepsburg());

        ui.getCurrentGame().setup(rules, board, marco.getId());

        assertEquals("And you're ready to start!\n", ui.getOutput());
    }

    private void setupGameBoard() throws ServerException {
        input.feed("marco", "polo");
        ui.show();
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        marco = new Player("marco");
        mario = new Player("mario");
        gianni = new Player("gianni");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.addPlayer(gianni);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { mario, marco, gianni });
        GameState state = new GameState();
        state.advanceInitState();
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        ui.clearOutput();
    }

    @Test
    public void restartGame() throws ServerException {
        input.feed("marco", "polo");
        ui.show();
        ui.clearOutput();

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { mario, marco });
        GameState state = new GameState();
        state.advanceInitState();
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(
                "And you're ready to start again, just from where you left!\n",
                ui.getOutput());
    }

    @Test
    public void gameOver() throws ServerException {
        setupGameBoard();

        HashMap<PlayerIdentifier, Integer> scores = new HashMap<PlayerIdentifier, Integer>();
        scores.put(mario.getId(), 7);
        scores.put(marco.getId(), 15);
        scores.put(gianni.getId(), 0);

        input.feed("q", "n");
        ui.gameOver(scores);

        assertEquals("The game is over!\n" + "Final ranking:\n"
                + "1st - marco with 15 points\n"
                + "2nd - mario with 7 points\n"
                + "3rd - gianni with 0 points\n" + "Please answer y or n.\n",
                ui.getOutput());
    }

    @Test
    public void blackSheep() throws ServerException, InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        board.getMap().moveBlackSheep(
                board.getBlackSheep(),
                board.getMap().getSheepTileFromId(
                        new SheepTileIdentifier(1 + 3 * 0 + 2)));
        ui.animateBlackSheepMoved(board.getMap().getSheepsburg(), board
                .getMap().getCurrentBlackSheepPosition());

        assertEquals("Tonf! And the black sheep jumped to land/2.\n",
                ui.getOutput());
    }

    @Test
    public void wolf() throws ServerException, InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        board.getMap().moveWolf(
                board.getMap().getSheepTileFromId(
                        new SheepTileIdentifier(1 + 3 * 0 + 2)));
        Sheep eaten = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(eaten);
        ui.animateWolfMoved(board.getMap().getSheepsburg(), board.getMap()
                .getCurrentWolfPosition(), eaten);

        assertEquals(
                "A wild animal was seen in land/2, eating a female sheep.\n",
                ui.getOutput());
    }

    @Test
    public void wolfNoEat() throws ServerException, InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        board.getMap().moveWolf(
                board.getMap().getSheepTileFromId(
                        new SheepTileIdentifier(1 + 3 * 0 + 2)));
        ui.animateWolfMoved(board.getMap().getSheepsburg(), board.getMap()
                .getCurrentWolfPosition(), null);

        assertEquals(
                "A wild animal was seen in land/2, but no harm was done.\n",
                ui.getOutput());
    }

    @Test
    public void shepherdMoveYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare road = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 1 + 3 * 0 + 2));
        shepherd.setCurrentPosition(road);
        road.addShepherd(shepherd);

        ui.animateShepherdMoved(marco, shepherd, road);

        assertEquals("You successfully jump between land/2 and sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void shepherdMoveEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare road = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 1 + 3 * 0 + 2));
        shepherd.setCurrentPosition(road);
        road.addShepherd(shepherd);

        ui.animateShepherdMoved(mario, shepherd, road);

        assertEquals(
                "Shepherd 1, working for enemy mario, jumped between land/2 and sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void sheepMoveYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Sheep(SheepType.RAM);
        board.addSheep(sheep);

        ui.animateSheepMoved(marco, shepherd, land2, board.getMap()
                .getSheepsburg(), sheep);

        assertEquals(
                "You successfully move a ram from land/2 to sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void sheepMoveEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Sheep(SheepType.RAM);
        board.addSheep(sheep);

        ui.animateSheepMoved(mario, shepherd, land2, board.getMap()
                .getSheepsburg(), sheep);

        assertEquals(
                "Shepherd 1, working for enemy mario, moved a ram from land/2 to sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void buyTerrainCardYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        // free card of grain
        TerrainCard card = board
                .getTerrainCardFromId(new TerrainCardIdentifier(1 * 6 + 3));

        ui.animateBoughtTerrainCard(marco, shepherd, card);

        assertEquals("You successfully buy a grain.\n", ui.getOutput());
    }

    @Test
    public void buyTerrainCardEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        // free card of grain
        TerrainCard card = board
                .getTerrainCardFromId(new TerrainCardIdentifier(1 * 6 + 3));

        ui.animateBoughtTerrainCard(mario, shepherd, card);

        assertEquals("Enemy mario bought a grain.\n", ui.getOutput());
    }

    @Test
    public void beginSheepMatingYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));

        ui.animateBeginSheepMating(marco, shepherd, land2);

        assertEquals(
                "You attempt to mate a female sheep and a ram on land/2.\n",
                ui.getOutput());
    }

    @Test
    public void beginSheepMatingEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));

        ui.animateBeginSheepMating(mario, shepherd, land2);

        assertEquals(
                "Shepherd 1, working for enemy mario, is attempting to mate a female sheep and a ram on land/2.\n",
                ui.getOutput());
    }

    @Test
    public void endSheepMatingYoursSuccess() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep newLamb = new Lamb(0);
        board.addSheep(newLamb);

        ui.animateEndSheepMating(marco, shepherd, land2, newLamb);

        assertEquals("You succeed, and a beautiful lamb is born.\n",
                ui.getOutput());
    }

    @Test
    public void endSheepMatingEnemySuccess() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep newLamb = new Lamb(0);
        board.addSheep(newLamb);

        ui.animateEndSheepMating(mario, shepherd, land2, newLamb);

        assertEquals("He succeeds, and a beautiful lamb is born.\n",
                ui.getOutput());
    }

    @Test
    public void endSheepMatingYoursFailure() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));

        ui.animateEndSheepMating(marco, shepherd, land2, null);

        assertEquals("You fail.\n", ui.getOutput());
    }

    @Test
    public void endSheepMatingEnemyFailure() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));

        ui.animateEndSheepMating(mario, shepherd, land2, null);

        assertEquals("He fails.\n", ui.getOutput());
    }

    @Test
    public void beginSheepKillingYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);

        ui.animateBeginSheepKilling(marco, shepherd, land2, sheep);

        assertEquals("You attempt to kill a 1 round old lamb on land/2.\n",
                ui.getOutput());
    }

    @Test
    public void beginSheepKillingEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);

        ui.animateBeginSheepKilling(mario, shepherd, land2, sheep);

        assertEquals(
                "Shepherd 1, working for enemy mario, is attempting to kill a 1 round old lamb on land/2.\n",
                ui.getOutput());
    }

    @Test
    public void endSheepKillingYoursSuccess() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);
        HashMap<Player, Integer> paidPlayers = new HashMap<Player, Integer>();
        paidPlayers.put(mario, 2);

        ui.animateEndSheepKilling(marco, shepherd, land2, sheep, paidPlayers);

        assertEquals("You succeed, but it does not come for free.\n"
                + "You pay 2 coins to mario.\n", ui.getOutput());
    }

    @Test
    public void endSheepKillingEnemySuccess() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);
        HashMap<Player, Integer> paidPlayers = new HashMap<Player, Integer>();
        paidPlayers.put(marco, 2);
        paidPlayers.put(gianni, 2);

        ui.animateEndSheepKilling(mario, shepherd, land2, sheep, paidPlayers);

        String output = ui.getOutput();
        assertTrue(output
                .equals("He succeeds, and pays 2 coins to gianni, 2 coins to you.\n")
                || output
                        .equals("He succeeds, and pays 2 coins to you, 2 coins to gianni.\n"));
    }

    @Test
    public void endSheepKillingYoursSuccessFree() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);
        HashMap<Player, Integer> paidPlayers = new HashMap<Player, Integer>();

        ui.animateEndSheepKilling(marco, shepherd, land2, sheep, paidPlayers);

        assertEquals("You succeed, and no one bats an eye.\n", ui.getOutput());
    }

    @Test
    public void endSheepKillingEnemySuccessFree() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);
        HashMap<Player, Integer> paidPlayers = new HashMap<Player, Integer>();

        ui.animateEndSheepKilling(mario, shepherd, land2, sheep, paidPlayers);

        assertEquals("He succeeds, and pays nothing for his misdeeds.\n",
                ui.getOutput());
    }

    @Test
    public void endSheepKillingYoursFailure() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = marco.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);

        ui.animateEndSheepKilling(marco, shepherd, land2, sheep, null);

        assertEquals("You fail. Lucky 1 round old lamb!\n", ui.getOutput());
    }

    @Test
    public void endSheepKillingEnemyFailure() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        Shepherd shepherd = mario.getShepherdFromId(new ShepherdIdentifier(0));
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        Sheep sheep = new Lamb(1);
        board.addSheep(sheep);

        ui.animateEndSheepKilling(mario, shepherd, land2, sheep, null);

        assertEquals("He fails.\n", ui.getOutput());
    }

    @Test
    public void sellMarketCardYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        // free card of grain
        TerrainCard card = board
                .getTerrainCardFromId(new TerrainCardIdentifier(1 * 6 + 3));
        card.markAcquired(marco);
        card.markForSale(3);

        ui.animateSoldMarketCard(marco, card, 3);

        assertEquals(
                "Your grain is now for sale at the market, for only 3 coins!\n",
                ui.getOutput());
    }

    @Test
    public void sellMarketCardEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        // free card of grain
        TerrainCard card = board
                .getTerrainCardFromId(new TerrainCardIdentifier(1 * 6 + 3));
        card.markAcquired(mario);
        card.markForSale(3);

        ui.animateSoldMarketCard(mario, card, 3);

        assertEquals(
                "A grain from mario is now for sale at the market, for 3 coins.\n",
                ui.getOutput());
    }

    @Test
    public void buyMarketCardYours() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        // free card of grain
        TerrainCard card = board
                .getTerrainCardFromId(new TerrainCardIdentifier(1 * 6 + 3));
        card.markAcquired(marco);

        ui.animateBoughtMarketCard(marco, card);

        assertEquals("You successfully buy a grain from the market.\n",
                ui.getOutput());
    }

    @Test
    public void buyMarketCardEnemy() throws ServerException,
            InvalidIdentifierException {
        setupGameBoard();

        GameBoard board = ui.getCurrentGame().getGameBoard();
        // free card of grain
        TerrainCard card = board
                .getTerrainCardFromId(new TerrainCardIdentifier(1 * 6 + 3));
        card.markAcquired(mario);

        ui.animateBoughtMarketCard(mario, card);

        assertEquals("Enemy mario bought a grain from the market.\n",
                ui.getOutput());
    }
}
