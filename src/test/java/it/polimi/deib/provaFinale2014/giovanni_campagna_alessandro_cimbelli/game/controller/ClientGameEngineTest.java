package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.TwoPlayersRuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GraphVisitor;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class ClientGameEngineTest {
    private ClientGameEngine engine;

    @Before
    public void setUp() {
        engine = new ClientGameEngine();
    }

    @Test(expected = IllegalArgumentException.class)
    public void unsealedRules() throws RuleViolationException {
        engine.setRules(new RuleSet());
    }

    @Test
    public void twiceSetRules() throws RuleViolationException {
        RuleSet r1 = new RuleSet();
        RuleSet r2 = new RuleSet();
        r1.seal();
        r2.seal();

        assertEquals(r1, r2);

        engine.setRules(r1);
        engine.setRules(r2);
    }

    @Test(expected = RuleViolationException.class)
    public void changeRules() throws RuleViolationException {
        RuleSet r1 = new RuleSet();
        RuleSet r2 = new TwoPlayersRuleSet();
        r1.seal();
        r2.seal();

        assertNotEquals(r1, r2);

        engine.setRules(r1);
        engine.setRules(r2);
    }

    @Test(expected = RuleViolationException.class)
    public void incompatibleRules() throws RuleViolationException {
        RuleSet rules = new FutureRuleSet();
        rules.seal();

        engine.setRules(rules);
    }

    @Test(expected = RuleViolationException.class)
    public void incompatibleResetRules() throws RuleViolationException {
        RuleSet rules = new FutureRuleSet(RuleSet.CURRENT_VERSION);
        rules.seal();
        RuleSet future = new FutureRuleSet(RuleSet.CURRENT_VERSION + 2);
        future.seal();

        engine.setRules(rules);
        engine.setRules(future);
    }

    @Test
    public void rulesHashability() {
        RuleSet rules = new RuleSet();
        rules.hashCode();
    }

    @Test
    public void rulesEquality() {
        RuleSet r1 = new RuleSet();
        RuleSet r2 = new RuleSet();
        r2.seal();
        assertNotEquals(r1, null);
        assertNotEquals(r1, new Object());
        assertEquals(r1, r2);
        assertNotEquals(r1, new TwoPlayersRuleSet());
    }

    private Player[] initializePlayers(GameBoard board) {
        Player[] playingOrder = { new Player(""), new Player("") };

        for (Player player : playingOrder) {
            board.addPlayer(player);
        }

        return playingOrder;
    }

    @Test
    public void manualSetup() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        engine.setRules(rules);

        GameBoard board = new GameBoard();
        Player[] playingOrder = initializePlayers(board);
        board.initializeForRules(rules);
        board.setPlayingOrder(playingOrder);

        engine.start(board);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void doubleSetup() throws InvalidIdentifierException,
            RuleViolationException {
        manualSetup();
        assertNotEquals(GamePhase.PREINIT, engine.getGamePhase());
        engine.start(engine.getGameBoard());
    }

    @Test
    public void gameProgression() throws InvalidIdentifierException,
            RuleViolationException {
        manualSetup();

        engine.advanceNextTurn(0, GamePhase.INIT);
        engine.advanceNextTurn(1, GamePhase.INIT);
        engine.advanceNextTurn(0, GamePhase.PLAY);
        engine.advanceNextTurn(1, GamePhase.PLAY);
        engine.advanceNextTurn(0, GamePhase.MARKET_SELL);
        engine.advanceNextTurn(1, GamePhase.MARKET_SELL);
        engine.advanceNextTurn(1, GamePhase.MARKET_BUY);
        engine.advanceNextTurn(0, GamePhase.MARKET_BUY);
        engine.advanceNextTurn(0, GamePhase.PLAY);
        engine.advanceNextTurn(1, GamePhase.FINAL);
        engine.advanceNextTurn(0, GamePhase.MARKET_SELL);
        engine.advanceNextTurn(1, GamePhase.MARKET_SELL);
        engine.advanceNextTurn(1, GamePhase.MARKET_BUY);
        engine.advanceNextTurn(0, GamePhase.MARKET_BUY);

        engine.end();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void noGoBackToPreinit() throws InvalidIdentifierException,
            RuleViolationException {
        manualSetup();
        engine.advanceNextTurn(0, GamePhase.PREINIT);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void noJumpToEnd() throws InvalidIdentifierException,
            RuleViolationException {
        manualSetup();
        engine.advanceNextTurn(0, GamePhase.END);
    }

    @Test
    public void serverSetup() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();

        final ServerGameEngine server = new ServerGameEngine(new Random(42),
                new EmptyServerDelegate(), rules);
        Player serverPlayer = new Player("human");
        Player serverOpponent = new Player("opponent");
        server.addPlayer(serverPlayer);
        server.addPlayer(serverOpponent);
        server.start();

        GameBoard clientBoard = server.cloneBoardForPlayer(serverPlayer);

        assertEquals(rules, server.getRules());
        engine.setRules(server.getRules());
        engine.start(clientBoard);

        PlayerIdentifier selfId = serverPlayer.getId();
        PlayerIdentifier opponentId = serverOpponent.getId();

        Player clientSelf = engine.getPlayerFromId(selfId);
        Player clientOpponent = engine.getPlayerFromId(opponentId);

        assertEquals(serverPlayer.getName(), clientSelf.getName());
        assertEquals(serverOpponent.getName(), clientOpponent.getName());

        assertEquals(serverPlayer.getInitialCard().getId(), clientSelf
                .getInitialCard().getId());
        assertNull(clientOpponent.getInitialCard());

        Player[] serverPlayingOrder = server.getPlayingOrder();
        Player serverCurrentPlayer = server.getCurrentPlayer();
        Player[] clientPlayingOrder = engine.getPlayingOrder();
        Player clientCurrentPlayer = engine.getCurrentPlayer();

        assertEquals(serverPlayingOrder[0].getId(),
                clientPlayingOrder[0].getId());
        assertEquals(serverPlayingOrder[1].getId(),
                clientPlayingOrder[1].getId());
        assertEquals(serverCurrentPlayer.getId(), clientCurrentPlayer.getId());

        engine.getGameBoard().getMap().forEachTile(new GraphVisitor() {
            public void visit(SheepTile clientTile) {
                try {
                    SheepTile serverTile = server.getSheepTileFromId(clientTile
                            .getId());

                    SheepType[] allTypes = SheepType.values();

                    for (SheepType type : allTypes) {
                        assertEquals(serverTile.getNumberOfSheep(type),
                                clientTile.getNumberOfSheep(type));
                    }

                    assertEquals(serverTile.getHasWolf(),
                            clientTile.getHasWolf());
                } catch (InvalidIdentifierException e) {
                    fail();
                }
            }
        });
    }
}
