package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.junit.Before;
import org.junit.Test;

public class GameObjectTest {
    private TestGameObject o;

    @Before
    public void setUp() {
        o = new TestGameObject();
    }

    @Test(expected = IllegalStateException.class)
    public void beforeSet() {
        o.getId();
    }

    @Test(expected = IllegalStateException.class)
    public void serializeBeforeSet() throws IOException {
        new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(o);
    }

    @Test
    public void serializeAfterSet() throws IOException {
        o.setId(new TestGameObjectIdentifier());
        new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(o);
    }

    @Test
    public void set() {
        GameObjectIdentifier id = new TestGameObjectIdentifier();

        o.setId(id);
        assertEquals(id, o.getId());
    }

    @Test(expected = IllegalStateException.class)
    public void doubleSet() {
        GameObjectIdentifier id = new TestGameObjectIdentifier();

        o.setId(id);
        o.setId(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullSet() {
        o.setId(null);
    }
}
