package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;

class TestGameObjectIdentifier extends GameObjectIdentifier {
    private static final long serialVersionUID = 1L;

    public TestGameObjectIdentifier() {
        super(TestGameObject.class, 0);
    }

    public TestGameObjectIdentifier(Class<? extends GameObject> _class,
            int _content) {
        super(_class, _content);
    }
}