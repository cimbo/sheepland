package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

import java.util.Map;

public class EmptyClientUI extends AbstractUI {

    @Override
    public void show() {
    }

    @Override
    public void quit() {
    }

    @Override
    public void initialize() {
    }

    @Override
    public void forceSynchronize() {
    }

    @Override
    public void gameOver(Map<PlayerIdentifier, Integer> scores) {
    }

    @Override
    public void setWaiting(boolean waiting) {
    }

    @Override
    public void setYourTurn(boolean yourTurn) {
    }

    @Override
    public void playerLeft(Player who, boolean clean) {
    }

    @Override
    public void playerJoined(Player who) {
    }

    @Override
    public void reportError(String error) {
    }

    @Override
    public void loginSuccess() {
    }

    @Override
    public void loginFailed() {
    }

    @Override
    public void animateBlackSheepMoved(SheepTile from, SheepTile to) {
    }

    @Override
    public void animateWolfMoved(SheepTile from, SheepTile to, Sheep eatenSheep) {
    }

    @Override
    public void animateShepherdMoved(Player who, Shepherd which, RoadSquare to) {
    }

    @Override
    public void animateSheepMoved(Player who, Shepherd which, SheepTile from,
            SheepTile to, Sheep sheep) {
    }

    @Override
    public void animateBoughtTerrainCard(Player who, Shepherd which,
            TerrainCard card) {
    }

    @Override
    public void animateBeginSheepMating(Player who, Shepherd which,
            SheepTile tile) {
    }

    @Override
    public void animateEndSheepMating(Player who, Shepherd which,
            SheepTile tile, Sheep newLamb) {
    }

    @Override
    public void animateBeginSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep) {
    }

    @Override
    public void animateEndSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep, Map<Player, Integer> paidPlayers) {
    }

    @Override
    public void animateSoldMarketCard(Player who, TerrainCard card, int cost) {
    }

    @Override
    public void animateBoughtMarketCard(Player who, TerrainCard card) {
    }

    @Override
    public void animateLambsGrew(SheepTile tile) {
    }

    @Override
    public void syncShepherdPositions() {
    }
}
