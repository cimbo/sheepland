package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GraphVisitor;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import org.junit.Before;
import org.junit.Test;

public class ClientServerConsistencyTest {
    private ServerGameEngine server;
    private ClientGameEngine client;

    @Before
    public void setUp() throws RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();

        server = new ServerGameEngine(new FakeRandom(), new ServerDelegate() {
            public void didMoveBlackSheep(SheepTileIdentifier destination) {
                try {
                    client.moveBlackSheep(client
                            .getSheepTileFromId(destination));
                } catch (InvalidIdentifierException e) {
                    fail();
                }
            }

            public void didMoveWolf(SheepTileIdentifier destination,
                    SheepIdentifier eatenSheep) {
                try {
                    client.moveWolf(
                            client.getSheepTileFromId(destination),
                            eatenSheep != null ? client
                                    .getSheepFromId(eatenSheep) : null);
                } catch (InvalidIdentifierException e) {
                    fail();
                }
            }

            public void didGrowLambs(SheepTileIdentifier tile,
                    Sheep[] newAdultSheep) {
                try {
                    client.growLambs(client.getSheepTileFromId(tile),
                            newAdultSheep);
                } catch (InvalidIdentifierException e) {
                    fail();
                }
            }
        }, rules);
        Player serverPlayer = new Player("human");
        Player serverOpponent = new Player("opponent");
        server.addPlayer(serverPlayer);
        server.addPlayer(serverOpponent);
        server.start();

        GameBoard clientBoard = server.cloneBoardForPlayer(serverPlayer);

        assertEquals(rules, server.getRules());
        client = new ClientGameEngine();
        client.setRules(server.getRules());
        client.start(clientBoard);
    }

    @Test
    public void blackSheepConsistencyTest() throws InvalidIdentifierException {
        GameMap serverMap = server.getGameBoard().getMap();
        GameMap clientMap = client.getGameBoard().getMap();

        // place the black sheep in land/1
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile serverSt1 = server.getSheepTileFromId(stId1);
        SheepTile clientSt1 = client.getSheepTileFromId(stId1);

        assertEquals(serverMap.getSheepsburg(),
                serverMap.getCurrentBlackSheepPosition());
        assertEquals(clientMap.getSheepsburg(),
                clientMap.getCurrentBlackSheepPosition());
        serverMap.moveBlackSheep(server.getGameBoard().getBlackSheep(),
                serverSt1);
        clientMap.moveBlackSheep(client.getGameBoard().getBlackSheep(),
                clientSt1);
        assertEquals(serverSt1, serverMap.getCurrentBlackSheepPosition());
        assertEquals(clientSt1, clientMap.getCurrentBlackSheepPosition());

        server.moveBlackSheep();

        assertEquals(serverMap.getCurrentBlackSheepPosition().getId(),
                clientMap.getCurrentBlackSheepPosition().getId());
    }

    @Test
    public void wolfConsistencyTest() throws InvalidIdentifierException {
        GameMap serverMap = server.getGameBoard().getMap();
        GameMap clientMap = client.getGameBoard().getMap();

        // place the wolf in land/1
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile serverSt1 = server.getSheepTileFromId(stId1);
        SheepTile clientSt1 = client.getSheepTileFromId(stId1);

        assertEquals(serverMap.getSheepsburg(),
                serverMap.getCurrentWolfPosition());
        assertEquals(clientMap.getSheepsburg(),
                clientMap.getCurrentWolfPosition());
        serverMap.moveWolf(serverSt1);
        clientMap.moveWolf(clientSt1);
        assertEquals(serverSt1, serverMap.getCurrentWolfPosition());
        assertEquals(clientSt1, clientMap.getCurrentWolfPosition());

        SheepType[] allTypes = SheepType.values();

        for (SheepType type : allTypes) {
            assertEquals(clientSt1.getNumberOfSheep(type),
                    serverSt1.getNumberOfSheep(type));
        }

        server.moveWolf();

        SheepTile serverSt2 = serverMap.getCurrentWolfPosition();
        SheepTile clientSt2 = clientMap.getCurrentWolfPosition();

        assertEquals(serverSt2.getId(), clientSt2.getId());

        for (SheepType type : allTypes) {
            assertEquals(clientSt1.getNumberOfSheep(type),
                    serverSt1.getNumberOfSheep(type));
            assertEquals(clientSt2.getNumberOfSheep(type),
                    serverSt2.getNumberOfSheep(type));
        }
    }

    @Test
    public void wolfEmptyConsistencyTest() throws InvalidIdentifierException {
        GameMap serverMap = server.getGameBoard().getMap();
        GameMap clientMap = client.getGameBoard().getMap();

        // place the wolf in land/1
        // because of the random dice always saying 2, it will go into
        // forest/0, which is empty
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile serverSt1 = server.getSheepTileFromId(stId1);
        SheepTile clientSt1 = client.getSheepTileFromId(stId1);
        SheepTileIdentifier stId2 = new SheepTileIdentifier(1 + 1 * 3 + 0);
        SheepTile serverSt2 = server.getSheepTileFromId(stId2);
        SheepTile clientSt2 = client.getSheepTileFromId(stId2);
        RoadSquareIdentifier rsId = new RoadSquareIdentifier(
                stId1.getContent(), stId2.getContent());
        RoadSquare serverRs = server.getRoadSquareFromId(rsId);
        RoadSquare clientRs = server.getRoadSquareFromId(rsId);
        assertEquals(2, serverRs.getDiceValue());
        assertEquals(2, clientRs.getDiceValue());
        assertFalse(serverRs.getHasFence());
        assertFalse(clientRs.getHasFence());

        // clean up st2 before the wolf goes there
        for (Sheep s : serverSt2.getAllSheep()) {
            serverSt2.removeSheep(s);
        }
        for (Sheep s : clientSt2.getAllSheep()) {
            clientSt2.removeSheep(s);
        }

        assertEquals(serverMap.getSheepsburg(),
                serverMap.getCurrentWolfPosition());
        assertEquals(clientMap.getSheepsburg(),
                clientMap.getCurrentWolfPosition());
        serverMap.moveWolf(serverSt1);
        clientMap.moveWolf(clientSt1);
        assertEquals(serverSt1, serverMap.getCurrentWolfPosition());
        assertEquals(clientSt1, clientMap.getCurrentWolfPosition());

        SheepType[] allTypes = SheepType.values();

        for (SheepType type : allTypes) {
            assertEquals(clientSt1.getNumberOfSheep(type),
                    serverSt1.getNumberOfSheep(type));
            assertEquals(clientSt2.getNumberOfSheep(type),
                    serverSt2.getNumberOfSheep(type));
        }

        server.moveWolf();

        assertEquals(serverSt2, serverMap.getCurrentWolfPosition());
        assertEquals(clientSt2, clientMap.getCurrentWolfPosition());

        for (SheepType type : allTypes) {
            assertEquals(clientSt1.getNumberOfSheep(type),
                    serverSt1.getNumberOfSheep(type));
            assertEquals(clientSt2.getNumberOfSheep(type),
                    serverSt2.getNumberOfSheep(type));
        }
    }

    @Test
    public void lambsGrowing() throws InvalidIdentifierException {
        // retrieve land/1
        SheepTileIdentifier stId = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile serverSt = server.getSheepTileFromId(stId);
        SheepTile clientSt = client.getSheepTileFromId(stId);

        SheepType[] allTypes = SheepType.values();

        for (SheepType type : allTypes) {
            assertEquals(clientSt.getNumberOfSheep(type),
                    serverSt.getNumberOfSheep(type));
        }

        // add a lamb that is about to grow
        Lamb serverLamb = new Lamb(Lamb.GROWING_AGE - 1);
        server.getGameBoard().addSheep(serverLamb);
        Lamb clientLamb = new Lamb(Lamb.GROWING_AGE - 1);
        clientLamb.setId(serverLamb.getId());
        client.getGameBoard().addSheep(clientLamb.getId(), clientLamb);
        serverSt.addSheep(serverLamb);
        clientSt.addSheep(clientLamb);

        for (SheepType type : allTypes) {
            assertEquals(clientSt.getNumberOfSheep(type),
                    serverSt.getNumberOfSheep(type));
        }

        server.growLambsOnTile(serverSt);

        for (SheepType type : allTypes) {
            assertEquals(clientSt.getNumberOfSheep(type),
                    serverSt.getNumberOfSheep(type));
        }
    }

    private void checkAllAges() {
        GameMap clientMap = client.getGameBoard().getMap();

        clientMap.forEachTile(new GraphVisitor() {
            public void visit(SheepTile clientTile) {
                try {
                    SheepTile serverTile = server.getSheepTileFromId(clientTile
                            .getId());

                    for (Sheep clientSheep : clientTile.getAllSheep()) {
                        Sheep serverSheep = server.getSheepFromId(clientSheep
                                .getId());

                        assertTrue(serverTile.containsSheep(serverSheep));
                        assertEquals(serverSheep.getClass(),
                                clientSheep.getClass());
                        assertEquals(serverSheep.getType(),
                                clientSheep.getType());
                        if (clientSheep instanceof Lamb) {
                            assertEquals(((Lamb) serverSheep).getAge(),
                                    ((Lamb) clientSheep).getAge());
                        }
                    }

                    for (Sheep serverSheep : serverTile.getAllSheep()) {
                        Sheep clientSheep = client.getSheepFromId(serverSheep
                                .getId());
                        assertTrue(clientTile.containsSheep(clientSheep));
                    }
                } catch (InvalidIdentifierException e) {
                    fail();
                }
            }
        });
    }

    @Test
    public void allLambsGrowing() {
        checkAllAges();
        server.growAllLambs();
        checkAllAges();
    }
}
