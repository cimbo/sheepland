package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Alessandro Cimbelli
 * 
 */

public class SheepTileTest {

    private SheepTile tile;
    private SheepTile tileConnection;
    private RoadSquare road;
    private Random dice;
    private Sheep blackSheep;

    @Before
    public void setUp() {
        tile = new SheepTile(TerrainType.LAND, 0);
        tileConnection = new SheepTile(TerrainType.FOREST, 0);
        road = new RoadSquare(0);
        dice = new Random(42);
        blackSheep = new Sheep(SheepType.BLACK_SHEEP);
    }

    @Test
    public void terrain() {
        assertEquals(TerrainType.LAND, tile.getTerrainType());
    }

    @Test
    public void blackSheepTest() {

        // At the beginning no black sheep must be on the tile
        assertFalse("The black sheep now is in = ", tile.getHasBlackSheep());

        // Try to add the black sheep on the tile
        tile.addSheep(blackSheep);
        assertTrue("The black sheep now is in = ", tile.getHasBlackSheep());

        // Verify that is impossible to add another black sheep on the same tile
        // again
        try {
            tile.addSheep(blackSheep);
            fail("code should not be reached");
        } catch (GameInvariantViolationException ex) {
            ;
        }

        // Try to remove the black sheep from the tile
        tile.removeSheep(blackSheep);

        assertFalse("The black sheep now is in =: ", tile.getHasBlackSheep());
    }

    @Test
    public void wolfTest() {
        // At the beginning no wolf must be on the tile
        assertFalse("The wolf is in this tile = ", tile.getHasWolf());

        // Try to add a wolf on this tile
        tile.addWolf();
        assertTrue("The wolf is in this tile = ", tile.getHasWolf());

        // Verify that is impossible to add another wolf on the same tile again
        try {
            tile.addWolf();
            fail("code should not be reached");
        } catch (GameInvariantViolationException ex) {
            ;
        }

        // Try to remove the wolf from this tile
        tile.removeWolf();
        assertFalse("The wolf is in this tile = ", tile.getHasWolf());
    }

    @Test
    public void whiteSheepTest() throws RuleViolationException {
        // Verify that at the beginning the tile is empty
        SheepType[] allTypes = SheepType.values();
        for (SheepType allType : allTypes) {
            assertEquals(0, tile.getNumberOfSheep(allType));
        }

        // Try to verify that you can't remove a sheep if the number of them is
        // equals to 0
        Sheep female = new Sheep(SheepType.FEMALE_SHEEP);
        try {
            tile.removeSheep(female);
            fail("code should not be reached");
        } catch (GameInvariantViolationException ex) {
            ;
        }

        tile.addSheep(female);
        assertEquals(1, tile.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, tile.getNumberOfSheep(SheepType.RAM));

        try {
            tile.removeSheep(female);
            tile.removeSheep(female);
            fail("code should not be reached");
        } catch (GameInvariantViolationException ex) {
            ;
        }

        assertEquals(0, tile.getNumberOfSheep(SheepType.FEMALE_SHEEP));
    }

    @Test
    public void lambsTest() {
        Lamb lamb = new Lamb();
        tile.addSheep(lamb);
        assertEquals(1, tile.getNumberOfSheep(SheepType.LAMB));
        assertEquals(0, lamb.getAge());

        tile.growLambs();
        assertEquals(1, tile.getNumberOfSheep(SheepType.LAMB));
        assertEquals(1, lamb.getAge());

        tile.growLambs();
        assertEquals(2, lamb.getAge());

        SheepType[] allTypes = SheepType.values();
        int total = 0;
        for (SheepType allType : allTypes) {
            total += tile.getNumberOfSheep(allType);
        }
        assertEquals(0, total);
    }

    @Test
    public void randomSheepTest() {
        // First, empty tile
        tile.getRandomSheep(dice);
        assertNull(tile.getRandomSheep(dice));

        // Almost empty tile
        tile.addSheep(blackSheep);
        assertNull(tile.getRandomSheep(dice));

        Sheep female = new Sheep(SheepType.FEMALE_SHEEP);
        tile.addSheep(female);
        assertEquals(SheepType.FEMALE_SHEEP, tile.getRandomSheep(dice)
                .getType());

        Sheep ram = new Sheep(SheepType.RAM);
        tile.addSheep(ram);
        SheepType random = tile.getRandomSheep(dice).getType();
        assertTrue(random == SheepType.FEMALE_SHEEP || random == SheepType.RAM);

        Lamb lamb = new Lamb(1);
        tile.removeSheep(ram);
        tile.addSheep(lamb);
        random = tile.getRandomSheep(dice).getType();
        assertTrue(random == SheepType.FEMALE_SHEEP || random == SheepType.LAMB);

        tile.removeSheep(female);
        assertEquals(SheepType.LAMB, tile.getRandomSheep(dice).getType());
    }

    @Test
    public void adjacency() {
        assertEquals(0, tile.getNeighbors().size());

        tile.connect(tileConnection, road);

        Edge tmp = tile.getNeighbors().get(0);
        assertEquals(tileConnection, tmp.getDestination());
        assertEquals(road, tmp.getRoadSquare());
    }

    @Test
    public void scoreValue() {
        assertEquals(0, tile.getScoreValue());

        tile.addSheep(new Sheep(SheepType.FEMALE_SHEEP));
        assertEquals(1, tile.getScoreValue());

        tile.addSheep(new Sheep(SheepType.BLACK_SHEEP));
        assertEquals(3, tile.getScoreValue());

        tile.addSheep(new Sheep(SheepType.LAMB));
        assertEquals(4, tile.getScoreValue());
    }
}
