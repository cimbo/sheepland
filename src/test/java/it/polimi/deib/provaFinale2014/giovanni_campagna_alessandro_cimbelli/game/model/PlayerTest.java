package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {
    private Player player;
    private RuleSet rules;

    @Before
    public void setUp() throws Exception {
        player = new Player("");
        rules = new RuleSet();
        rules.seal();

        player.initializeForRules(rules);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void positionBeforeInit() {
        new Player("").isPositioned();
    }

    @Test
    public void initialState() {
        assertEquals(rules.getInitialCoinsPerPlayer(),
                player.getRemainingCoins());
        assertFalse(player.isPositioned());

        TerrainType[] allTypes = TerrainType.values();
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            assertEquals(0, player.getNumberOfCards(allTypes[i]));
        }

        try {
            player.getNumberOfCards(TerrainType.SHEEPSBURG);
            fail("code should not be reached");
        } catch (IllegalArgumentException e) {
            ;
        }
    }

    @Test
    public void acquireCard() {
        int before = player.getRemainingCoins();

        TerrainCard card = new TerrainCard(TerrainType.LAND);
        card.markForSale(4);
        player.acquireCard(card);
        int after = player.getRemainingCoins();

        assertEquals(before - 4, after);
        assertEquals(1, player.getNumberOfCards(TerrainType.LAND));
    }

    @Test
    public void releaseCard() {
        TerrainCard card = new TerrainCard(TerrainType.LAND);
        card.markForSale(0);
        player.acquireCard(card);
        assertEquals(1, player.getNumberOfCards(TerrainType.LAND));
        player.releaseCard(card);
        assertEquals(0, player.getNumberOfCards(TerrainType.LAND));
    }

    @Test(expected = GameInvariantViolationException.class)
    public void releaseCardNotHeld() {
        TerrainCard card = new TerrainCard(TerrainType.LAND);
        player.releaseCard(card);
    }

    @Test
    public void payment() {
        int before = player.getRemainingCoins();
        player.payCoins(7);
        int after = player.getRemainingCoins();
        assertEquals(before - 7, after);

        // And no card was gained
        TerrainType[] allTypes = TerrainType.values();
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            assertEquals(0, player.getNumberOfCards(allTypes[i]));
        }
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badPayment() {
        player.payCoins(player.getRemainingCoins() + 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativePayment() {
        player.payCoins(-1);
    }

    @Test
    public void earning() {
        int before = player.getRemainingCoins();
        player.earnCoins(7);
        int after = player.getRemainingCoins();
        assertEquals(before + 7, after);

        // And no card was gained or lost
        TerrainType[] allTypes = TerrainType.values();
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            assertEquals(0, player.getNumberOfCards(allTypes[i]));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeEarning() {
        player.earnCoins(-1);
    }

    @Test
    public void scoreWhiteBox() {
        int[] scoreValues = { 2, 0, 1, 0, 0, 0 };

        TerrainCard c1 = new TerrainCard(TerrainType.LAND);
        c1.markForSale(1);
        player.acquireCard(c1);

        TerrainCard c2 = new TerrainCard(TerrainType.LAND);
        c2.markForSale(2);
        player.acquireCard(c2);

        TerrainCard c3 = new TerrainCard(TerrainType.FOREST);
        c3.markForSale(1);
        player.acquireCard(c3);

        TerrainCard c4 = new TerrainCard(TerrainType.WATER);
        c4.markForSale(1);
        player.acquireCard(c4);

        int sheepPoints = 5;
        int paidCoins = 5;
        int expected = rules.getInitialCoinsPerPlayer() + sheepPoints
                - paidCoins;
        assertEquals(expected, player.getScore(scoreValues));
    }

    @Test
    public void scoreBlackBox() throws InvalidIdentifierException {
        GameMap map = new GameMap();
        Sheep blackSheep = new Sheep(SheepType.BLACK_SHEEP);

        SheepTile st1, st2, st3;
        // the ids must be == mod 3
        // (this guarantees they have different terrain type)
        st1 = map.getSheepTileFromId(new SheepTileIdentifier(4));
        st2 = map.getSheepTileFromId(new SheepTileIdentifier(7));
        st3 = map.getSheepTileFromId(new SheepTileIdentifier(10));

        TerrainCard c1 = new TerrainCard(st1.getTerrainType());
        c1.markForSale(1);
        player.acquireCard(c1);

        TerrainCard c2 = new TerrainCard(st1.getTerrainType());
        c2.markForSale(2);
        player.acquireCard(c2);

        TerrainCard c3 = new TerrainCard(st2.getTerrainType());
        c3.markForSale(1);
        player.acquireCard(c3);

        TerrainCard c4 = new TerrainCard(st3.getTerrainType());
        c4.markForSale(1);
        player.acquireCard(c4);

        st1.addSheep(blackSheep);
        st3.addSheep(new Sheep(SheepType.FEMALE_SHEEP));

        int sheepPoints = 5;
        int paidCoins = 5;
        int expected = rules.getInitialCoinsPerPlayer() + sheepPoints
                - paidCoins;
        assertEquals(expected, player.getScore(map.getScoreValues()));
    }
}
