package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import static org.junit.Assert.assertEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

import org.junit.Before;
import org.junit.Test;

public class EdgeTest {
    private Edge a;
    private Edge b;

    @Before
    public void setUp() {
        a = new Edge(new SheepTile(TerrainType.LAND, 0), new RoadSquare(1));
        b = new Edge(new SheepTile(TerrainType.LAND, 1), new RoadSquare(1));
    }

    @Test
    public void getDestinationTest() {
        assertEquals(TerrainType.LAND, a.getDestination().getTerrainType());

        assertEquals(TerrainType.LAND, b.getDestination().getTerrainType());
    }

    @Test
    public void getRoadSquareTest() {
        assertEquals(1, a.getRoadSquare().getDiceValue());
        assertEquals(1, b.getRoadSquare().getDiceValue());
    }

}
