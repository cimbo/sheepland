package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;

import org.junit.Test;

public class SheepTest {
    @Test
    public void allSheep() {
        SheepType types[] = SheepType.values();

        for (SheepType type : types) {
            Sheep s = new Sheep(type);
            assertEquals(type, s.getType());
        }
    }

    @Test
    public void lamb() {
        Lamb l = new Lamb();
        assertEquals(SheepType.LAMB, l.getType());
        assertEquals(0, l.getAge());

        l.grow();
        assertEquals(1, l.getAge());

        l.grow();
        assertEquals(2, l.getAge());

        Lamb l2 = new Lamb(1);
        assertEquals(1, l2.getAge());
    }

    @Test(expected = GameInvariantViolationException.class)
    public void overgrowing() {
        Lamb l = new Lamb(2);
        l.grow();
    }
}
