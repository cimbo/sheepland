package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

class FakeGameOptions extends GameOptions {
    @Override
    public int getGameStartingTimeout() {
        // something high enough the game won't really start
        return 3600;
    }

    @Override
    public int getPingTimeout() {
        // something high enough that we won't try to ping
        return 3600;
    }

    @Override
    public int getGameTurnTimeout() {
        // something high enough that it won't trigger
        return 3600;
    }

    @Override
    public int getGameTurnDisconnectedTimeout() {
        // something high enough that it won't trigger
        return 3600;
    }
}