package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;

public class FutureRuleSet extends RuleSet {
    private static final long serialVersionUID = 1L;

    private int version;

    public FutureRuleSet() {
        this(RuleSet.CURRENT_VERSION + 1);
    }

    public FutureRuleSet(int version) {
        this.version = version;
    }

    @Override
    public int getVersion() {
        return version;
    }
}
