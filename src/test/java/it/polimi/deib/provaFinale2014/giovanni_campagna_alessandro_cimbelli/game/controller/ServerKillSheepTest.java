package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.util.Map;

import org.junit.Test;

public class ServerKillSheepTest extends ServerPlayTestBase {
    public ServerKillSheepTest() {
        super(new FakeRandom());
    }

    @Test
    public void correctTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // the road is a 2, which conveniently is what FakeRandom()
        // gives as the dice (1+1)
        SheepTileIdentifier stId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st = getEngine().getSheepTileFromId(stId);
        assertEquals(TerrainType.LAND, st.getTerrainType());
        assertEquals(2, shepherd.getCurrentPosition().getDiceValue());

        int femaleSheepAtOrigin = st.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st.getNumberOfSheep(SheepType.LAMB);
        assertFalse(st.getHasWolf());
        assertEquals(1, femaleSheepAtOrigin + ramsAtOrigin + lambsAtOrigin);

        SheepType killedType;
        if (femaleSheepAtOrigin == 1) {
            killedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin == 1) {
            killedType = SheepType.RAM;
        } else {
            killedType = SheepType.LAMB;
        }

        Sheep killed = st.getAnySheepOfType(killedType);
        KillSheepMove ksm = new KillSheepMove(players[0].getId(), shepherdId,
                stId, killed.getId());
        executeMove(ksm);

        assertEquals(0, st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st.getNumberOfSheep(SheepType.LAMB));

        Map<Player, Integer> paidPlayers = ksm.getPaidPlayers(getEngine());
        assertNotNull(paidPlayers);
        assertTrue(paidPlayers.isEmpty());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test
    public void killWrongSheep() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // the road is a 2, which conveniently is what FakeRandom()
        // gives as the dice (1+1)
        SheepTileIdentifier stId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st = getEngine().getSheepTileFromId(stId);
        SheepTileIdentifier stId2 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        assertEquals(TerrainType.LAND, st.getTerrainType());
        assertEquals(2, shepherd.getCurrentPosition().getDiceValue());

        int femaleSheepAtOrigin = st.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st.getNumberOfSheep(SheepType.LAMB);
        assertFalse(st.getHasWolf());
        assertEquals(1, femaleSheepAtOrigin + ramsAtOrigin + lambsAtOrigin);

        SheepType killedType;
        if (femaleSheepAtOrigin == 1) {
            killedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin == 1) {
            killedType = SheepType.RAM;
        } else {
            killedType = SheepType.LAMB;
        }

        Sheep killed = st.getAnySheepOfType(killedType);
        try {
            KillSheepMove ksm = new KillSheepMove(players[0].getId(),
                    shepherdId, stId2, killed.getId());
            executeMove(ksm);
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void killBlackSheep() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // the road is a 2, which conveniently is what FakeRandom()
        // gives as the dice (1+1)
        SheepTileIdentifier stId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st = getEngine().getSheepTileFromId(stId);
        assertEquals(TerrainType.LAND, st.getTerrainType());
        assertEquals(2, shepherd.getCurrentPosition().getDiceValue());

        GameBoard board = getEngine().getGameBoard();
        board.getMap().moveBlackSheep(board.getBlackSheep(), st);
        assertTrue(st.containsSheep(board.getBlackSheep()));
        assertFalse(st.getHasWolf());

        Sheep killed = board.getBlackSheep();
        try {
            KillSheepMove ksm = new KillSheepMove(players[0].getId(),
                    shepherdId, stId, killed.getId());
            executeMove(ksm);
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertTrue(st.containsSheep(board.getBlackSheep()));
    }

    @Test
    public void notEnoughCoins() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // the road is a 2, which conveniently is what FakeRandom()
        // gives as the dice (1+1)
        // we'll place another shepherd between grain1 and land2
        SheepTileIdentifier stId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st = getEngine().getSheepTileFromId(stId);
        RoadSquareIdentifier rsId = new RoadSquareIdentifier(0 * 3 + 2 + 1,
                3 * 3 + 1 + 1);
        RoadSquare rs = getEngine().getRoadSquareFromId(rsId);
        assertEquals(TerrainType.LAND, st.getTerrainType());
        assertEquals(2, shepherd.getCurrentPosition().getDiceValue());
        assertEquals(1, rs.getDiceValue());
        assertTrue(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), rs));

        Shepherd opponent = players[1]
                .getShepherdFromId(new ShepherdIdentifier(0));
        opponent.getCurrentPosition().removeShepherd(opponent);
        rs.addShepherd(opponent);
        opponent.setCurrentPosition(rs);

        players[0].payCoins(players[0].getRemainingCoins() - 1);
        assertEquals(1, players[0].getRemainingCoins());

        int femaleSheepAtOrigin = st.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st.getNumberOfSheep(SheepType.LAMB);
        assertFalse(st.getHasWolf());
        assertEquals(1, femaleSheepAtOrigin + ramsAtOrigin + lambsAtOrigin);

        SheepType killedType;
        if (femaleSheepAtOrigin == 1) {
            killedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin == 1) {
            killedType = SheepType.RAM;
        } else {
            killedType = SheepType.LAMB;
        }

        Sheep killed = st.getAnySheepOfType(killedType);
        try {
            KillSheepMove ksm = new KillSheepMove(players[0].getId(),
                    shepherdId, stId, killed.getId());
            executeMove(ksm);
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st.getNumberOfSheep(SheepType.LAMB));
    }
}
