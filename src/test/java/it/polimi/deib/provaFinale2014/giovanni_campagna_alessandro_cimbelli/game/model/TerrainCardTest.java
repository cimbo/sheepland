package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;

import org.junit.Test;

public class TerrainCardTest {
    @Test
    public void baseTest() {
        TerrainCard card = new TerrainCard(TerrainType.LAND);

        assertNull(card.getOwner());
        assertFalse(card.isForSale());

        card.markForSale(4);
        assertTrue(card.isForSale());
        assertEquals(4, card.getCost());
    }

    @Test(expected = IllegalArgumentException.class)
    public void sheepsburgCard() {
        new TerrainCard(TerrainType.SHEEPSBURG);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeCost() {
        TerrainCard card = new TerrainCard(TerrainType.LAND);
        card.markForSale(-1);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void notForSale() {
        TerrainCard card = new TerrainCard(TerrainType.LAND);
        card.getCost();
    }
}
