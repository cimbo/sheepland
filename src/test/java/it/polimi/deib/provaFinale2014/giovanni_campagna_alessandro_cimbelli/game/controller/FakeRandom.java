package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import java.util.Random;

/**
 * This is a pseudorandom class the give always 1 as nextInt()
 * 
 * It is used in the wolf and black sheep tests, so that they will try to move
 * through road 2 ({@link ServerGameEngine.rollDice} will return 1+1)
 * 
 * @author Alessandro Cimbelli
 */
public class FakeRandom extends Random {
    private static final long serialVersionUID = 1L;

    @Override
    public int nextInt(int x) {
        if (x <= 1) {
            return 0;
        } else {
            return 1;
        }
    }
}
