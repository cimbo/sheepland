package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class ServerMateSheepTest extends ServerPlayTestBase {
    public ServerMateSheepTest() {
        super(new FakeRandom());
    }

    @Test
    public void correctTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // the road is a 2, which conveniently is what FakeRandom()
        // gives as the dice (1+1)
        SheepTileIdentifier stId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st = getEngine().getSheepTileFromId(stId);
        assertEquals(TerrainType.LAND, st.getTerrainType());
        assertEquals(2, shepherd.getCurrentPosition().getDiceValue());

        // add a female sheep and a ram
        Sheep newSheep = new Sheep(SheepType.FEMALE_SHEEP);
        getEngine().getGameBoard().addSheep(newSheep);
        st.addSheep(newSheep);
        newSheep = new Sheep(SheepType.RAM);
        getEngine().getGameBoard().addSheep(newSheep);
        st.addSheep(newSheep);

        int femaleSheepAtOrigin = st.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        assertTrue(femaleSheepAtOrigin >= 1);
        int ramsAtOrigin = st.getNumberOfSheep(SheepType.RAM);
        assertTrue(ramsAtOrigin >= 1);
        int lambsAtOrigin = st.getNumberOfSheep(SheepType.LAMB);
        assertFalse(st.getHasWolf());

        MateSheepMove msm = new MateSheepMove(players[0].getId(), shepherdId,
                stId);
        executeMove(msm);

        assertEquals(femaleSheepAtOrigin,
                st.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin + 1, st.getNumberOfSheep(SheepType.LAMB));

        Sheep newLamb = msm.getResult();
        assertNotNull(newLamb);
        assertNotNull(getEngine().getGameBoard()
                .getSheepFromId(newLamb.getId()));
        assertTrue(st.containsSheep(newLamb));
        assertTrue(newLamb instanceof Lamb);
        assertEquals(0, ((Lamb) newLamb).getAge());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }
}