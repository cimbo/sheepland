package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnectionHandler;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InvalidClientProtocolTest {
    private SocketServerNetwork serverNetwork;
    private ServerConnection server;
    private Socket client;
    private OutputStream clientOut;
    private ObjectOutputStream clientObjectOut;

    @Before
    public void setUp() throws IOException {
        serverNetwork = new SocketServerNetwork();
        serverNetwork.setConnectionHandler(new ServerConnectionHandler() {
            public void handleNewConnection(ServerConnection connection) {
                connection.setServerReceiver(new ServerReceiver() {
                    public void connectionClosed() {
                        synchronized (InvalidClientProtocolTest.this) {
                            server = null;
                            InvalidClientProtocolTest.this.notify();
                        }
                    }

                    public void doAbandonGame() {
                        fail();
                    }

                    public void doForceSynchronizeMe() {
                        fail();
                    }

                    public void doExecuteMove(Move move) {
                        fail();
                    }

                    public void doLoginToServer(String username, String password) {
                        fail();
                    }

                    public void doPong() {
                        fail();
                    }
                });

                synchronized (InvalidClientProtocolTest.this) {
                    server = connection;
                    InvalidClientProtocolTest.this.notify();
                }
            }
        });
        serverNetwork.startListening();

        client = new Socket("127.0.0.1", SocketServerNetwork.DEFAULT_PORT);
        clientOut = client.getOutputStream();
        waitConnection();
    }

    @After
    public void tearDown() throws IOException {
        if (!client.isClosed()) {
            client.close();
        }

        serverNetwork.stopListening();
    }

    private synchronized void waitConnection() {
        while (server == null) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    private synchronized void waitClosed() {
        while (server != null) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    @Test(timeout = 5000)
    public void missingOOSHeader() throws IOException {
        byte[] badHeader = new byte[50];
        Arrays.fill(badHeader, (byte) 42);

        clientOut.write(badHeader);
        waitClosed();
    }

    @Test(timeout = 5000)
    public void invalidVersion() throws IOException {
        clientObjectOut = new ObjectOutputStream(clientOut);

        clientObjectOut.writeByte(Protocol.V1.getVersion() + 1);
        clientObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void badFlags() throws IOException {
        clientObjectOut = new ObjectOutputStream(clientOut);

        clientObjectOut.writeByte(Protocol.V1.getVersion());
        clientObjectOut.writeInt(7);
        clientObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void badOperationNegative() throws IOException {
        clientObjectOut = new ObjectOutputStream(clientOut);

        clientObjectOut.writeByte(Protocol.V1.getVersion());
        clientObjectOut.writeInt(0);
        clientObjectOut.writeInt(-1);
        clientObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void badOperationExceeding() throws IOException {
        clientObjectOut = new ObjectOutputStream(clientOut);

        clientObjectOut.writeByte(Protocol.V1.getVersion());
        clientObjectOut.writeInt(0);
        clientObjectOut.writeInt(ServerOperation.values().length);
        clientObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void invalidArguments() throws IOException {
        clientObjectOut = new ObjectOutputStream(clientOut);

        clientObjectOut.writeByte(Protocol.V1.getVersion());
        clientObjectOut.writeInt(0);
        clientObjectOut.writeInt(ServerOperation.LOGIN_TO_SERVER.ordinal());
        clientObjectOut.writeObject(new ShepherdIdentifier(0));
        clientObjectOut.flush();
        waitClosed();
    }
}
