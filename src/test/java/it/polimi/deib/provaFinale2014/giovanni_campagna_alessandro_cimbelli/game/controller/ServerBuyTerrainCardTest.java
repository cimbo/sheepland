package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class ServerBuyTerrainCardTest extends ServerPlayTestBase {
    @Test
    public void correctTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // buy land card
        SheepTileIdentifier tileId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile tile = getEngine().getSheepTileFromId(tileId);
        assertEquals(TerrainType.LAND, tile.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .isSquareAdjacentToTile(tile, shepherd.getCurrentPosition()));

        int cardsBefore = players[0].getNumberOfCards(TerrainType.LAND);
        int coinsBefore = players[0].getRemainingCoins();
        int cost = 0;

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.LAND, null);
        assertNotNull(card);
        executeMove(new BuyTerrainCardMove(players[0].getId(), shepherdId,
                card.getId()));

        assertEquals(cardsBefore + 1,
                players[0].getNumberOfCards(TerrainType.LAND));
        assertEquals(coinsBefore - cost, players[0].getRemainingCoins());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test
    public void correctTestTweaked() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // but some other player had bought two cards of land
        // buy land card
        SheepTileIdentifier tileId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile tile = getEngine().getSheepTileFromId(tileId);
        assertEquals(TerrainType.LAND, tile.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .isSquareAdjacentToTile(tile, shepherd.getCurrentPosition()));
        for (int i = 0; i < 2; i++) {
            TerrainCard card = getEngine().getGameBoard()
                    .getCheapestTerrainCard(TerrainType.LAND, null);
            assertNotNull(card);
            getEngine().getGameBoard().markCardAcquired(card);
        }

        int cardsBefore = players[0].getNumberOfCards(TerrainType.LAND);
        int coinsBefore = players[0].getRemainingCoins();
        int cost = 2;

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.LAND, null);
        assertNotNull(card);
        executeMove(new BuyTerrainCardMove(players[0].getId(), shepherdId,
                card.getId()));

        assertEquals(cardsBefore + 1,
                players[0].getNumberOfCards(TerrainType.LAND));
        assertEquals(coinsBefore - cost, players[0].getRemainingCoins());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test
    public void doubleMove() throws InvalidMoveException {
        correctTest();

        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // buy grain card
        // (will fail because you can't do the same move twice)
        SheepTileIdentifier tileId = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile tile = getEngine().getSheepTileFromId(tileId);
        assertEquals(TerrainType.GRAIN, tile.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .isSquareAdjacentToTile(tile, shepherd.getCurrentPosition()));

        int cardsBefore = players[0].getNumberOfCards(TerrainType.GRAIN);
        int coinsBefore = players[0].getRemainingCoins();

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.GRAIN, null);
        assertNotNull(card);
        try {
            executeMove(new BuyTerrainCardMove(players[0].getId(), shepherdId,
                    card.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(cardsBefore,
                players[0].getNumberOfCards(TerrainType.GRAIN));
        assertEquals(coinsBefore, players[0].getRemainingCoins());
    }

    @Test
    public void wrongShepherd() throws InvalidIdentifierException,
            RuleViolationException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        // wrong shepherd!
        Shepherd shepherd = getEngine().getShepherdFromId(players[1],
                shepherdId);

        // we're between water1 and water2 (see ServerPlayTestBase)
        // buy water card
        // (will fail because it's the wrong shepherd)
        SheepTileIdentifier tileId = new SheepTileIdentifier(2 * 3 + 1 + 1);
        SheepTile tile = getEngine().getSheepTileFromId(tileId);
        assertEquals(TerrainType.WATER, tile.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .isSquareAdjacentToTile(tile, shepherd.getCurrentPosition()));

        int cardsBefore = players[0].getNumberOfCards(TerrainType.WATER);
        int coinsBefore = players[0].getRemainingCoins();

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.LAND, null);
        assertNotNull(card);
        try {
            // go straight to moveSheep() here, going through
            // the move forces the right thing (because shepherd ids are
            // scoped to a player)
            getEngine().buyTerrainCard(players[0], shepherd, card);
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(cardsBefore,
                players[0].getNumberOfCards(TerrainType.WATER));
        assertEquals(coinsBefore, players[0].getRemainingCoins());
    }

    @Test
    public void notEnoughCoins() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // buy land card after someone else bought land card (so now it
        // costs 1), and we have no money
        SheepTileIdentifier tileId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile tile = getEngine().getSheepTileFromId(tileId);
        assertEquals(TerrainType.LAND, tile.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .isSquareAdjacentToTile(tile, shepherd.getCurrentPosition()));

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.LAND, null);
        assertNotNull(card);
        getEngine().getGameBoard().markCardAcquired(card);
        players[0].payCoins(players[0].getRemainingCoins());

        int cardsBefore = players[0].getNumberOfCards(TerrainType.LAND);
        int coinsBefore = players[0].getRemainingCoins();
        assertEquals(0, coinsBefore);

        card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.LAND, null);
        assertNotNull(card);
        try {
            executeMove(new BuyTerrainCardMove(players[0].getId(), shepherdId,
                    card.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(cardsBefore, players[0].getNumberOfCards(TerrainType.LAND));
        assertEquals(coinsBefore, players[0].getRemainingCoins());
    }

    @Test
    public void wrongTerrainType() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // buy desert card
        SheepTileIdentifier tileId = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile tile = getEngine().getSheepTileFromId(tileId);
        assertEquals(TerrainType.LAND, tile.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .isSquareAdjacentToTile(tile, shepherd.getCurrentPosition()));

        int cardsBefore = players[0].getNumberOfCards(TerrainType.DESERT);
        int coinsBefore = players[0].getRemainingCoins();

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.DESERT, null);
        assertNotNull(card);
        try {
            executeMove(new BuyTerrainCardMove(players[0].getId(), shepherdId,
                    card.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(cardsBefore,
                players[0].getNumberOfCards(TerrainType.DESERT));
        assertEquals(coinsBefore, players[0].getRemainingCoins());
    }
}
