package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class ServerMarketTest extends ServerPlayTestBase {
    public ServerMarketTest() {
        super(new FakeRandom());
    }

    @Test
    public void correctTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.GRAIN, null);
        getEngine().buyTerrainCard(players[0],
                players[0].getShepherdFromId(shepherdId), card);
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(1, players[0].getNumberOfCards(TerrainType.GRAIN));
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        executeMove(new SellMarketCardMove(players[0].getId(), card.getId(), 3));

        assertEquals(
                card,
                getEngine().getGameBoard().getCheapestTerrainCard(
                        TerrainType.GRAIN, players[0]));
        assertEquals(
                1,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0]).length);
        assertEquals(card,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0])[0]);
        assertTrue(card.isForSale());
        assertEquals(3, card.getCost());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        executeMove(new BuyMarketCardMove(players[1].getId(), card.getId()));

        assertEquals(
                0,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0]).length);
        assertNull(getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.GRAIN, players[0]));
        assertFalse(card.isForSale());
        assertEquals(players[1], card.getOwner());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer() + 3,
                players[0].getRemainingCoins());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer() - 3,
                players[1].getRemainingCoins());

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(0, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(0, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test
    public void correctTestNoBuy() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.GRAIN, null);
        getEngine().buyTerrainCard(players[0],
                players[0].getShepherdFromId(shepherdId), card);
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(1, players[0].getNumberOfCards(TerrainType.GRAIN));
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        executeMove(new SellMarketCardMove(players[0].getId(), card.getId(), 3));

        assertEquals(
                card,
                getEngine().getGameBoard().getCheapestTerrainCard(
                        TerrainType.GRAIN, players[0]));
        assertEquals(
                1,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0]).length);
        assertEquals(card,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0])[0]);
        assertTrue(card.isForSale());
        assertEquals(3, card.getCost());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(0, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(0, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        assertEquals(
                0,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0]).length);
        assertNull(getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.GRAIN, players[0]));
        assertFalse(card.isForSale());
        assertEquals(players[0], card.getOwner());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[1].getRemainingCoins());
    }

    @Test
    public void sellInitialCard() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        try {
            executeMove(new SellMarketCardMove(players[0].getId(), players[0]
                    .getInitialCard().getId(), 3));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test
    public void sellWrongOwnerCard() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        try {
            executeMove(new SellMarketCardMove(players[0].getId(), players[1]
                    .getInitialCard().getId(), 3));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }

    @Test
    public void notEnoughCoins() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.GRAIN, null);
        getEngine().buyTerrainCard(players[0],
                players[0].getShepherdFromId(shepherdId), card);
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(1, players[0].getNumberOfCards(TerrainType.GRAIN));
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        executeMove(new SellMarketCardMove(players[0].getId(), card.getId(), 3));

        assertEquals(
                card,
                getEngine().getGameBoard().getCheapestTerrainCard(
                        TerrainType.GRAIN, players[0]));
        assertEquals(
                1,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0]).length);
        assertEquals(card,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0])[0]);
        assertTrue(card.isForSale());
        assertEquals(3, card.getCost());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        players[1].payCoins(players[1].getRemainingCoins() - 2);
        assertEquals(2, players[1].getRemainingCoins());

        try {
            executeMove(new BuyMarketCardMove(players[1].getId(), card.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(
                1,
                getEngine().getGameBoard().getTerrainCardsOnSale(players[0]).length);
        assertEquals(
                card,
                getEngine().getGameBoard().getCheapestTerrainCard(
                        TerrainType.GRAIN, players[0]));
        assertTrue(card.isForSale());
        assertEquals(players[0], card.getOwner());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(2, players[1].getRemainingCoins());
    }

    @Test
    public void buyInitialCard() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        try {
            executeMove(new BuyMarketCardMove(players[1].getId(), players[0]
                    .getInitialCard().getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(players[0], players[0].getInitialCard().getOwner());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[1].getRemainingCoins());
    }

    @Test
    public void buyGameBoardCard() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);
        getEngine().checkAndAdvanceNextTurn();
        getEngine().forceEndTurn(players[1]);
        getEngine().checkAndAdvanceNextTurn();

        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[0]);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_SELL, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        getEngine().forceEndTurn(players[1]);

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.MARKET_BUY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        TerrainCard card = getEngine().getGameBoard().getCheapestTerrainCard(
                TerrainType.LAND, null);
        assertTrue(card.isForSale());
        assertEquals(0, card.getCost());
        assertNull(card.getOwner());

        try {
            executeMove(new BuyMarketCardMove(players[1].getId(), card.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(
                card,
                getEngine().getGameBoard().getCheapestTerrainCard(
                        TerrainType.LAND, null));
        assertNull(card.getOwner());
        assertTrue(card.isForSale());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[0].getRemainingCoins());
        assertEquals(getEngine().getRules().getInitialCoinsPerPlayer(),
                players[1].getRemainingCoins());
    }
}