package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class ServerMoveWolfTest {
    private SheepTileIdentifier sentOutDestinationId;
    private SheepIdentifier sentOutSheepId;

    @Before
    public void clearOutTemporaries() {
        sentOutDestinationId = null;
        sentOutSheepId = null;
    }

    @Test
    public void simpleWolfTest() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine(new FakeRandom());
        GameMap map = engine.getGameBoard().getMap();

        // place the wolf in land/1
        // because of the random dice always saying 2, it will go into
        // forest/0
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st1 = engine.getSheepTileFromId(stId1);
        SheepTileIdentifier stId2 = new SheepTileIdentifier(1 + 1 * 3 + 0);
        SheepTile st2 = engine.getSheepTileFromId(stId2);
        RoadSquareIdentifier rsId = new RoadSquareIdentifier(
                stId1.getContent(), stId2.getContent());
        RoadSquare rs = engine.getRoadSquareFromId(rsId);
        assertEquals(2, rs.getDiceValue());
        assertFalse(rs.getHasFence());

        assertEquals(map.getSheepsburg(), map.getCurrentWolfPosition());
        map.moveWolf(st1);
        assertEquals(st1, map.getCurrentWolfPosition());

        int femaleSheepBefore = st2.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsBefore = st2.getNumberOfSheep(SheepType.RAM);
        int lambsBefore = st2.getNumberOfSheep(SheepType.LAMB);
        assertEquals(1, femaleSheepBefore + ramsBefore + lambsBefore);

        engine.moveWolf();

        assertEquals(st2, map.getCurrentWolfPosition());
        assertEquals(stId2, sentOutDestinationId);
        assertNotNull(sentOutSheepId);
        Sheep eatenSheep = engine.getSheepFromId(sentOutSheepId);

        int femaleSheepAfter = st2.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAfter = st2.getNumberOfSheep(SheepType.RAM);
        int lambsAfter = st2.getNumberOfSheep(SheepType.LAMB);
        assertEquals(0, femaleSheepAfter + ramsAfter + lambsAfter);

        if (eatenSheep.getType() == SheepType.FEMALE_SHEEP) {
            assertEquals(femaleSheepBefore - 1, femaleSheepAfter);
            assertEquals(ramsBefore, ramsAfter);
            assertEquals(lambsBefore, lambsAfter);
        } else if (eatenSheep.getType() == SheepType.RAM) {
            assertEquals(femaleSheepBefore, femaleSheepAfter);
            assertEquals(ramsBefore - 1, ramsAfter);
            assertEquals(lambsBefore, lambsAfter);
        } else if (eatenSheep.getType() == SheepType.LAMB) {
            assertEquals(femaleSheepBefore, femaleSheepAfter);
            assertEquals(ramsBefore, ramsAfter);
            assertEquals(lambsBefore - 1, lambsAfter);
        } else {
            fail();
        }
    }

    @Test
    public void emptyDestinationTile() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine(new FakeRandom());
        GameMap map = engine.getGameBoard().getMap();

        // place the wolf in land/1
        // because of the random dice always saying 2, it will go into
        // forest/0, which only contains the black sheep, and therefore
        // will not eat there
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st1 = engine.getSheepTileFromId(stId1);
        SheepTileIdentifier stId2 = new SheepTileIdentifier(1 + 1 * 3 + 0);
        SheepTile st2 = engine.getSheepTileFromId(stId2);
        RoadSquareIdentifier rsId = new RoadSquareIdentifier(
                stId1.getContent(), stId2.getContent());
        RoadSquare rs = engine.getRoadSquareFromId(rsId);
        assertEquals(2, rs.getDiceValue());
        assertFalse(rs.getHasFence());

        // clean up st2 before the wolf goes there
        for (Sheep s : st2.getAllSheep()) {
            st2.removeSheep(s);
        }
        map.moveBlackSheep(engine.getGameBoard().getBlackSheep(), st2);

        assertEquals(map.getSheepsburg(), map.getCurrentWolfPosition());
        map.moveWolf(st1);
        assertEquals(st1, map.getCurrentWolfPosition());

        int femaleSheepBefore = st2.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsBefore = st2.getNumberOfSheep(SheepType.RAM);
        int lambsBefore = st2.getNumberOfSheep(SheepType.LAMB);
        assertEquals(0, femaleSheepBefore + ramsBefore + lambsBefore);
        assertEquals(1, st2.getNumberOfSheep(SheepType.BLACK_SHEEP));

        engine.moveWolf();

        assertEquals(st2, map.getCurrentWolfPosition());
        assertEquals(stId2, sentOutDestinationId);
        assertNull(sentOutSheepId);

        int femaleSheepAfter = st2.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAfter = st2.getNumberOfSheep(SheepType.RAM);
        int lambsAfter = st2.getNumberOfSheep(SheepType.LAMB);
        assertEquals(0, femaleSheepAfter + ramsAfter + lambsAfter);
        assertEquals(1, st2.getNumberOfSheep(SheepType.BLACK_SHEEP));
    }

    @Test
    public void wolfFenceLockedMove() throws InvalidIdentifierException,
            RuleViolationException {
        // Tanto all'inizio il lupo sta a sheepsburg
        // Questo tmp serve a verificare che il lupo non torni in una posizione
        // identica a quella di prima

        ServerGameEngine engine = getAGameEngine(new Random(42));
        GameMap map = engine.getGameBoard().getMap();

        SheepTile tmp = map.getSheepTileFromId(new SheepTileIdentifier(
                1 + 4 * 3 + 0));

        for (int j = 0; j < 2; j++) {
            RoadSquare[] listOfRoad = map.getAllAdjacentSquares(map
                    .getCurrentWolfPosition());

            for (int i = 0; i < listOfRoad.length; i++) {
                if (!listOfRoad[i].getHasFence()) {
                    listOfRoad[i].placeFence(false);
                }
            }

            SheepTile[] listOfSheepTiles = map.getAllAdjacentTiles(map
                    .getCurrentWolfPosition());

            engine.moveWolf();

            assertNotEquals(tmp, map.getCurrentWolfPosition());
            assertEquals(map.getCurrentWolfPosition().getId(),
                    sentOutDestinationId);

            // The wolf is locked now, so it will escape
            assertTrue(isInOneOfAdjacentTile(listOfSheepTiles,
                    map.getCurrentWolfPosition(), map));
            tmp = map.getCurrentWolfPosition();
        }
    }

    private boolean isInOneOfAdjacentTile(SheepTile[] list,
            SheepTile wolfPosition, GameMap map) {
        for (SheepTile tileAdjacent : list) {
            if (tileAdjacent.equals(map.getCurrentWolfPosition())) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void wolfDosentCrossFence() throws RuleViolationException {

        ServerGameEngine engine = getAGameEngine(new FakeRandom());
        GameMap map = engine.getGameBoard().getMap();

        SheepTile wolfSheepTile = map.getCurrentWolfPosition();

        // Now i set a fence in the road the the false FakeRandom will choose so
        // i
        // control if the wolf will across that road or not
        map.getAdjacentSquareForValue(wolfSheepTile, 2).placeFence(false);
        assertEquals(wolfSheepTile, map.getCurrentWolfPosition());

        engine.moveWolf();

        // Now the wolf should be in the same tile in which was before the try
        // of moving
        assertEquals(wolfSheepTile, map.getCurrentWolfPosition());
        assertNull(sentOutDestinationId);
        assertNull(sentOutSheepId);
    }

    @Test
    public void wolfDosentGoIntotheSea() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngine(new FakeRandom());
        GameMap map = engine.getGameBoard().getMap();

        SheepTileIdentifier id = new SheepTileIdentifier(1 + 5 * 3 + 2);

        // My pseudo random dice will give me always 2 as result so i put the
        // wolf in a tile in which the road(2) dosen't exist so i will verify
        // that he will not move from that tile
        SheepTile tile = engine.getSheepTileFromId(id);
        map.moveWolf(tile);
        assertEquals(tile, map.getCurrentWolfPosition());

        // Here the wolf will try to go in another tile (That dosen't exist)
        engine.moveWolf();

        // Now the wolf should be in the same tile in which was before the try
        // of moving
        assertEquals(tile, map.getCurrentWolfPosition());
        assertNull(sentOutDestinationId);
        assertNull(sentOutSheepId);
    }

    private ServerGameEngine getAGameEngine(Random dice)
            throws RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();

        ServerGameEngine engine = new ServerGameEngine(dice,
                new EmptyServerDelegate() {
                    @Override
                    public void didMoveWolf(SheepTileIdentifier destination,
                            SheepIdentifier sheep) {
                        sentOutDestinationId = destination;
                        sentOutSheepId = sheep;
                    }
                }, rules);
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();

        return engine;
    }
}
