package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

import org.junit.Before;
import org.junit.Test;

public class RoadSquareTest {
    private RoadSquare square;
    private SheepTile st1, st2;

    @Before
    public void setUp() throws Exception {
        square = new RoadSquare(4);
        st1 = new SheepTile(TerrainType.LAND, 0);
        st2 = new SheepTile(TerrainType.MOUNTAIN, 0);

        square.connect(st1, st2);
        st1.connect(st2, square);
        st2.connect(st1, square);
    }

    @Test
    public void dice() {
        // http://xkcd.com/221/
        assertEquals(4, square.getDiceValue());
    }

    @Test
    public void privateGetters() {
        assertEquals(st1, square.getOriginTile());
        assertEquals(st2, square.getDestinationTile());

        assertEquals(st1, square.getMatchingTile(st2));
        assertEquals(st2, square.getMatchingTile(st1));
    }

    @Test
    public void shepherdTest() {
        Player player = new Player("");
        Shepherd s1 = new Shepherd(player);
        Shepherd s2 = new Shepherd(player);

        assertNull(square.getCurrentShepherd());

        square.addShepherd(s1);
        assertEquals(s1, square.getCurrentShepherd());

        try {
            square.addShepherd(s2);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }

        try {
            square.addShepherd(s1);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }

        try {
            square.removeShepherd(s2);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }

        square.removeShepherd(s1);
        square.addShepherd(s2);
        assertEquals(s2, square.getCurrentShepherd());

        square.removeShepherd(s2);
        assertNull(square.getCurrentShepherd());
    }

    @Test
    public void fenceTest() {
        assertFalse(square.getHasFence());
        assertFalse(square.getHasFinalFence());

        square.placeFence(true);
        assertTrue(square.getHasFence());
        assertTrue(square.getHasFinalFence());

        Player player = new Player("");
        Shepherd s1 = new Shepherd(player);
        try {
            square.addShepherd(s1);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }
    }

    @Test
    public void doubleFenceTest() {
        assertFalse(square.getHasFence());
        assertFalse(square.getHasFinalFence());

        square.placeFence(false);
        assertTrue(square.getHasFence());
        assertFalse(square.getHasFinalFence());

        try {
            square.placeFence(false);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }
    }

    @Test
    public void fenceShepherdTest() {
        Player player = new Player("");
        Shepherd s1 = new Shepherd(player);
        square.addShepherd(s1);

        try {
            square.placeFence(false);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }
        assertFalse(square.getHasFence());

        square.removeShepherd(s1);
        square.placeFence(false);
        assertTrue(square.getHasFence());
    }
}
