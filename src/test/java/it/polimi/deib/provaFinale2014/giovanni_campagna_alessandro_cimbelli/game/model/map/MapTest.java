package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class MapTest {
    private GameMap map;
    private Sheep blackSheep;
    private Graph graph;
    private Random dice;

    @Before
    public void setUp() throws Exception {
        graph = new Graph();
        map = new GameMap(graph);
        dice = new Random(42);
        blackSheep = new Sheep(SheepType.BLACK_SHEEP);
    }

    private SheepTile getTile(TerrainType type, int ord) {
        if (type == TerrainType.SHEEPSBURG) {
            return graph.getRoot();
        }

        assert ord < 3 && ord >= 0;
        int code = type.ordinal() * 3 + ord;
        return graph.getSheepTiles()[code];
    }

    private SheepTile getTile(int code) {
        if (code < 0) {
            return graph.getRoot();
        } else {
            return graph.getSheepTiles()[code];
        }
    }

    @Test
    public void visit() {
        final HashSet<SheepTile> set = new HashSet<SheepTile>();

        map.forEachTile(new GraphVisitor() {
            public void visit(SheepTile tile) {
                if (set.contains(tile)) {
                    fail("Tile " + tile + " visited twice");
                }
                set.add(tile);
            }
        });

        assertEquals(19, set.size());
    }

    @Test
    public void sheepsburgTest() {
        assertEquals(TerrainType.SHEEPSBURG, map.getSheepsburg()
                .getTerrainType());
    }

    @Test
    public void randomTileTest() {
        SheepTile random = map.getRandomSheepTile(dice);

        assertTrue(!random.getTerrainType().equals(TerrainType.SHEEPSBURG));
    }

    @Test
    public void blackSheepTest() {
        SheepTile random = map.getRandomSheepTile(dice);

        map.moveBlackSheep(blackSheep, random);
        assertTrue(random.getHasBlackSheep());
        assertEquals(random, map.getCurrentBlackSheepPosition());

        map.moveBlackSheep(blackSheep, random);
        assertTrue(random.getHasBlackSheep());

        SheepTile st2 = random.getNeighbors().get(0).getDestination();
        assertFalse(st2.getHasBlackSheep());

        map.moveBlackSheep(blackSheep, st2);
        assertFalse(random.getHasBlackSheep());
        assertTrue(st2.getHasBlackSheep());
        assertEquals(st2, map.getCurrentBlackSheepPosition());
    }

    @Test
    public void wolfTest() {
        SheepTile random = map.getRandomSheepTile(dice);

        map.moveWolf(random);
        assertTrue(random.getHasWolf());
        assertEquals(random, map.getCurrentWolfPosition());

        map.moveWolf(random);
        assertTrue(random.getHasWolf());

        SheepTile st2 = random.getNeighbors().get(0).getDestination();
        assertFalse(st2.getHasWolf());

        map.moveWolf(st2);
        assertFalse(random.getHasWolf());
        assertTrue(st2.getHasWolf());
        assertEquals(st2, map.getCurrentWolfPosition());
    }

    @Test
    public void allTiles() {
        TerrainType[] types = TerrainType.values();
        for (TerrainType type : types) {
            for (int j = 0; j < 3; j++) {
                SheepTile tile = getTile(type, j);

                assertEquals(type, tile.getTerrainType());

                int neigh = tile.getNeighbors().size();
                assertTrue(neigh >= 3);
                assertTrue(neigh <= 6);
            }
        }
    }

    @Test
    public void adjacency() {
        SheepTile land0 = getTile(TerrainType.LAND, 0);
        SheepTile grain0 = getTile(TerrainType.GRAIN, 0);
        SheepTile land1 = getTile(TerrainType.LAND, 1);
        SheepTile sburg = getTile(TerrainType.SHEEPSBURG, 0);
        SheepTile water1 = getTile(TerrainType.WATER, 1);
        SheepTile water0 = getTile(TerrainType.WATER, 0);
        SheepTile forest1 = getTile(TerrainType.FOREST, 1);
        SheepTile forest2 = getTile(TerrainType.FOREST, 2);
        SheepTile mountain0 = getTile(TerrainType.MOUNTAIN, 0);
        SheepTile grain2 = getTile(TerrainType.GRAIN, 2);
        SheepTile mountain1 = getTile(TerrainType.MOUNTAIN, 1);
        SheepTile mountain2 = getTile(TerrainType.MOUNTAIN, 2);

        assertTrue(map.areTilesAdjacent(land0, grain0));
        assertTrue(map.areTilesAdjacent(land0, land1));
        assertFalse(map.areTilesAdjacent(land1, grain0));
        assertFalse(map.areTilesAdjacent(land0, sburg));
        assertFalse(map.areTilesAdjacent(land1, sburg));
        assertFalse(map.areTilesAdjacent(grain0, sburg));
        assertFalse(map.areTilesAdjacent(sburg, water1));
        assertFalse(map.areTilesAdjacent(sburg, forest1));
        assertTrue(map.areTilesAdjacent(sburg, forest2));
        assertTrue(map.areTilesAdjacent(sburg, water0));
        assertTrue(map.areTilesAdjacent(water0, forest2));
        assertTrue(map.areTilesAdjacent(water0, water1));
        assertTrue(map.areTilesAdjacent(forest1, forest2));
        assertFalse(map.areTilesAdjacent(forest1, water0));
        assertFalse(map.areTilesAdjacent(grain2, mountain2));
        assertTrue(map.areTilesAdjacent(grain2, mountain0));
        assertTrue(map.areTilesAdjacent(grain2, mountain1));
        assertTrue(map.areTilesAdjacent(mountain1, mountain0));
    }

    @Test
    public void symmetricAdjacency() {
        int n = graph.getSheepTiles().length;
        for (int i = -1; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                SheepTile st1 = getTile(i);
                SheepTile st2 = getTile(j);

                boolean adj1 = map.areTilesAdjacent(st1, st2);
                boolean adj2 = map.areTilesAdjacent(st2, st1);
                assertTrue(adj1 == adj2);
            }
        }
    }

    @Test
    public void irreflexiveAdjacency() {
        int n = graph.getSheepTiles().length;
        for (int i = -1; i < n; i++) {
            SheepTile tile = getTile(i);

            assertFalse(map.areTilesAdjacent(tile, tile));
        }
    }

    private TerrainType getAdjacentType(SheepTile tile, RoadSquare square) {
        return square.getMatchingTile(tile).getTerrainType();
    }

    @Test
    public void adjacentSquareForValue() {
        SheepTile sburg = getTile(TerrainType.SHEEPSBURG, 0);

        TerrainType[] types = { TerrainType.MOUNTAIN, TerrainType.DESERT,
                TerrainType.WATER, TerrainType.FOREST, TerrainType.LAND,
                TerrainType.GRAIN };
        for (int i = 0; i < 6; i++) {
            assertEquals(
                    types[i],
                    getAdjacentType(sburg,
                            map.getAdjacentSquareForValue(sburg, i + 1)));
        }

        SheepTile mountain0 = getTile(TerrainType.MOUNTAIN, 0);
        SheepTile mountain1 = getTile(TerrainType.MOUNTAIN, 1);
        SheepTile grain2 = getTile(TerrainType.GRAIN, 2);

        assertEquals(mountain1, map.getAdjacentSquareForValue(mountain0, 3)
                .getMatchingTile(mountain0));
        assertEquals(grain2, map.getAdjacentSquareForValue(mountain0, 1)
                .getMatchingTile(mountain0));
        assertNull(map.getAdjacentSquareForValue(mountain0, 6));

        SheepTile desert1 = getTile(TerrainType.DESERT, 1);
        SheepTile desert2 = getTile(TerrainType.DESERT, 2);
        assertNull(map.getAdjacentSquareForValue(desert1, 3));
        assertEquals(desert2, map.getAdjacentSquareForValue(desert1, 5)
                .getMatchingTile(desert1));
    }

    @Test
    public void uniqueAdjacency() {
        int n = graph.getSheepTiles().length;
        for (int i = -1; i < n; i++) {
            SheepTile tile = getTile(i);
            HashMap<Integer, RoadSquare> neighMap = new HashMap<Integer, RoadSquare>();
            HashSet<RoadSquare> squareSet = new HashSet<RoadSquare>();
            HashSet<SheepTile> tileSet = new HashSet<SheepTile>();

            for (Edge e : tile.getNeighbors()) {
                RoadSquare square = e.getRoadSquare();
                SheepTile otherTile = e.getDestination();
                int value = square.getDiceValue();

                if (squareSet.contains(square)) {
                    fail("Square " + square + " contained for two neighbors");
                }
                squareSet.add(square);

                if (neighMap.get(value) != null) {
                    fail("Dice value " + value
                            + " contained for two squares: \"" + square
                            + "\" and \"" + neighMap.get(value) + "\"");
                }
                neighMap.put(value, square);

                if (tileSet.contains(otherTile)) {
                    fail("Tile " + otherTile
                            + " is contained twice in the adjacency list of "
                            + tile);
                }
                tileSet.add(otherTile);

                assertEquals(otherTile, square.getMatchingTile(tile));
                assertEquals(tile, square.getMatchingTile(otherTile));
            }
        }
    }

    @Test
    public void symmetricAdjacencyForValue() {
        int n = graph.getSheepTiles().length;
        for (int i = -1; i < n; i++) {
            SheepTile st1 = getTile(i);

            for (int j = 0; j < 6; j++) {
                RoadSquare rs = map.getAdjacentSquareForValue(st1, j + 1);
                if (rs == null) {
                    continue;
                }

                SheepTile st2 = rs.getMatchingTile(st1);
                assertEquals(rs, map.getAdjacentSquareForValue(st2, j + 1));
                assertTrue(map.areTilesAdjacent(st1, st2));
            }
        }
    }

    @Test
    public void incidency() {
        int n = graph.getSheepTiles().length;
        for (int i = -1; i < n; i++) {
            SheepTile tile = getTile(i);

            for (Edge e : tile.getNeighbors()) {
                RoadSquare square = e.getRoadSquare();

                SheepTile[] incident = map.getIncidentTiles(square);

                assertTrue(incident[0].equals(tile) || incident[1].equals(tile));
                assertEquals(2, incident.length);
                assertFalse(incident[0].equals(incident[1]));
                assertTrue(map.isSquareAdjacentToTile(incident[0], square));
                assertTrue(map.isSquareAdjacentToTile(incident[1], square));
            }
        }
    }

    private HashSet<RoadSquare> getAllSquares() {
        final HashSet<RoadSquare> set = new HashSet<RoadSquare>();

        map.forEachTile(new GraphVisitor() {
            public void visit(SheepTile tile) {
                for (Edge e : tile.getNeighbors()) {
                    set.add(e.getRoadSquare());
                }
            }
        });

        return set;
    }

    @Test
    public void symmetricSquareAdjacency() {
        HashSet<RoadSquare> allSquares = getAllSquares();

        for (RoadSquare rs1 : allSquares) {
            for (RoadSquare rs2 : allSquares) {
                if (rs1 == rs2) {
                    assertFalse(map.areSquaresAdjacent(rs1, rs2));
                    continue;
                }

                boolean adj1 = map.areSquaresAdjacent(rs1, rs2);
                boolean adj2 = map.areSquaresAdjacent(rs2, rs1);
                assertTrue(adj1 == adj2);
            }
        }
    }

    @Test
    public void irreflexiveSquareAdjacency() {
        HashSet<RoadSquare> allSquares = getAllSquares();

        for (RoadSquare rs : allSquares) {
            assertFalse(map.areSquaresAdjacent(rs, rs));
        }
    }

    private RoadSquare getAdjacentSquareForTile(SheepTile tile,
            SheepTile destination) {
        for (Edge e : tile.getNeighbors()) {
            if (e.getDestination() == destination) {
                return e.getRoadSquare();
            }
        }

        fail("wut?");
        return null;
    }

    @Test
    public void squareAdjacency() {
        int n = graph.getSheepTiles().length;
        for (int i = -1; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                for (int k = j + 1; k < n; k++) {
                    SheepTile sti = getTile(i);
                    SheepTile stj = getTile(j);
                    SheepTile stk = getTile(k);

                    if (map.areTilesAdjacent(sti, stj)
                            && map.areTilesAdjacent(sti, stk)
                            && map.areTilesAdjacent(stj, stk)) {
                        RoadSquare rs1 = getAdjacentSquareForTile(sti, stj);
                        RoadSquare rs2 = getAdjacentSquareForTile(stj, stk);
                        RoadSquare rs3 = getAdjacentSquareForTile(sti, stk);

                        assertTrue(map.areSquaresAdjacent(rs1, rs2));
                        assertTrue(map.areSquaresAdjacent(rs2, rs3));
                        assertTrue(map.areSquaresAdjacent(rs1, rs3));
                    }
                }
            }
        }
    }

    @Test
    public void scoreValue() {
        SheepTile mountain0 = getTile(TerrainType.MOUNTAIN, 0);
        SheepTile mountain1 = getTile(TerrainType.MOUNTAIN, 1);
        SheepTile desert2 = getTile(TerrainType.DESERT, 2);

        int[] scoreValue = map.getScoreValues();
        assertEquals(TerrainType.NUMBER_OF_TERRAINS, scoreValue.length);
        int[] empty = {};
        int[] reference = Arrays.copyOf(empty, TerrainType.NUMBER_OF_TERRAINS);

        assertArrayEquals(reference, scoreValue);

        Sheep s1 = new Sheep(SheepType.FEMALE_SHEEP);
        mountain0.addSheep(s1);
        reference[TerrainType.MOUNTAIN.ordinal()] = 1;
        assertArrayEquals(reference, map.getScoreValues());

        mountain0.addSheep(new Sheep(SheepType.BLACK_SHEEP));
        reference[TerrainType.MOUNTAIN.ordinal()] = 3;
        assertArrayEquals(reference, map.getScoreValues());

        mountain1.addSheep(new Sheep(SheepType.FEMALE_SHEEP));
        mountain0.removeSheep(s1);
        assertArrayEquals(reference, map.getScoreValues());

        desert2.addSheep(new Sheep(SheepType.RAM));
        reference[TerrainType.DESERT.ordinal()] = 1;
        assertArrayEquals(reference, map.getScoreValues());
    }
}
