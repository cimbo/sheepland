package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class AnimationTest {
    private double progress;
    private int callCount;

    private class DirectAnimator extends Animator {
        @Override
        public void update(double p) {
            progress = p;
        }
    }

    private class CountAnimationListener implements AnimationListener {
        public void onComplete() {
            callCount++;
        }
    }

    @Before
    public void setUp() {
        progress = -1;
        callCount = 0;
    }

    @Test
    public void baseTest() {
        Animation animation = new Animation(new DirectAnimator(),
                AnimationAlpha.LINEAR, 1000);
        assertFalse(animation.isRunning());

        animation.start(0);
        assertTrue(animation.isRunning());
        assertEquals(0.0, progress, 0.001);

        animation.update(100);
        assertTrue(animation.isRunning());
        assertEquals(0.1, progress, 0.001);

        animation.update(500);
        assertTrue(animation.isRunning());
        assertEquals(0.5, progress, 0.001);

        animation.update(850);
        assertTrue(animation.isRunning());
        assertEquals(0.85, progress, 0.001);

        animation.stop();
        assertFalse(animation.isRunning());

        animation.update(1000);
        assertFalse(animation.isRunning());
        assertEquals(0.85, progress, 0.001);

        animation.start(1100);
        assertTrue(animation.isRunning());
        assertEquals(0.0, progress, 0.001);

        animation.stopAndComplete();
        assertFalse(animation.isRunning());
        assertEquals(1.0, progress, 0.001);

        animation.stopAndRewind();
        assertFalse(animation.isRunning());
        assertEquals(0.0, progress, 0.001);
    }

    @Test
    public void loopTest() {
        Animation animation = new Animation(new DirectAnimator(),
                AnimationAlpha.LINEAR, 1000);
        animation.setLoop(true);

        animation.start(0);
        assertEquals(0.0, progress, 0.001);

        animation.update(100);
        assertEquals(0.1, progress, 0.001);

        animation.update(500);
        assertEquals(0.5, progress, 0.001);

        animation.update(850);
        assertEquals(0.85, progress, 0.001);

        animation.update(1000);
        assertEquals(1.0, progress, 0.001);

        animation.update(1100);
        assertEquals(0.1, progress, 0.001);

        animation.update(1200);
        assertEquals(0.2, progress, 0.001);

        animation.update(2400);
        assertEquals(0.4, progress, 0.001);

        animation.stopAndComplete();
        assertEquals(1.0, progress, 0.001);

        animation.stopAndRewind();
        assertEquals(0.0, progress, 0.001);
    }

    @Test
    public void loopReverseTest() {
        Animation animation = new Animation(new DirectAnimator(),
                AnimationAlpha.LINEAR, 1000);
        animation.setLoop(true);
        animation.setLoopReverse(true);

        animation.start(0);
        assertEquals(0.0, progress, 0.001);

        animation.update(100);
        assertEquals(0.1, progress, 0.001);

        animation.update(500);
        assertEquals(0.5, progress, 0.001);

        animation.update(850);
        assertEquals(0.85, progress, 0.001);

        animation.update(1000);
        assertEquals(1.0, progress, 0.001);

        animation.update(1100);
        assertEquals(0.9, progress, 0.001);

        animation.update(1200);
        assertEquals(0.8, progress, 0.001);

        animation.update(2400);
        assertEquals(0.4, progress, 0.001);

        animation.stopAndComplete();
        assertEquals(1.0, progress, 0.001);

        animation.stopAndRewind();
        assertEquals(0.0, progress, 0.001);
    }

    @Test
    public void onCompleteTest() {
        Animation animation = new Animation(new DirectAnimator(),
                AnimationAlpha.LINEAR, 1000);
        animation.addAnimationListener(new CountAnimationListener());

        animation.start(0);
        assertEquals(0.0, progress, 0.001);
        assertEquals(0, callCount);

        animation.update(100);
        assertEquals(0.1, progress, 0.001);
        assertEquals(0, callCount);

        animation.update(500);
        assertEquals(0.5, progress, 0.001);

        animation.update(850);
        assertEquals(0.85, progress, 0.001);
        assertEquals(0, callCount);

        animation.update(1000);
        assertEquals(1.0, progress, 0.001);
        assertEquals(1, callCount);

        animation.update(1100);
        assertEquals(1.0, progress, 0.001);
        assertEquals(1, callCount);

        animation.start(2000);
        assertEquals(0.0, progress, 0.001);
        assertEquals(1, callCount);

        animation.update(2500);
        assertEquals(0.5, progress, 0.001);
        assertEquals(1, callCount);

        animation.update(3100);
        assertEquals(1.0, progress, 0.001);
        assertEquals(2, callCount);

        animation.setLoop(true);
        animation.start(4000);
        assertEquals(0.0, progress, 0.001);
        assertEquals(2, callCount);

        animation.update(4500);
        assertEquals(0.5, progress, 0.001);
        assertEquals(2, callCount);

        animation.update(5100);
        assertEquals(0.1, progress, 0.001);
        assertEquals(2, callCount);

        animation.update(5600);
        assertEquals(0.6, progress, 0.001);
        assertEquals(2, callCount);

        animation.stop();
        assertEquals(0.6, progress, 0.001);
        assertEquals(2, callCount);

        animation.stopAndComplete();
        assertEquals(1.0, progress, 0.001);
        assertEquals(3, callCount);

        animation.stopAndRewind();
        assertEquals(0.0, progress, 0.001);
        assertEquals(3, callCount);

        animation.setLoop(false);
        animation.start(6000);
        assertEquals(0.0, progress, 0.001);
        assertEquals(3, callCount);

        animation.update(6500);
        assertEquals(0.5, progress, 0.001);
        assertEquals(3, callCount);

        animation.stopAndComplete();
        assertEquals(1.0, progress, 0.001);
        assertEquals(4, callCount);
    }

    @Test
    public void nonLinear() {
        Animation animation = new Animation(new DirectAnimator(),
                AnimationAlpha.EASE_OUT_QUAD, 1000);

        animation.start(0);
        assertEquals(0.0, progress, 0.001);

        animation.update(100);
        assertEquals(AnimationAlpha.EASE_OUT_QUAD.ease(0.1), progress, 0.001);

        animation.update(500);
        assertEquals(AnimationAlpha.EASE_OUT_QUAD.ease(0.5), progress, 0.001);

        animation.update(850);
        assertEquals(AnimationAlpha.EASE_OUT_QUAD.ease(0.85), progress, 0.001);

        animation.update(1000);
        assertEquals(1.0, progress, 0.001);
    }
}
