package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.util.Map;

class EmptyClientReceiver implements ClientReceiver {

    public void setup(RuleSet rules, GameBoard board, PlayerIdentifier self) {
    }

    public void moveAcknowledged(Exception result) {
    }

    public void forceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) {
    }

    public void advanceNextTurn(int turn, GamePhase phase) {
    }

    public void blackSheepMoved(SheepTileIdentifier destination) {
    }

    public void wolfMoved(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) {
    }

    public void lambsGrew(SheepTileIdentifier where, Sheep[] newAdultSheep) {
    }

    public void didExecuteMove(Move move) {
    }

    public void loginAnswer(boolean answer) {
    }

    public void ping() {
    }

    public void endGame(Map<PlayerIdentifier, Integer> scores) {
    }

    public void playerLeft(PlayerIdentifier who, boolean clean) {
    }

    public void playerJoined(PlayerIdentifier who) {
    }

    public void abandonComplete() {
    }

    public void connectionClosed() {
    }

}
