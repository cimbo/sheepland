package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;

import org.junit.Test;

public class ServerForceEndTurnTest extends ServerMoveTestBase {
    @Test
    public void playTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.INIT, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        // force an advance to the play state
        getEngine().getGameState().advancePlayState();
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        executeMove(new ForceEndTurnMove(players[0].getId(), false));

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());
    }

    @Test
    public void initTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.INIT, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        executeMove(new ForceEndTurnMove(players[0].getId(), false));

        // there is only one shepherd in our shepherd
        assertTrue(players[0].isPositioned());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.INIT, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        executeMove(new ForceEndTurnMove(players[1].getId(), false));
        assertTrue(players[1].isPositioned());

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(0, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }
}
