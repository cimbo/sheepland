package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.util.Random;

public abstract class ServerPlayTestBase extends ServerMoveTestBase {
    protected ServerPlayTestBase() {
        super();
    }

    protected ServerPlayTestBase(Random random) {
        super(random);
    }

    @Override
    public void setUp() throws InvalidIdentifierException,
            RuleViolationException {
        super.setUp();

        Player[] players = getEngine().getPlayingOrder();

        // between grain1 and land1
        RoadSquareIdentifier rsId1 = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 1 + 1);
        RoadSquare rs1 = getEngine().getRoadSquareFromId(rsId1);
        assertEquals(2, rs1.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd1 = getEngine().getShepherdFromId(players[0],
                shepherdId);
        getEngine().initiallyPlaceShepherd(players[0], shepherd1, rs1);
        getEngine().checkAndAdvanceNextTurn();

        // between water1 and water2
        RoadSquareIdentifier rsId2 = new RoadSquareIdentifier(2 * 3 + 1 + 1,
                2 * 3 + 2 + 1);
        RoadSquare rs2 = getEngine().getRoadSquareFromId(rsId2);
        assertEquals(5, rs2.getDiceValue());

        Shepherd shepherd2 = getEngine().getShepherdFromId(players[1],
                shepherdId);
        getEngine().initiallyPlaceShepherd(players[1], shepherd2, rs2);
        getEngine().checkAndAdvanceNextTurn();
    }
}
