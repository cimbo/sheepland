package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.ServerException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CommandsTest {
    private ArrayLineReader input;
    private FakeCLIUIManager ui;
    private RuleSet rules;
    private GameBoard board;
    private GameState state;
    private Player marco;
    private Player mario;

    @Before
    public void setUp() throws InvalidInputException {
        input = new ArrayLineReader();
        input.feed("marco", "polo");
        ui = new FakeCLIUIManager(input) {
            @Override
            protected void beginTurn() {
                clearOutput();
            }
        };
        ui.show();

        rules = new RuleSet();
        rules.seal();
        board = new GameBoard();
        marco = new Player("marco");
        mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        marco.setInitialCard(board.getInitialTerrainCard(TerrainType.LAND));
        board.setPlayingOrder(new Player[] { marco, mario });
        Sheep blackSheep = new Sheep(SheepType.BLACK_SHEEP);
        board.addSheep(blackSheep);
        board.getMap().moveBlackSheep(blackSheep,
                board.getMap().getSheepsburg());
        board.getMap().moveWolf(board.getMap().getSheepsburg());
        state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.prepareNextTurn();
        GameOptions.get().setOption("hints", "on");
    }

    @After
    public void tearDown() {
        if (ui.getCurrentGame() != null) {
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    ui.quit();
                }
            });
        }
    }

    @Test
    public void testBeShepherd() throws ServerException,
            InvalidIdentifierException {

        assertNull(ui.getChosenShepherd());
        input.feed("be shepherd 1", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marco.getShepherdFromId(new ShepherdIdentifier(0)),
                ui.getChosenShepherd());
        assertEquals("You are now shepherd 1.\n", ui.getOutput());
    }

    @Test
    public void testBeShepherdAlreadyChosen() throws ServerException,
            InvalidIdentifierException {
        state.advancePlayState();

        assertNull(ui.getChosenShepherd());
        input.feed("be shepherd 1", "be shepherd 2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marco.getShepherdFromId(new ShepherdIdentifier(0)),
                ui.getChosenShepherd());
        assertEquals("You are now shepherd 1.\n"
                + "You already chose which shepherd to be for this turn.\n",
                ui.getOutput());
    }

    @Test
    public void testBeShepherdUnknown() throws ServerException,
            InvalidIdentifierException {
        assertNull(ui.getChosenShepherd());
        input.feed("be shepherd 7", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "You try to be shepherd 7, but you fail. You probably don't know how to be him.\n",
                ui.getOutput());
    }

    @Test
    public void testBuyTerrainCard() throws ServerException,
            InvalidIdentifierException {
        Shepherd s = marco.getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(s);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        s.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(s);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("buy land", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(s, ui.getChosenShepherd());
        assertEquals("You successfully buy a land.\n", ui.getOutput());
    }

    @Test
    public void testBuyTerrainCardNoChoose() throws ServerException,
            InvalidIdentifierException, InvalidInputException {
        Shepherd s = marco.getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        s.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(s);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("buy land", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "As the farm owner, you would never engage with cards directly.\n"
                        + "[Use the be command to choose a shepherd before buying a card]\n",
                ui.getOutput());

        ui.clearOutput();
        GameOptions.get().setOption("hints", "off");

        input.feed("buy land", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "As the farm owner, you would never engage with cards directly.\n",
                ui.getOutput());
    }

    @Test
    public void testBuyTerrainCardFail() throws ServerException,
            InvalidIdentifierException {
        Shepherd s = marco.getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(s);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        s.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(s);
        state.advancePlayState();
        state.prepareNextTurn();

        // wrong terrain type
        input.feed("buy desert", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(s, ui.getChosenShepherd());
        assertTrue(ui.getOutput().startsWith("I'm afraid I can't allow that: "));
    }

    @Test
    public void testJump() throws ServerException, InvalidIdentifierException {
        Shepherd s = marco.getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(s);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));

        input.feed("jump between land/2 and sheepsburg", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(s, land2Sheepsburg.getCurrentShepherd());
        assertEquals("You successfully jump between land/2 and sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void testJumpPlay() throws ServerException,
            InvalidIdentifierException {
        Shepherd s = marco.getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(s);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        s.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(s);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("jump between land/2 and land/1", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(s, ui.getChosenShepherd());
        assertNull(land2Sheepsburg.getCurrentShepherd());
        assertEquals(s, land1land2.getCurrentShepherd());
        assertEquals("You successfully jump between land/2 and land/1.\n",
                ui.getOutput());
    }

    @Test
    public void testJumpReversed() throws ServerException,
            InvalidIdentifierException {
        Shepherd s = marco.getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(s);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));

        input.feed("jump between sheepsburg and land/2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(s, land2Sheepsburg.getCurrentShepherd());
        assertEquals("You successfully jump between land/2 and sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void testJumpNoChoose() throws ServerException,
            InvalidIdentifierException {
        marco.getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));

        input.feed("jump between sheepsburg and land/2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertNull(land2Sheepsburg.getCurrentShepherd());
        assertEquals(
                "As the farm owner, you have no intention of ever moving away from Sheepsburg.\n"
                        + "[Use the be command to choose a shepherd before jumping]\n",
                ui.getOutput());
    }

    @Test
    public void testKillSheep() throws ServerException,
            InvalidIdentifierException, InvalidInputException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.RAM);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("kill ram on land/2");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals("You attempt to kill a ram on land/2.\n", ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        ui.chooseShepherd(null);

        input.feed("kill ram on land/2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "A businessman like you would never touch a sheep directly!\n"
                        + "[Use the be command to choose a shepherd before killing]\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        GameOptions.get().setOption("hints", "off");

        input.feed("kill ram on land/2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "A businessman like you would never touch a sheep directly!\n",
                ui.getOutput());
    }

    @Test
    public void testLookInventory() throws ServerException,
            InvalidIdentifierException {
        input.feed("look inventory", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals("Your bag contains:\n" + "- 1 cards of land\n"
                + "Your wallet contains 20 sheepcoins.\n", ui.getOutput());
    }

    @Test
    public void testLookInventoryEmptyWallet() throws ServerException,
            InvalidIdentifierException {
        marco.payCoins(marco.getRemainingCoins());

        input.feed("look inventory", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals("Your bag contains:\n" + "- 1 cards of land\n"
                + "Your wallet is empty.\n", ui.getOutput());
    }

    @Test
    public void testLookGeneric() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "You are shepherd 1, and you are between land/2 and sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void testLookGenericNoChoose() throws ServerException,
            InvalidIdentifierException, InvalidInputException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals("You are a proud farm owner in the land of Sheepland.\n"
                + "[Use the be command to switch to a shepherd]\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        GameOptions.get().setOption("hints", "off");

        input.feed("look", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals("You are a proud farm owner in the land of Sheepland.\n",
                ui.getOutput());
    }

    @Test
    public void testLookShepherd() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look shepherd", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "You are shepherd 1, and you are between land/2 and sheepsburg.\n"
                        + "The road you are standing on appears to be a 5.\n"
                        + "On your left, you see sheepsburg.\n"
                        + "It contains 1 black sheep, a wolf.\n"
                        + "On your right, you see land/2.\n"
                        + "It contains 1 female sheep.\n", ui.getOutput());
    }

    @Test
    public void testLookShepherdNoChoose() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look shepherd", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(
                "You see your shepherd 1 between land/2 and sheepsburg.\n",
                ui.getOutput());
    }

    @Test
    public void testLookShepherdNoChooseNoPosition() throws ServerException,
            InvalidIdentifierException {
        input.feed("look shepherd", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(
                "All your shepherds are floating, there is nothing to look at.\n",
                ui.getOutput());
    }

    @Test
    public void testLookShepherdNoPosition() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        input.feed("look shepherd", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(
                "You are shepherd 1, and you are currently floating in the air waiting to reach the map.\n"
                        + "[Use the jump command to place your shepherd]\n",
                ui.getOutput());
    }

    @Test
    public void testLookOpponents() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look opponents", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals("You see enemy mario in the distance.\n"
                + "His shepherd 1 is between land/2 and land/1.\n"
                + "Using your telepathic powers, you see his bag is empty.\n"
                + "His wallet contains 20 sheepcoins.\n", ui.getOutput());
    }

    @Test
    public void testLookOpponentsNonEmptyBag() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        mario.acquireCard(card);
        card.markAcquired(mario);
        board.markCardAcquired(card);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look opponents", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals("You see enemy mario in the distance.\n"
                + "His shepherd 1 is between land/2 and land/1.\n"
                + "Using your telepathic powers, you see in his bag he has:\n"
                + "- 1 cards of water\n"
                + "His wallet contains 20 sheepcoins.\n", ui.getOutput());
    }

    @Test
    public void testLookOpponentsEmptyWallet() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        mario.payCoins(mario.getRemainingCoins());
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look opponents", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals("You see enemy mario in the distance.\n"
                + "His shepherd 1 is between land/2 and land/1.\n"
                + "Using your telepathic powers, you see his bag is empty.\n"
                + "His wallet is empty.\n", ui.getOutput());
    }

    @Test
    public void testLookEmporium() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        mario.acquireCard(card);
        card.markAcquired(mario);
        board.markCardAcquired(card);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look emporium", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals("At the emporium, you find these items for sale:\n"
                + "- a card of land, for free\n"
                + "- a card of forest, for free\n"
                + "- a card of water, for 1 sheepcoins\n"
                + "- a card of grain, for free\n"
                + "- a card of mountain, for free\n"
                + "- a card of desert, for free\n"
                + "- a stack of 20 fences, for free\n", ui.getOutput());
    }

    @Test
    public void testLookMap() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.RAM);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look map", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());

        String output = ui.getOutput();

        String[] lines = output.trim().split("\n");
        assertEquals(19 + 2, lines.length);

        for (String line : lines) {
            if (line.startsWith("On land/2")) {
                assertEquals("On land/2 you see 1 ram.", line);
            } else if (line.startsWith("On sheepsburg")) {
                assertEquals("On sheepsburg you see 1 black sheep, a wolf.",
                        line);
            } else if (line.startsWith("Your")) {
                assertEquals(
                        "Your shepherd 1 is sleeping between land/2 and sheepsburg.",
                        line);
            } else if (line.startsWith("Enemy")) {
                assertEquals(
                        "Enemy shepherd 1, working for mario, is sleeping between land/2 and land/1.",
                        line);
            } else {
                assertTrue(line.endsWith("is empty."));
            }
        }
    }

    @Test
    public void testLookMarketPlaying() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        marco.acquireCard(card);
        card.markAcquired(marco);
        board.markCardAcquired(card);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("look market", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "You are not at the market now, so you cannot look inside.\n",
                ui.getOutput());
    }

    @Test
    public void testLookMarket() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        mario.acquireCard(card);
        card.markAcquired(mario);
        board.markCardAcquired(card);
        card.markForSale(2);
        board.markCardForSale(card);
        state.advancePlayState();
        state.advanceMarketSellState();
        state.advanceMarketBuyState();
        state.prepareNextTurn();

        input.feed("look market", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals("You are not selling anything at the market.\n"
                + "Enemy mario is selling:\n"
                + "- a card of water for 2 sheepcoins\n", ui.getOutput());
    }

    @Test
    public void testLookMarketSelling() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        marco.acquireCard(card);
        card.markAcquired(marco);
        board.markCardAcquired(card);
        card.markForSale(2);
        board.markCardForSale(card);
        state.advancePlayState();
        state.advanceMarketSellState();
        state.advanceMarketBuyState();
        state.prepareNextTurn();

        input.feed("look market", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals("You are selling:\n"
                + "- a card of water for 2 sheepcoins\n", ui.getOutput());
    }

    @Test
    public void testMateSheep() throws ServerException,
            InvalidIdentifierException, InvalidInputException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        Sheep ram = new Sheep(SheepType.RAM);
        board.addSheep(ram);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        land2.addSheep(ram);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("mate sheep on land/2");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "You attempt to mate a female sheep and a ram on land/2.\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        ui.chooseShepherd(null);

        input.feed("mate sheep on land/2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "A businessman like you would never touch a sheep directly (let alone for such disgusting practices!)\n"
                        + "[Use the be command to choose a shepherd before mating]\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        GameOptions.get().setOption("hints", "off");

        input.feed("mate sheep on land/2", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "A businessman like you would never touch a sheep directly (let alone for such disgusting practices!)\n",
                ui.getOutput());
    }

    @Test
    public void testMoveSheep() throws ServerException,
            InvalidIdentifierException, InvalidInputException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        SheepTile land2 = board.getMap().getSheepTileFromId(
                new SheepTileIdentifier(1 + 3 * 0 + 2));
        land2.addSheep(sheep);
        state.advancePlayState();
        state.prepareNextTurn();

        input.feed("move female sheep from land/2 to sheepsburg");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "You successfully move a female sheep from land/2 to sheepsburg.\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        ui.chooseShepherd(null);

        input.feed("move female sheep from land/2 to sheepsburg", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "A businessman like you would never touch a sheep directly!\n"
                        + "[Use the be command to choose a shepherd before moving]\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        GameOptions.get().setOption("hints", "off");

        input.feed("move female sheep from land/2 to sheepsburg", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertNull(ui.getChosenShepherd());
        assertEquals(
                "A businessman like you would never touch a sheep directly!\n",
                ui.getOutput());
    }

    @Test
    public void testSellMarket() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        marco.acquireCard(card);
        card.markAcquired(marco);
        board.markCardAcquired(card);
        state.advancePlayState();
        state.advanceMarketSellState();
        state.prepareNextTurn();

        input.feed("sell water for 3", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "Your water is now for sale at the market, for only 3 coins!\n",
                ui.getOutput());
    }

    @Test
    public void testSellMarketNoCard() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        marco.acquireCard(card);
        card.markAcquired(marco);
        board.markCardAcquired(card);
        state.advancePlayState();
        state.advanceMarketSellState();
        state.prepareNextTurn();

        input.feed("sell grain for 3", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals("It seems that you don't have a card of this type.\n",
                ui.getOutput());
    }

    @Test
    public void testSellMarketInitialCard() throws ServerException,
            InvalidIdentifierException, InvalidInputException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        marco.acquireCard(card);
        card.markAcquired(marco);
        board.markCardAcquired(card);
        state.advancePlayState();
        state.advanceMarketSellState();
        state.prepareNextTurn();

        input.feed("sell land for 3", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals(
                "It seems that you don't have a card of this type.\n"
                        + "[The card you see in your inventory is the initial card. You cannot sell that]\n",
                ui.getOutput());

        state.prepareNextTurn();
        ui.clearOutput();
        GameOptions.get().setOption("hints", "off");

        input.feed("sell land for 3", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals("It seems that you don't have a card of this type.\n",
                ui.getOutput());
    }

    @Test
    public void testBuyMarket() throws ServerException,
            InvalidIdentifierException {
        Shepherd marcoShepherd = marco
                .getShepherdFromId(new ShepherdIdentifier(0));
        Shepherd marioShepherd = mario
                .getShepherdFromId(new ShepherdIdentifier(0));
        ui.chooseShepherd(marcoShepherd);
        RoadSquare land2Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 3 * 0 + 2 + 1));
        RoadSquare land1land2 = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(3 * 0 + 1 + 1, 3 * 0 + 2 + 1));
        marcoShepherd.setCurrentPosition(land2Sheepsburg);
        land2Sheepsburg.addShepherd(marcoShepherd);
        marioShepherd.setCurrentPosition(land1land2);
        land1land2.addShepherd(marioShepherd);
        TerrainCard card = board
                .getCheapestTerrainCard(TerrainType.WATER, null);
        mario.acquireCard(card);
        card.markAcquired(mario);
        board.markCardAcquired(card);
        card.markForSale(2);
        board.markCardForSale(card);
        state.advancePlayState();
        state.advanceMarketSellState();
        state.advanceMarketBuyState();
        state.prepareNextTurn();

        input.feed("buy water from mario", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        assertEquals(marcoShepherd, ui.getChosenShepherd());
        assertEquals("You successfully buy a water from the market.\n",
                ui.getOutput());
    }

    @Test
    public void setOptions() throws ServerException, InvalidIdentifierException {
        input.feed("set hints off", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(false, GameOptions.get().getShowHints());

        input.feed("set hints on", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(true, GameOptions.get().getShowHints());

        input.feed("set hints false", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(false, GameOptions.get().getShowHints());

        input.feed("set hints true", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(true, GameOptions.get().getShowHints());

        input.feed("set hints 0", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(false, GameOptions.get().getShowHints());

        input.feed("set hints 1", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(true, GameOptions.get().getShowHints());

        input.feed("set debug on", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(true, GameOptions.get().getDebug());

        input.feed("set debug off", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(false, GameOptions.get().getDebug());
    }

    @Test
    public void helpTest() throws ServerException, InvalidInputException {
        input.feed("help", "end");
        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);

        String help = ui.getOutput();
        List<BaseCommand> commands = new ArrayList<BaseCommand>();

        for (String helpLine : help.split("\n")) {
            if (helpLine.startsWith("[") || helpLine.startsWith("Available")
                    || helpLine.trim().isEmpty()) {
                continue;
            }

            String cmd = helpLine.split("-")[0].trim();
            input.feed("help " + cmd);

            BaseCommand command = BaseCommand.getCommand(cmd);
            if (command == null) {
                throw new InvalidInputException(cmd);
            }
            commands.add(command);
        }
        input.feed("end");

        FakeCLIUIManager fakeCli = new FakeCLIUIManager(new ArrayLineReader());
        for (BaseCommand command : commands) {
            command.printHelp(fakeCli);
        }

        ui.getCurrentGame()
                .forceSynchronize(rules, board, marco.getId(), state);
        assertEquals(fakeCli.getOutput(), ui.getOutput());
    }
}
