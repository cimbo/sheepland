package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InvalidServerProtocolTest {
    private ServerSocket listenSocket;
    private ClientConnection client;
    private Socket server;
    private OutputStream serverOut;
    private ObjectOutputStream serverObjectOut;

    @Before
    public void setUp() throws IOException {
        assertNull(server);
        assertNull(listenSocket);
        listenSocket = new ServerSocket();
        listenSocket.setReuseAddress(true);
        listenSocket.bind(new InetSocketAddress("127.0.0.1",
                SocketServerNetwork.DEFAULT_PORT));

        new Thread(new Runnable() {
            public void run() {
                try {
                    Socket s;
                    while (true) {
                        s = listenSocket.accept();
                        synchronized (InvalidServerProtocolTest.this) {
                            assertNull(server);
                            server = s;
                            serverOut = s.getOutputStream();
                            InvalidServerProtocolTest.this.notify();
                        }
                    }
                } catch (IOException e) {
                }
            }
        }).start();

        client = new SocketClientNetwork().connectToServer();
        client.setClientReceiver(new ClientReceiver() {
            public void setup(RuleSet rules, GameBoard board,
                    PlayerIdentifier self) {
                fail();
            }

            public void moveAcknowledged(Exception result) {
                fail();
            }

            public void forceSynchronize(RuleSet rules, GameBoard newBoard,
                    PlayerIdentifier self, GameState newState) {
                fail();
            }

            public void advanceNextTurn(int turn, GamePhase phase) {
                fail();
            }

            public void blackSheepMoved(SheepTileIdentifier destination) {
                fail();
            }

            public void wolfMoved(SheepTileIdentifier destination,
                    SheepIdentifier eatenSheep) {
                fail();
            }

            public void lambsGrew(SheepTileIdentifier where,
                    Sheep[] newAdultSheep) {
                fail();
            }

            public void didExecuteMove(Move move) {
                fail();
            }

            public void loginAnswer(boolean answer) {
                fail();
            }

            public void ping() {
                fail();
            }

            public void endGame(Map<PlayerIdentifier, Integer> scores) {
                fail();
            }

            public void playerLeft(PlayerIdentifier who, boolean clean) {
                fail();
            }

            public void playerJoined(PlayerIdentifier who) {
                fail();
            }

            public void abandonComplete() {
                fail();
            }

            public void connectionClosed() {
                synchronized (InvalidServerProtocolTest.this) {
                    client = null;
                    InvalidServerProtocolTest.this.notify();
                }
            }

        });
        waitConnection();
    }

    @After
    public void tearDown() throws IOException, InterruptedException {
        if (server != null && !server.isClosed()) {
            server.close();
        }
        server = null;
        listenSocket.close();
        listenSocket = null;

        // HACK: make sure all connections are taken down
        Thread.sleep(10);
    }

    private synchronized void waitConnection() {
        while (server == null) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    private synchronized void waitClosed() {
        while (client != null) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    @Test(timeout = 5000)
    public void missingOOSHeader() throws IOException {
        byte[] badHeader = new byte[50];
        Arrays.fill(badHeader, (byte) 42);

        serverOut.write(badHeader);
        waitClosed();
    }

    @Test(timeout = 5000)
    public void invalidVersion() throws IOException {
        serverObjectOut = new ObjectOutputStream(serverOut);

        serverObjectOut.writeByte(Protocol.V1.getVersion() + 1);
        serverObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void badFlags() throws IOException {
        serverObjectOut = new ObjectOutputStream(serverOut);

        serverObjectOut.writeByte(Protocol.V1.getVersion());
        serverObjectOut.writeInt(7);
        serverObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void badOperationNegative() throws IOException {
        serverObjectOut = new ObjectOutputStream(serverOut);

        serverObjectOut.writeByte(Protocol.V1.getVersion());
        serverObjectOut.writeInt(0);
        serverObjectOut.writeInt(-1);
        serverObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void badOperationExceeding() throws IOException {
        serverObjectOut = new ObjectOutputStream(serverOut);

        serverObjectOut.writeByte(Protocol.V1.getVersion());
        serverObjectOut.writeInt(0);
        serverObjectOut.writeInt(ClientOperation.values().length);
        serverObjectOut.flush();
        waitClosed();
    }

    @Test(timeout = 5000)
    public void invalidArguments() throws IOException {
        serverObjectOut = new ObjectOutputStream(serverOut);

        serverObjectOut.writeByte(Protocol.V1.getVersion());
        serverObjectOut.writeInt(0);
        serverObjectOut.writeInt(ClientOperation.LOGIN_ANSWER.ordinal());
        serverObjectOut.writeObject("true");
        serverObjectOut.flush();
        waitClosed();
    }
}