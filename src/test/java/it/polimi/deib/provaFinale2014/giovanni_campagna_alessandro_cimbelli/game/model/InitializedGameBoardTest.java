package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;

import org.junit.Before;
import org.junit.Test;

public class InitializedGameBoardTest {
    private GameBoard board;
    private RuleSet rules;

    @Before
    public void setUp() {
        board = new GameBoard();
        board.addPlayer(new Player(""));
        board.addPlayer(new Player(""));
        rules = new RuleSet();
        board.initializeForRules(rules);
    }

    @Test
    public void map() {
        assertNotNull(board.getMap());
    }

    @Test
    public void initialTerrainCardTest() {
        TerrainType[] allTypes = TerrainType.values();

        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            assertEquals(rules.getBuyableCards(),
                    board.getNumberOfTerrainCards(allTypes[i]));
        }
    }

    @Test
    public void acquireCards() {
        for (int i = 0; i < rules.getBuyableCards(); i++) {
            TerrainCard card = board.getCheapestTerrainCard(TerrainType.LAND,
                    null);
            assertNotNull(card);
            board.markCardAcquired(card);
            assertEquals(rules.getBuyableCards() - (i + 1),
                    board.getNumberOfTerrainCards(TerrainType.LAND));
        }

        TerrainCard card = board.getCheapestTerrainCard(TerrainType.LAND, null);
        assertNull(card);

        assertEquals(rules.getBuyableCards(),
                board.getNumberOfTerrainCards(TerrainType.MOUNTAIN));

        card = board.getCheapestTerrainCard(TerrainType.MOUNTAIN, null);
        assertNotNull(card);
        board.markCardAcquired(card);
        assertEquals(rules.getBuyableCards() - 1,
                board.getNumberOfTerrainCards(TerrainType.MOUNTAIN));
    }

    @Test
    public void fenceTest() {
        assertEquals(rules.getNumberOfFences(), board.getRemainingFences());

        for (int i = 0; i < rules.getNumberOfFences(); i++) {
            board.markFencePlaced();
            assertEquals(rules.getNumberOfFences() - (i + 1),
                    board.getRemainingFences());
        }

        try {
            board.markFencePlaced();
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }
    }

    @Test
    public void finalFenceTest() {
        assertEquals(rules.getNumberOfFinalFences(),
                board.getRemainingFinalFences());

        for (int i = 0; i < rules.getNumberOfFinalFences(); i++) {
            board.markFinalFencePlaced();
            assertEquals(rules.getNumberOfFinalFences() - (i + 1),
                    board.getRemainingFinalFences());
        }

        try {
            board.markFinalFencePlaced();
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }
    }
}
