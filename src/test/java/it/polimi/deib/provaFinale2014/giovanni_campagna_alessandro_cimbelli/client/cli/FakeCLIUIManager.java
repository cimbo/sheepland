package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.FakeClientNetwork;

public class FakeCLIUIManager extends CLIUIManager {
    private final StringBuilder output;

    public FakeCLIUIManager(LineReader in) {
        super(new FakeClientNetwork(), new CommandParser(in));
        output = new StringBuilder();
    }

    @Override
    public void showMessage(String message) {
        output.append(message);
        output.append("\n");
    }

    public void clearOutput() {
        output.setLength(0);
    }

    public String getOutput() {
        return output.toString();
    }
}
