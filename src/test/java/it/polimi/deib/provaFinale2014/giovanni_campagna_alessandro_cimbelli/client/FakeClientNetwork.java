package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientSender;

import java.io.IOException;

public class FakeClientNetwork implements ClientNetwork, ClientConnection,
        ClientSender {
    private ClientReceiver receiver;

    public ClientConnection connectToServer() throws IOException {
        return this;
    }

    public ClientSender getClientSender() {
        return this;
    }

    public ClientReceiver getClientReceiver() {
        return receiver;
    }

    public void setClientReceiver(ClientReceiver receiver) {
        this.receiver = receiver;
    }

    public void close() {
        receiver.connectionClosed();
    }

    public void sendAbandonGame() throws IOException {
        receiver.abandonComplete();
    }

    public void sendForceSynchronizeMe() throws IOException {
    }

    public void sendExecuteMove(Move move) throws IOException {
    }

    public void sendLoginToServer(String username, String password)
            throws IOException {
    }

    public void sendPong() throws IOException {
    }
}
