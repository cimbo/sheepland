package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Before;
import org.junit.Test;

public class GameStateTest {
    public GameState state;

    @Before
    public void setUp() {
        state = new GameState();
        state.setRules(new RuleSet());
    }

    @Test
    public void hashability() {
        GameState s2 = new GameState();
        assertEquals(state, s2);
        assertEquals(state.hashCode(), s2.hashCode());
    }

    private GameState createPlayGameState() {
        GameState s = new GameState();
        s.setRules(new RuleSet());
        s.advanceInitState();
        s.advancePlayState();
        s.prepareNextTurn();
        return s;
    }

    @Test
    public void equality() {
        assertEquals(state, new GameState());
        assertNotEquals(state, null);
        assertNotEquals(state, new Object());

        GameState s1 = createPlayGameState();
        GameState s2 = createPlayGameState();
        s1.recordMove(null, MoveType.MOVE_SHEPHERD);
        s2.recordMove(null, MoveType.MOVE_SHEEP);
        assertNotEquals(s1, s2);

        s1 = createPlayGameState();
        s2 = createPlayGameState();
        s1.recordMove(null, MoveType.MOVE_SHEPHERD);
        s2.recordMove(null, MoveType.MOVE_SHEPHERD);
        assertEquals(s1, s2);
        assertNotEquals(state, s2);

        s1 = new GameState();
        s2 = new GameState();
        s1.advanceInitState();
        s2.advanceInitState();
        assertEquals(s1, s2);
        assertNotEquals(state, s2);

        s1 = new GameState();
        s2 = new GameState();
        s1.setCurrentTurn(1);
        s2.setCurrentTurn(1);
        assertEquals(s1, s2);
        assertNotEquals(state, s2);

        s1 = createPlayGameState();
        s1.recordMove(null, MoveType.MOVE_SHEEP);
        s2 = createPlayGameState();
        s2.recordMove(null, MoveType.MOVE_SHEPHERD);
        assertNotEquals(s1, s2);
        s1.recordMove(null, MoveType.MOVE_SHEPHERD);
        assertNotEquals(s1, s2);
        s2.recordMove(null, MoveType.MOVE_SHEEP);
        assertNotEquals(s1, s2);
        s1.recordMove(null, MoveType.BUY_TERRAIN);
        assertNotEquals(s1, s2);
        s2.recordMove(null, MoveType.BUY_TERRAIN);
        assertEquals(s1, s2);

        Shepherd shepherd1 = new Shepherd(new Player(""));
        Shepherd shepherd2 = new Shepherd(shepherd1.getOwner());
        shepherd1.setId(new ShepherdIdentifier(0));
        shepherd2.setId(new ShepherdIdentifier(1));

        s1 = createPlayGameState();
        s1.recordMove(shepherd1, MoveType.MOVE_SHEPHERD);
        s2 = createPlayGameState();
        s2.recordMove(null, MoveType.MOVE_SHEPHERD);
        assertNotEquals(s1, s2);

        s1 = createPlayGameState();
        s1.recordMove(null, MoveType.MOVE_SHEPHERD);
        s2 = createPlayGameState();
        s2.recordMove(shepherd1, MoveType.MOVE_SHEPHERD);
        assertNotEquals(s1, s2);

        s1 = createPlayGameState();
        s1.recordMove(shepherd1, MoveType.MOVE_SHEPHERD);
        s2 = createPlayGameState();
        s2.recordMove(shepherd2, MoveType.MOVE_SHEPHERD);
        assertNotEquals(s1, s2);
    }

    @Test
    public void goodTransitions() {
        state.advanceInitState();
        state.advancePlayState();
        state.advanceMarketSellState();
        state.advanceMarketBuyState();
        state.advanceFinalState();
        state.advanceMarketSellState();
        state.advanceMarketBuyState();
        state.advanceEndState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionInit() {
        state.advanceInitState();
        state.advancePlayState();
        state.advanceInitState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionPlay() {
        state.advancePlayState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionMarketSell() {
        state.advanceMarketSellState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionMarketBuy() {
        state.advanceMarketBuyState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionFinal() {
        state.advanceFinalState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionEnd() {
        state.advanceEndState();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionExplicitPreinit() {
        state.advanceInitState();
        state.setCurrentPhase(GamePhase.PREINIT);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionExplicitInit() {
        state.advanceInitState();
        state.advancePlayState();
        state.setCurrentPhase(GamePhase.INIT);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void badTransitionExplicitEnd() {
        state.setCurrentPhase(GamePhase.END);
    }

    @Test(expected = IllegalArgumentException.class)
    public void badTurn() {
        state.setCurrentTurn(-1);
    }

    @Test(expected = RuleViolationException.class)
    public void badMoveNoMoreMoves() throws RuleViolationException {
        state.checkBeforeMove(MoveType.MOVE_SHEPHERD);
    }

    @Test(expected = RuleViolationException.class)
    public void badMoveMustMoveShepherd() throws RuleViolationException {
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();

        Shepherd s = new Shepherd(new Player(""));
        s.setId(new ShepherdIdentifier(0));
        state.recordMove(s, MoveType.MOVE_SHEEP);
        state.recordMove(s, MoveType.BUY_TERRAIN);
        state.checkBeforeMove(s, MoveType.MOVE_SHEEP);
    }

    @Test(expected = RuleViolationException.class)
    public void badMoveNoDoubles() throws RuleViolationException {
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();

        Shepherd s = new Shepherd(new Player(""));
        s.setId(new ShepherdIdentifier(0));
        state.recordMove(s, MoveType.MOVE_SHEEP);
        state.checkBeforeMove(s, MoveType.MOVE_SHEEP);
    }

    @Test(expected = RuleViolationException.class)
    public void badMoveChangedShepherd() throws RuleViolationException {
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();

        Shepherd s1 = new Shepherd(new Player(""));
        s1.setId(new ShepherdIdentifier(0));
        Shepherd s2 = new Shepherd(s1.getOwner());
        s2.setId(new ShepherdIdentifier(1));
        state.recordMove(s1, MoveType.MOVE_SHEEP);
        state.checkBeforeMove(s2, MoveType.MOVE_SHEPHERD);
    }
}
