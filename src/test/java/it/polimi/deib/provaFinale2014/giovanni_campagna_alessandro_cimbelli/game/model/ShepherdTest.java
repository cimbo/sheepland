package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;

import org.junit.Before;
import org.junit.Test;

public class ShepherdTest {
    private Shepherd shepherd;
    private RoadSquare square;
    private Player player;

    @Before
    public void setUp() {
        player = new Player("");
        shepherd = new Shepherd(player);
        square = new RoadSquare(4);
    }

    @Test
    public void position() {
        assertFalse(shepherd.isPositioned());
        try {
            shepherd.getCurrentPosition();
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }

        shepherd.setCurrentPosition(square);
        assertTrue(shepherd.isPositioned());
        assertEquals(square, shepherd.getCurrentPosition());
    }

    @Test
    public void owner() {
        assertEquals(player, shepherd.getOwner());
    }
}
