package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;

import java.util.Random;

import org.junit.Before;

public abstract class ServerMoveTestBase {
    private ServerGameEngine engine;
    private final Random random;

    protected ServerMoveTestBase() {
        random = new Random(42);
    }

    protected ServerMoveTestBase(Random random) {
        this.random = random;
    }

    @Before
    public void setUp() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();

        rules.seal();
        engine = new ServerGameEngine(random, new EmptyServerDelegate(), rules);

        int n = getNumberOfPlayers();
        for (int i = 0; i < n; i++) {
            engine.addPlayer(new Player(""));
        }

        engine.start();
    }

    protected ServerGameEngine getEngine() {
        return engine;
    }

    protected int getNumberOfPlayers() {
        // slightly wrong, because we play with the rules for 3+
        // players
        return 2;
    }

    protected void executeMove(Move move) throws InvalidMoveException {
        move.execute(engine);
        if (move instanceof CompletableMove) {
            ((CompletableMove) move).computeResult(engine);
        }
    }
}
