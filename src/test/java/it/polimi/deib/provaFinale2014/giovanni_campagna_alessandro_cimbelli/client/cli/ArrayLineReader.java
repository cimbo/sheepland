package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayLineReader implements LineReader {
    private final ArrayList<String> lines;
    private int counter;

    public ArrayLineReader() {
        lines = new ArrayList<String>();
        counter = 0;
    }

    public void feed(String line) {
        lines.add(line);
    }

    public void feed(String... lines) {
        this.lines.addAll(Arrays.asList(lines));
    }

    public String nextLine(String prompt) {
        return lines.get(counter++);
    }
}
