package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class ServerMoveShepherdTest extends ServerPlayTestBase {
    @Test
    public void correctTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // move to between grain1 and land2 (adjacent)
        RoadSquareIdentifier nextRsId = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 2 + 1);
        RoadSquare nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(1, nextRs.getDiceValue());
        assertTrue(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        int coinsBefore = players[0].getRemainingCoins();
        RoadSquare rsBefore = shepherd.getCurrentPosition();
        int fencesBefore = getEngine().getGameBoard().getRemainingFences();
        int finalFencesBefore = getEngine().getGameBoard()
                .getRemainingFinalFences();

        executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                nextRsId));

        assertEquals(nextRs, shepherd.getCurrentPosition());
        assertEquals(coinsBefore, players[0].getRemainingCoins());
        assertNull(rsBefore.getCurrentShepherd());
        assertTrue(rsBefore.getHasFence());
        assertEquals(shepherd, nextRs.getCurrentShepherd());
        assertEquals(fencesBefore - 1, getEngine().getGameBoard()
                .getRemainingFences());
        assertEquals(finalFencesBefore, getEngine().getGameBoard()
                .getRemainingFinalFences());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        // move to between desert1 and desert0 (not adjacent)
        rsBefore = nextRs;
        nextRsId = new RoadSquareIdentifier(5 * 3 + 1 + 1, 5 * 3 + 0 + 1);
        nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(6, nextRs.getDiceValue());
        assertFalse(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                nextRsId));

        assertEquals(nextRs, shepherd.getCurrentPosition());
        assertEquals(coinsBefore - 1, players[0].getRemainingCoins());
        assertNull(rsBefore.getCurrentShepherd());
        assertTrue(rsBefore.getHasFence());
        assertEquals(shepherd, nextRs.getCurrentShepherd());
        assertEquals(fencesBefore - 2, getEngine().getGameBoard()
                .getRemainingFences());
        assertEquals(finalFencesBefore, getEngine().getGameBoard()
                .getRemainingFinalFences());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        // move to between water0 and desert0 (adjacent)
        rsBefore = nextRs;
        nextRsId = new RoadSquareIdentifier(5 * 3 + 0 + 1, 2 * 3 + 0 + 1);
        nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(1, nextRs.getDiceValue());
        assertTrue(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                nextRsId));

        assertEquals(nextRs, shepherd.getCurrentPosition());
        assertEquals(coinsBefore - 1, players[0].getRemainingCoins());
        assertNull(rsBefore.getCurrentShepherd());
        assertTrue(rsBefore.getHasFence());
        assertEquals(shepherd, nextRs.getCurrentShepherd());
        assertEquals(fencesBefore - 3, getEngine().getGameBoard()
                .getRemainingFences());
        assertEquals(finalFencesBefore, getEngine().getGameBoard()
                .getRemainingFinalFences());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());
    }

    @Test(expected = RuleViolationException.class)
    public void outOfOrderTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[1],
                shepherdId);

        // we're between water1 and water2 (see ServerPlayTestBase)
        // move to between grain1 and land2 (not adjacent)
        RoadSquareIdentifier nextRsId = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 2 + 1);
        RoadSquare nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(1, nextRs.getDiceValue());
        assertFalse(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        executeMove(new MoveShepherdMove(players[1].getId(), shepherdId,
                nextRsId));
    }

    @Test
    public void alreadyOccupiedSquare() throws InvalidMoveException {
        correctTest();

        Player[] players = getEngine().getPlayingOrder();

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[1],
                shepherdId);

        // we're between water1 and water2 (see ServerPlayTestBase)
        // move to between water0 and desert0 (not adjacent and occupied)
        RoadSquareIdentifier nextRsId = new RoadSquareIdentifier(2 * 3 + 0 + 1,
                5 * 3 + 0 + 1);
        RoadSquare nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(1, nextRs.getDiceValue());
        assertFalse(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        try {
            executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                    nextRsId));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }

    @Test
    public void fencedSquare() throws InvalidMoveException {
        correctTest();

        Player[] players = getEngine().getPlayingOrder();

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[1],
                shepherdId);

        // we're between water1 and water2 (see ServerPlayTestBase)
        // move to between grain1 and land1 (not adjacent and fenced)
        RoadSquareIdentifier nextRsId = new RoadSquareIdentifier(0 * 3 + 1 + 1,
                3 * 3 + 1 + 1);
        RoadSquare nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(2, nextRs.getDiceValue());
        assertFalse(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        try {
            executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                    nextRsId));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }

    @Test
    public void moveToSame() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        try {
            executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                    shepherd.getCurrentPosition().getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }

    @Test
    public void noCoinsLeft() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();
        players[0].payCoins(players[0].getRemainingCoins());

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // move to between grain0 and grain2 (not adjacent)
        RoadSquareIdentifier nextRsId = new RoadSquareIdentifier(3 * 3 + 0 + 1,
                3 * 3 + 2 + 1);
        RoadSquare nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(3, nextRs.getDiceValue());
        assertFalse(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        try {
            executeMove(new MoveShepherdMove(players[0].getId(), shepherdId,
                    nextRsId));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }
}
