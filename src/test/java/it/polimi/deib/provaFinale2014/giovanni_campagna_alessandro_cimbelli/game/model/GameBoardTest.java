package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;

import org.junit.Before;
import org.junit.Test;

public class GameBoardTest {
    private GameBoard board;

    @Before
    public void setUp() {
        board = new GameBoard();
    }

    @Test
    public void addingPlayersTest() {
        Player p1 = new Player("");
        board.addPlayer(p1);

        Player p2 = new Player("");
        board.addPlayer(p2);

        try {
            board.getAllPlayers();
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }

        board.initializeForRules(new RuleSet());

        Player[] list = board.getAllPlayers();
        assertEquals(2, list.length);
        assertNotNull(list[0].getId());
        assertNotNull(list[1].getId());
        if (p1 == list[0]) {
            assertSame(p2, list[1]);
        } else {
            assertSame(p2, list[0]);
            assertSame(p1, list[1]);
        }

        assertEquals(2, board.getNumberOfPlayers());
    }

    @Test
    public void initializeRules() {
        board.initializeForRules(new RuleSet());
    }

    @Test(expected = GameInvariantViolationException.class)
    public void addingPlayersAfterRules() {
        board.initializeForRules(new RuleSet());
        board.addPlayer(new Player(""));
    }

    @Test
    public void sheep() {
        Sheep s1 = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(s1);
        assertNotNull(s1.getId());

        assertNull(board.getBlackSheep());
        Sheep s2 = new Sheep(SheepType.BLACK_SHEEP);
        board.addSheep(s2);
        assertNotNull(s2.getId());
        assertEquals(s2, board.getBlackSheep());
    }

    @Test(expected = GameInvariantViolationException.class)
    public void doubleBlackSheep() {
        Sheep s1 = new Sheep(SheepType.BLACK_SHEEP);
        board.addSheep(s1);

        Sheep s2 = new Sheep(SheepType.BLACK_SHEEP);
        board.addSheep(s2);
    }

    @Test
    public void terrainCards() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        board.initializeForRules(rules);

        assertTrue(board.terrainCardStorageOk());

        TerrainType[] allTypes = TerrainType.values();
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            TerrainCard initialCard = board.getInitialTerrainCard(allTypes[i]);

            assertFalse(initialCard.isForSale());
            assertEquals(initialCard,
                    board.getTerrainCardFromId(initialCard.getId()));

            TerrainCard cheapestCard = board.getCheapestTerrainCard(
                    allTypes[i], null);

            assertTrue(cheapestCard.isForSale());
            assertEquals(0, cheapestCard.getCost());
            assertEquals(cheapestCard,
                    board.getTerrainCardFromId(cheapestCard.getId()));

        }
    }

    @Test
    public void terrainCardsOnSale() {
        RuleSet rules = new RuleSet();
        board.initializeForRules(rules);

        TerrainCard cheapestCard = board.getCheapestTerrainCard(
                TerrainType.LAND, null);

        board.markCardAcquired(cheapestCard);

        TerrainCard secondCheapest = board.getCheapestTerrainCard(
                TerrainType.LAND, null);

        assertTrue(secondCheapest.isForSale());
        assertEquals(1, secondCheapest.getCost());

        assertEquals(0, board.getTerrainCardsOnSale(new Player("")).length);
        assertEquals(0, board.getTerrainCardsOnSale(TerrainType.LAND).length);
    }

    @Test(expected = GameInvariantViolationException.class)
    public void invalidAcquire() {
        board.markCardAcquired(new TerrainCard(TerrainType.LAND));
    }

    @Test(expected = GameInvariantViolationException.class)
    public void invalidMarkForSale() {
        board.markCardForSale(new TerrainCard(TerrainType.LAND));
    }

    @Test
    public void terrainCardsMarket() {
        RuleSet rules = new RuleSet();
        board.initializeForRules(rules);

        Player player = new Player("");
        TerrainCard card = board.getCheapestTerrainCard(TerrainType.LAND, null);

        player.acquireCard(card);
        card.markAcquired(player);
        board.markCardAcquired(card);

        card.markForSale(2);
        board.markCardForSale(card);

        TerrainCard[] expected = { card };
        assertEquals(card,
                board.getCheapestTerrainCard(TerrainType.LAND, player));
        assertArrayEquals(expected, board.getTerrainCardsOnSale(player));
        assertArrayEquals(expected,
                board.getTerrainCardsOnSale(TerrainType.LAND));

        try {
            board.markCardForSale(card);
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }

        try {
            board.markCardForSale(board.getCheapestTerrainCard(
                    TerrainType.LAND, null));
            fail("code should not be reached");
        } catch (GameInvariantViolationException e) {
            ;
        }
    }

    @Test(expected = GameInvariantViolationException.class)
    public void cardsBeforeInit() {
        board.getInitialTerrainCard(TerrainType.LAND);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sheepsburgCard() {
        RuleSet rules = new RuleSet();
        board.initializeForRules(rules);
        board.getInitialTerrainCard(TerrainType.SHEEPSBURG);
    }
}
