package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

public class EmptyServerDelegate implements ServerDelegate {

    public void didMoveBlackSheep(SheepTileIdentifier destination) {
    }

    public void didMoveWolf(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) {
    }

    public void didGrowLambs(SheepTileIdentifier tile, Sheep[] newAdultSheep) {
    }

}
