package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerSender;

import java.io.IOException;
import java.util.Map;

class FakeServerConnection implements ServerConnection, ServerSender {
    private ServerReceiver receiver;

    public void setServerReceiver(ServerReceiver receiver) {
        this.receiver = receiver;
    }

    public ServerSender getServerSender() {
        return this;
    }

    public void close() {
        receiver.connectionClosed();
    }

    public void sendPing() throws IOException {
        receiver.doPong();
    }

    public void sendSetup(RuleSet rules, GameBoard board, PlayerIdentifier self)
            throws IOException {
    }

    public void sendMoveAcknowledged(Exception result) throws IOException {
    }

    public void sendForceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) throws IOException {
    }

    public void sendAdvanceNextTurn(int turn, GamePhase phase)
            throws IOException {
    }

    public void sendBlackSheepMoved(SheepTileIdentifier destination)
            throws IOException {
    }

    public void sendWolfMoved(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) throws IOException {
    }

    public void sendLambsGrew(SheepTileIdentifier where, Sheep[] newAdultSheep)
            throws IOException {
    }

    public void sendDidExecuteMove(Move move) throws IOException {
    }

    public void sendLoginAnswer(boolean answer) throws IOException {
    }

    public void sendEndGame(Map<PlayerIdentifier, Integer> scores)
            throws IOException {
    }

    public void sendPlayerLeft(PlayerIdentifier who, boolean clean)
            throws IOException {
    }

    public void sendPlayerJoined(PlayerIdentifier who) throws IOException {
    }

    public void sendAbandonComplete() throws IOException {
    }

}
