package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import org.junit.Test;

public class NetworkUtilTest {

    @Test
    public void portOnlyTest() throws NumberFormatException,
            UnknownHostException {
        InetSocketAddress address = NetworkUtil.parseHostPort("5000", true);

        assertTrue(address.getAddress().isAnyLocalAddress());
        assertEquals(5000, address.getPort());
    }

    @Test
    public void ipv6Test() throws NumberFormatException, UnknownHostException {
        InetSocketAddress address = NetworkUtil.parseHostPort(
                "[2001:0db8:0000:0000:0000:ff00:0042:8329]:5000", true);

        assertEquals(
                InetAddress
                        .getByName("2001:0db8:0000:0000:0000:ff00:0042:8329"),
                address.getAddress());
        assertEquals(5000, address.getPort());
    }

    @Test
    public void hostNameTest() throws NumberFormatException,
            UnknownHostException {
        InetSocketAddress address = NetworkUtil.parseHostPort("localhost:5000",
                true);

        assertEquals("127.0.0.1", address.getAddress().getHostAddress());
        assertEquals(5000, address.getPort());

        try {
            NetworkUtil.parseHostPort("example.com:8080", true);
        } catch (UnknownHostException e) {
            Logger.getGlobal()
                    .info("Skipping hostname test, resolution failed");
        }
    }

    @Test
    public void ipv4Test() throws NumberFormatException, UnknownHostException {
        InetSocketAddress address = NetworkUtil.parseHostPort("127.0.0.1:5000",
                true);

        assertEquals("127.0.0.1", address.getAddress().getHostAddress());
        assertEquals(5000, address.getPort());
    }

    @Test(expected = UnknownHostException.class)
    public void portTestNoAny() throws NumberFormatException,
            UnknownHostException {
        NetworkUtil.parseHostPort("5000", false);
    }

    @Test(expected = UnknownHostException.class)
    public void ipv4TestNoAny() throws NumberFormatException,
            UnknownHostException {
        NetworkUtil.parseHostPort("0.0.0.0:5000", false);
    }

    @Test(expected = UnknownHostException.class)
    public void ipv6TestNoAny() throws NumberFormatException,
            UnknownHostException {
        NetworkUtil.parseHostPort("[::]:5000", false);
    }

    @Test(expected = NumberFormatException.class)
    public void ipv4TestNoPort() throws NumberFormatException,
            UnknownHostException {
        NetworkUtil.parseHostPort("127.0.0.1", false);
    }

    @Test(expected = NumberFormatException.class)
    public void ipv6TestNoPort() throws NumberFormatException,
            UnknownHostException {
        NetworkUtil.parseHostPort("[::1]", false);
    }
}
