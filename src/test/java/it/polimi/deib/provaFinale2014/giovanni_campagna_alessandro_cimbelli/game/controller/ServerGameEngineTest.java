package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GraphVisitor;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class ServerGameEngineTest {
    private ServerGameEngine engine;

    @Before
    public void setUp() throws RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        engine = new ServerGameEngine(new Random(42),
                new EmptyServerDelegate(), rules);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unsealedRules() throws RuleViolationException {
        new ServerGameEngine(new EmptyServerDelegate(), new RuleSet());
    }

    @Test
    public void startTest() throws RuleViolationException {
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void emptyStart() {
        engine.start();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void doubleStart() {
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();
        engine.start();
    }

    @Test(expected = GameInvariantViolationException.class)
    public void addingPlayersAfterStartTest() {
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();
        engine.addPlayer(new Player("giovanni"));
    }

    @Test
    public void playingOrder() {
        Player p1 = new Player("marco");
        Player p2 = new Player("polo");

        engine.addPlayer(p1);
        engine.addPlayer(p2);

        assertNull(engine.getPlayingOrder());

        engine.start();

        Player[] playingOrder = engine.getPlayingOrder();
        assertNotNull(playingOrder);
        assertEquals(2, playingOrder.length);
        if (p1.equals(playingOrder[0])) {
            assertEquals(p2, playingOrder[1]);
        } else if (p1.equals(playingOrder[1])) {
            assertEquals(p2, playingOrder[0]);
        } else {
            fail("playing order is not a permutation");
        }
    }

    @Test
    public void initialPopulation() throws InvalidIdentifierException {
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();

        engine.getGameBoard().getMap().forEachTile(new GraphVisitor() {
            public void visit(SheepTile tile) {
                int whiteSum = 0;
                int blackSum = 0;

                for (Sheep s : tile.getAllSheep()) {
                    if (s.getType() == SheepType.BLACK_SHEEP) {
                        blackSum++;
                    } else {
                        whiteSum++;
                    }
                }

                if (tile.getTerrainType() == TerrainType.SHEEPSBURG) {
                    assertEquals(1, blackSum);
                    assertEquals(0, whiteSum);
                } else {
                    assertEquals(0, blackSum);
                    assertEquals(1, whiteSum);
                }
            }
        });
    }

    private void simulateOneRound(GameState state, GamePhase expectedPhase,
            GamePhase expectedPhaseAtEnd) {
        assertEquals(-1, engine.checkAndAdvanceNextTurn());
        state.forceEndTurn();
        assertEquals(1, engine.checkAndAdvanceNextTurn());
        assertEquals(1, state.getCurrentTurn());
        assertEquals(expectedPhase, state.getCurrentPhase());

        assertEquals(-1, engine.checkAndAdvanceNextTurn());
        state.forceEndTurn();
        assertEquals(0, engine.checkAndAdvanceNextTurn());
        assertEquals(0, state.getCurrentTurn());
        assertEquals(GamePhase.MARKET_SELL, state.getCurrentPhase());

        assertEquals(-1, engine.checkAndAdvanceNextTurn());
        state.forceEndTurn();
        assertEquals(1, engine.checkAndAdvanceNextTurn());
        assertEquals(1, state.getCurrentTurn());
        assertEquals(GamePhase.MARKET_SELL, state.getCurrentPhase());

        assertEquals(-1, engine.checkAndAdvanceNextTurn());
        state.forceEndTurn();
        int nextTurn = engine.checkAndAdvanceNextTurn();
        assertTrue(nextTurn == 0 || nextTurn == 1);
        assertEquals(nextTurn, state.getCurrentTurn());
        assertEquals(GamePhase.MARKET_BUY, state.getCurrentPhase());

        if (nextTurn == 0) {
            assertEquals(-1, engine.checkAndAdvanceNextTurn());
            state.forceEndTurn();
            assertEquals(1, engine.checkAndAdvanceNextTurn());
            assertEquals(1, state.getCurrentTurn());
            assertEquals(GamePhase.MARKET_BUY, state.getCurrentPhase());

            assertEquals(-1, engine.checkAndAdvanceNextTurn());
            state.forceEndTurn();
            assertEquals(0, engine.checkAndAdvanceNextTurn());
            assertEquals(0, state.getCurrentTurn());
            assertEquals(expectedPhaseAtEnd, state.getCurrentPhase());
        } else {
            assertEquals(-1, engine.checkAndAdvanceNextTurn());
            state.forceEndTurn();
            assertEquals(0, engine.checkAndAdvanceNextTurn());
            assertEquals(0, state.getCurrentTurn());
            assertEquals(GamePhase.MARKET_BUY, state.getCurrentPhase());

            assertEquals(-1, engine.checkAndAdvanceNextTurn());
            state.forceEndTurn();
            assertEquals(0, engine.checkAndAdvanceNextTurn());
            assertEquals(0, state.getCurrentTurn());
            assertEquals(expectedPhaseAtEnd, state.getCurrentPhase());
        }
    }

    @Test
    public void stateTransitions() {
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();

        GameState state = engine.getGameState();
        assertEquals(GamePhase.INIT, state.getCurrentPhase());
        assertEquals(0, state.getCurrentTurn());

        // a normal advance
        assertEquals(-1, engine.checkAndAdvanceNextTurn());
        state.forceEndTurn();
        assertEquals(1, engine.checkAndAdvanceNextTurn());
        assertEquals(1, state.getCurrentTurn());
        assertEquals(GamePhase.INIT, state.getCurrentPhase());

        // check that we stay in the init phase until we
        // position
        assertEquals(-1, engine.checkAndAdvanceNextTurn());
        state.forceEndTurn();
        assertEquals(0, engine.checkAndAdvanceNextTurn());
        assertEquals(0, state.getCurrentTurn());
        assertEquals(GamePhase.INIT, state.getCurrentPhase());

        // force an advance to the play state
        state.advancePlayState();
        assertEquals(0, state.getCurrentTurn());
        assertEquals(GamePhase.PLAY, state.getCurrentPhase());

        // play for a while
        // (we run this test 10 times to make sure we get
        // both paths in the random choice before market
        // buy)
        for (int i = 0; i < 10; i++) {
            simulateOneRound(state, GamePhase.PLAY, GamePhase.PLAY);
        }

        // force an advance to the final state
        while (engine.getGameBoard().getRemainingFences() > 0) {
            engine.getGameBoard().markFencePlaced();
        }
        state.advanceFinalState();

        simulateOneRound(state, GamePhase.FINAL, GamePhase.END);
    }
}
