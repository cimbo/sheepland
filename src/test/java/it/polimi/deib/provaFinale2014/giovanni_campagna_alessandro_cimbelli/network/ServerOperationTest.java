package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import static org.junit.Assert.assertEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyTerrainCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.InitiallyPlaceShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.KillSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MateSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.SellMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIServerNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.ServerOperation;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketServerNetwork;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ServerOperationTest {
    private final ServerNetwork serverNetwork;
    private final ClientNetwork clientNetwork;
    private ClientConnection client;
    private ServerConnection server;
    private ClientSender clientSender;
    private ServerOperation receivedCommand;
    private Move receivedMove;
    private String receivedUsername;
    private String receivedPassword;

    @Parameters
    public static Collection<Object[]> networks() throws IOException {
        return Arrays.asList(new Object[][] {
                { new SocketClientNetwork(), new SocketServerNetwork() },
                { new RMIClientNetwork(), new RMIServerNetwork() },
                { new AutomaticClientNetwork(), new SocketServerNetwork() },
                { new AutomaticClientNetwork(), new RMIServerNetwork() } });
    }

    public ServerOperationTest(ClientNetwork clientNetwork,
            ServerNetwork serverNetwork) {
        this.serverNetwork = serverNetwork;
        this.clientNetwork = clientNetwork;

        final ServerOperationTest self = this;
        serverNetwork.setConnectionHandler(new ServerConnectionHandler() {
            public void handleNewConnection(ServerConnection connection) {
                connection.setServerReceiver(new ServerReceiver() {
                    public void connectionClosed() {
                        synchronized (self) {
                            server = null;
                            self.notify();
                        }
                    }

                    public void doAbandonGame() {
                        synchronized (self) {
                            receivedCommand = ServerOperation.ABANDON_GAME;
                            self.notify();
                        }
                    }

                    public void doForceSynchronizeMe() {
                        synchronized (self) {
                            receivedCommand = ServerOperation.FORCE_SYNCHRONIZE_ME;
                            self.notify();
                        }
                    }

                    public void doExecuteMove(Move move) {
                        synchronized (self) {
                            receivedCommand = ServerOperation.EXECUTE_MOVE;
                            receivedMove = move;
                            self.notify();
                        }
                    }

                    public void doLoginToServer(String username, String password) {
                        synchronized (self) {
                            receivedCommand = ServerOperation.LOGIN_TO_SERVER;
                            receivedUsername = username;
                            receivedPassword = password;
                            self.notify();
                        }
                    }

                    public void doPong() {
                        synchronized (self) {
                            receivedCommand = ServerOperation.PONG;
                            self.notify();
                        }
                    }
                });

                synchronized (self) {
                    server = connection;
                    self.notify();
                }
            }
        });
    }

    @Before
    public void setUp() throws IOException {
        receivedCommand = null;
        receivedMove = null;
        receivedUsername = null;
        receivedPassword = null;

        final ServerOperationTest self = this;

        serverNetwork.startListening();

        client = clientNetwork.connectToServer();
        clientSender = client.getClientSender();
        client.setClientReceiver(new EmptyClientReceiver() {
            @Override
            public void connectionClosed() {
                synchronized (self) {
                    client = null;
                    clientSender = null;
                    self.notify();
                }
            }

        });

        synchronized (this) {
            while (server == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @After
    public void tearDown() {
        synchronized (this) {
            if (client != null) {
                client.close();
            }
            while (client != null || clientSender != null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }

        synchronized (this) {
            if (server != null) {
                server.close();
            }
            while (server != null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }

        serverNetwork.stopListening();
    }

    private void waitCommand() {
        synchronized (this) {
            while (receivedCommand == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Test(timeout = 5000)
    public void abandonGame() throws IOException {
        clientSender.sendAbandonGame();
        waitCommand();
        assertEquals(ServerOperation.ABANDON_GAME, receivedCommand);
    }

    @Test(timeout = 5000)
    public void forceSynchronizeMe() throws IOException {
        clientSender.sendForceSynchronizeMe();
        waitCommand();
        assertEquals(ServerOperation.FORCE_SYNCHRONIZE_ME, receivedCommand);
    }

    @Test(timeout = 5000 * 7)
    public void executeMove() throws IOException {
        Move move = new InitiallyPlaceShepherdMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new RoadSquareIdentifier(7, 7));
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new MoveShepherdMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new RoadSquareIdentifier(7, 7));
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new MoveSheepMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new SheepTileIdentifier(7),
                new SheepTileIdentifier(8), new SheepIdentifier());
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new BuyTerrainCardMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new TerrainCardIdentifier(8));
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new KillSheepMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new SheepTileIdentifier(8),
                new SheepIdentifier());
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new MateSheepMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new SheepTileIdentifier(8));
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new SellMarketCardMove(new PlayerIdentifier(),
                new TerrainCardIdentifier(7), 7);
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new BuyMarketCardMove(new PlayerIdentifier(),
                new TerrainCardIdentifier(7));
        clientSender.sendExecuteMove(move);
        waitCommand();
        assertEquals(ServerOperation.EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);
    }

    @Test(timeout = 5000)
    public void loginToServer() throws IOException {
        clientSender.sendLoginToServer("marco", "polo");
        waitCommand();
        assertEquals(ServerOperation.LOGIN_TO_SERVER, receivedCommand);
        assertEquals("marco", receivedUsername);
        assertEquals("polo", receivedPassword);
    }

    @Test(timeout = 5000)
    public void pong() throws IOException {
        clientSender.sendPong();
        waitCommand();
        assertEquals(ServerOperation.PONG, receivedCommand);
    }
}