package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import org.junit.Before;
import org.junit.Test;

public class ServerMoveBlackSheepTest {
    private SheepTileIdentifier sentOutDestinationId;

    @Before
    public void clearOutTemporaries() {
        sentOutDestinationId = null;
    }

    @Test
    public void simpleTest() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngineWithFakeDice();
        GameMap map = engine.getGameBoard().getMap();

        // place the black in land/1
        // because of the random dice always saying 2, it will go into
        // forest/0
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 + 0 * 3 + 2);
        SheepTile st1 = engine.getSheepTileFromId(stId1);
        SheepTileIdentifier stId2 = new SheepTileIdentifier(1 + 1 * 3 + 0);
        SheepTile st2 = engine.getSheepTileFromId(stId2);
        RoadSquareIdentifier rsId = new RoadSquareIdentifier(
                stId1.getContent(), stId2.getContent());
        RoadSquare rs = engine.getRoadSquareFromId(rsId);
        assertEquals(2, rs.getDiceValue());
        assertFalse(rs.getHasFence());

        assertEquals(map.getSheepsburg(), map.getCurrentBlackSheepPosition());
        map.moveBlackSheep(engine.getGameBoard().getBlackSheep(), st1);
        assertEquals(st1, map.getCurrentBlackSheepPosition());

        engine.moveBlackSheep();

        assertEquals(st2, map.getCurrentBlackSheepPosition());
        assertEquals(stId2, sentOutDestinationId);
    }

    @Test
    public void blackSheepAgainstFence() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngineWithFakeDice();
        GameMap map = engine.getGameBoard().getMap();

        SheepTile blackSheepTile = map.getCurrentBlackSheepPosition();

        assertEquals(map.getCurrentBlackSheepPosition(), map.getSheepsburg());

        RoadSquare roadForTest = map.getAdjacentSquareForValue(blackSheepTile,
                2);
        roadForTest.placeFence(false);

        engine.moveBlackSheep();

        assertEquals(map.getCurrentBlackSheepPosition(), blackSheepTile);
        assertNull(sentOutDestinationId);
    }

    @Test
    public void blackSheepAgainstShepherd() throws InvalidIdentifierException,
            RuleViolationException {
        ServerGameEngine engine = getAGameEngineWithFakeDice();
        GameMap map = engine.getGameBoard().getMap();

        SheepTile blackSheepTile = map.getCurrentBlackSheepPosition();

        assertEquals(map.getCurrentBlackSheepPosition(), map.getSheepsburg());

        RoadSquare roadForTest = map.getAdjacentSquareForValue(blackSheepTile,
                2);

        Player player = new Player("");
        roadForTest.addShepherd(new Shepherd(player));

        engine.moveBlackSheep();

        assertEquals(map.getCurrentBlackSheepPosition(), blackSheepTile);
        assertNull(sentOutDestinationId);
    }

    @Test
    public void blackSheepDosentGoIntoTheSea()
            throws InvalidIdentifierException, RuleViolationException {
        ServerGameEngine engine = getAGameEngineWithFakeDice();
        GameMap map = engine.getGameBoard().getMap();

        SheepTileIdentifier id = new SheepTileIdentifier(1 + 5 * 3 + 2);

        // My pseudo random dice will give me always 2 as result so i put the
        // wolf in a tile in which the road(2) dosen't exist so i will verify
        // that he will not move from that tile
        SheepTile tile = engine.getSheepTileFromId(id);

        map.moveBlackSheep(engine.getGameBoard().getBlackSheep(), tile);

        assertEquals(map.getCurrentBlackSheepPosition(), tile);
        engine.moveBlackSheep();
        assertEquals(map.getCurrentBlackSheepPosition(), tile);
        assertNull(sentOutDestinationId);
    }

    private ServerGameEngine getAGameEngineWithFakeDice()
            throws RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();

        ServerGameEngine engine = new ServerGameEngine(new FakeRandom(),
                new EmptyServerDelegate() {
                    @Override
                    public void didMoveBlackSheep(
                            SheepTileIdentifier destination) {
                        sentOutDestinationId = destination;
                    }
                }, rules);
        engine.addPlayer(new Player("marco"));
        engine.addPlayer(new Player("polo"));
        engine.start();

        return engine;
    }
}
