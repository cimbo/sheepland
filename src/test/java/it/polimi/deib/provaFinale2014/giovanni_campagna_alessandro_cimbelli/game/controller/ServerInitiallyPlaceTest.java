package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class ServerInitiallyPlaceTest extends ServerMoveTestBase {
    @Test
    public void correctTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.INIT, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        // between grain1 and land1
        RoadSquareIdentifier rsId1 = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 1 + 1);
        RoadSquare rs1 = getEngine().getRoadSquareFromId(rsId1);
        assertEquals(2, rs1.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd1 = getEngine().getShepherdFromId(players[0],
                shepherdId);

        executeMove(new InitiallyPlaceShepherdMove(players[0].getId(),
                shepherdId, rsId1));

        assertEquals(rs1, shepherd1.getCurrentPosition());
        assertEquals(shepherd1, rs1.getCurrentShepherd());
        assertFalse(rs1.getHasFence());
        assertTrue(shepherd1.isPositioned());
        assertTrue(players[0].isPositioned());

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.INIT, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());

        // between water1 and water2
        RoadSquareIdentifier rsId2 = new RoadSquareIdentifier(2 * 3 + 1 + 1,
                2 * 3 + 2 + 1);
        RoadSquare rs2 = getEngine().getRoadSquareFromId(rsId2);
        assertEquals(5, rs2.getDiceValue());

        Shepherd shepherd2 = getEngine().getShepherdFromId(players[1],
                shepherdId);
        executeMove(new InitiallyPlaceShepherdMove(players[1].getId(),
                shepherdId, rsId2));

        assertEquals(rs2, shepherd2.getCurrentPosition());
        assertEquals(shepherd2, rs2.getCurrentShepherd());
        assertFalse(rs2.getHasFence());
        assertTrue(shepherd2.isPositioned());
        assertTrue(players[1].isPositioned());

        assertEquals(players[1], getEngine().getCurrentPlayer());
        assertEquals(0, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test(expected = RuleViolationException.class)
    public void outOfOrderTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        // between water1 and water2
        RoadSquareIdentifier rsId2 = new RoadSquareIdentifier(2 * 3 + 1 + 1,
                2 * 3 + 2 + 1);
        RoadSquare rs2 = getEngine().getRoadSquareFromId(rsId2);
        assertEquals(5, rs2.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        executeMove(new InitiallyPlaceShepherdMove(players[1].getId(),
                shepherdId, rsId2));
    }

    @Test
    public void twiceSameShepherd() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        // between grain1 and land1
        RoadSquareIdentifier rsId1 = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 1 + 1);
        RoadSquare rs1 = getEngine().getRoadSquareFromId(rsId1);
        assertEquals(2, rs1.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        executeMove(new InitiallyPlaceShepherdMove(players[0].getId(),
                shepherdId, rsId1));

        // between water1 and water2
        RoadSquareIdentifier rsId2 = new RoadSquareIdentifier(2 * 3 + 1 + 1,
                2 * 3 + 2 + 1);
        RoadSquare rs2 = getEngine().getRoadSquareFromId(rsId2);
        assertEquals(5, rs2.getDiceValue());

        try {
            executeMove(new InitiallyPlaceShepherdMove(players[0].getId(),
                    shepherdId, rsId2));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }

    @Test
    public void alreadyOccupiedSquare() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        // between grain1 and land1
        RoadSquareIdentifier rsId1 = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 1 + 1);
        RoadSquare rs1 = getEngine().getRoadSquareFromId(rsId1);
        assertEquals(2, rs1.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        executeMove(new InitiallyPlaceShepherdMove(players[0].getId(),
                shepherdId, rsId1));
        getEngine().checkAndAdvanceNextTurn();

        try {
            executeMove(new InitiallyPlaceShepherdMove(players[1].getId(),
                    shepherdId, rsId1));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }

    @Test(expected = RuleViolationException.class)
    public void wrongShepherd() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        // between grain1 and land1
        RoadSquareIdentifier rsId1 = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 1 + 1);
        RoadSquare rs1 = getEngine().getRoadSquareFromId(rsId1);
        assertEquals(2, rs1.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd2 = getEngine().getShepherdFromId(players[1],
                shepherdId);
        // go straight to initiallyPlaceShepherd() here, going through
        // the move forces the right thing (because shepherd ids are
        // scoped to a player)
        getEngine().initiallyPlaceShepherd(players[0], shepherd2, rs1);
    }

    @Test
    public void wrongPhase() throws InvalidMoveException {
        correctTest();

        Player[] players = getEngine().getPlayingOrder();

        // between grain1 and land1
        RoadSquareIdentifier rsId1 = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 1 + 1);
        RoadSquare rs1 = getEngine().getRoadSquareFromId(rsId1);
        assertEquals(2, rs1.getDiceValue());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        try {
            executeMove(new InitiallyPlaceShepherdMove(players[0].getId(),
                    shepherdId, rsId1));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }
    }
}
