package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

import org.junit.Test;

public class IdentifiersTest {
    @Test
    public void base() {
        GameObjectIdentifier id1 = new TestGameObjectIdentifier(
                TestGameObject.class, 0);
        GameObjectIdentifier id2 = new TestGameObjectIdentifier(
                TestGameObject.class, 0);
        GameObjectIdentifier id3 = new TestGameObjectIdentifier(
                TestGameObject.class, 1);
        GameObjectIdentifier id4 = new TestGameObjectIdentifier(Shepherd.class,
                4);
        GameObjectIdentifier id5 = new ShepherdIdentifier(4);

        assertTrue(id1.equals(id2));
        assertFalse(id1.equals(id3));
        assertFalse(id1.equals(id4));
        assertTrue(id4.equals(id5));
    }

    @Test
    public void sheepTilesPath() throws InvalidIdentifierException {
        // create two worlds, 1 and 2, and check that identifiers
        // are consistent between them, by following a path across
        // numbered road squares in both maps, and checking at
        // each step

        GameMap map1 = new GameMap();
        GameMap map2 = new GameMap();

        SheepTile tile1 = map1.getSheepsburg();
        SheepTile tile2 = map2.getSheepsburg();

        assertFalse(tile1 == tile2);
        assertTrue(tile1.getId().equals(tile2.getId()));
        assertEquals(tile2, map2.getSheepTileFromId(tile1.getId()));
        assertEquals(tile1, map1.getSheepTileFromId(tile2.getId()));
        assertEquals(new SheepTileIdentifier(0), tile1.getId());

        int[] path = { 5, 6, 3, 1, 4, 1, 2, 6 };
        int[] ids = { 0 * 3 + 2, 1 * 3 + 2, 1 * 3 + 1, 1 * 3 + 0, 0 * 3 + 1,
                0 * 3 + 0, 3 * 3 + 0, 4 * 3 + 1 };
        for (int i = 0; i < path.length; i++) {
            SheepTile next1 = map1.getAdjacentSquareForValue(tile1, path[i])
                    .getMatchingTile(tile1);
            SheepTile next2 = map2.getAdjacentSquareForValue(tile2, path[i])
                    .getMatchingTile(tile2);

            assertFalse(next1 == next2);
            assertTrue(next1.getId().equals(next2.getId()));
            assertEquals(next2, map2.getSheepTileFromId(next1.getId()));
            assertEquals(next1, map1.getSheepTileFromId(next2.getId()));
            assertEquals(new SheepTileIdentifier(1 + ids[i]), next1.getId());
            assertEquals(new TestGameObjectIdentifier(SheepTile.class,
                    1 + ids[i]), next1.getId());

            tile1 = next1;
            tile2 = next2;
        }
    }

    @Test
    public void sheepTileInvalid() {
        GameMap map = new GameMap();

        try {
            map.getSheepTileFromId(new SheepTileIdentifier(42));
            fail("code should not be reached");
        } catch (InvalidIdentifierException e) {
            ;
        }

        try {
            map.getSheepTileFromId(new ShepherdIdentifier(0));
            fail("code should not be reached");
        } catch (InvalidIdentifierException e) {
            ;
        }
    }

    @Test
    public void players() throws InvalidIdentifierException {
        GameBoard serverBoard = new GameBoard();
        RuleSet rules = new RuleSet();

        serverBoard.addPlayer(new Player(""));
        serverBoard.addPlayer(new Player(""));

        serverBoard.initializeForRules(rules);
        Player[] serverList = serverBoard.getAllPlayers();
        for (Player element : serverList) {
            assertNotNull(element.getId());
            assertEquals(element, serverBoard.getPlayerFromId(element.getId()));
            for (Shepherd s : element.getAllShepherds()) {
                assertNotNull(s.getId());
                assertEquals(s, element.getShepherdFromId(s.getId()));
            }
        }
    }

    @Test(expected = InvalidIdentifierException.class)
    public void invalidPlayers() throws InvalidIdentifierException {
        GameBoard serverBoard = new GameBoard();
        RuleSet rules = new RuleSet();

        serverBoard.addPlayer(new Player(""));
        serverBoard.addPlayer(new Player(""));

        serverBoard.initializeForRules(rules);

        serverBoard.getPlayerFromId(new PlayerIdentifier());
    }

    @Test
    public void sheep() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();

        Sheep s1 = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(s1);
        assertEquals(s1, board.getSheepFromId(s1.getId()));

        Sheep s2 = new Sheep(SheepType.RAM);
        s2.setId(new SheepIdentifier());
        board.addSheep(s2.getId(), s2);
        assertEquals(s2, board.getSheepFromId(s2.getId()));
    }

    @Test(expected = InvalidIdentifierException.class)
    public void invalidSheep() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();

        Sheep s = new Sheep(SheepType.FEMALE_SHEEP);
        s.setId(new SheepIdentifier());
        board.getSheepFromId(s.getId());
    }

    @Test
    public void cards() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();
        RuleSet rules = new RuleSet();

        board.initializeForRules(rules);

        // non initial card
        TerrainCardIdentifier id1 = new TerrainCardIdentifier(
                (rules.getBuyableCards() + 1) * 2 + TerrainType.GRAIN.ordinal());
        TerrainCard card1 = board.getTerrainCardFromId(id1);

        assertEquals(TerrainType.GRAIN, card1.getType());
        assertTrue(card1.isForSale());
        assertEquals(1, card1.getCost());

        // initial card
        TerrainCardIdentifier id2 = new TerrainCardIdentifier(
                (rules.getBuyableCards() + 1) * 0 + TerrainType.GRAIN.ordinal());
        TerrainCard card2 = board.getTerrainCardFromId(id2);

        assertEquals(TerrainType.GRAIN, card2.getType());
        assertFalse(card2.isForSale());
    }

    @Test(expected = InvalidIdentifierException.class)
    public void cardsInvalidCode() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();
        RuleSet rules = new RuleSet();

        board.initializeForRules(rules);

        board.getTerrainCardFromId(new TerrainCardIdentifier(42));
    }

    @Test(expected = InvalidIdentifierException.class)
    public void cardsInvalidType() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();
        RuleSet rules = new RuleSet();

        board.initializeForRules(rules);

        board.getTerrainCardFromId(new ShepherdIdentifier(0));
    }

    @Test(expected = InvalidIdentifierException.class)
    public void nullPlayer() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();
        board.getPlayerFromId(null);
    }

    @Test(expected = InvalidIdentifierException.class)
    public void nullShepherd() throws InvalidIdentifierException {
        Player player = new Player("");
        player.getShepherdFromId(null);
    }

    @Test(expected = InvalidIdentifierException.class)
    public void nullCard() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();
        board.getTerrainCardFromId(null);
    }

    @Test(expected = InvalidIdentifierException.class)
    public void nullSheep() throws InvalidIdentifierException {
        GameBoard board = new GameBoard();
        board.getSheepFromId(null);
    }

    @Test(expected = InvalidIdentifierException.class)
    public void nullTile() throws InvalidIdentifierException {
        GameMap map = new GameMap();
        map.getSheepTileFromId(null);
    }

    @Test(expected = InvalidIdentifierException.class)
    public void nullSquare() throws InvalidIdentifierException {
        GameMap map = new GameMap();
        map.getRoadSquareFromId(null);
    }
}
