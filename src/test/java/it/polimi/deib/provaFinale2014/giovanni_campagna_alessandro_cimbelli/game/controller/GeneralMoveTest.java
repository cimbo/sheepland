package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

import org.junit.Test;

public class GeneralMoveTest {

    @Test
    public void forceEndTurnMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        RoadSquareIdentifier rs = new RoadSquareIdentifier(0, 1 + 3 * 0 + 2);

        Move move = new ForceEndTurnMove(player, false);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new ForceEndTurnMove(player, false));
        assertEquals(move.hashCode(),
                new ForceEndTurnMove(player, false).hashCode());
        assertNotEquals(move, new ForceEndTurnMove(new PlayerIdentifier(),
                false));
        assertNotEquals(move, new ForceEndTurnMove(player, true));
        assertNotEquals(move, new MoveShepherdMove(player, shepherd, rs));
    }

    @Test
    public void initiallyPlaceShepherdMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        RoadSquareIdentifier rs = new RoadSquareIdentifier(0, 1 + 3 * 0 + 2);

        Move move = new InitiallyPlaceShepherdMove(player, shepherd, rs);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new InitiallyPlaceShepherdMove(player, shepherd, rs));
        assertEquals(move.hashCode(), new InitiallyPlaceShepherdMove(player,
                shepherd, rs).hashCode());
        assertNotEquals(move, new InitiallyPlaceShepherdMove(
                new PlayerIdentifier(), shepherd, rs));
        assertNotEquals(move, new InitiallyPlaceShepherdMove(player,
                new ShepherdIdentifier(1), rs));
        assertNotEquals(move, new InitiallyPlaceShepherdMove(player, shepherd,
                new RoadSquareIdentifier(1 + 3 * 0 + 1, 1 + 3 * 0 + 2)));
        assertNotEquals(move, new MoveShepherdMove(player, shepherd, rs));
    }

    @Test
    public void moveShepherdMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        RoadSquareIdentifier rs = new RoadSquareIdentifier(0, 1 + 3 * 0 + 2);

        Move move = new MoveShepherdMove(player, shepherd, rs);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new MoveShepherdMove(player, shepherd, rs));
        assertEquals(move.hashCode(),
                new MoveShepherdMove(player, shepherd, rs).hashCode());
        assertNotEquals(move, new MoveShepherdMove(new PlayerIdentifier(),
                shepherd, rs));
        assertNotEquals(move, new MoveShepherdMove(player,
                new ShepherdIdentifier(1), rs));
        assertNotEquals(move, new MoveShepherdMove(player, shepherd,
                new RoadSquareIdentifier(1 + 3 * 0 + 1, 1 + 3 * 0 + 2)));
        assertNotEquals(move, new InitiallyPlaceShepherdMove(player, shepherd,
                rs));
    }

    @Test
    public void moveSheepMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        SheepTileIdentifier tile1 = new SheepTileIdentifier(0);
        SheepTileIdentifier tile2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        SheepIdentifier sheep = new SheepIdentifier();

        Move move = new MoveSheepMove(player, shepherd, tile1, tile2, sheep);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new MoveSheepMove(player, shepherd, tile1, tile2,
                sheep));
        assertEquals(move.hashCode(), new MoveSheepMove(player, shepherd,
                tile1, tile2, sheep).hashCode());
        assertNotEquals(move, new MoveSheepMove(new PlayerIdentifier(),
                shepherd, tile1, tile2, sheep));
        assertNotEquals(move, new MoveSheepMove(player, new ShepherdIdentifier(
                1), tile1, tile2, sheep));
        assertNotEquals(move, new MoveSheepMove(player, shepherd,
                new SheepTileIdentifier(5 * 3 + 0 + 1), tile2, sheep));
        assertNotEquals(move, new MoveSheepMove(player, shepherd, tile1,
                new SheepTileIdentifier(5 * 3 + 0 + 1), sheep));
        assertNotEquals(move, new MoveSheepMove(player, shepherd, tile1, tile2,
                new SheepIdentifier()));
        assertNotEquals(move, new KillSheepMove(player, shepherd, tile1, sheep));
    }

    @Test
    public void killSheepMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        SheepTileIdentifier tile = new SheepTileIdentifier(0);
        SheepIdentifier sheep = new SheepIdentifier();

        Move move = new KillSheepMove(player, shepherd, tile, sheep);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new KillSheepMove(player, shepherd, tile, sheep));
        assertEquals(move.hashCode(), new KillSheepMove(player, shepherd, tile,
                sheep).hashCode());
        assertNotEquals(move, new KillSheepMove(new PlayerIdentifier(),
                shepherd, tile, sheep));
        assertNotEquals(move, new KillSheepMove(player, new ShepherdIdentifier(
                1), tile, sheep));
        assertNotEquals(move, new KillSheepMove(player, shepherd,
                new SheepTileIdentifier(5 * 3 + 0 + 1), sheep));
        assertNotEquals(move, new KillSheepMove(player, shepherd, tile,
                new SheepIdentifier()));
        assertNotEquals(move, new MateSheepMove(player, shepherd, tile));
    }

    @Test
    public void mateSheepMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        SheepTileIdentifier tile = new SheepTileIdentifier(0);

        Move move = new MateSheepMove(player, shepherd, tile);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new MateSheepMove(player, shepherd, tile));
        assertEquals(move.hashCode(),
                new MateSheepMove(player, shepherd, tile).hashCode());
        assertNotEquals(move, new MateSheepMove(new PlayerIdentifier(),
                shepherd, tile));
        assertNotEquals(move, new MateSheepMove(player, new ShepherdIdentifier(
                1), tile));
        assertNotEquals(move, new MateSheepMove(player, shepherd,
                new SheepTileIdentifier(5 * 3 + 0 + 1)));
        assertNotEquals(move, new KillSheepMove(player, shepherd, tile,
                new SheepIdentifier()));
    }

    @Test
    public void buyTerrainCardMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        ShepherdIdentifier shepherd = new ShepherdIdentifier(0);
        TerrainCardIdentifier card = new TerrainCardIdentifier(1 * 6 + 3);

        Move move = new BuyTerrainCardMove(player, shepherd, card);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new BuyTerrainCardMove(player, shepherd, card));
        assertEquals(move.hashCode(), new BuyTerrainCardMove(player, shepherd,
                card).hashCode());
        assertNotEquals(move, new BuyTerrainCardMove(new PlayerIdentifier(),
                shepherd, card));
        assertNotEquals(move, new BuyTerrainCardMove(player,
                new ShepherdIdentifier(1), card));
        assertNotEquals(move, new BuyTerrainCardMove(player, shepherd,
                new TerrainCardIdentifier(2 * 6 + 3)));
        assertNotEquals(move, new BuyMarketCardMove(player, card));
    }

    @Test
    public void buyMarketCardMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        TerrainCardIdentifier card = new TerrainCardIdentifier(1 * 6 + 3);

        Move move = new BuyMarketCardMove(player, card);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new BuyMarketCardMove(player, card));
        assertEquals(move.hashCode(),
                new BuyMarketCardMove(player, card).hashCode());
        assertNotEquals(move, new BuyMarketCardMove(new PlayerIdentifier(),
                card));
        assertNotEquals(move, new BuyMarketCardMove(player,
                new TerrainCardIdentifier(2 * 6 + 3)));
        assertNotEquals(move, new BuyTerrainCardMove(player,
                new ShepherdIdentifier(0), card));
    }

    @Test
    public void sellMarketCardMove() {
        PlayerIdentifier player = new PlayerIdentifier();
        TerrainCardIdentifier card = new TerrainCardIdentifier(1 * 6 + 3);

        Move move = new SellMarketCardMove(player, card, 3);

        assertEquals(move, move);
        assertNotEquals(move, null);
        assertEquals(move, new SellMarketCardMove(player, card, 3));
        assertEquals(move.hashCode(),
                new SellMarketCardMove(player, card, 3).hashCode());
        assertNotEquals(move, new SellMarketCardMove(new PlayerIdentifier(),
                card, 3));
        assertNotEquals(move, new SellMarketCardMove(player,
                new TerrainCardIdentifier(2 * 6 + 3), 3));
        assertNotEquals(move, new SellMarketCardMove(player, card, 4));
        assertNotEquals(move, new BuyMarketCardMove(player, card));
    }
}
