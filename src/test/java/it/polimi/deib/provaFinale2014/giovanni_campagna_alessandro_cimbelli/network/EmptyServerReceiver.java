package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;

class EmptyServerReceiver implements ServerReceiver {

    public void doAbandonGame() {
    }

    public void doForceSynchronizeMe() {
    }

    public void doExecuteMove(Move move) {
    }

    public void doLoginToServer(String username, String password) {
    }

    public void doPong() {
    }

    public void connectionClosed() {
    }

}
