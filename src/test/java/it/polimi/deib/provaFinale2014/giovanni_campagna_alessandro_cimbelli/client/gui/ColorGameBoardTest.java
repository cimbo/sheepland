package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.awt.Point;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class ColorGameBoardTest {
    ColorGameBoard colorMap;
    GameMap map;

    @Before
    public void setUp() throws IOException {
        map = new GameMap();
        colorMap = new ColorGameBoard();
    }

    @Test
    public void roadsTest() {
        for (RoadSquare rs : map.getAllRoadSquares()) {
            RoadSquareIdentifier rsId = rs.getId();

            Point pt = colorMap.getRoadCenter(rsId);
            assertNotNull(pt);

            RoadSquareIdentifier foundId = colorMap.takeRoadElement(pt);
            assertEquals(rsId, foundId);
        }
    }

    @Test
    public void tilesTest() {
        for (int i = 0; i < 19; i++) {
            SheepTileIdentifier tileId = new SheepTileIdentifier(i);

            Point pt = colorMap.getTileCenter(tileId);
            assertNotNull(pt);

            SheepTileIdentifier foundId = colorMap.takeTileElement(pt);
            assertEquals(tileId, foundId);
        }
    }

    @Test
    public void outsideTest() {
        Point sheepsburg = colorMap.getTileCenter(new SheepTileIdentifier(0));

        assertNotNull(colorMap.takeTileElement(sheepsburg));
        assertNull(colorMap.takeTileElement(new Point(-1, (int) sheepsburg
                .getY())));
        assertNull(colorMap.takeTileElement(new Point((int) sheepsburg.getX(),
                -1)));
        assertNull(colorMap.takeTileElement(new Point(527, (int) sheepsburg
                .getY())));
        assertNull(colorMap.takeTileElement(new Point((int) sheepsburg.getX(),
                700)));
        assertNull(colorMap.takeTileElement(new Point(-1, (int) sheepsburg
                .getY())));
        assertNull(colorMap.takeTileElement(new Point(-1, -1)));
        assertNull(colorMap.takeTileElement(new Point(527, 700)));
    }
}
