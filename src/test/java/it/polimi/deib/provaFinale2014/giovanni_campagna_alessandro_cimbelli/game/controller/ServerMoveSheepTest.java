package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class ServerMoveSheepTest extends ServerPlayTestBase {
    @Test
    public void correctFirstMove() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // move the sheep from land1 to grain1
        // land1 is guaranteed to have 1 and exactly 1 sheep, because
        // neither the black sheep nor the wolf can reach there
        SheepTileIdentifier stId1 = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.LAND, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.GRAIN, st2.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);
        assertFalse(st1.getHasWolf());
        assertEquals(1, femaleSheepAtOrigin + ramsAtOrigin + lambsAtOrigin);
        int femaleSheepAtDestination = st2
                .getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtDestination = st2.getNumberOfSheep(SheepType.RAM);
        int lambsAtDestination = st2.getNumberOfSheep(SheepType.LAMB);
        // if the wolf got here, the number at destination will be 0, otherwise
        // 1
        if (st1.getHasWolf()) {
            assertEquals(0, femaleSheepAtDestination + ramsAtDestination
                    + lambsAtDestination);
        } else {
            assertEquals(1, femaleSheepAtDestination + ramsAtDestination
                    + lambsAtDestination);
        }

        SheepType movedType;
        if (femaleSheepAtOrigin == 1) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin == 1) {
            movedType = SheepType.RAM;
        } else {
            movedType = SheepType.LAMB;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        executeMove(new MoveSheepMove(players[0].getId(), shepherdId, stId1,
                stId2, moved.getId()));

        assertEquals(0, st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(0, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(0, st1.getNumberOfSheep(SheepType.LAMB));
        if (movedType == SheepType.FEMALE_SHEEP) {
            assertEquals(femaleSheepAtDestination + 1,
                    st2.getNumberOfSheep(SheepType.FEMALE_SHEEP));
            assertEquals(ramsAtDestination, st2.getNumberOfSheep(SheepType.RAM));
            assertEquals(lambsAtDestination,
                    st2.getNumberOfSheep(SheepType.LAMB));
        } else if (movedType == SheepType.RAM) {
            assertEquals(femaleSheepAtDestination,
                    st2.getNumberOfSheep(SheepType.FEMALE_SHEEP));
            assertEquals(ramsAtDestination + 1,
                    st2.getNumberOfSheep(SheepType.RAM));
            assertEquals(lambsAtDestination,
                    st2.getNumberOfSheep(SheepType.LAMB));
        } else {
            assertEquals(femaleSheepAtDestination,
                    st2.getNumberOfSheep(SheepType.FEMALE_SHEEP));
            assertEquals(ramsAtDestination, st2.getNumberOfSheep(SheepType.RAM));
            assertEquals(lambsAtDestination + 1,
                    st2.getNumberOfSheep(SheepType.LAMB));
        }

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());
    }

    @Test
    public void doubleSheepMove() throws InvalidMoveException {
        correctFirstMove();

        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // and we just moved the only sheep from land1 to grain1
        // now try to move it back (and fail, because you can't do
        // the same move twice)
        SheepTileIdentifier stId1 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.GRAIN, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.LAND, st2.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        try {
            executeMove(new MoveSheepMove(players[0].getId(), shepherdId,
                    stId1, stId2, moved.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void correctDoubleSheepMove() throws InvalidMoveException {
        correctFirstMove();

        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        Shepherd shepherd = getEngine().getShepherdFromId(players[0],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // move to between grain1 and land2 (adjacent)
        RoadSquareIdentifier nextRsId = new RoadSquareIdentifier(3 * 3 + 1 + 1,
                0 * 3 + 2 + 1);
        RoadSquare nextRs = getEngine().getRoadSquareFromId(nextRsId);
        assertEquals(1, nextRs.getDiceValue());
        assertTrue(getEngine().getGameBoard().getMap()
                .areSquaresAdjacent(shepherd.getCurrentPosition(), nextRs));

        getEngine().moveShepherd(players[0], shepherd, nextRs);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(-1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        // now we're between grain1 and land2 and grain1 has at least
        // one sheep (that we moved in correctFirstMove()) - two if
        // the wolf is not here
        // move it to land2
        SheepTileIdentifier stId1 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.GRAIN, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.LAND, st2.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);
        if (st1.getHasWolf()) {
            assertEquals(1, femaleSheepAtOrigin + ramsAtOrigin + lambsAtOrigin);
        } else {
            assertEquals(2, femaleSheepAtOrigin + ramsAtOrigin + lambsAtOrigin);
        }

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        getEngine().moveSheep(players[0], shepherd, st1, st2, moved);

        assertEquals(players[0], getEngine().getCurrentPlayer());
        assertEquals(1, getEngine().checkAndAdvanceNextTurn());
        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[1], getEngine().getCurrentPlayer());
    }

    @Test
    public void wrongSheepOrigin() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // try to move a sheep from land1 to grain1, but move the wrong one
        SheepTileIdentifier stId1 = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.LAND, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.GRAIN, st2.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);
        int femaleSheepAtDestination = st2
                .getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtDestination = st2.getNumberOfSheep(SheepType.RAM);
        int lambsAtDestination = st2.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtDestination > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtDestination > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtDestination > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        // st2, not st1, even though we move from st1 to st2
        Sheep moved = st2.getAnySheepOfType(movedType);
        assertNotNull(moved);
        try {
            executeMove(new MoveSheepMove(players[0].getId(), shepherdId,
                    stId1, stId2, moved.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void sameOriginDestination() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // move the sheep from land1 to grain1
        // land1 is guaranteed to have 1 and exactly 1 sheep, because
        // neither the black sheep nor the wolf can reach there
        SheepTileIdentifier stId1 = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.LAND, st1.getTerrainType());

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        assertNotNull(moved);
        try {
            executeMove(new MoveSheepMove(players[0].getId(), shepherdId,
                    stId1, stId1, moved.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void invalidShepherdTest() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);
        // wrong shepherd!
        Shepherd shepherd = getEngine().getShepherdFromId(players[1],
                shepherdId);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // try to move a sheep from land1 to grain1
        SheepTileIdentifier stId1 = new SheepTileIdentifier(0 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.LAND, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.GRAIN, st2.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        try {
            // go straight to moveSheep() here, going through
            // the move forces the right thing (because shepherd ids are
            // scoped to a player)
            getEngine().moveSheep(players[0], shepherd, st1, st2, moved);
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void wrongOriginTile() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // try to move a sheep from forest0 to grain1
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 * 3 + 0 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.FOREST, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.GRAIN, st2.getTerrainType());
        assertFalse(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        try {
            executeMove(new MoveSheepMove(players[0].getId(), shepherdId,
                    stId1, stId2, moved.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void wrongDestinationTile() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // try to move a sheep from grain1 to forest0
        SheepTileIdentifier stId1 = new SheepTileIdentifier(3 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.GRAIN, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(1 * 3 + 0 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.FOREST, st2.getTerrainType());
        assertFalse(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        try {
            executeMove(new MoveSheepMove(players[0].getId(), shepherdId,
                    stId1, stId2, moved.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }

    @Test
    public void wrongOriginAndDestinationTile() throws InvalidMoveException {
        Player[] players = getEngine().getPlayingOrder();

        assertEquals(GamePhase.PLAY, getEngine().getGameState()
                .getCurrentPhase());
        assertEquals(players[0], getEngine().getCurrentPlayer());

        ShepherdIdentifier shepherdId = new ShepherdIdentifier(0);

        // we're between grain1 and land1 (see ServerPlayTestBase)
        // try to move a sheep from forest0 and forest1
        SheepTileIdentifier stId1 = new SheepTileIdentifier(1 * 3 + 1 + 1);
        SheepTile st1 = getEngine().getSheepTileFromId(stId1);
        assertEquals(TerrainType.FOREST, st1.getTerrainType());
        SheepTileIdentifier stId2 = new SheepTileIdentifier(1 * 3 + 0 + 1);
        SheepTile st2 = getEngine().getSheepTileFromId(stId2);
        assertEquals(TerrainType.FOREST, st2.getTerrainType());
        assertTrue(getEngine().getGameBoard().getMap()
                .areTilesAdjacent(st1, st2));

        int femaleSheepAtOrigin = st1.getNumberOfSheep(SheepType.FEMALE_SHEEP);
        int ramsAtOrigin = st1.getNumberOfSheep(SheepType.RAM);
        int lambsAtOrigin = st1.getNumberOfSheep(SheepType.LAMB);

        SheepType movedType;
        if (femaleSheepAtOrigin > 0) {
            movedType = SheepType.FEMALE_SHEEP;
        } else if (ramsAtOrigin > 0) {
            movedType = SheepType.RAM;
        } else if (lambsAtOrigin > 0) {
            movedType = SheepType.LAMB;
        } else {
            fail();
            return;
        }

        Sheep moved = st1.getAnySheepOfType(movedType);
        try {
            executeMove(new MoveSheepMove(players[0].getId(), shepherdId,
                    stId1, stId2, moved.getId()));
            fail("code should not be reached");
        } catch (RuleViolationException e) {
            ;
        }

        assertEquals(femaleSheepAtOrigin,
                st1.getNumberOfSheep(SheepType.FEMALE_SHEEP));
        assertEquals(ramsAtOrigin, st1.getNumberOfSheep(SheepType.RAM));
        assertEquals(lambsAtOrigin, st1.getNumberOfSheep(SheepType.LAMB));
    }
}
