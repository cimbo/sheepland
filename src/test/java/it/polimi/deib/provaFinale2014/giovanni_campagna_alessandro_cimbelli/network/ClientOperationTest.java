package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyTerrainCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.InitiallyPlaceShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.KillSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MateSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.SellMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIServerNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.ClientOperation;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketServerNetwork;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ClientOperationTest {
    private final ServerNetwork serverNetwork;
    private final ClientNetwork clientNetwork;
    private ClientConnection client;
    private ServerConnection server;
    private ServerSender serverSender;
    private ClientOperation receivedCommand;
    private RuleSet receivedRules;
    private GameBoard receivedBoard;
    private PlayerIdentifier receivedPlayer;
    private Exception receivedException;
    private GameState receivedState;
    private GamePhase receivedPhase;
    private SheepTileIdentifier receivedSheepTile;
    private SheepIdentifier receivedSheep;
    private Sheep[] receivedSheepArray;
    private Move receivedMove;
    private int receivedInt;
    private boolean receivedBoolean;
    private Map<PlayerIdentifier, Integer> receivedPlayerIntegerMap;

    @Parameters
    public static Collection<Object[]> networks() throws IOException {
        return Arrays.asList(new Object[][] {
                { new SocketClientNetwork(), new SocketServerNetwork() },
                { new RMIClientNetwork(), new RMIServerNetwork() },
                { new AutomaticClientNetwork(), new SocketServerNetwork() },
                { new AutomaticClientNetwork(), new RMIServerNetwork() } });
    }

    public ClientOperationTest(ClientNetwork clientNetwork,
            ServerNetwork serverNetwork) {
        this.serverNetwork = serverNetwork;
        this.clientNetwork = clientNetwork;

        final ClientOperationTest self = this;
        serverNetwork.setConnectionHandler(new ServerConnectionHandler() {
            public void handleNewConnection(ServerConnection connection) {
                connection.setServerReceiver(new EmptyServerReceiver() {
                    @Override
                    public void connectionClosed() {
                        synchronized (self) {
                            server = null;
                            serverSender = null;
                            self.notify();
                        }
                    }
                });

                synchronized (self) {
                    server = connection;
                    serverSender = connection.getServerSender();
                    self.notify();
                }
            }
        });
    }

    @Before
    public void setUp() throws IOException {
        receivedCommand = null;
        receivedRules = null;
        receivedBoard = null;
        receivedPlayer = null;
        receivedException = null;
        receivedState = null;
        receivedPhase = null;
        receivedSheepTile = null;
        receivedSheep = null;
        receivedSheepArray = null;
        receivedMove = null;
        receivedInt = -1;
        receivedBoolean = false;
        receivedPlayerIntegerMap = null;

        final ClientOperationTest self = this;

        serverNetwork.startListening();

        client = clientNetwork.connectToServer();
        client.setClientReceiver(new ClientReceiver() {
            public void setup(RuleSet rules, GameBoard board,
                    PlayerIdentifier selfPlayer) {
                synchronized (self) {
                    receivedCommand = ClientOperation.SETUP;
                    receivedRules = rules;
                    receivedBoard = board;
                    receivedPlayer = selfPlayer;
                    self.notify();
                }
            }

            public void moveAcknowledged(Exception result) {
                synchronized (self) {
                    receivedCommand = ClientOperation.MOVE_ACKNOWLEDGED;
                    receivedException = result;
                    self.notify();
                }
            }

            public void forceSynchronize(RuleSet rules, GameBoard newBoard,
                    PlayerIdentifier selfPlayer, GameState newState) {
                synchronized (self) {
                    receivedCommand = ClientOperation.FORCE_SYNCHRONIZE;
                    receivedRules = rules;
                    receivedBoard = newBoard;
                    receivedPlayer = selfPlayer;
                    receivedState = newState;
                    self.notify();
                }
            }

            public void advanceNextTurn(int turn, GamePhase phase) {
                synchronized (self) {
                    receivedCommand = ClientOperation.ADVANCE_NEXT_TURN;
                    receivedInt = turn;
                    receivedPhase = phase;
                    self.notify();
                }
            }

            public void blackSheepMoved(SheepTileIdentifier destination) {
                synchronized (self) {
                    receivedCommand = ClientOperation.BLACK_SHEEP_MOVED;
                    receivedSheepTile = destination;
                    self.notify();
                }
            }

            public void wolfMoved(SheepTileIdentifier destination,
                    SheepIdentifier eatenSheep) {
                synchronized (self) {
                    receivedCommand = ClientOperation.WOLF_MOVED;
                    receivedSheepTile = destination;
                    receivedSheep = eatenSheep;
                    self.notify();
                }
            }

            public void lambsGrew(SheepTileIdentifier where,
                    Sheep[] newAdultSheep) {
                synchronized (self) {
                    receivedCommand = ClientOperation.LAMBS_GREW;
                    receivedSheepTile = where;
                    receivedSheepArray = newAdultSheep;
                    self.notify();
                }
            }

            public void didExecuteMove(Move move) {
                synchronized (self) {
                    receivedCommand = ClientOperation.DID_EXECUTE_MOVE;
                    receivedMove = move;
                    self.notify();
                }
            }

            public void loginAnswer(boolean answer) {
                synchronized (self) {
                    receivedCommand = ClientOperation.LOGIN_ANSWER;
                    receivedBoolean = answer;
                    self.notify();
                }
            }

            public void ping() {
                synchronized (self) {
                    receivedCommand = ClientOperation.PING;
                    self.notify();
                }
            }

            public void endGame(Map<PlayerIdentifier, Integer> scores) {
                synchronized (self) {
                    receivedCommand = ClientOperation.END_GAME;
                    receivedPlayerIntegerMap = scores;
                    self.notify();
                }
            }

            public void playerLeft(PlayerIdentifier who, boolean clean) {
                synchronized (self) {
                    receivedCommand = ClientOperation.PLAYER_LEFT;
                    receivedPlayer = who;
                    receivedBoolean = clean;
                    self.notify();
                }
            }

            public void playerJoined(PlayerIdentifier who) {
                synchronized (self) {
                    receivedCommand = ClientOperation.PLAYER_JOINED;
                    receivedPlayer = who;
                    self.notify();
                }
            }

            public void abandonComplete() {
                synchronized (self) {
                    receivedCommand = ClientOperation.ABANDON_COMPLETE;
                    self.notify();
                }
            }

            public void connectionClosed() {
                synchronized (self) {
                    client = null;
                    self.notify();
                }
            }

        });

        synchronized (this) {
            while (server == null || serverSender == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @After
    public void tearDown() {
        synchronized (this) {
            if (server != null) {
                server.close();
            }
            while (server != null || serverSender != null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }

        synchronized (this) {
            if (client != null) {
                client.close();
            }
            while (client != null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }

        serverNetwork.stopListening();
    }

    private void waitCommand() {
        synchronized (this) {
            while (receivedCommand == null) {
                try {
                    wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }

    @Test(timeout = 5000)
    public void setup() throws IOException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        PlayerIdentifier self = new PlayerIdentifier();

        serverSender.sendSetup(rules, board, self);
        waitCommand();
        assertEquals(ClientOperation.SETUP, receivedCommand);
        assertNotNull(receivedBoard);
        assertTrue(receivedRules.isSealed());
        assertEquals(rules, receivedRules);
        assertEquals(self, receivedPlayer);
    }

    @Test(expected = IllegalStateException.class, timeout = 5000)
    public void setupUnsealedRules() throws IOException {
        serverSender.sendSetup(new RuleSet(), new GameBoard(),
                new PlayerIdentifier());
    }

    @Test(timeout = 5000)
    public void moveAcknowledged() throws IOException {
        Exception e = new Exception("Fuffa");
        serverSender.sendMoveAcknowledged(e);
        waitCommand();
        assertEquals(ClientOperation.MOVE_ACKNOWLEDGED, receivedCommand);
        assertEquals(e.getMessage(), receivedException.getMessage());
    }

    @Test(timeout = 5000)
    public void moveAcknoweledgedNull() throws IOException {
        serverSender.sendMoveAcknowledged(null);
        waitCommand();
        assertEquals(ClientOperation.MOVE_ACKNOWLEDGED, receivedCommand);
        assertNull(receivedException);
    }

    @Test(timeout = 5000)
    public void forceSynchronize() throws IOException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        PlayerIdentifier self = new PlayerIdentifier();
        GameState state = new GameState();

        serverSender.sendForceSynchronize(rules, board, self, state);
        waitCommand();
        assertEquals(ClientOperation.FORCE_SYNCHRONIZE, receivedCommand);
        assertNotNull(receivedBoard);
        assertTrue(receivedRules.isSealed());
        assertEquals(rules, receivedRules);
        assertEquals(self, receivedPlayer);
        assertEquals(state, receivedState);
    }

    @Test(timeout = 5000)
    public void advanceNextTurn() throws IOException {
        serverSender.sendAdvanceNextTurn(7, GamePhase.PLAY);
        waitCommand();
        assertEquals(ClientOperation.ADVANCE_NEXT_TURN, receivedCommand);
        assertEquals(7, receivedInt);
        assertEquals(GamePhase.PLAY, receivedPhase);
    }

    @Test(timeout = 5000)
    public void blackSheepMoved() throws IOException {
        SheepTileIdentifier id = new SheepTileIdentifier(7);

        serverSender.sendBlackSheepMoved(id);
        waitCommand();
        assertEquals(ClientOperation.BLACK_SHEEP_MOVED, receivedCommand);
        assertEquals(id, receivedSheepTile);
    }

    @Test(timeout = 5000)
    public void wolfMoved() throws IOException {
        SheepTileIdentifier tileId = new SheepTileIdentifier(7);
        SheepIdentifier sheep = new SheepIdentifier();

        serverSender.sendWolfMoved(tileId, sheep);
        waitCommand();
        assertEquals(ClientOperation.WOLF_MOVED, receivedCommand);
        assertEquals(tileId, receivedSheepTile);
        assertEquals(sheep, receivedSheep);
    }

    @Test(timeout = 5000)
    public void lambsGrew() throws IOException {
        SheepTileIdentifier tileId = new SheepTileIdentifier(7);
        Sheep[] sheep = new Sheep[4];
        for (int i = 0; i < 4; i++) {
            sheep[i] = new Sheep(SheepType.FEMALE_SHEEP);
            sheep[i].setId(new SheepIdentifier());
        }

        serverSender.sendLambsGrew(tileId, sheep);
        waitCommand();
        assertEquals(ClientOperation.LAMBS_GREW, receivedCommand);
        assertEquals(tileId, receivedSheepTile);
        assertEquals(sheep.length, receivedSheepArray.length);
        for (int i = 0; i < 4; i++) {
            assertEquals(sheep[i].getId(), receivedSheepArray[i].getId());
        }
    }

    @Test(timeout = 5000 * 7)
    public void didExecuteMove() throws IOException {
        Move move = new InitiallyPlaceShepherdMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new RoadSquareIdentifier(7, 7));
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new MoveShepherdMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new RoadSquareIdentifier(7, 7));
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new MoveSheepMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new SheepTileIdentifier(7),
                new SheepTileIdentifier(8), new SheepIdentifier());
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new BuyTerrainCardMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new TerrainCardIdentifier(8));
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new KillSheepMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new SheepTileIdentifier(8),
                new SheepIdentifier());
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new MateSheepMove(new PlayerIdentifier(),
                new ShepherdIdentifier(7), new SheepTileIdentifier(8));
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new SellMarketCardMove(new PlayerIdentifier(),
                new TerrainCardIdentifier(7), 7);
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);

        receivedCommand = null;
        move = new BuyMarketCardMove(new PlayerIdentifier(),
                new TerrainCardIdentifier(7));
        serverSender.sendDidExecuteMove(move);
        waitCommand();
        assertEquals(ClientOperation.DID_EXECUTE_MOVE, receivedCommand);
        assertEquals(move, receivedMove);
    }

    @Test(timeout = 5000)
    public void loginAnswer() throws IOException {
        serverSender.sendLoginAnswer(true);
        waitCommand();
        assertEquals(ClientOperation.LOGIN_ANSWER, receivedCommand);
        assertEquals(true, receivedBoolean);
    }

    @Test(timeout = 5000)
    public void ping() throws IOException {
        serverSender.sendPing();
        waitCommand();
        assertEquals(ClientOperation.PING, receivedCommand);
    }

    @Test(timeout = 5000)
    public void endGame() throws IOException {
        HashMap<PlayerIdentifier, Integer> scores = new HashMap<PlayerIdentifier, Integer>();
        scores.put(new PlayerIdentifier(), 7);
        scores.put(new PlayerIdentifier(), 5);
        serverSender.sendEndGame(scores);
        waitCommand();
        assertEquals(ClientOperation.END_GAME, receivedCommand);
        assertEquals(scores, receivedPlayerIntegerMap);
    }

    @Test(timeout = 5000)
    public void playerLeft() throws IOException {
        PlayerIdentifier who = new PlayerIdentifier();
        serverSender.sendPlayerLeft(who, true);
        waitCommand();
        assertEquals(ClientOperation.PLAYER_LEFT, receivedCommand);
        assertEquals(who, receivedPlayer);
        assertEquals(true, receivedBoolean);
    }

    @Test(timeout = 5000)
    public void playerJoined() throws IOException {
        PlayerIdentifier who = new PlayerIdentifier();
        serverSender.sendPlayerJoined(who);
        waitCommand();
        assertEquals(ClientOperation.PLAYER_JOINED, receivedCommand);
        assertEquals(who, receivedPlayer);
    }

    @Test(timeout = 5000)
    public void abandonComplete() throws IOException {
        serverSender.sendAbandonComplete();
        waitCommand();
        assertEquals(ClientOperation.ABANDON_COMPLETE, receivedCommand);
    }
}
