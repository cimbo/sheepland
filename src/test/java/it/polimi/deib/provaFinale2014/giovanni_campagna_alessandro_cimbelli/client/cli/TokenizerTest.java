package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import org.junit.Test;

public class TokenizerTest {
    @Test
    public void baseTest() throws InvalidInputException {
        Tokenizer tkn = new Tokenizer("a very long command");

        assertEquals("a", tkn.readAny());
        assertEquals("very", tkn.readAny());
        assertEquals("long", tkn.readAny());
        assertEquals("command", tkn.readAny());
        assertFalse(tkn.hasNext());
    }

    @Test
    public void whiteSpaceTest() throws InvalidInputException {
        Tokenizer tkn = new Tokenizer("   a    very \t long    \tcommand    ");

        assertEquals("a", tkn.readAny());
        assertEquals("very", tkn.readAny());
        assertEquals("long", tkn.readAny());
        assertEquals("command", tkn.readAny());
        assertFalse(tkn.hasNext());
    }

    @Test
    public void readComplex() throws InvalidInputException {
        Tokenizer tkn = new Tokenizer(
                "look a female sheep on sheepsburg and shepherd 1 between land/0 and land/1 mountain map");

        assertTrue(tkn.readCommand() instanceof LookCommand);
        tkn.expect("a");
        assertEquals(SheepType.FEMALE_SHEEP, tkn.readSheepType());
        tkn.expect("on");
        assertEquals(new SheepTileIdentifier(0), tkn.readSheepTile());
        tkn.expect("and");
        tkn.expect("shepherd");
        assertEquals(new ShepherdIdentifier(0), tkn.readShepherd());
        assertEquals(new RoadSquareIdentifier(1 + 3 * 0 + 0, 1 + 3 * 0 + 1),
                tkn.readRoadSquare());
        assertEquals(TerrainType.MOUNTAIN, tkn.readTerrainType());
        assertEquals(LookSubCommand.MAP, tkn.readLookSubCommand());
    }

    @Test
    public void allSheepTypes() throws InvalidInputException {
        for (SheepType type : SheepType.values()) {
            String name = type.name().toLowerCase().replace("_", " ");
            assertEquals(type, new Tokenizer(name).readSheepType());
        }
    }

    @Test
    public void allTerrainTypes() throws InvalidInputException {
        for (TerrainType type : TerrainType.values()) {
            String name = type.name().toLowerCase().replace("_", " ");
            assertEquals(type, new Tokenizer(name).readTerrainType());
        }
    }

    @Test
    public void allCommands() throws InvalidInputException,
            RuleViolationException, InvalidIdentifierException {
        FakeCLIUIManager fakeCli = new FakeCLIUIManager(new ArrayLineReader());
        Tokenizer helpTkn = new Tokenizer("help");
        BaseCommand helpCmd = helpTkn.readCommand();
        helpCmd.parseAndDo(helpTkn, fakeCli);

        String help = fakeCli.getOutput();

        for (String helpLine : help.split("\n")) {
            if (helpLine.startsWith("[") || helpLine.startsWith("Available")
                    || helpLine.trim().isEmpty()) {
                continue;
            }

            String cmd = helpLine.split("-")[0];
            Tokenizer tkn = new Tokenizer(cmd);
            assertTrue(tkn.readCommand() instanceof BaseCommand);
        }
    }

    @Test(expected = InvalidInputException.class)
    public void invalidCommand() throws InvalidInputException {
        new Tokenizer("foo").readCommand();
    }

    @Test(expected = InvalidInputException.class)
    public void invalidSheepTileCode() throws InvalidInputException {
        new Tokenizer("desert/7").readSheepTile();
    }

    @Test(expected = InvalidInputException.class)
    public void invalidRoadSquare() throws InvalidInputException {
        new Tokenizer("between sheepsburg or desert/0").readSheepTile();
    }

    @Test(expected = InvalidInputException.class)
    public void invalidLookSubCommand() throws InvalidInputException {
        new Tokenizer("help").readLookSubCommand();
    }

    @Test(expected = InvalidInputException.class)
    public void invalidShepherd() throws InvalidInputException {
        new Tokenizer("foo").readShepherd();
    }

    @Test(expected = InvalidInputException.class)
    public void expectationFailed() throws InvalidInputException {
        new Tokenizer("something").expect("something_else");
    }
}
