package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyTerrainCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ForceEndTurnMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.InitiallyPlaceShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.KillSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.SellMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ClientGameTest {
    private int pending;
    private int count;
    private String sentUsername;
    private String sentPassword;
    private boolean expectTurn;
    private Sheep expectEatenSheep;
    private SheepTileIdentifier expectWolfFrom;
    private boolean expectLeave;
    private boolean expectClean;

    private synchronized void simpleComplete() {
        if (pending <= 0) {
            fail();
        }
        pending--;
        notify();
    }

    private class TestClientNetwork extends FakeClientNetwork {
        @Override
        public void sendLoginToServer(String username, String password) {
            simpleComplete();
        }

        @Override
        public void sendForceSynchronizeMe() {
            fail();
        }

        @Override
        public void close() {
            simpleComplete();
            super.close();
        }
    }

    @Before
    public void setUp() {
        pending = 0;
        count = 0;
        sentUsername = null;
        sentPassword = null;
        expectTurn = false;
        expectEatenSheep = null;
        expectWolfFrom = null;
        expectLeave = false;
        expectClean = false;
    }

    private void closeGame(final Game game, int expected) {
        pending = expected;
        // abandon MUST be called from the UI thread, otherwise
        // it races with the abandonComplete task queued by
        // NetworkReceiver, which closes the connection (and
        // may close the connection before abandon() has a chance
        // to flush it
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                game.abandon(true);
            }
        });
        waitDone();
    }

    private synchronized void waitDone() {
        while (pending > 0) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    @Test
    public void loginTest() {
        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendLoginToServer(String username, String password) {
                sentUsername = username;
                sentPassword = password;
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void loginSuccess() {
                simpleComplete();
            }

            @Override
            public void loginFailed() {
                fail();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();
        assertEquals("marco", sentUsername);
        assertEquals("polo", sentPassword);

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        pending = 1;
        receiver.loginAnswer(true);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void failedLogin() {
        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendLoginToServer(String username, String password) {
                synchronized (ClientGameTest.this) {
                    if (pending <= 0) {
                        fail();
                    }
                    sentUsername = username;
                    sentPassword = password;
                    pending--;
                    ClientGameTest.this.notify();
                }
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void loginSuccess() {
                fail();
            }

            @Override
            public void loginFailed() {
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();
        assertEquals("marco", sentUsername);
        assertEquals("polo", sentPassword);

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        pending = 1;
        receiver.loginAnswer(false);
        waitDone();

        pending = 1;
        game.reportError(new IOException("Close yourself"));
        waitDone();
    }

    @Test(timeout = 5000)
    public void setupTest() {
        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void initialize() {
                simpleComplete();
            }

            @Override
            public void setYourTurn(boolean myTurn) {
                assertEquals(expectTurn, myTurn);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });

        pending = 2;
        expectTurn = true;
        receiver.setup(rules, board, marco.getId());
        waitDone();
        assertEquals(GamePhase.INIT, game.getGamePhase());
        assertEquals(rules, game.getRules());
        assertEquals(marco.getId(), game.getSelfPlayer().getId());
        assertArrayEquals(board.getPlayingOrder(), game.getAllPlayers());
        assertEquals(board, game.getGameBoard());

        expectTurn = false;
        // setyourturn + closed
        // closeGame(game, 2);
    }

    @Test(timeout = 5000)
    public void forceSyncTest() {
        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void initialize() {
                fail();
            }

            @Override
            public void setYourTurn(boolean myTurn) {
                assertEquals(expectTurn, myTurn);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        GameState state = new GameState();
        state.advanceInitState();
        state.advancePlayState();

        pending = 2;
        expectTurn = true;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();
        assertEquals(GamePhase.PLAY, game.getGamePhase());

        expectTurn = false;
        // setyourturn + closed
        closeGame(game, 2);
    }

    @Test(timeout = 5000)
    public void forceSyncAfterSetupTest() {
        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void initialize() {
                simpleComplete();
            }

            @Override
            public void setYourTurn(boolean myTurn) {
                assertEquals(expectTurn, myTurn);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        GameState state = new GameState();
        state.advanceInitState();
        state.advancePlayState();

        pending = 2;
        expectTurn = true;
        receiver.setup(rules, board, marco.getId());
        waitDone();
        assertEquals(GamePhase.INIT, game.getGamePhase());

        pending = 2;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();
        assertEquals(GamePhase.PLAY, game.getGamePhase());

        expectTurn = false;
        // setyourturn + closed
        closeGame(game, 2);
    }

    @Test(timeout = 5000)
    public void endGameTest() {
        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void initialize() {
                simpleComplete();
            }

            @Override
            public void setYourTurn(boolean myTurn) {
                simpleComplete();
            }

            @Override
            public void gameOver(Map<PlayerIdentifier, Integer> scores) {
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });

        pending = 2;
        receiver.setup(rules, board, marco.getId());
        waitDone();

        assertEquals(GamePhase.INIT, game.getGamePhase());
        game.getEngine().getGameState().advancePlayState();
        game.getEngine().getGameState().advanceFinalState();
        game.getEngine().getGameState().advanceMarketSellState();
        game.getEngine().getGameState().advanceMarketBuyState();

        pending = 3; // setYourTurn + gameOver + closed
        receiver.endGame(new HashMap<PlayerIdentifier, Integer>());
        waitDone();

        assertEquals(GamePhase.END, game.getGamePhase());
    }

    @Test(timeout = 5000)
    public void moveAck() {
        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void setWaiting(boolean waiting) {
                simpleComplete();
            }

            @Override
            public void reportError(String error) {
                fail();
            }

        };

        pending = 2;
        Game game = new Game(tcn, ui);
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        pending = 2;
        receiver.moveAcknowledged(null);
        waitDone();

        // 2*setwaiting + closed
        closeGame(game, 3);
    }

    @Test(timeout = 5000)
    public void moveFail() {
        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendForceSynchronizeMe() {
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void setWaiting(boolean waiting) {
                simpleComplete();
            }

            @Override
            public void reportError(String error) {
                simpleComplete();
            }

        };

        pending = 3; // 2*setWaiting + loginToServer
        Game game = new Game(tcn, ui);
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        pending = 4; // 2*reportError + setWaiting + forceSyncMe
        receiver.moveAcknowledged(new Exception("Test"));
        waitDone();

        // 2*setwaiting + closed
        closeGame(game, 3);
    }

    @Test(timeout = 5000)
    public void advanceNextTurn() {
        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void setYourTurn(boolean yourTurn) {
                assertEquals(expectTurn, yourTurn);
                simpleComplete();
            }

        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });

        pending = 1; // setYourTurn
        expectTurn = true;
        receiver.setup(rules, board, marco.getId());
        waitDone();

        pending = 1;
        expectTurn = false;
        receiver.advanceNextTurn(1, GamePhase.INIT);
        waitDone();

        // setyourturn + closed
        closeGame(game, 2);
    }

    @Test(timeout = 5000)
    public void moveBlackSheepWolfAndLambs() throws InvalidIdentifierException,
            ServerException {
        // land/2
        final SheepTileIdentifier blackSheepPosition = new SheepTileIdentifier(
                0 * 3 + 2 + 1);
        // desert/0
        final SheepTileIdentifier wolfPosition = new SheepTileIdentifier(
                5 * 3 + 0 + 1);

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void animateBlackSheepMoved(SheepTile from, SheepTile to) {
                assertEquals(TerrainType.SHEEPSBURG, from.getTerrainType());
                assertEquals(blackSheepPosition, to.getId());
                simpleComplete();
            }

            @Override
            public void animateWolfMoved(SheepTile from, SheepTile to,
                    Sheep eatenSheep) {
                assertEquals(expectWolfFrom, from.getId());
                assertEquals(wolfPosition, to.getId());
                assertEquals(expectEatenSheep, eatenSheep);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        Player marco = new Player("marco");
        Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        SheepTile desert0 = board.getMap().getSheepTileFromId(wolfPosition);
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        desert0.addSheep(sheep);
        board.addSheep(new Sheep(SheepType.BLACK_SHEEP));
        board.getMap().moveBlackSheep(board.getBlackSheep(),
                board.getMap().getSheepsburg());
        board.getMap().moveWolf(board.getMap().getSheepsburg());

        receiver.setup(rules, board, marco.getId());

        pending = 1;
        receiver.blackSheepMoved(blackSheepPosition);
        waitDone();

        pending = 1;
        expectWolfFrom = board.getMap().getSheepsburg().getId();
        expectEatenSheep = sheep;
        receiver.wolfMoved(wolfPosition, expectEatenSheep.getId());
        waitDone();

        pending = 1;
        expectWolfFrom = wolfPosition;
        expectEatenSheep = null;
        receiver.wolfMoved(wolfPosition, null);
        waitDone();

        game.didGrowLambs(desert0.getId(), new Sheep[0]);

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteMove() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        // mario must play first, because we're sending a move from
        // him
        board.setPlayingOrder(new Player[] { mario, marco });
        final Shepherd marioShepherd = mario.getAllShepherds()[0];
        // between sheepsburg and land/2
        final RoadSquareIdentifier roadSquare = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(roadSquare);

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void animateShepherdMoved(Player who, Shepherd which,
                    RoadSquare to) {
                assertEquals(mario, who);
                assertEquals(marioShepherd, which);
                assertEquals(roadSquare, to.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        receiver.setup(rules, board, marco.getId());

        pending = 1;
        Move move = new InitiallyPlaceShepherdUIMove(mario.getId(),
                marioShepherd.getId(), roadSquare);
        receiver.didExecuteMove(move);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteNonUIMove() throws InvalidIdentifierException,
            ServerException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        // mario must play first, because we're sending a move from
        // him
        board.setPlayingOrder(new Player[] { mario, marco });
        final Shepherd marioShepherd = mario.getAllShepherds()[0];
        // between sheepsburg and land/2
        final RoadSquareIdentifier roadSquare = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(roadSquare);

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void animateShepherdMoved(Player who, Shepherd which,
                    RoadSquare to) {
                fail();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        game.setup(rules, board, marco.getId());

        Move move = new InitiallyPlaceShepherdMove(mario.getId(),
                marioShepherd.getId(), roadSquare);
        game.didExecuteMove(move);

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteFaultyMove() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        // mario must play first, because we're sending a move from
        // him
        board.setPlayingOrder(new Player[] { mario, marco });
        final Shepherd marioShepherd = mario.getAllShepherds()[0];
        // between sheepsburg and land/2
        final RoadSquareIdentifier roadSquare = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(roadSquare);

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendForceSynchronizeMe() {
                simpleComplete();
            }
        };
        final StringBuilder errorBuilder = new StringBuilder();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void reportError(String message) {
                errorBuilder.append(message);
            }

            @Override
            public void animateShepherdMoved(Player who, Shepherd which,
                    RoadSquare to) {
                fail();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        receiver.setup(rules, board, marco.getId());

        pending = 1;
        // wrong phase, triggers ServerRuleViolationException
        Move move = new MoveShepherdUIMove(mario.getId(),
                marioShepherd.getId(), roadSquare);
        receiver.didExecuteMove(move);
        waitDone();

        assertTrue(errorBuilder.toString().startsWith(
                "Detected unexpected server behavior"));

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteTimeout() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });

        TestClientNetwork tcn = new TestClientNetwork();
        final StringBuilder errorBuilder = new StringBuilder();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void reportError(String message) {
                errorBuilder.append(message);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        receiver.setup(rules, board, marco.getId());

        pending = 1;
        Move move = new ForceEndTurnMove(marco.getId(), true);
        receiver.didExecuteMove(move);
        waitDone();

        assertTrue(errorBuilder.toString().startsWith(
                "The turn was ended by timeout"));

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteMoveComplete() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        final SheepTileIdentifier land2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        final Sheep expectNewSheep = new Lamb(0);
        GameState state = new GameState();
        state.advanceInitState();
        state.advancePlayState();

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void animateBeginSheepMating(Player who, Shepherd which,
                    SheepTile tile) {
                fail();
            }

            @Override
            public void animateEndSheepMating(Player who, Shepherd which,
                    SheepTile tile, Sheep newSheep) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(land2, tile.getId());
                assertEquals(expectNewSheep, newSheep);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        receiver.forceSynchronize(rules, board, marco.getId(), state);

        pending = 1;
        board.addSheep(expectNewSheep);
        Move move = new MateSheepUIMove(marco.getId(), marcoShepherd.getId(),
                land2) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Sheep getResult() {
                return expectNewSheep;
            }
        };
        receiver.didExecuteMove(move);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteMoveMating() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { mario, marco });
        final Shepherd marioShepherd = mario.getAllShepherds()[0];
        final SheepTileIdentifier land2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        final Sheep expectNewSheep = new Lamb(0);
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        final RoadSquareIdentifier land2Sheepshburg = new RoadSquareIdentifier(
                0, 0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepshburg)
                .addShepherd(marioShepherd);
        marioShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepshburg));
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        board.getMap().getSheepTileFromId(land2).addSheep(sheep);
        sheep = new Sheep(SheepType.RAM);
        board.addSheep(sheep);
        board.getMap().getSheepTileFromId(land2).addSheep(sheep);

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void animateBeginSheepMating(Player who, Shepherd which,
                    SheepTile tile) {
                assertEquals(mario, who);
                assertEquals(marioShepherd, which);
                assertEquals(land2, tile.getId());
                simpleComplete();
            }

            @Override
            public void animateEndSheepMating(Player who, Shepherd which,
                    SheepTile tile, Sheep newSheep) {
                assertEquals(mario, who);
                assertEquals(marioShepherd, which);
                assertEquals(land2, tile.getId());
                assertEquals(expectNewSheep, newSheep);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        receiver.forceSynchronize(rules, board, marco.getId(), state);

        pending = 2;
        board.addSheep(expectNewSheep);
        Move move = new MateSheepUIMove(mario.getId(), marioShepherd.getId(),
                land2) {
            private static final long serialVersionUID = 1L;

            @Override
            protected Sheep getResult() {
                return expectNewSheep;
            }
        };
        receiver.didExecuteMove(move);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void didExecuteMoveKilling() throws InvalidIdentifierException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { mario, marco });
        final Shepherd marioShepherd = mario.getAllShepherds()[0];
        Shepherd marcoShepherd = marco.getAllShepherds()[0];
        final SheepTileIdentifier land2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        final RoadSquareIdentifier land2Sheepshburg = new RoadSquareIdentifier(
                0, 0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepshburg)
                .addShepherd(marioShepherd);
        marioShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepshburg));
        RoadSquare desert0Sheepsburg = board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 1 + 5 * 3 + 0));
        desert0Sheepsburg.addShepherd(marcoShepherd);
        marcoShepherd.setCurrentPosition(desert0Sheepsburg);
        final Sheep expectSheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(expectSheep);
        board.getMap().getSheepTileFromId(land2).addSheep(expectSheep);
        final HashMap<Player, Integer> expectPaidPlayers = new HashMap<Player, Integer>();
        expectPaidPlayers.put(marco, 2);

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void animateBeginSheepKilling(Player who, Shepherd which,
                    SheepTile tile, Sheep sheep) {
                assertEquals(mario, who);
                assertEquals(marioShepherd, which);
                assertEquals(land2, tile.getId());
                assertEquals(expectSheep.getId(), sheep.getId());
                simpleComplete();
            }

            @Override
            public void animateEndSheepKilling(Player who, Shepherd which,
                    SheepTile tile, Sheep sheep,
                    Map<Player, Integer> paidPlayers) {
                assertEquals(mario, who);
                assertEquals(marioShepherd, which);
                assertEquals(land2, tile.getId());
                assertEquals(expectSheep.getId(), sheep.getId());
                assertEquals(expectPaidPlayers, paidPlayers);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        receiver.forceSynchronize(rules, board, marco.getId(), state);

        pending = 2;
        Move move = new KillSheepUIMove(mario.getId(), marioShepherd.getId(),
                land2, expectSheep.getId()) {
            private static final long serialVersionUID = 1L;

            @Override
            protected HashMap<Player, Integer> getPaidPlayers(GameEngine engine) {
                return expectPaidPlayers;
            }
        };
        receiver.didExecuteMove(move);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeInitiallyPlaceMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.prepareNextTurn();
        final RoadSquareIdentifier land2Sheepsburg = new RoadSquareIdentifier(
                0, 0 * 3 + 2 + 1);
        final Move expectMove = new InitiallyPlaceShepherdMove(marco.getId(),
                marcoShepherd.getId(), land2Sheepsburg);

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateShepherdMoved(Player who, Shepherd which,
                    RoadSquare to) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(land2Sheepsburg, to.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.initiallyPlaceShepherd(marcoShepherd, board.getMap()
                .getRoadSquareFromId(land2Sheepsburg));
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeMoveShepherdMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        RoadSquareIdentifier land2Sheepsburg = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepsburg)
                .addShepherd(marcoShepherd);
        marcoShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepsburg));
        final RoadSquareIdentifier land1land2 = new RoadSquareIdentifier(
                0 * 3 + 2 + 1, 0 * 3 + 1 + 1);

        final Move expectMove = new MoveShepherdMove(marco.getId(),
                marcoShepherd.getId(), land1land2);

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateShepherdMoved(Player who, Shepherd which,
                    RoadSquare to) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(land1land2, to.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.moveShepherd(marcoShepherd,
                board.getMap().getRoadSquareFromId(land1land2));
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeMoveSheepMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        final SheepTileIdentifier land2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        RoadSquareIdentifier land2Sheepshburg = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepshburg)
                .addShepherd(marcoShepherd);
        marcoShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepshburg));
        final Sheep expectSheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(expectSheep);
        board.getMap().getSheepTileFromId(land2).addSheep(expectSheep);
        final Move expectMove = new MoveSheepMove(marco.getId(),
                marcoShepherd.getId(), land2, board.getMap().getSheepsburg()
                        .getId(), expectSheep.getId());

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateSheepMoved(Player who, Shepherd which,
                    SheepTile from, SheepTile to, Sheep sheep) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(land2, from.getId());
                assertEquals(new SheepTileIdentifier(0), to.getId());
                assertEquals(expectSheep.getId(), sheep.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.moveSheep(marcoShepherd, board.getMap().getSheepTileFromId(land2),
                board.getMap().getSheepsburg(), expectSheep);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeBuyTerrainCardMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        RoadSquareIdentifier land2Sheepshburg = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepshburg)
                .addShepherd(marcoShepherd);
        marcoShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepshburg));
        final TerrainCard expectCard = board.getCheapestTerrainCard(
                TerrainType.LAND, null);
        final Move expectMove = new BuyTerrainCardMove(marco.getId(),
                marcoShepherd.getId(), expectCard.getId());

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateBoughtTerrainCard(Player who, Shepherd which,
                    TerrainCard card) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(expectCard.getId(), card.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.buyTerrainCard(marcoShepherd, expectCard);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeMateSheepMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        final SheepTileIdentifier land2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        final RoadSquareIdentifier land2Sheepshburg = new RoadSquareIdentifier(
                0, 0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepshburg)
                .addShepherd(marcoShepherd);
        marcoShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepshburg));
        Sheep sheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(sheep);
        board.getMap().getSheepTileFromId(land2).addSheep(sheep);
        sheep = new Sheep(SheepType.RAM);
        board.addSheep(sheep);
        board.getMap().getSheepTileFromId(land2).addSheep(sheep);
        final Move expectMove = new MateSheepUIMove(marco.getId(),
                marcoShepherd.getId(), land2);

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateBeginSheepMating(Player who, Shepherd which,
                    SheepTile tile) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(land2, tile.getId());
                simpleComplete();
            }

            @Override
            public void animateEndSheepMating(Player who, Shepherd which,
                    SheepTile tile, Sheep newSheep) {
                fail();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.mateSheep(marcoShepherd, board.getMap().getSheepTileFromId(land2));
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeKillSheepMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        final Shepherd marcoShepherd = marco.getAllShepherds()[0];
        Shepherd marioShepherd = mario.getAllShepherds()[0];
        final SheepTileIdentifier land2 = new SheepTileIdentifier(0 * 3 + 2 + 1);
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.prepareNextTurn();
        RoadSquareIdentifier land2Sheepshburg = new RoadSquareIdentifier(0,
                0 * 3 + 2 + 1);
        board.getMap().getRoadSquareFromId(land2Sheepshburg)
                .addShepherd(marcoShepherd);
        marcoShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                land2Sheepshburg));
        marioShepherd.setCurrentPosition(board.getMap().getRoadSquareFromId(
                new RoadSquareIdentifier(0, 1 + 5 * 3 + 0)));
        final Sheep expectSheep = new Sheep(SheepType.FEMALE_SHEEP);
        board.addSheep(expectSheep);
        board.getMap().getSheepTileFromId(land2).addSheep(expectSheep);
        final Move expectMove = new KillSheepMove(marco.getId(),
                marcoShepherd.getId(), land2, expectSheep.getId());

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateBeginSheepKilling(Player who, Shepherd which,
                    SheepTile tile, Sheep sheep) {
                assertEquals(marco, who);
                assertEquals(marcoShepherd, which);
                assertEquals(land2, tile.getId());
                assertEquals(expectSheep.getId(), sheep.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.killSheep(marcoShepherd, board.getMap().getSheepTileFromId(land2),
                expectSheep);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeSellMarketCardMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.advanceMarketSellState();
        state.prepareNextTurn();
        final TerrainCard expectCard = board.getCheapestTerrainCard(
                TerrainType.LAND, null);
        marco.acquireCard(expectCard);
        expectCard.markAcquired(marco);
        board.markCardAcquired(expectCard);
        final Move expectMove = new SellMarketCardMove(marco.getId(),
                expectCard.getId(), 3);

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateSoldMarketCard(Player who, TerrainCard card,
                    int cost) {
                assertEquals(marco, who);
                assertEquals(expectCard.getId(), card.getId());
                assertEquals(3, cost);
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.sellMarketCard(expectCard, 3);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void executeBuyMarketCardMove() throws InvalidIdentifierException,
            RuleViolationException {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });
        GameState state = new GameState();
        state.setRules(rules);
        state.advanceInitState();
        state.advancePlayState();
        state.advanceMarketSellState();
        state.advanceMarketBuyState();
        state.prepareNextTurn();
        final TerrainCard expectCard = board.getCheapestTerrainCard(
                TerrainType.LAND, null);
        mario.acquireCard(expectCard);
        expectCard.markAcquired(mario);
        board.markCardAcquired(expectCard);
        expectCard.markForSale(3);
        board.markCardForSale(expectCard);
        final Move expectMove = new BuyMarketCardMove(marco.getId(),
                expectCard.getId());

        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendExecuteMove(Move move) {
                assertEquals(expectMove, move);
                simpleComplete();
            }
        };
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void forceSynchronize() {
                simpleComplete();
            }

            @Override
            public void animateBoughtMarketCard(Player who, TerrainCard card) {
                assertEquals(marco, who);
                assertEquals(expectCard.getId(), card.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        // we use a forcesync instead of a setup because it's
        // a simple way to drop into PLAY instead of INIT
        pending = 1;
        receiver.forceSynchronize(rules, board, marco.getId(), state);
        waitDone();

        pending = 2;
        game.buyMarketCard(expectCard);
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void pingTest() {
        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public void sendPong() {
                simpleComplete();
            }
        };

        Game game = new Game(tcn, new EmptyClientUI());

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        pending = 1;
        receiver.ping();
        waitDone();

        closeGame(game, 1);
    }

    @Test(timeout = 5000)
    public void playerLeftJoined() {
        RuleSet rules = new RuleSet();
        rules.seal();
        GameBoard board = new GameBoard();
        final Player marco = new Player("marco");
        final Player mario = new Player("mario");
        board.addPlayer(marco);
        board.addPlayer(mario);
        board.initializeForRules(rules);
        board.setPlayingOrder(new Player[] { marco, mario });

        TestClientNetwork tcn = new TestClientNetwork();
        AbstractUI ui = new EmptyClientUI() {
            @Override
            public void playerLeft(Player who, boolean clean) {
                assertTrue(expectLeave);
                assertEquals(clean, expectClean);
                assertEquals(mario.getId(), who.getId());
                simpleComplete();
            }

            @Override
            public void playerJoined(Player who) {
                assertFalse(expectLeave);
                assertEquals(mario.getId(), who.getId());
                simpleComplete();
            }
        };

        Game game = new Game(tcn, ui);

        pending = 1;
        game.joinGame("marco", "polo");
        waitDone();

        ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        receiver.setup(rules, board, marco.getId());

        pending = 1;
        expectLeave = true;
        expectClean = false;
        receiver.playerLeft(mario.getId(), expectClean);
        waitDone();

        pending = 1;
        expectLeave = false;
        receiver.playerJoined(mario.getId());
        waitDone();

        pending = 1;
        expectLeave = true;
        expectClean = true;
        receiver.playerLeft(mario.getId(), expectClean);
        waitDone();

        closeGame(game, 1);
    }

    @Test
    public void reconnectionTimer() {
        TestClientNetwork tcn = new TestClientNetwork() {
            @Override
            public ClientConnection connectToServer() throws IOException {
                synchronized (ClientGameTest.this) {
                    count++;
                    simpleComplete();
                }
                return super.connectToServer();
            }
        };
        AbstractUI ui = new EmptyClientUI();

        Game game = new Game(tcn, ui);

        pending = 2; // connect+loginToServer
        game.joinGame("marco", "polo");
        waitDone();

        final ClientReceiver receiver = tcn.getClientReceiver();
        assertNotNull(receiver);

        pending = 3; // close+connect+loginToServer again
        receiver.connectionClosed();
        waitDone();

        assertEquals(2, count);

        closeGame(game, 1);
    }
}
