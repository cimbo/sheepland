package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A interface for an item queued from the network input thread(s) to the UI
 * (AWT) thread.
 * 
 * @author Giovanni Campagna
 */
interface ServerExceptionRunnable {
    /**
     * Run this queued item.
     */
    void run() throws ServerException;
}

/**
 * This is the client side processing layer from messages from the server. It
 * marshals them into calls to Game from the UI thread.
 * 
 * This object is thread safe.
 * 
 * @author Giovanni Campagna
 * 
 */
public class NetworkReceiver implements ClientReceiver {
    private final Game game;

    /**
     * Construct a new NetworkReceiver for the given Game.
     * 
     * @param g
     *            the game
     */
    public NetworkReceiver(Game g) {
        game = g;
    }

    private void queue(final ServerExceptionRunnable runnable) {
        EventQueue.invokeLater(new Runnable() {
            /**
             * {@inheritDoc}
             */
            public void run() {
                assert EventQueue.isDispatchThread();

                try {
                    runnable.run();
                } catch (ServerException exc) {
                    Logger.getGlobal().log(Level.INFO,
                            "Server exception in server command", exc);

                    game.reportError(exc);
                } catch (Exception exc) {
                    Logger.getGlobal().log(Level.WARNING,
                            "Unexpected exception in server command", exc);

                    game.reportError(new ServerException(exc));
                }
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void setup(final RuleSet rules, final GameBoard board,
            final PlayerIdentifier self) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.setup(rules, board, self);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void moveAcknowledged(final Exception exception) {
        if (exception == null) {
            queue(new ServerExceptionRunnable() {
                /**
                 * {@inheritDoc}
                 */
                public void run() throws ServerException {
                    game.moveAcknowledged();
                }
            });
        } else {
            queue(new ServerExceptionRunnable() {
                /**
                 * {@inheritDoc}
                 */
                public void run() throws ServerException {
                    game.moveFailed(exception);
                }
            });
        }
    }

    /**
     * {@inheritDoc}
     */
    public void forceSynchronize(final RuleSet rules, final GameBoard newBoard,
            final PlayerIdentifier newSelf, final GameState newState) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.forceSynchronize(rules, newBoard, newSelf, newState);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void advanceNextTurn(final int turn, final GamePhase phase) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.advanceNextTurn(turn, phase);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void blackSheepMoved(final SheepTileIdentifier destination) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.didMoveBlackSheep(destination);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void wolfMoved(final SheepTileIdentifier destination,
            final SheepIdentifier eatenSheep) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.didMoveWolf(destination, eatenSheep);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void lambsGrew(final SheepTileIdentifier where,
            final Sheep[] newAdultSheep) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.didGrowLambs(where, newAdultSheep);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void didExecuteMove(final Move move) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.didExecuteMove(move);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void connectionClosed() {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() {
                game.reportError(new IOException("Connection reset by peer"));
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void loginAnswer(final boolean answer) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() {
                game.loginAnswer(answer);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void ping() {
        game.didPing();
    }

    /**
     * {@inheritDoc}
     */
    public void endGame(final Map<PlayerIdentifier, Integer> scores) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() {
                game.endGame(scores);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void playerLeft(final PlayerIdentifier who, final boolean clean) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.playerLeft(who, clean);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void playerJoined(final PlayerIdentifier who) {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws ServerException {
                game.playerJoined(who);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void abandonComplete() {
        queue(new ServerExceptionRunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() {
                game.abandonComplete();
            }
        });
    }
}
