package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * This class represents a "initially place shepherd" move. See the base class
 * {@link Move} for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class InitiallyPlaceShepherdMove extends ShepherdMove {
    private static final long serialVersionUID = 1L;

    private final RoadSquareIdentifier toId;

    /**
     * Construct a new "initially place shepherd" move
     * 
     * @param who
     *            the player executing the move
     * @param which
     *            the shepherd he is moving
     * @param to
     *            the chosen square for the shepherd
     */
    public InitiallyPlaceShepherdMove(PlayerIdentifier who,
            ShepherdIdentifier which, RoadSquareIdentifier to) {
        super(who, which);
        toId = to;
    }

    /**
     * Retrieve the actual road square object, by resolving the identifier
     * through the passed in engine.
     * 
     * @param engine
     * @return the road square as game object
     * @throws InvalidIdentifierException
     */
    protected RoadSquare getTo(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getRoadSquareFromId(toId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.initiallyPlaceShepherd(getWho(engine), getWhich(engine),
                getTo(engine));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + toId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof InitiallyPlaceShepherdMove)) {
            return false;
        }
        InitiallyPlaceShepherdMove other = (InitiallyPlaceShepherdMove) obj;
        if (!toId.equals(other.toId)) {
            return false;
        }
        return true;
    }
}
