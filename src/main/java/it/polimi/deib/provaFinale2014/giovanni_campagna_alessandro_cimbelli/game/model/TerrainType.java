package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

/* If you change the order of the items here, I
 * will come and kill you.
 * 
 * - Giovanni
 */
/**
 * The type of a terrain (sheep tile), and also the type of a card
 * 
 * @author Giovanni Campagna
 */
public enum TerrainType {

    LAND, FOREST, WATER, GRAIN, MOUNTAIN, DESERT,

    SHEEPSBURG;

    /**
     * The number of terrain types (excluding Sheepsburg)
     */
    public static final int NUMBER_OF_TERRAINS = 6;
}
