package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

/**
 * The current phase of the game.
 * 
 * @author Giovanni Campagna
 * 
 */
public enum GamePhase {
    /**
     * Before the game has started (players are still joining in). At this
     * point, players don't have a RuleSet yet, so it's invalid to call most
     * methods on the model.
     */
    PREINIT,
    /**
     * Initial setup (players are choosing the initial position).
     */
    INIT,
    /**
     * Normal play state (you move your shepherd, move sheeps, place fences,
     * etc.)
     */
    PLAY,
    /**
     * Final play state (you place final fences, until all players have had an
     * equal number of rounds)
     */
    FINAL,
    /**
     * Market move: selling phase
     */
    MARKET_SELL,
    /**
     * Market move: buying phase
     */
    MARKET_BUY,
    /**
     * Game is over, score is computed
     */
    END;

    /**
     * @return true if this is a playing state (PLAY or FINAL), false otherwise
     */
    public boolean isPlaying() {
        return this == PLAY || this == FINAL;
    }

    /**
     * @return true if this is a market state (sell or buy), false otherwise
     */
    public boolean isMarket() {
        return this == MARKET_SELL || this == MARKET_BUY;
    }
}
