package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

/**
 * An icon with a number of animals.
 * 
 * @author Alessandro Cimbelli
 */
public class NumberOfAnimal extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 10;
    private static final int Y_SIZE = 10;

    /**
     * Construct a new icon, with the given path
     * 
     * @param path
     *            the image resource path
     */
    public NumberOfAnimal(String path) {
        super(X_SIZE, Y_SIZE, path);
    }
}
