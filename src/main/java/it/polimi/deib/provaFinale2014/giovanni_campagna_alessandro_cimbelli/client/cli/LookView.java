package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.playerToString;
import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.roadSquareToString;
import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.sheepTileToString;
import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.sheepTypeToString;
import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.shepherdToString;
import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.terrainTypeToString;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A collection of helpers for implementing the look command.
 * 
 * @author Giovanni Campagna
 * 
 */
public class LookView {
    private LookView() {
    }

    /**
     * Look around, implementing the generic "look" command
     * 
     * @param ui
     *            the UI manager on which output should happen
     */
    public static void lookAround(CLIUIManager ui) {
        Shepherd shepherd = ui.getChosenShepherd();
        lookAround(ui, shepherd);
    }

    private static void lookAround(CLIUIManager ui, Shepherd shepherd) {
        if (shepherd != null) {
            if (shepherd.isPositioned()) {
                ui.showMessage("You are %s, and you are %s.",
                        shepherdToString(shepherd),
                        roadSquareToString(shepherd.getCurrentPosition()));
            } else {
                ui.showMessage(
                        "You are %s, and you are currently floating in the air waiting to reach the map.",
                        shepherdToString(shepherd));
                if (GameOptions.get().getShowHints()) {
                    ui.showMessage("[Use the jump command to place your shepherd]");
                }
            }
        } else {
            ui.showMessage("You are a proud farm owner in the land of Sheepland.");
            if (GameOptions.get().getShowHints()) {
                ui.showMessage("[Use the be command to switch to a shepherd]");
            }
        }
    }

    private static void lookInventory(CLIUIManager ui, Player self,
            RuleSet rules) {
        TerrainType[] allTypes = TerrainType.values();

        ui.showMessage("Your bag contains:");
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            int cards = self.getNumberOfCards(allTypes[i]);
            if (cards > 0) {
                ui.showMessage("- %d cards of %s", cards, allTypes[i].name()
                        .toLowerCase());
            }
        }

        int coins = self.getRemainingCoins();
        if (coins == 0) {
            ui.showMessage("Your wallet is empty.");
        } else {
            String egg;
            if (coins > rules.getInitialCoinsPerPlayer()) {
                egg = " Ice cream, maybe?";
            } else {
                egg = "";
            }

            ui.showMessage("Your wallet contains %d sheepcoins.%s", coins, egg);
        }
    }

    private static void lookSheepTile(CLIUIManager ui, SheepTile tile,
            boolean brief) {
        boolean emptyTile = true;
        SheepType[] allTypes = SheepType.values();
        StringBuilder tileContents = new StringBuilder();

        for (SheepType allType : allTypes) {
            int count = tile.getNumberOfSheep(allType);

            if (count > 0) {
                if (emptyTile) {
                    emptyTile = false;
                } else {
                    tileContents.append(", ");
                }

                tileContents.append(count + " " + sheepTypeToString(allType));
            }
        }
        boolean hasWolf = tile.getHasWolf();
        if (hasWolf) {
            if (emptyTile) {
                emptyTile = false;
            } else {
                tileContents.append(", ");
            }

            tileContents.append("a wolf");
        }

        if (emptyTile) {
            if (brief) {
                ui.showMessage("It's empty.");
            } else {
                ui.showMessage("%s is empty.", sheepTileToString(tile));
            }
        } else {
            if (brief) {
                ui.showMessage("It contains %s.", tileContents);
            } else {
                ui.showMessage("On %s you see %s.", sheepTileToString(tile),
                        tileContents);
            }
        }
    }

    private static void lookShepherd(CLIUIManager ui, Player who,
            Shepherd shepherd, GameMap map) {
        if (shepherd == null) {
            boolean any = false;
            for (Shepherd s : who.getAllShepherds()) {
                if (s.isPositioned()) {
                    RoadSquare rs = s.getCurrentPosition();

                    ui.showMessage("You see your %s %s.", shepherdToString(s),
                            roadSquareToString(rs));
                    any = true;
                }
            }

            if (!any) {
                ui.showMessage("All your shepherds are floating, there is nothing to look at.");
            }
        } else {
            lookAround(ui, shepherd);
            if (!shepherd.isPositioned()) {
                return;
            }

            RoadSquare currentPosition = shepherd.getCurrentPosition();
            ui.showMessage("The road you are standing on appears to be a %s.",
                    currentPosition.getDiceValue());
            SheepTile[] tiles = map.getIncidentTiles(currentPosition);
            ui.showMessage("On your left, you see %s.",
                    sheepTileToString(tiles[0]));
            lookSheepTile(ui, tiles[0], true);
            ui.showMessage("On your right, you see %s.",
                    sheepTileToString(tiles[1]));
            lookSheepTile(ui, tiles[1], true);
        }
    }

    private static void lookMap(CLIUIManager ui, Player self, GameMap map)
            throws InvalidIdentifierException {
        // small hack
        for (int i = 0; i < 19; i++) {
            SheepTile tile = map.getSheepTileFromId(new SheepTileIdentifier(i));
            lookSheepTile(ui, tile, false);

            for (int j = i + 1; j < 19; j++) {
                try {
                    RoadSquareIdentifier rsId = new RoadSquareIdentifier(i, j);
                    RoadSquare rs = map.getRoadSquareFromId(rsId);
                    if (rs.getHasFence()) {
                        ui.showMessage("The road %s is blocked with a fence.",
                                roadSquareToString(rs));
                    } else {
                        Shepherd shepherd = rs.getCurrentShepherd();

                        if (shepherd != null) {
                            Player owner = shepherd.getOwner();

                            if (owner == self) {
                                ui.showMessage("Your %s is sleeping %s.",
                                        shepherdToString(shepherd),
                                        roadSquareToString(rs));
                            } else {
                                ui.showMessage(
                                        "Enemy %s, working for %s, is sleeping %s.",
                                        shepherdToString(shepherd),
                                        playerToString(owner),
                                        roadSquareToString(rs));
                            }
                        }
                    }
                } catch (InvalidIdentifierException e) {
                    Logger.getGlobal().log(
                            Level.FINEST,
                            String.format("Road square %d-%d does not exist",
                                    i, j), e);
                }
            }
        }
    }

    private static void lookEmporium(CLIUIManager ui, GameBoard board,
            RuleSet rules, GamePhase phase) {
        ui.showMessage("At the emporium, you find these items for sale:");

        TerrainType[] allTypes = TerrainType.values();
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            int remaining = board.getNumberOfTerrainCards(allTypes[i]);
            if (remaining > 0) {
                int cost = rules.getBuyableCards() - remaining;
                if (cost > 0) {
                    ui.showMessage("- a card of %s, for %d sheepcoins",
                            terrainTypeToString(allTypes[i]), cost);
                } else {
                    ui.showMessage("- a card of %s, for free",
                            terrainTypeToString(allTypes[i]));
                }
            }
        }

        int fences;
        if (phase == GamePhase.FINAL) {
            fences = board.getRemainingFinalFences();
        } else {
            fences = board.getRemainingFences();
        }
        ui.showMessage("- a stack of %d fences, for free", fences);
    }

    private static void lookOpponent(CLIUIManager ui, RuleSet rules,
            Player player) {
        ui.showMessage("You see enemy %s in the distance.",
                playerToString(player));
        int numberOfShepherd = rules.getNumberOfShepherdsPerPlayer();

        for (int i = 0; i < numberOfShepherd; i++) {
            ShepherdIdentifier shepherdId = new ShepherdIdentifier(i);

            try {
                Shepherd shepherd = player.getShepherdFromId(shepherdId);

                if (shepherd.isPositioned()) {
                    ui.showMessage("His %s is %s.",
                            shepherdToString(shepherdId),
                            roadSquareToString(shepherd.getCurrentPosition()));
                } else {
                    ui.showMessage("His %s is floating in the air right now.",
                            shepherdToString(shepherdId));
                }
            } catch (InvalidIdentifierException e) {
                Logger.getGlobal().log(Level.WARNING,
                        "Invalid identifier exception in look command", e);

                ui.showMessage(
                        "You also try to look at %s, but you fail. This is not a good omen.",
                        shepherdToString(shepherdId));
            }
        }

        TerrainType[] allTypes = TerrainType.values();

        StringBuilder bagBuilder = new StringBuilder();
        boolean emptyBag = true;
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            int cards = player.getNumberOfCards(allTypes[i]);
            if (cards > 0) {
                if (emptyBag) {
                    emptyBag = false;
                }
                bagBuilder.append(String.format("- %d cards of %s\n", cards,
                        allTypes[i].name().toLowerCase()));
            }
        }

        StringBuilder bagLineBuilder = new StringBuilder(
                "Using your telepathic powers, you see ");
        if (emptyBag) {
            bagLineBuilder.append("his bag is empty.");
            ui.showMessage(bagLineBuilder);
        } else {
            bagLineBuilder.append("in his bag he has:");
            ui.showMessage(bagLineBuilder);
            ui.showMessage(bagBuilder.toString().trim());
        }

        int coins = player.getRemainingCoins();
        if (coins > 0) {
            ui.showMessage("His wallet contains %d sheepcoins.", coins);
        } else {
            ui.showMessage("His wallet is empty.");
        }
    }

    private static void lookOpponents(CLIUIManager ui, Player[] players,
            Player self, RuleSet rules) {
        for (Player player : players) {
            if (player == self) {
                continue;
            }
            lookOpponent(ui, rules, player);
        }
    }

    private static void lookCardList(CLIUIManager ui, TerrainCard[] cards) {
        for (TerrainCard card : cards) {
            ui.showMessage("- a card of %s for %d sheepcoins",
                    terrainTypeToString(card.getType()), card.getCost());
        }
    }

    private static void lookMarket(CLIUIManager ui, GameBoard board,
            GamePhase phase, Player[] players, Player self) {
        if (!phase.isMarket()) {
            ui.showMessage("You are not at the market now, so you cannot look inside.");
            return;
        }

        TerrainCard[] cards = board.getTerrainCardsOnSale(self);
        if (cards.length == 0) {
            ui.showMessage("You are not selling anything at the market.");
        } else {
            ui.showMessage("You are selling:");
            lookCardList(ui, cards);
        }

        for (Player player : players) {
            if (player == self) {
                continue;
            }

            cards = board.getTerrainCardsOnSale(player);
            if (cards.length != 0) {
                ui.showMessage("Enemy %s is selling:", playerToString(player));
                lookCardList(ui, cards);
            }
        }
    }

    private static void lookMapCommand(CLIUIManager ui, Game game,
            Tokenizer tokenizer) throws InvalidIdentifierException,
            InvalidInputException {
        if (tokenizer.hasNext()) {
            lookSheepTile(ui,
                    game.getSheepTileFromId(tokenizer.readSheepTile()), false);
        } else {
            lookMap(ui, game.getSelfPlayer(), game.getGameBoard().getMap());
        }
    }

    /**
     * Implement the "look something" commands, where something is given by the
     * subCommand parameter
     * 
     * @param ui
     *            the UI manager on which output should happen
     * @param tokenizer
     *            the tokenizer used for obtaining further input, if needed
     * @param subCommand
     *            the specific subcommand to implement
     * @throws InvalidInputException
     *             if further input was not recognized
     * @throws InvalidIdentifierException
     *             if the command referenced an object that was not recognized
     */
    public static void lookAround(CLIUIManager ui, Tokenizer tokenizer,
            LookSubCommand subCommand) throws InvalidInputException,
            InvalidIdentifierException {
        Game game = ui.getCurrentGame();
        Shepherd shepherd = ui.getChosenShepherd();

        switch (subCommand) {
        case INVENTORY:
            lookInventory(ui, game.getSelfPlayer(), game.getRules());
            break;
        case SHEPHERD:
            lookShepherd(ui, game.getSelfPlayer(), shepherd, game
                    .getGameBoard().getMap());
            break;
        case MAP:
            lookMapCommand(ui, game, tokenizer);
            break;
        case EMPORIUM:
            lookEmporium(ui, game.getGameBoard(), game.getRules(),
                    game.getGamePhase());
            break;
        case OPPONENTS:
            lookOpponents(ui, game.getAllPlayers(), game.getSelfPlayer(),
                    game.getRules());
            break;
        case MARKET:
            lookMarket(ui, game.getGameBoard(), game.getGamePhase(),
                    game.getAllPlayers(), game.getSelfPlayer());
            break;
        default:
            throw new AssertionError();
        }
    }

}
