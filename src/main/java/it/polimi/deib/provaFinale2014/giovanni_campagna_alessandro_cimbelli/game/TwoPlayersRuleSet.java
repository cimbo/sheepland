package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game;

import java.io.Serializable;

/**
 * A specialized set of rules for games with only two players.
 * 
 * @author Alessandro Cimbelli
 */
public class TwoPlayersRuleSet extends RuleSet implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new ruleset for two players.
     */
    public TwoPlayersRuleSet() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getInitialCoinsPerPlayer() {
        return 30;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNumberOfShepherdsPerPlayer() {
        return 2;
    }
}
