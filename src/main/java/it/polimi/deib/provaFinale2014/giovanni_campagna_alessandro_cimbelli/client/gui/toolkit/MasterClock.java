package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.swing.Timer;

/**
 * The master clock of the sheepland animation framework.
 * 
 * This class is responsible for handling the swing timeouts and updating
 * animations as necessary.
 * 
 * Note that this class is only appropriate if few animations are to be drawn at
 * the same time (thus there is no risk of dropping frames), because it makes no
 * attempt to adapt the frame rate below 60fps.
 * 
 * @author Giovanni Campagna
 * 
 */
public class MasterClock implements ActionListener {
    private final Set<Animation> animations;
    private boolean running;
    private final Timer timer;
    private long currentTime;

    /**
     * Construct a new master clock.
     * 
     * The clock is initially not running.
     */
    public MasterClock() {
        animations = new HashSet<Animation>();
        timer = new Timer(1000 / 60, this);
    }

    /**
     * Start the master clock, starting also any timer required and scheduling
     * the update of all animations.
     * 
     * Any animations that were added to the master clock are implicitly
     * started.
     */
    public void start() {
        if (running) {
            return;
        }

        running = true;
        currentTime = System.currentTimeMillis();

        if (animations.isEmpty()) {
            return;
        }

        for (Animation a : animations) {
            a.start(currentTime);
        }
        timer.start();
    }

    /**
     * Stop the master clock, freezing the update of all animations.
     * 
     * Note that animations are not themselves stopped, you need to call stop()
     * on each one to obtain that.
     */
    public void stop() {
        if (!running) {
            return;
        }

        running = false;
        timer.stop();
    }

    /**
     * Add an animation to the clock, and start it if the clock is running.
     * 
     * The animation will be removed automatically when it completes. To remove
     * an animation earlier, call stop() (or stopAndComplete()/stopAndRewind())
     * on it.
     * 
     * @param a
     *            the new animation
     */
    public void addAnimation(Animation a) {
        animations.add(a);

        if (running) {
            if (timer.isRunning()) {
                a.start(currentTime);
            } else {
                currentTime = System.currentTimeMillis();
                a.start(currentTime);
                timer.start();
            }
        }
    }

    private void update(long currentTime) {
        Iterator<Animation> iter = animations.iterator();
        while (iter.hasNext()) {
            Animation animation = iter.next();
            animation.update(currentTime);

            if (!animation.isRunning()) {
                iter.remove();
            }
        }

        if (animations.isEmpty()) {
            timer.stop();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void actionPerformed(ActionEvent event) {
        currentTime = System.currentTimeMillis();
        update(currentTime);
    }
}
