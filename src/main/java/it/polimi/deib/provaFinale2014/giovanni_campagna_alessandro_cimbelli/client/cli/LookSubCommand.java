package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import java.util.HashMap;
import java.util.Map;

/**
 * A subcommand of "look"
 * 
 * @author Giovanni Campagna
 */
public enum LookSubCommand {
    /**
     * "look inventory", looks at the cards and wallet of the human player
     */
    INVENTORY,
    /**
     * "look shepherd", looks at the chosen shepherd and his surroundings
     */
    SHEPHERD,
    /**
     * "look map", looks at all regions and roads on the map and reports what
     * they contain
     */
    MAP,
    /**
     * "look emporium", looks at the game board and shows the number of fences
     * remaining, as well as terrain cards
     */
    EMPORIUM,
    /**
     * "look opponents", looks at all opponents and shows their hands, wallets
     * and shepherds
     */
    OPPONENTS,
    /**
     * "look market", looks at the cards on sale at the market
     */
    MARKET;

    private static Map<String, LookSubCommand> commands;

    private static void ensureCommands() {
        if (commands == null) {
            commands = new HashMap<String, LookSubCommand>();
            LookSubCommand[] allCommands = values();

            for (LookSubCommand c : allCommands) {
                commands.put(c.name().toLowerCase(), c);
            }
        }
    }

    /**
     * Retrieve the subcommand enumeration matching the given input string.
     * 
     * @param token
     *            the subcommand name
     * @return the subcommand
     * @throws InvalidInputException
     *             if the name was not recognized
     */
    public static LookSubCommand forToken(String token)
            throws InvalidInputException {
        ensureCommands();
        LookSubCommand self = commands.get(token);
        if (self == null) {
            throw new InvalidInputException(token);
        }
        return self;
    }
}
