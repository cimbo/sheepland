package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents an incoming connection and player in a game in the
 * server.
 * 
 * This is the most complex class in the server application logic, because at
 * times it can represent different things.
 * 
 * In particular, at any given time there will be one player associated with a
 * given ServerConnection (as the ServerReceiver for that connection).
 * Initially, all players are associated with the connection they're
 * instantiated with, but they can relinquish the association for another player
 * after login.
 * 
 * Moreover, at any given time there will be at most one player canonical for a
 * current user. The canonical player will be associated with new connections
 * for the given user after login (potentially closing the existing connection),
 * and will be detached from the user when abandoning the game.
 * 
 * The player can also be associated with a game, that enables executing
 * commands from the player's connection on the game itself. Most network
 * commands will fail or have no effect before the player is associated with the
 * game.
 * 
 * This class is not threadsafe: except when otherwise noted, the methods must
 * be called from the associated connection input thread.
 * 
 * @author Alessandro Cimbelli
 * @author Giovanni Campagna
 */
public class ServerPlayer implements ServerReceiver {
    // threadsafe
    private final GameIncubator incubator;
    private final GameOptions options;

    // protected by connections' lock (ie, only accessed while processing an
    // input command)
    private User user;

    // protected by this lock
    private ServerConnection connection;
    private SenderThread sender;
    private Pinger pinger;
    private long turnBeginTime;
    private Timer turnTimer;
    private boolean added;

    // under correct client behavior, this is written by BuildGame
    // (once) and read by the client after setup() (which is a guaranteed
    // happen-before arc) on the connection's thread
    // under invalid client behavior, the read in checkInvalidGame()
    // races with setServerGame(), but:
    // - if the read succeeds first, we see it null and return the exception
    // to the client
    // - if the write happens first, we see it not-null and then proceed
    // to lock on it (which ensures that .setup() and other initialization
    // happens consistently) - this means that the client command will be
    // processed as if it was sent properly after sending setup, which is
    // ok, if the client is ok with the model getting completely out of sync
    private volatile Game game;

    // protected by game's lock
    private Player me;
    private PlayerIdentifier meId;
    // this variable is true if the player is "joined" to the game
    // (ie, the other players believe that the player is in)
    // it is used for playerLeft/playerJoined notifications
    // it is normally equal to connection != null, but can be occasionally
    // different because of the different locking (this is protected by
    // game's lock, connection is protected by player's lock)
    private boolean left;
    private boolean abandoned;

    /**
     * Construct a new server player, initially associated with the given
     * connection.
     * 
     * @param connection
     *            the connection
     * @param incubator
     *            the game incubator
     */
    public ServerPlayer(ServerConnection connection, GameIncubator incubator,
            GameOptions options) {
        this.incubator = incubator;
        this.options = options;
        added = false;

        // This access is safe because at this point connection is only
        // reachable from our thread (which, on socket, is the main thread
        // in the startListening() loop, while on rmi is an input thread
        // handling Server.joinGame())
        setServerConnection(connection);
    }

    // For testing
    Game getGame() {
        return game;
    }

    /**
     * Retrieve the game player associated with this server player
     * 
     * This method is only valid under Game's lock, but this is not checked
     * because it's allowed to call this method before setting the game.
     * 
     * If the canonical player is not initialized yet, it is created using the
     * username associated with the player.
     * 
     * This method is only valid on a canonical (user != null) player.
     * 
     * @return the game player
     */
    public Player getPlayer() {
        assert user != null;

        if (me == null) {
            me = new Player(user.getUserName());
        }

        return me;
    }

    /**
     * Check if the player is disconnected or abandoned the game.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * @return true, if the player left the game, false otherwise
     */
    public boolean hasLeft() {
        assert game != null;
        assert Thread.holdsLock(game);

        return left;
    }

    /**
     * Check if the player has abandoned the game.
     * 
     * If this method returns true, then hasLeft() will also return true.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * @return true, if the player abandoned the game (left on purpose), false
     *         otherwise
     */
    public boolean isAbandoned() {
        assert game != null;
        assert Thread.holdsLock(game);

        return abandoned;
    }

    /**
     * Retrieve the sender thread associated with this player (or null if the
     * connection is closed)
     * 
     * This method is only valid under Player's lock, and only for the canonical
     * player. The returned sender is only valid until the Player's lock is
     * relinquished.
     * 
     * @return the sender queue, or null
     */
    public SenderThread getServerThread() {
        assert user != null;
        assert Thread.holdsLock(this);

        return sender;
    }

    /**
     * Associate this player with the given game.
     * 
     * This method must be called at most once with a given player, and the game
     * must not be null.
     * 
     * After this method, the player will accept and process game commands
     * (execute move, abandon, forceSynchronize) - although proper protocol is
     * for the client to wait until setup
     * 
     * @param game
     *            the new server game
     */
    public synchronized void setServerGame(Game game) {
        assert game != null;
        assert this.game == null;

        this.game = game;
        meId = me.getId();
        added = false;
        // this access is safe because we're holding both the game
        // and the player
        left = connection == null || abandoned;
    }

    private synchronized void createOutgoingQueues() {
        if (sender == null) {
            sender = new SenderThread(this, connection.getServerSender());
            sender.start();
            assert pinger == null;
            pinger = new Pinger(sender, this, options);
        } else {
            Logger.getGlobal().warning(
                    "Player " + user.getUserName()
                            + " logged in while already connected and logged");
        }
    }

    private void closeOutgoingQueues() {
        assert sender != null;
        assert pinger != null;
        pinger.close();
        sender.close();
        pinger = null;
        sender = null;
    }

    private void safelyCloseConnection() {
        // This must be called in synchronized context

        if (sender != null) {
            closeOutgoingQueues();
        }
        connection.close();

        while (connection != null) {
            try {
                wait();
            } catch (InterruptedException e) {
                // spurious wake up, just go back to the loop and wait
                Logger.getGlobal().log(Level.FINEST, "Interrupt exception", e);
            }
        }
    }

    private synchronized void setServerConnection(ServerConnection connection) {
        assert this.connection != connection;

        if (this.connection != null) {
            // On socket, this will trigger the ServerReaderThread and cause
            // a SocketException there, that will cause it to close itself and
            // call receiver.connectionClosed(), that will null out
            // this.connection - but this won't happen until we wait() a few
            // lines later, because we're in a synchronized method now.
            // The wait() will relinquish the lock on @this, and therefore will
            // allow connection's ServerReaderThread to process any just parsed
            // message, until it goes to read again, fails and wakes us.
            //
            // On RMI, this will not wait until other methods are called. This
            // means that they might be processed at a later time, if they got
            // through despite us forcing the removal.
            // It also means that the replies will happen from the second
            // connection.
            // Because of the ordering imposed by locking on the game (as well
            // as the proper behavior of the client not allowing us to go
            // through
            // with other sends until it has processed the receive), we either
            // send the synchronize first and the reply later, or the reply
            // first and the synchronize later, but in any case the final state
            // as seen by the client will be correct.
            // (And if it's not, the client will complain, force a resync and
            // be happy again...)

            safelyCloseConnection();
        }

        this.connection = connection;
        connection.setServerReceiver(this);
    }

    private void continueAfterLogin() {
        sendLoginAnswer(true);

        // If he was playing at a game
        if (game != null) {
            synchronized (game) {
                abandoned = false;
                if (left) {
                    game.playerJoined(this);
                    left = false;
                }

                rescheduleTimeout(options.getGameTurnTimeout());
                doForceSynchronizeMe();
            }
        } else {
            synchronized (this) {
                if (!added && game == null) {
                    incubator.addServerPlayer(this);
                    added = true;
                }
            }
        }

    }

    /**
     * Process a login command on the associated connection.
     */
    public void doLoginToServer(String username, String password) {
        Logger.getGlobal().info("Attempting login from " + username);

        // The threading model of this command is very special - this
        // is the only do* command that we allow calling from two different
        // threads in some cases
        //
        // In particular, one fragile scenario is as follows (with
        // proof of correctness, or acceptable bugginess):
        // - One connection for a new username, calls into login.
        // - Another connection for the same new username, calls into login.
        // The first server player wins the race over the database,
        // becomes the canonical one.
        //
        // The second player sees the first one, and calls
        // setServerConnection on it, thus switching the first player
        // to the second connection. The first connection is reliably
        // closed.
        // The both players proceed on with continueAfterLogin(),
        // issuing two sendAnswer() (the one from the second server player
        // stack might be wrong, the other one will be valid because it
        // was a new registration) and forcing the synchronization twice.
        // Both answers happen from the second connection, because of
        // the setServerConnection from the second player.
        // This is technically incorrect, but it's unavoidable, and it is
        // acceptable because the client should not attempt two connections
        // at the same time (or if it does due to unstable network connectivity
        // it will discard the commands from one of the two).
        //
        // Another fragile scenario (with proof of correctness):
        // - One existing and logged in connection
        // - Another server player for a connection is about to
        // setServerConnection
        // - While about to close the first connection, another login command
        // is read and processed, enters the first server player
        // It follows the existing == this path (since it was the canonical
        // player after the successful login), goes on to continueAfterLogin()
        // and blocks there, because the second server player is in
        // setServerConnection (and holds the *first* server player lock)
        // This is a proof of the lock inversion sketched in the comment over
        // setServerConnection(), but it's also a proof of correctness: the
        // client will see two login answers (now they can be both valid, both
        // invalid or any combination, because the initial login happened
        // at some time in the past) and send two resyncs, both on the
        // second connection, which is assumed to be the canonical one because
        // it happened at a later time.
        // This is racy in any case (the scenario works just fine if the
        // first connection issues the second login command before the second
        // one), but once again this is acceptable because it's deterministic
        // (always happens on the second connection) and because it's invalid
        // protocol behavior for the client to log in again from the same
        // connection.

        try {
            User newUser = UserDatabase.get().getOrCreateUser(username,
                    password);

            ServerPlayer existing = newUser.getCurrentPlayer(this);

            // This access is safe because it happens from the input thread
            // handling a command from connection itself (both on RMI and
            // on socket)
            if (existing != this) {
                existing.setServerConnection(connection);

                Logger.getGlobal()
                        .info("Successful reconnection from "
                                + newUser.getUserName());
            } else {
                user = newUser;

                Logger.getGlobal().info(
                        "New login for " + newUser.getUserName());
            }

            existing.createOutgoingQueues();
            existing.continueAfterLogin();
        } catch (InvalidPasswordException e) {
            Logger.getGlobal().log(Level.INFO, "Failed login for " + username,
                    e);

            SenderThread oldSender;
            Pinger oldPinger;

            synchronized (this) {
                createOutgoingQueues();
                sendLoginAnswer(false);

                oldSender = sender;
                oldPinger = pinger;
                sender = null;
                pinger = null;
            }

            if (oldSender != null) {
                oldSender.flushQueue();
                assert oldPinger != null;
                oldPinger.close();
                oldSender.close();
            }
        }
    }

    private synchronized void sendLoginAnswer(boolean answer) {
        if (sender != null) {
            sender.sendLoginAnswer(answer);
        }
    }

    /**
     * Send the setup command for associated game on the connection.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     */
    public void sendSetup() {
        assert game != null;
        assert Thread.holdsLock(game);

        GameBoard board = game.getEngine().cloneBoardForPlayer(me);

        synchronized (this) {
            if (sender != null) {
                sender.sendSetup(game.getEngine().getRules(), board, meId);
            }
        }
    }

    /**
     * Send the advance next turn command for associated game on the connection.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     */
    public synchronized void sendAdvanceNextTurn(int turn, GamePhase phase) {
        assert game != null;
        assert Thread.holdsLock(game);

        if (sender != null) {
            sender.sendAdvanceNextTurn(turn, phase);
        }
    }

    /**
     * Send the move acknowledged command for associated game on the connection,
     * with the given result.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     */
    public synchronized void sendAck(Exception e) {
        assert game != null;
        assert Thread.holdsLock(game);

        sendAckInternal(e);
    }

    // To be used only from checkInvalidGame
    // Not locking on the game is dangerous, it can make the order of server
    // commands unpredictable, but
    // 1) sendAck is special because it's always queued synchronously from
    // the connection input thread (and only from there!)
    // 2) the only case we don't lock on the game is also invalid protocol
    // behavior for the client, so it's allowed to send it (almost) garbage
    // - GIGO
    private synchronized void sendAckInternal(Exception e) {
        if (e != null) {
            Logger.getGlobal().log(Level.WARNING,
                    "Move failed for player " + user.getUserName(), e);
        }

        if (sender != null) {
            sender.sendMoveAcknowledged(e);
        }
    }

    /**
     * Send the did execute move command for associated game on the connection,
     * with the given move.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     */
    public synchronized void sendMove(Move move) {
        assert game != null;
        assert Thread.holdsLock(game);

        if (sender != null) {
            sender.sendDidExecuteMove(move);
        }
    }

    /**
     * Send the player left command for associated game on the connection, with
     * the given player.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     */
    public synchronized void sendPlayerLeft(PlayerIdentifier who, boolean clean) {
        assert game != null;
        assert Thread.holdsLock(game);

        assert game != null;
        assert Thread.holdsLock(game);

        if (sender != null) {
            sender.sendPlayerLeft(who, clean);
        }
    }

    /**
     * Send the player joined command for associated game on the connection,
     * with the given player.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     */
    public synchronized void sendPlayerJoined(PlayerIdentifier who) {
        assert game != null;
        assert Thread.holdsLock(game);

        if (sender != null) {
            sender.sendPlayerJoined(who);
        }
    }

    /**
     * Send the end game command for associated game on the connection, with the
     * given scores.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     * 
     * If the connection is currently closed, the command is silently discarded.
     * 
     * In addition to sending the command, this player is detached from the user
     * and is no longer canonical.
     */
    public synchronized void endGame(Map<PlayerIdentifier, Integer> scores) {
        assert game != null;
        assert Thread.holdsLock(game);

        user.detachPlayer(this);

        if (sender != null) {
            sender.sendEndGame(scores);
        }
    }

    private void turnTimeout() {
        game.turnTimeout(this);
    }

    private synchronized void rescheduleTimeout(long fullTimeout) {
        if (turnTimer != null) {
            turnTimer.cancel();
        }

        long elapsed = System.currentTimeMillis() - turnBeginTime;
        long delay = fullTimeout == 0 ? 0 : fullTimeout * 1000 - elapsed;
        if (delay < 0) {
            delay = 0;
        }
        turnTimer = new Timer();
        turnTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                turnTimeout();
            }
        }, delay);
    }

    /**
     * Set that it is currently the player's turn, or not, starting or
     * cancelling the turn timers as appropriate.
     * 
     * This method is only valid under Game's lock, and only after the game is
     * set.
     */
    public synchronized void setYourTurn(boolean yourTurn) {
        assert game != null;
        assert Thread.holdsLock(game);

        if (yourTurn) {
            turnBeginTime = System.currentTimeMillis();
            int timeout;

            if (abandoned) {
                timeout = 0;
            } else if (sender == null) {
                timeout = options.getGameTurnDisconnectedTimeout();
            } else {
                timeout = options.getGameTurnTimeout();
            }

            rescheduleTimeout(timeout);
        } else {
            if (turnTimer != null) {
                turnTimer.cancel();
                turnTimer = null;
            }
        }
    }

    /**
     * Process a do abandon game command from the associated connection.
     */
    public void doAbandonGame() {
        if (user == null) {
            Logger.getGlobal().info("Player abandoned the game before login");

            synchronized (this) {
                assert sender == null;
                assert pinger == null;
                assert game == null;
            }
            return;
        }

        SenderThread oldSender;
        Pinger oldPinger;

        synchronized (this) {
            user.detachPlayer(this);

            // atomically swap the old outgoing queues with null,
            // so we can close them later without blocking with
            // our lock and the game lock while we flush the queue
            oldSender = sender;
            oldPinger = pinger;
            sender = null;
            pinger = null;

            if (game == null) {
                Logger.getGlobal().info(
                        "Player " + user.getUserName()
                                + " abandoned the game at preinit");

                if (added) {
                    incubator.maybeRemoveServerPlayer(this);
                    added = false;
                }

                abandoned = true;
            }
        }

        if (game != null) {
            Logger.getGlobal().info(
                    "User " + user.getUserName() + " abandoned the game");

            synchronized (game) {
                boolean wasMyTurn = false;

                abandoned = true;
                if (!left) {
                    game.playerLeft(this, true);
                    left = true;
                }

                synchronized (this) {
                    if (turnTimer != null) {
                        turnTimer.cancel();
                        turnTimer = null;
                        wasMyTurn = true;
                    }
                }

                if (wasMyTurn) {
                    if (!game.isAbandoned()) {
                        turnTimeout();
                    } else {
                        Logger.getGlobal()
                                .info("Game "
                                        + game.getId()
                                        + " closed because all players abandoned");
                    }
                }
            }
        }

        if (oldSender != null) {
            oldSender.sendAbandonComplete();
            oldSender.flushQueue();
            assert oldPinger != null;
            oldPinger.close();
            oldSender.close();
        }
    }

    /**
     * Process a force synchronize me command from the associated connection.
     */
    public void doForceSynchronizeMe() {
        if (game == null) {
            if (user != null) {
                Logger.getGlobal().info(
                        "Player " + user.getUserName()
                                + " asked for synchronization at preinit");
            } else {
                Logger.getGlobal().info(
                        "Player asked for synchronization before login");
            }
            return;
        }

        synchronized (game) {
            GameBoard board = game.getEngine().cloneBoardForPlayer(me);

            synchronized (this) {
                // be careful, do not move this synchronized to the
                // method - it has to be inside the synchronized(game)!
                if (sender != null) {
                    sender.sendForceSynchronize(game.getEngine().getRules(),
                            board, meId, game.getEngine().getGameState());
                }
            }
        }
    }

    private boolean checkInvalidGame() {
        if (game == null) {
            if (user != null) {
                sendAckInternal(new RuleViolationException(
                        "Game is not started yet"));
            } else {
                // don't even send an ack before login, the client
                // is f... up anyway
                Logger.getGlobal().warning(
                        "Player asked for a move before login");
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean checkValidPlayerMove(Move move) {
        if (!move.getWhoId().equals(meId)) {
            sendAck(new RuleViolationException(
                    "Move is not for the right player"));
            return false;
        } else {
            return true;
        }
    }

    /**
     * Process a execute move command from the associated connection.
     */
    public void doExecuteMove(Move move) {
        if (checkInvalidGame()) {
            return;
        }
        synchronized (game) {
            if (!checkValidPlayerMove(move)) {
                return;
            }

            game.executeMove(this, move);
        }
    }

    /**
     * Process a pong command from the associated connection.
     */
    public synchronized void doPong() {
        if (pinger != null) {
            pinger.pong();
        }
    }

    /**
     * Record that the associated connection was closed, due an error from the
     * receiver thread.
     */
    public void connectionClosed() {
        if (user == null) {
            Logger.getGlobal().log(Level.INFO,
                    "Connection closed before login.");

            synchronized (this) {
                assert sender == null;
                assert pinger == null;
                assert game == null;
            }
            return;
        }

        SenderThread oldSender;
        Pinger oldPinger;

        synchronized (this) {
            Logger.getGlobal().log(Level.INFO,
                    "Connection for " + user.getUserName() + " closed.");

            oldSender = sender;
            oldPinger = pinger;
            sender = null;
            pinger = null;
            connection = null;
            notify();
        }

        // This must be after the synchronized above, which
        // will trigger a happen-before arc between setting
        // the variable in setServerGame() and reading it here
        // (if we win the race all the way through, we get
        // to setServerGame() with connection == null, which
        // sets left to true and sends the notification from
        // the initialization code in Game; if we lose the race,
        // setServerGame() will see connection != null and we
        // will see game != null)
        if (game != null) {
            synchronized (game) {
                synchronized (this) {
                    if (turnTimer != null) {
                        rescheduleTimeout(options
                                .getGameTurnDisconnectedTimeout());
                    }
                }

                if (!left) {
                    game.playerLeft(this, false);
                    left = true;
                }
            }
        }

        if (oldSender != null) {
            assert oldPinger != null;
            oldPinger.close();
            oldSender.close();
        }
    }

    /**
     * Report that the given outgoing connection failed writing, or there was a
     * ping timeout.
     * 
     * If the given connection was not current at the time of the call, the call
     * is silently ignored.
     * 
     * @param thread
     *            the outgoing connection that failed.
     */
    public synchronized void reportIOWriteException(SenderThread thread) {
        if (thread != sender) {
            // Race condition, we were already reconnected, or disconnected
            // by a reader failure
            return;
        }

        Logger.getGlobal().log(
                Level.INFO,
                "Write failure or ping timeout in connection for "
                        + user.getUserName() + ".");

        safelyCloseConnection();
    }

}
