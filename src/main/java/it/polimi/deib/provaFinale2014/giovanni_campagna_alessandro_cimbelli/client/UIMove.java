package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;

/**
 * An interface implemented by moves that have an effect in the UI.
 * 
 * @author Giovanni Campagna
 * 
 */
public interface UIMove {
    /**
     * Animate the beginning of the move, as happened on the given engine.
     * 
     * @param engine
     *            the engine doing the move
     * @param ui
     *            the UI animating it
     * @throws InvalidIdentifierException
     *             the move was corrupt
     */
    void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException;

    /**
     * Animate the end of the move, as happened on the given engine.
     * 
     * This method will be called only for CompletableMoves.
     * 
     * @param engine
     *            the engine doing the move
     * @param ui
     *            the UI animating it
     * @throws InvalidIdentifierException
     *             the move was corrupt
     */
    void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException;
}
