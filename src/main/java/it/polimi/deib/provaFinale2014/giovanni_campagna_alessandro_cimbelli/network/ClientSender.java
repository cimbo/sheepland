package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;

import java.io.IOException;

/**
 * The interface to send commands from the client to the server.
 * 
 * @author Giovanni Campagna
 */
public interface ClientSender {
    /**
     * Send an "abandon game" command.
     * 
     * This command is sent when then client wishes to abandon the current game.
     * 
     * @throws IOException
     */
    void sendAbandonGame() throws IOException;

    /**
     * Send a "force synchronize me" command.
     * 
     * This command is sent when the client recognized to be out of sync and
     * wants a full resynchronization with the server.
     * 
     * @throws IOException
     */
    void sendForceSynchronizeMe() throws IOException;

    /**
     * Send a "execute move" command.
     * 
     * This command is sent when the client whishes to execute a game move.
     * 
     * @param move
     *            the move to execute.
     * @throws IOException
     */
    void sendExecuteMove(Move move) throws IOException;

    /**
     * Send a "login to server" command.
     * 
     * This command is sent immediately after connection, to identify the client
     * to the server.
     * 
     * @param username
     *            the username
     * @param password
     *            the password
     * @throws IOException
     */
    void sendLoginToServer(String username, String password) throws IOException;

    /**
     * Send a "pong" command.
     * 
     * This command is sent in reply to a "ping" command.
     * 
     * @throws IOException
     */
    void sendPong() throws IOException;
}
