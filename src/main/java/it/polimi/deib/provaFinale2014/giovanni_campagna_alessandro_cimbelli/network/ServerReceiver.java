package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;

/**
 * The interface to handle client generated messages on the server.
 * 
 * Implementations of this interface must be thread-safe.
 * 
 * @author Giovanni Campagna
 */
public interface ServerReceiver {
    /**
     * Handle an "abandon game" command.
     * 
     * This command is sent when then client wishes to abandon the current game.
     */
    void doAbandonGame();

    /**
     * Handle a "force synchronize me" command.
     * 
     * This command is sent when the client recognized to be out of sync and
     * wants a full resynchronization with the server.
     */
    void doForceSynchronizeMe();

    /**
     * Handle a "execute move" command.
     * 
     * This command is sent when the client wishes to execute a game move.
     * 
     * @param move
     *            the move to execute.
     * @throws IOException
     */
    void doExecuteMove(Move move);

    /**
     * Handle a "login to server" command.
     * 
     * This command is sent immediately after connection, to identify the client
     * to the server.
     * 
     * @param username
     *            the username
     * @param password
     *            the password
     * @throws IOException
     */
    void doLoginToServer(String username, String password);

    /**
     * Send a "pong" command.
     * 
     * This command is sent in reply to a "ping" command.
     * 
     * @throws IOException
     */
    void doPong();

    // This method isn't shared on the network
    /**
     * Report that the connection was closed due to the receiver error.
     */
    void connectionClosed();

}
