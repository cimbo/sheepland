package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A collection of useful methods for dealing with the network.
 * 
 * @author Giovanni Campagna
 */
public class NetworkUtil {

    private NetworkUtil() {
        // This class is not meant to be instantiated
    }

    private static InetSocketAddress parseHostPort(String hostPort)
            throws UnknownHostException, NumberFormatException {
        // Stack Overflow tells me that a proper regular expression
        // matching IPv6 addresses (and only IPv6 addresses) is several
        // lines long, so here we don't try that.
        // Instead, we allow everything that looks like [stuff]:port,
        // where stuff is at least hexadecimal and :, and then check
        // for the address validity
        Pattern ipv6Regex = Pattern
                .compile("(\\[[\\p{XDigit}:]+\\]):(\\p{Digit}{1,4})");
        Matcher ipv6Matcher = ipv6Regex.matcher(hostPort);
        if (ipv6Matcher.matches()) {
            String ipv6 = ipv6Matcher.group(1);
            String port = ipv6Matcher.group(2);

            InetAddress ipv6Address = InetAddress.getByName(ipv6);
            if (!(ipv6Address instanceof Inet6Address)) {
                throw new NumberFormatException("Invalid IPv6 address");
            }

            return new InetSocketAddress(ipv6Address, Integer.parseInt(port));
        }

        Pattern ipv4OrHostRegex = Pattern
                .compile("([\\p{Alnum}\\-\\.]+):(\\p{Digit}{1,4})");
        Matcher ipv4OrHostMatcher = ipv4OrHostRegex.matcher(hostPort);
        if (ipv4OrHostMatcher.matches()) {
            String ipv4OrHost = ipv4OrHostMatcher.group(1);
            String port = ipv4OrHostMatcher.group(2);

            return new InetSocketAddress(InetAddress.getByName(ipv4OrHost),
                    Integer.parseInt(port));
        }

        return new InetSocketAddress(InetAddress.getByName("0.0.0.0"),
                Integer.parseInt(hostPort));
    }

    /**
     * Split a string in the form of [IPv6 address]:port or IPv4:port (ie, a
     * valid authority part of a URI), or port (ie, just a number) into a
     * InetSocketAddress address-port combination.
     * 
     * If the host part is not specified, it is assumed to be 0.0.0.0
     * 
     * @param hostPort
     *            the string to parse
     * @param allowAny
     *            if the wildcard address (0.0.0.0 on IPv4) is to be allowed
     * @return a new InetSocketAddress
     * @throws UnknownHostException
     *             the string specified a host, and resolution failed, or the
     *             string specified the any address and this was not allowed
     * @throws NumberFormatException
     *             the string was not in a parseable format
     */
    public static InetSocketAddress parseHostPort(String hostPort,
            boolean allowAny) throws UnknownHostException,
            NumberFormatException {
        InetSocketAddress address = parseHostPort(hostPort);

        if (!allowAny && address.getAddress().isAnyLocalAddress()) {
            throw new UnknownHostException();
        }

        return address;
    }
}
