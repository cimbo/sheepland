package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MateSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * The UI counterpart of a mate sheep move.
 * 
 * @author Giovanni Campagna
 * 
 */
public class MateSheepUIMove extends MateSheepMove implements UIMove {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a mate sheep move.
     * 
     * @param who
     *            the player doing it
     * @param which
     *            the shepherd doing it
     * @param tile
     *            the tile where the mating happens
     */
    public MateSheepUIMove(PlayerIdentifier who, ShepherdIdentifier which,
            SheepTileIdentifier tile) {
        super(who, which, tile);
    }

    /**
     * {@inheritDoc}
     */
    public void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateBeginSheepMating(getWho(engine), getWhich(engine),
                getTile(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateEndSheepMating(getWho(engine), getWhich(engine),
                getTile(engine), getResult());
    }
}
