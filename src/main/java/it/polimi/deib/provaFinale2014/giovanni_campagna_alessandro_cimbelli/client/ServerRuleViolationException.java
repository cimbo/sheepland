package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;

/**
 * A server exception that indicates a rule violation from the server (and
 * therefore is very likely to represent the two models going out of sync)
 * 
 * @author Giovanni Campagna
 * 
 */
public class ServerRuleViolationException extends ServerException {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new ServerRuleViolationException with the given cause.
     * 
     * @param origin
     *            the inner exception
     */
    public ServerRuleViolationException(RuleViolationException origin) {
        super("The server violated our rules: " + origin.getMessage(), origin);
    }
}