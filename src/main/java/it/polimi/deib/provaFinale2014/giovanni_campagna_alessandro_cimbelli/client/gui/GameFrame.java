package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

/**
 * The main window of the game.
 * 
 * @author Alessandro Cimbelli
 */
public class GameFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 647;
    private static final int Y_SIZE = 700;

    // Indentifica lo stato della gameFrame

    private boolean isLogin = false;
    private boolean isGameBoard = false;
    private LoginPanelBackGround loginBackground;
    private GameBoardPanel gameBoard;
    private JPanel waitingForStart;
    private JPanel yourTurn;
    private JPanel placeShepherd;
    private JPanel chooseShepherd;
    private CardPanel cardPanel;
    private Game currentGame;

    private final GraphicUIManager uiManager;

    /**
     * Construct a frame for the given ui manager.
     * 
     * @param ui
     *            the owner of this frame.
     */
    public GameFrame(GraphicUIManager ui) {
        super("SheepLand");

        uiManager = ui;

        Container content = getContentPane();
        content.setLayout(null);
        content.setSize(X_SIZE, Y_SIZE);
        content.setPreferredSize(new Dimension(X_SIZE, Y_SIZE));
        pack();
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    /**
     * Retrieve the ui manager associated with this frame.
     * 
     * An example of use case for this method is to retrieve the master clock,
     * and then attach an animation to it.
     * 
     * @return the ui manager
     */
    public GraphicUIManager getUIManager() {
        return uiManager;
    }

    private int getQuitConfirm() {
        if (currentGame == null) {
            return JOptionPane.YES_OPTION;
        }

        return JOptionPane
                .showConfirmDialog(
                        this,
                        "Do you really want to quit the game? You will not be able to join again.",
                        "Really quit?", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dispose() {
        int answer = getQuitConfirm();
        if (answer == JOptionPane.YES_OPTION) {
            super.dispose();
            uiManager.quit();
        }
    }

    /**
     * Begin the login phase of the game, showing the login UI.
     */
    public void login() {
        loginBackground = new LoginPanelBackGround(uiManager);
        isLogin = true;
        if (isGameBoard) {
            this.remove(gameBoard);
            isGameBoard = false;
        }
        this.add(loginBackground);
        setVisible(true);
        repaint();
    }

    /**
     * Begin the actual game, showing the game UI (card panel and game board).
     */
    public void gameBoard() {
        cardPanel = new CardPanel();
        isGameBoard = true;
        try {
            gameBoard = new GameBoardPanel(this);
            this.add(gameBoard);
            this.add(cardPanel);
            gameBoard.setLocation(120, 0);
            cardPanel.setLocation(0, 0);
            setVisible(true);

            waitingForStart = createOverlayPanel(new Color(255, 255, 0),
                    new Color(0, 0, 0), "Waiting for other players...");
            this.add(waitingForStart, 0);
            waitingForStart.setLocation(cardPanel.getWidth(),
                    gameBoard.getHeight() / (700 / 200));
            repaint();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.SEVERE, "Mistake", e);
            loginBackground.showMessage(e.getMessage());
            return;
        }
        if (isLogin) {
            this.remove(loginBackground);
            isLogin = false;
        }
    }

    /**
     * Report a login failure to the user.
     */
    public void wrongLogin() {
        loginBackground.addPasswordError();
    }

    /**
     * Animate the motion of a sheep.
     * 
     * See {@link GameBoadPanel.drawMoveSheep} for details.
     */
    public void drawMoveSheep(Player mover, SheepTile from, SheepTile to,
            Sheep sheep) {
        gameBoard.drawMoveSheep(mover, from.getId(), to.getId(),
                sheep.getType());
    }

    private void drawAnimals() {
        GameMap map = currentGame.getGameBoard().getMap();
        for (int i = 0; i <= 18; i++) {
            try {
                gameBoard.drawAnimals(map
                        .getSheepTileFromId(new SheepTileIdentifier(i)));
            } catch (InvalidIdentifierException e) {
                Logger.getGlobal().log(Level.SEVERE, "Mistake", e);
            }
        }
    }

    private void drawCards(Player player) {
        TerrainType[] allTypes = TerrainType.values();
        for (TerrainType type : allTypes) {
            if (type != TerrainType.SHEEPSBURG) {
                cardPanel.updateCard(type, player.getNumberOfCards(type));
            }
        }
    }

    /**
     * Animate the motion or placement of a shepherd.
     * 
     * See {@link GameBoadPanel.drawShepherd} for details.
     */
    public void drawShepherd(Player who, Shepherd which, RoadSquare to) {
        gameBoard.drawShepherd(who, which, to);
        gameBoard.updateMoney();
    }

    private void freezeUI() {
        gameBoard.getCurrentPhase().freeze();
    }

    private void startUI() {
        gameBoard.getCurrentPhase().thaw();
    }

    /**
     * Set the "waiting" state.
     * 
     * If the UI is marked as waiting, all user interaction is ignored.
     * 
     * It is allowed to call this method multiple times with the same value, and
     * it has no effect.
     * 
     * @param b
     *            true, if interaction is to be ignored, false otherwise
     */
    public void setWaiting(boolean b) {
        if (gameBoard != null) {
            gameBoard.getCurrentPhase().setWaiting(b);
        }
    }

    /**
     * Update the current turn indication.
     * 
     * See {@link GameBoard.updateCurrentTurn} for details.
     */
    public void updateCurrentTurn() {
        if (currentGame != null) {
            gameBoard.updateCurrentTurn();
        }
    }

    /**
     * Begin the turn of an opponent (updating the view and UI state machine as
     * appropriate)
     */
    public void opponentTurn() {
        gameBoard.getCurrentPhase().setPhase(GraphicMove.OPPONENT_TURN);
        gameBoard.setTurnShepherd(null);
        gameBoard.clearOpenPanels();
    }

    /**
     * Begin the turn of the player, in the given game phase (updating the view
     * and the UI state machine as appropriate).
     * 
     * gamePhase must be a phase in which turns happen (so not PREINIT or END),
     * otherwise an exception is generated.
     * 
     * @param gamePhase
     *            the current game phase
     */
    public void yourTurn(GamePhase gamePhase) {

        switch (gamePhase) {
        case INIT:
            placeYourShepherd();
            break;

        case PLAY:
        case FINAL:
            playYourTurn();
            break;

        case MARKET_SELL:
            playYourMarketTime();
            break;

        case MARKET_BUY:
            playYourMarketBuyTime();
            break;

        default:
            throw new IllegalStateException("Game phase " + gamePhase
                    + " should not be reached");
        }
    }

    private void playYourMarketBuyTime() {
        yourTurn = createOverlayPanel(new Color(101, 255, 26), new Color(0, 0,
                0), "Market turn: it's time to buy!");
        gameBoard.getCurrentPhase().setPhase(GraphicMove.MARKET_BUY);
        freezeUI();
        this.add(yourTurn, 0);
        yourTurn.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 200));
        Timer timer = new Timer(2 * 1000, new DisposeYourTurnTask());
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Inform the user that he needs to place his shepherd, and then set the UI
     * in the proper state to do so.
     */
    public void placeYourShepherd() {
        gameBoard.getCurrentPhase().setPhase(GraphicMove.PLACE_SHEPHERD);
        freezeUI();
        this.repaint();

        if (placeShepherd == null) {
            placeShepherd = createOverlayPanel(new Color(255, 255, 0),
                    new Color(0, 0, 0), "Place your shepherd!");
        }

        this.add(placeShepherd, 0);
        placeShepherd.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 200));
        Timer timer = new Timer(2 * 1000, new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(placeShepherd);
                repaint();
                startUI();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    private void playYourMarketTime() {
        yourTurn = createOverlayPanel(new Color(101, 255, 26), new Color(0, 0,
                0), "Market turn: it's time to sell!");
        gameBoard.getCurrentPhase().setPhase(GraphicMove.MARKET_SELL);
        freezeUI();
        this.add(yourTurn, 0);
        yourTurn.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 200));
        Timer timer = new Timer(2 * 1000, new DisposeYourTurnTask());
        timer.setRepeats(false);
        timer.start();
    }

    private void playYourTurn() {
        yourTurn = createOverlayPanel(new Color(255, 0, 0), new Color(255, 255,
                255), "It's your turn");
        gameBoard.getCurrentPhase().setPhase(GraphicMove.NULL);
        freezeUI();
        gameBoard.resetShepherdDrag();
        gameBoard.updateMoney();
        this.add(yourTurn, 0);
        yourTurn.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 200));
        Timer timer = new Timer(2 * 1000, new DisposeYourTurnTask());
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Inform the user that he needs to choose a shepherd for the current turn,
     * and then set the UI in the proper state to do so.
     */
    public void showChooseShepherd() {
        gameBoard.getCurrentPhase().setPhase(GraphicMove.CHOOSE_SHEPHERD);
        freezeUI();

        if (chooseShepherd == null) {
            chooseShepherd = createOverlayPanel(new Color(255, 255, 0),
                    new Color(0, 0, 0), "Choose your shepherd");
        }

        add(chooseShepherd, 0);
        repaint();
        chooseShepherd.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 200));
        Timer timer = new Timer(2 * 1000, new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(chooseShepherd);
                repaint();
                startUI();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    private void beginActualTurn() {
        switch (currentGame.getGamePhase()) {
        case PLAY:
        case FINAL:
            gameBoard.setTurnShepherd(currentGame.getSelfPlayer()
                    .getAllShepherds()[0]);
            gameBoard.getCurrentPhase().setPhase(GraphicMove.NULL);
            break;

        case MARKET_SELL:
            gameBoard.sellCards();
            break;

        case MARKET_BUY:
            gameBoard.buyCardsChooseType();
            break;

        default:
            break;
        }
    }

    private class DisposeYourTurnTask implements ActionListener {
        /**
         * {@inheritDoc}
         */
        public void actionPerformed(ActionEvent event) {
            remove(yourTurn);
            repaint();
            startUI();

            if (currentGame.getAllPlayers().length == 2
                    && !currentGame.getGamePhase().isMarket()) {
                showChooseShepherd();
            } else {
                beginActualTurn();
            }
        }
    }

    /**
     * Initialize the frame for the given game, filling in all the details in
     * the board panel and in the card panel.
     */
    public void initialize(Game game) {
        if (waitingForStart != null) {
            remove(waitingForStart);
            waitingForStart = null;
        }

        currentGame = game;
        gameBoard.fillFenceArray();

        drawAnimals();
        drawCards(currentGame.getSelfPlayer());
        gameBoard.placePlayerIcon();
    }

    /**
     * Retrieve the current game associated with this frame.
     * 
     * @return the game
     */
    public Game getCurrentGame() {
        return currentGame;
    }

    private JPanel createOverlayPanel(Color bg, Color fg, String text) {
        JPanel panel = new JPanel();
        panel.setSize(527, 40);
        panel.setBackground(bg);
        panel.setLayout(null);
        JLabel label = new JLabel(text);
        label.setForeground(fg);
        label.setLocation(0, 0);
        label.setSize(527, 40);
        label.setVerticalAlignment(SwingConstants.CENTER);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(label);
        return panel;
    }

    /**
     * Report that a move is not allowed, with the reason given in the exception
     * 
     * @param e
     *            the reason for not allowing the move
     */
    public void moveNotAllowed(RuleViolationException e) {
        showMessage("Move not allowed: " + e.getMessage() + "!");
    }

    /**
     * Animate the motion of the wolf.
     * 
     * See {@link GameBoardPanel.drawMoveWolf} for details.
     */
    public void drawMoveWolf(SheepTile from, SheepTile to, Sheep eatenSheep) {
        gameBoard.drawMoveWolf(from, to, eatenSheep);
    }

    /**
     * Report that the given player bought a card (and update the number of
     * cards on screen).
     * 
     * See {@link GameBoardPanel.drawBoughtCard} for details.
     */
    public void drawBoughtCard(Player who, TerrainCard card) {
        gameBoard.drawCardBought(who, card);
        drawCards(currentGame.getSelfPlayer());
    }

    /**
     * Show a message with the given text and icons, regarding the buying or
     * selling of cards.
     * 
     * @param pawn
     *            the player icon associed with this message
     * @param card
     *            the card icon associated with this message
     * @param text
     *            the text of the message
     */
    public void showBoughtOrSoldCardMessage(PlayerIcon pawn, Card card,
            String text) {
        freezeUI();
        final BoughtOrSoldCardPanel boughtCardMessage = new BoughtOrSoldCardPanel(
                pawn, card, text);

        this.add(boughtCardMessage, 0);
        boughtCardMessage.setLocation(cardPanel.getWidth(),
                (int) (gameBoard.getHeight() / (700 / 200) + boughtCardMessage
                        .getHeight() * 1.5));
        this.repaint();
        Timer timer = new Timer((int) (3.5 * 1000), new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(boughtCardMessage);
                repaint();
                startUI();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Draw the completion of the mate operation.
     * 
     * See {@link GameBoard.drawMateCompleted} for details.
     */
    public void drawMateCompleted(Player who, Shepherd which, SheepTile tile,
            Sheep newLamb) {
        gameBoard.drawMateCompleted(who, tile, newLamb);
    }

    /**
     * Show a message indicating a successful mating operation, with the given
     * player and sheep icons.
     * 
     * @param pawn
     *            the player icon associated with this message
     * @param lamb
     *            the sheep icon associated with this message
     */
    public void showMateCompletedMessage(PlayerIcon pawn, SheepIcon lamb) {
        freezeUI();
        final MateOrKillCompletedPanel panel = new MateOrKillCompletedPanel(
                pawn, lamb, "Sheepland has a new entry!");
        this.add(panel, 0);
        panel.setLocation(cardPanel.getWidth(), (int) (gameBoard.getHeight()
                / (700 / 200) + panel.getHeight() * 1.5));
        this.repaint();
        Timer timer = new Timer((int) (3.5 * 1000), new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(panel);
                repaint();
                startUI();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Show a failure message for a mating or killing operation, with the given
     * text.
     * 
     * @param text
     *            the text to show
     */
    public void showFailureMessage(String text) {
        final MateOrKillCompletedPanel panel = new MateOrKillCompletedPanel(
                null, null, text);
        freezeUI();
        this.add(panel, 0);
        panel.setLocation(cardPanel.getWidth(), (int) (gameBoard.getHeight()
                / (700 / 200) + panel.getHeight() * 1.5));
        this.repaint();
        Timer timer = new Timer((int) (3.5 * 1000), new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(panel);
                repaint();
                startUI();
            }
        });
        timer.setRepeats(false);
        timer.start();

    }

    /**
     * Draw the successful completion of the kill operation.
     * 
     * See {@link GameBoard.drawSheepKilled} for details.
     */
    public void drawSheepKilled(Player who, SheepTile tile, Sheep sheep,
            Map<Player, Integer> paidPlayers) {
        gameBoard.drawSheepKilled(who, tile, sheep, paidPlayers);
    }

    /**
     * Show a message indicating a successful killing operation, with the given
     * player and sheep icons.
     * 
     * @param pawn
     *            the player icon associated with this message
     * @param lamb
     *            the sheep icon associated with this message
     */
    public void showKillCompletedMessage(PlayerIcon pawn, SheepIcon sheep,
            final Map<Player, Integer> paidPlayers) {
        freezeUI();
        final MateOrKillCompletedPanel panel = new MateOrKillCompletedPanel(
                pawn, sheep, "killed");
        this.add(panel, 0);
        panel.setLocation(cardPanel.getWidth(), (int) (gameBoard.getHeight()
                / (700 / 200) + panel.getHeight() * 1.5));
        this.repaint();
        Timer timer = new Timer((int) (3.5 * 1000), new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(panel);
                repaint();

                if (paidPlayers.isEmpty()) {
                    startUI();
                    return;
                }

                gameBoard.drawBuySilence(paidPlayers);
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Show a message indicating that a killing operation required a player to
     * pay the others.
     * 
     * @param pawn
     *            the player icons associated with this message, and the amount
     *            of money they got
     */
    public void showMoneySpentForSilenceMessage(Map<PlayerIcon, Integer> money) {
        final PlayerListPanel panel = new PlayerListPanel(money,
                "Money spent for the silence");
        this.add(panel, 0);
        panel.setLocation(cardPanel.getWidth(), (int) (gameBoard.getHeight()
                / (700 / 200) + panel.getHeight() * 1.5));
        this.repaint();
        Timer timer = new Timer((int) (3.5 * 1000), new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(panel);
                repaint();
                startUI();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }

    /**
     * Show a message indicating that the game is over, with the given final
     * scores.
     * 
     * @param scores
     *            the player icons associated with this message, and their score
     */
    public void showGameOverMessage(Map<PlayerIcon, Integer> scores) {
        freezeUI();
        final PlayerListPanel panel = new PlayerListPanel(scores, "Game over!");
        this.add(panel, 0);
        panel.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 200));
        repaint();
    }

    /**
     * Draw that the given card is for sale.
     * 
     * See {@link GameBoard.drawSoldMarketCard} for details.
     */
    public void drawSoldMarketCard(Player who, TerrainCard card, int cost) {
        gameBoard.drawSoldMarketCard(who, card, cost);
    }

    /**
     * Draw that the game is over.
     * 
     * See {@link GameBoard.drawGameOver} for details.
     */
    public void drawGameOver(Map<PlayerIdentifier, Integer> scores) {
        gameBoard.drawGameOver(scores);
        currentGame = null;
    }

    /**
     * Update the number of animals on the given tile due to lambs growing.
     * 
     * See {@link GameBoard.lambsGrew} for details.
     */
    public void lambsGrew(SheepTile tile) {
        gameBoard.lambsGrew(tile);
    }

    /**
     * (Re)initialize the window for the current game after a forced
     * synchronization with the server
     */
    public void forceSynchronize(Game game) {
        if (currentGame != null) {
            remove(gameBoard);

            try {
                gameBoard = new GameBoardPanel(this);
                add(gameBoard);
                gameBoard.setLocation(120, 0);
            } catch (IOException e) {
                Logger.getGlobal()
                        .log(Level.SEVERE,
                                "Failed to reload the GameBoard after the first loading",
                                e);
            }
        }

        initialize(game);
        gameBoard.forceSynchronize();

        repaint();
    }

    /**
     * Sync the position of all shepherd icons after the server forced a move.
     */
    public void syncShepherdPositions() {
        gameBoard.syncShepherdPositions();
    }

    /**
     * Show a generic overlay message.
     * 
     * @param message
     *            the message to show.
     */
    public void showMessage(String message) {
        if (gameBoard == null) {
            loginBackground.showMessage(message);
            return;
        }

        repaint();

        final JPanel messagePanel = createOverlayPanel(new Color(0, 0, 0),
                new Color(255, 255, 255), message);
        this.add(messagePanel, 0);
        messagePanel.setLocation(cardPanel.getWidth(), gameBoard.getHeight()
                / (700 / 160));
        Timer timer = new Timer(2 * 1000, new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent event) {
                remove(messagePanel);
                repaint();
            }
        });
        timer.setRepeats(false);
        timer.start();
    }
}
