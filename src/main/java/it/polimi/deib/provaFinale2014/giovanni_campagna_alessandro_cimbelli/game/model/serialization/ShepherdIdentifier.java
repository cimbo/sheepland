package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;

/**
 * Identifies a shepherd.
 * 
 * Numeric IDs for shepherd are assigned progressively by the Player owner,
 * starting from 0. This means that they are only meaningful within one Player,
 * and that two different shepherds can have equal identifiers if they have
 * different owners.
 * 
 * @author Giovanni Campagna
 */
public class ShepherdIdentifier extends GameObjectIdentifier {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new shepherd identifier, for the given shepherd index.
     * 
     * @param number
     *            the index of the identifier shepherd
     */
    public ShepherdIdentifier(int number) {
        super(Shepherd.class, number);
    }
}
