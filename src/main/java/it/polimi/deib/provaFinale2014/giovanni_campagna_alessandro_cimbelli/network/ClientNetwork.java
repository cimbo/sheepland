package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import java.io.IOException;

/**
 * An interface for the abstract concept of a client-side network
 * implementation. Provides the ability to connect to the server.
 * 
 * @author Giovanni Campagna
 */
public interface ClientNetwork {
    /**
     * Synchronously connect to the server. The server address is implementation
     * dependent.
     * 
     * This method will block until a connection is obtained.
     * 
     * @return the connection to the server.
     */
    ClientConnection connectToServer() throws IOException;
}
