package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

/**
 * An interface for visiting the graph of the map
 * 
 * @author Giovanni Campagna
 * 
 */
public interface GraphVisitor {
    /**
     * This method will be called once and only once for every node (SheepTile)
     * in the map. The precise order on which this method is called depends on
     * the type of visit which is called on Graph.
     * 
     * @param tile
     *            the current node
     */
    void visit(SheepTile tile);
}
