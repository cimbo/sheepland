package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

import java.util.ArrayList;
import java.util.List;

/**
 * A controller that drives an animation, by reacting to time updates from a
 * master clock and propagating them to the animator after looping and
 * interpolation.
 * 
 * @author Giovanni Campagna
 */
public class Animation {
    private final AnimationAlpha alpha;
    private final Animator animator;
    private final List<AnimationListener> listeners;

    private long duration;
    private boolean loop;
    private boolean loopReverse;

    private long startTime;
    private boolean running;
    private boolean completed;

    /**
     * Construct a new animation, with the given animator, easing function and
     * duration.
     * 
     * @param animator
     *            the animator
     * @param alpha
     *            the easing function/adapter
     * @param duration
     *            the total duration (from 0.0 to 1.0) of the animation, in
     *            milliseconds
     */
    public Animation(Animator animator, AnimationAlpha alpha, long duration) {
        this.animator = animator;
        this.alpha = alpha;
        listeners = new ArrayList<AnimationListener>();
        setDuration(duration);
    }

    /**
     * Check if the animation is running (ie, a further call to update() will
     * modify the animator)
     * 
     * @returns true if the animation is running, false otherwise
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Add an animation listener to the given animation.
     * 
     * Methods on the listener will be called as events happen, such as the
     * animation completing.
     * 
     * @param listener
     */
    public void addAnimationListener(AnimationListener listener) {
        listeners.add(listener);
    }

    /**
     * Retrieve the duration of the animation.
     * 
     * @return the duration, in milliseconds
     */
    public long getDuration() {
        return duration;
    }

    /**
     * Change the duration of the animation.
     * 
     * It is undefined to call this method if the animation is already running.
     * 
     * @param duration
     *            the new duration, in milliseconds
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    /**
     * Check if this animation is looping.
     * 
     * A looping animation will be automatically restarted (jumping to 0.0) when
     * it completes.
     * 
     * @return true if the animation is looping, false otherwise
     */
    public boolean getLoop() {
        return loop;
    }

    /**
     * Changes if this animation is looping.
     * 
     * @param loop
     *            if the animation should loop or not.
     */
    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    /**
     * Check if this animation is reversed looping.
     * 
     * A reversed looping animation is a looping animation that will go
     * backwards (from 1.0 to 0.0) on even iterations, giving the effect of the
     * animation moving back and forth as time progresses.
     * 
     * Note that this property has no effect if this animation is not also
     * looping.
     * 
     * @return true, if the animation is reversed looping.
     */
    public boolean getLoopReverse() {
        return loopReverse;
    }

    /**
     * Changes if this animation is reverse looping.
     * 
     * @param reverseLoop
     *            if the animation should reverse loop or not.
     */
    public void setLoopReverse(boolean loopReverse) {
        this.loopReverse = loopReverse;
    }

    /**
     * Start the animation, recording the start time and updating the animator
     * for the first time.
     * 
     * Applications should never call this method directly, animations are
     * automatically started when added to a running master clock.
     * 
     * @param start
     *            the start time, in milliseconds relative to some unspecified
     *            time source.
     */
    public void start(long start) {
        running = true;
        completed = false;
        startTime = start;
        internalUpdate(startTime);
    }

    /**
     * Stop the animation, freezing the animation to whatever value it was last
     * set.
     * 
     * Further updates have no effect until the animation is restarted again.
     */
    public void stop() {
        running = false;
    }

    /**
     * Stop the animation, and rewind the animator to the beginning.
     */
    public void stopAndRewind() {
        stop();
        internalUpdate(startTime);
    }

    /**
     * Stop the animation, and fast forward the animator to the end.
     */
    public void stopAndComplete() {
        stop();
        internalUpdate(startTime + duration);
        if (loop) {
            onComplete();
        }
    }

    private void onComplete() {
        if (completed) {
            return;
        }

        completed = true;
        for (AnimationListener l : listeners) {
            l.onComplete();
        }
    }

    private void internalUpdate(long currentTime) {
        long time = currentTime;
        if (time > startTime + duration) {
            if (loop) {
                long iteration = (time - startTime) / duration;

                if (loopReverse && iteration % 2 != 0) {
                    time = startTime + duration - (time - startTime) % duration;
                } else {
                    time = startTime + (time - startTime) % duration;
                }
            } else {
                time = startTime + duration;
            }
        }

        assert time >= startTime && time <= startTime + duration;

        animator.update(alpha.ease((double) (time - startTime) / duration));

        if (time == startTime + duration && !loop) {
            stop();
            onComplete();
        }
    }

    /**
     * Update the animation for the given current time.
     * 
     * The current time needs to be relative to the same time source as the
     * previous call to start().
     * 
     * This method has no effect if the animation is not running.
     * 
     * Applications should rarely need to call this method directly. Instead,
     * animations are automatically updated by the owning master clock as time
     * progresses.
     * 
     * @param currentTime
     *            the current time, in milliseconds
     */
    public void update(long currentTime) {
        if (running) {
            internalUpdate(currentTime);
        }
    }
}
