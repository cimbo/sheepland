package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

/**
 * This interface is used by the ServerGameEngine to report of events outside
 * the normal flow of moves.
 * 
 * The engine will call methods here at the appropriate point of the game
 * advancement, and it is expected that the server implementation of it will
 * propagate them to all players. For this reason, all methods use identifiers
 * instead of game objects.
 * 
 * No other operation is expected from the implementor except for propagation to
 * clients.
 * 
 * @author Giovanni Campagna
 */
public interface ServerDelegate {
    /**
     * Record that the black sheep moved to the given tile.
     * 
     * @param destination
     *            the identifier of the new position of the black sheep
     */
    void didMoveBlackSheep(SheepTileIdentifier destination);

    /**
     * Record that the wolf moved to the given tile, and ate the given sheep.
     * 
     * @param destination
     *            the new position of the wolf
     * @param eatenSheep
     *            the sheep that was eaten by the wolf, or null if there was no
     *            sheep to eat
     */
    void didMoveWolf(SheepTileIdentifier destination, SheepIdentifier eatenSheep);

    /**
     * Record that the lambs at the given tile grew, and the passed adult sheep
     * were formed.
     * 
     * This method uses game objects instead of game object identifiers because
     * it expects the full serialized object will be sent to the client.
     * 
     * @param tile
     *            the tile were the growing happened
     * @param newAdultSheep
     *            the newly "born" adult sheep, replacing the lambs that
     *            disappeared due to growing
     */
    void didGrowLambs(SheepTileIdentifier tile, Sheep[] newAdultSheep);
}
