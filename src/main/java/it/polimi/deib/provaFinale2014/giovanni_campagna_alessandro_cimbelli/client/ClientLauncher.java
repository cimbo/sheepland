package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.CLIUIManager;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.GraphicUIManager;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.CommandLineParseException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.AutomaticClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.NetworkUtil;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketClientNetwork;

import java.net.UnknownHostException;

/**
 * The starting point of the client application.
 * 
 * @author Giovanni Campagna
 * 
 */
public class ClientLauncher {
    private ClientNetwork network;
    private boolean useGui;
    private boolean useUISet;

    /**
     * Construct a client launcher with the given command line arguments
     * 
     * @param args
     *            the client command line arguments
     * @throws CommandLineParseException
     *             if arguments where not recognized
     */
    public ClientLauncher(String[] args) throws CommandLineParseException {
        parseArguments(args);
        fillDefaultNetwork();
        fillDefaultUI();
    }

    private void maybeSetNetwork(ClientNetwork network)
            throws CommandLineParseException {
        if (this.network != null) {
            throw new CommandLineParseException(
                    "Multiple network options are not supported");
        }

        this.network = network;
    }

    private void maybeSetUI(boolean gui) throws CommandLineParseException {
        if (useUISet) {
            throw new CommandLineParseException(
                    "Multiple UI options are not supported");
        }

        useGui = gui;
        useUISet = true;
    }

    private void parseArguments(String[] args) throws CommandLineParseException {
        String connectOption = null;
        boolean inConnect = false;
        boolean inRmi = false;

        for (String arg : args) {
            if (inConnect && arg.startsWith("-")) {
                throw new CommandLineParseException(connectOption
                        + " requires an argument");
            }

            if (inConnect) {
                try {
                    maybeSetNetwork(new SocketClientNetwork(
                            NetworkUtil.parseHostPort(arg, false)));
                    inConnect = false;
                } catch (NumberFormatException e) {
                    throw new CommandLineParseException(
                            "Could not parse connect address", e);
                } catch (UnknownHostException e) {
                    throw new CommandLineParseException(
                            "Could not resolve connect address", e);
                }
            } else if (inRmi && !arg.startsWith("-")) {
                try {
                    maybeSetNetwork(new RMIClientNetwork(
                            NetworkUtil.parseHostPort(arg, false)));
                    inRmi = false;
                } catch (NumberFormatException e) {
                    throw new CommandLineParseException(
                            "Could not parse connect address", e);
                } catch (UnknownHostException e) {
                    throw new CommandLineParseException(
                            "Could not resolve connect address", e);
                }
            } else {
                if (inRmi) {
                    maybeSetNetwork(new RMIClientNetwork());
                    inRmi = false;
                }
                if ("-C".equals(arg) || "--connect".equals(arg)) {
                    connectOption = arg;
                    inConnect = true;
                } else if ("--socket".equals(arg)) {
                    maybeSetNetwork(new SocketClientNetwork());
                } else if ("--rmi".equals(arg)) {
                    inRmi = true;
                } else if ("--gui".equals(arg)) {
                    maybeSetUI(true);
                } else if ("--cli".equals(arg)) {
                    maybeSetUI(false);
                } else {
                    throw new CommandLineParseException("Unknown option " + arg);
                }
            }
        }
        if (inRmi) {
            maybeSetNetwork(new RMIClientNetwork());
        }
    }

    private void fillDefaultNetwork() {
        if (network == null) {
            network = new AutomaticClientNetwork();
        }
    }

    private void fillDefaultUI() {
        if (!useUISet) {
            useGui = false;
        }
    }

    /**
     * Run the client application.
     */
    public void run() {
        AbstractUI ui;
        if (useGui) {
            ui = new GraphicUIManager(network);
        } else {
            ui = new CLIUIManager(network);
        }

        ui.show();
    }
}
