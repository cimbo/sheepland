package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;

/**
 * The "kill" command (called KillSheep because it's followed by a sheep type),
 * kills a sheep on the given region.
 * 
 * @author Giovanni Campagna
 * 
 */
class KillSheepCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("kill <SHEEP TYPE> on <REGION>: kill a sheep on this region");
        baseHelpSheepType(ui);
        baseHelpRegion(ui);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, InvalidIdentifierException,
            RuleViolationException {
        if (ui.getChosenShepherd() == null) {
            ui.showMessage("A businessman like you would never touch a sheep directly!");
            if (GameOptions.get().getShowHints()) {
                ui.showMessage("[Use the be command to choose a shepherd before killing]");
            }
            return;
        }

        Game game = ui.getCurrentGame();
        SheepType type = tokenizer.readSheepType();
        tokenizer.expect("on");
        SheepTile tile = game.getSheepTileFromId(tokenizer.readSheepTile());

        Sheep sheep = tile.getAnySheepOfType(type);
        if (sheep == null) {
            ui.showMessage("You don't see anything like that there.");
            return;
        }

        game.killSheep(ui.getChosenShepherd(), tile, sheep);
    }
}