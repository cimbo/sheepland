package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The RMI interface implemented by the Server.
 * 
 * An object implementing this interface is available on the registry under the
 * name given by the SERVER_NAME constant.
 * 
 * @author Alessandro Cimbelli
 */

public interface Server extends Remote {
    /**
     * The name at which the server is reachable in the registry.
     */
    String SERVER_NAME = "it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.sheepland.server";

    /**
     * Initiate a new RMI connection with the server.
     * 
     * @param me
     *            the client interface
     * @return the server interface specific to the connecting client
     * @throws RemoteException
     */
    Game connect(Client me) throws RemoteException;
}
