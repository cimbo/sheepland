package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

/**
 * This class represents a "force end turn" move. See the base class
 * {@link Move} for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class ForceEndTurnMove extends Move {
    private static final long serialVersionUID = 1L;

    private final boolean forced;

    /**
     * Construct a new "force end turn" move
     * 
     * @param who
     *            the player executing the move
     * @param forced
     *            true if the move is executed by the server due to a timeout,
     *            false otherwise
     */
    public ForceEndTurnMove(PlayerIdentifier who, boolean forced) {
        super(who);
        this.forced = forced;
    }

    public boolean isForced() {
        return forced;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.forceEndTurn(getWho(engine));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (forced ? 1231 : 1237);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof ForceEndTurnMove)) {
            return false;
        }
        ForceEndTurnMove other = (ForceEndTurnMove) obj;
        if (forced != other.forced) {
            return false;
        }
        return true;
    }

}
