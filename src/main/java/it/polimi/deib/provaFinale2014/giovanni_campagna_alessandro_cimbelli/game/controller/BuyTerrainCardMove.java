package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

/**
 * This class represents a "buy terrain card" move. See the base class
 * {@link Move} for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class BuyTerrainCardMove extends ShepherdMove {
    private static final long serialVersionUID = 1L;

    private final TerrainCardIdentifier cardId;

    /**
     * Construct a new "buy terrain card" move
     * 
     * @param who
     *            the player executing the move
     * @param which
     *            the shepherd he is using
     * @param card
     *            the card to buy
     */
    public BuyTerrainCardMove(PlayerIdentifier who, ShepherdIdentifier which,
            TerrainCardIdentifier card) {
        super(who, which);
        cardId = card;
    }

    /**
     * Retrieve the actual card object, by resolving the identifier through the
     * passed in engine.
     * 
     * @param engine
     * @return the card as game object
     * @throws InvalidIdentifierException
     */
    protected TerrainCard getCard(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getTerrainCardFromId(cardId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.buyTerrainCard(getWho(engine), getWhich(engine), getCard(engine));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + cardId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof BuyTerrainCardMove)) {
            return false;
        }
        BuyTerrainCardMove other = (BuyTerrainCardMove) obj;
        if (!cardId.equals(other.cardId)) {
            return false;
        }
        return true;
    }
}
