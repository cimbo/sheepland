package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Provide initial settings to the game engine.
 * 
 * An object of this class is constructed by the application layer and passed to
 * the GameEngine (which in turn passes it down to the GameBoard)
 * 
 * It provides options such as enabled and disabled rules, minimum and maximum
 * players, etc. It also provides a "version", which is used as a marker to
 * check that the serialized objects are compatible with the local versions in
 * terms of the code and constants, or in other words that the server is using
 * the same version of the game logic as the client.
 * 
 * The RuleSet class implements a "seal" system, that prevents changes to the
 * state after the rules are in use: in order to pass the rule set through
 * serialization or to use them to initialize a game engine, the object must be
 * sealed. Once sealed, all methods that would change the ruleset internal state
 * will throw an IllegalStateException.
 * 
 * Currently, there are no mutable fields in a RuleSet class or subclass, but
 * this might change in the future.
 * 
 * @author Giovanni Campagna
 * 
 */
public class RuleSet implements Serializable {
    /**
     * The current version of the game rules.
     * 
     * This applies to the ruleset object here (the values returned by the
     * public API) as well as any implicit rule that is written in GameEngine
     * and GameState.
     * 
     * Any change to the model constraints to make them more restrictive is also
     * a rule version incompatibility.
     * 
     * If incompatible changes to the game rules are performed, and
     * compatibility with older clients or servers is desired, the version here
     * must be increased, and the game engine must be updated to account for
     * that.
     */
    public static final int CURRENT_VERSION = 1;

    // Unlike CURRENT_VERSION, which is also a marker to determine
    // incompatibility
    // between server and client, this should be bumped only when changing the
    // public API or the field layout of this class
    private static final long serialVersionUID = 1L;

    private final int version;
    private transient boolean sealed;

    /**
     * Construct a new rule set object
     */
    public RuleSet() {
        version = CURRENT_VERSION;
    }

    /**
     * Retrieves the version of the rules.
     * 
     * This will always be equal to CURRENT_VERSION for the process that creates
     * the ruleset, but might not be if the ruleset is received over some
     * channel.
     * 
     * As such, the version field can be used to check the compatibility between
     * the client and server game logic.
     * 
     * @return the version of the rules
     */
    public int getVersion() {
        return version;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return getClass().hashCode() ^ getVersion();
    }

    /**
     * Checks that two ruleset objects are equal.
     * 
     * Because rulesets don't have state, this checks that the objects are of
     * the same version and subclass.
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof RuleSet)) {
            return false;
        }

        return o.getClass().equals(getClass())
                && ((RuleSet) o).getVersion() == getVersion();
    }

    /**
     * Check if this object was sealed.
     * 
     * @return true, if the object was sealed, false otherwise
     */
    public boolean isSealed() {
        return sealed;
    }

    /**
     * Seal the object, preventing future changes.
     * 
     * Sealing the object multiple times is allowed and has no effect.
     */
    public void seal() {
        sealed = true;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        if (!sealed) {
            throw new IllegalStateException(
                    "Before serializing a RuleSet object you must seal it");
        }

        out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException,
            ClassNotFoundException {
        in.defaultReadObject();
        seal();
    }

    /**
     * Retrieve the maximum number of players in one game, irrespective of the
     * RuleSet used.
     * 
     * @return the maximum number of players.
     */
    public static int getMaxNumberOfPlayers() {
        return 4;
    }

    /**
     * Retrieve the amount of coins each player has at the beginning of the
     * game.
     * 
     * @return the initial coins
     */
    public int getInitialCoinsPerPlayer() {
        return 20;
    }

    /**
     * Retrieve the number of non-final fences in a game.
     * 
     * This is also the number of times any shepherd must move before entering
     * the final phase, and therefore determines the length of the game.
     * 
     * @return the number of fences
     */
    public int getNumberOfFences() {
        return 20;
    }

    /**
     * Retrieve the number of final fences in a game.
     * 
     * To avoid any possible deadlock, this number must be at least equal to the
     * number of players times the number of moves in a turn.
     * 
     * @return the number of final fences
     */
    public int getNumberOfFinalFences() {
        return getMaxNumberOfPlayers() * getMovesPerTurn();
    }

    /**
     * Retrieve the number of cards that can be bought from the board during the
     * playing phase.
     * 
     * This is also 1 plus the maximum cost of a terrain card bought from the
     * board.
     * 
     * @return
     */
    public int getBuyableCards() {
        return 5;
    }

    /**
     * Retrieve the number of shepherds each player controls.
     * 
     * @return the number of shepherds per player
     */
    public int getNumberOfShepherdsPerPlayer() {
        return 1;
    }

    /**
     * Retrieve the maximum number of moves in a playing (not INIT or market)
     * turn.
     * 
     * @return the number of moves
     */
    public int getMovesPerTurn() {
        return 3;
    }

    /**
     * Retrieve the maximum price of a card sold at the market.
     * 
     * @return the maximum card price
     */
    public int getMaximumCardPrice() {
        return 4;
    }
}
