package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This is a helper class that deals with checking that a connection is alive.
 * 
 * Once created, a pinger will periodically queue a "ping" command on the
 * associated outgoing connection, and will check that a "pong" command is
 * received by the end of the next ping timeout.
 * 
 * A pinger is strongly associated with the outgoing connection, and must be
 * closed before closing the queue.
 * 
 * @author Alessandro Cimbelli
 */
public class Pinger extends TimerTask {
    private final ServerPlayer player;
    private final SenderThread sender;
    private final Timer timer;
    private boolean ponged;

    /**
     * Construct a new pinger, pinging on a specific outgoing connection.
     * 
     * The pinger is tied to the connection, if the latter is closed the pinger
     * must be closed as well.
     * 
     * The pinger will report to player if the pong is not received before the
     * timeout.
     * 
     * This constructor must be called holding ServerPlayer's lock.
     * 
     * @param sender
     *            the connection queue on which to write ping commands
     * @param player
     *            the player to which ping timeouts should be reported
     * @param options
     *            the GameOptions controlling ping timeouts
     */
    public Pinger(SenderThread sender, ServerPlayer player, GameOptions options) {
        this.sender = sender;
        this.player = player;
        timer = new Timer();

        // Schedule one immediately, then one each period
        timer.schedule(this, 0, options.getPingTimeout() * 1000);

        // Start out ponged.
        ponged = true;
    }

    /**
     * Close this Pinger, unscheduling any running timeout.
     * 
     * This method is threadsafe.
     * 
     * Any operation on this pinger after calling this method (except other
     * close()) is undefined.
     */
    public void close() {
        timer.cancel();
    }

    /**
     * Run an iteration of this pinger timeout.
     * 
     * It will check that a valid "pong" command was received, and send a new
     * ping.
     */
    @Override
    public void run() {
        synchronized (player) {
            if (ponged) {
                ponged = false;
                sender.sendPing();
            } else {
                player.reportIOWriteException(sender);
            }
        }
    }

    /**
     * Mark that the client successfully responded to a ping.
     * 
     * Must be called holding ServerPlayer's lock.
     */
    public void pong() {
        assert Thread.holdsLock(player);

        ponged = true;
    }
}
