package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.BuyTerrainCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

/**
 * The UI counterpart of a buy terrain card move.
 * 
 * @author Giovanni Campagna
 * 
 */
public class BuyTerrainCardUIMove extends BuyTerrainCardMove implements UIMove {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a buy terrain card move.
     * 
     * @param who
     *            the player doing it
     * @param which
     *            the shepherd doing it
     * @param card
     *            the card involved
     */
    public BuyTerrainCardUIMove(PlayerIdentifier who, ShepherdIdentifier which,
            TerrainCardIdentifier card) {
        super(who, which, card);
    }

    /**
     * {@inheritDoc}
     */
    public void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateBoughtTerrainCard(getWho(engine), getWhich(engine),
                getCard(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        // This is not a completable move, so we have nothing to do at the end
        //
        // (this comment courtesy of sonar)
    }

}
