package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

/**
 * An interface for low-level input implementation capable of obtaining a single
 * line of input from the user.
 * 
 * @author Giovanni Campagna
 */
interface LineReader {
    /**
     * Prompt the user (with the given prompt) and return the next line of
     * input.
     * 
     * @param prompt
     *            the prompt to show
     * @return the input line
     */
    String nextLine(String prompt);
}
