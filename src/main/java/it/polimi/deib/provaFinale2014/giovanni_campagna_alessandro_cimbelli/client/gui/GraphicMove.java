package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

/**
 * The state of the GUI state machine.
 * 
 * @author Alessandro Cimbelli
 */
public enum GraphicMove {

    NULL, OPPONENT_TURN, PLACE_SHEPHERD, JUMP_TURN, CHOOSE_SHEPHERD, MOVE_SHEPHERD, MOVING_SHEPHERD, KILL_SHEEP, KILLING_SHEEP, MATE_SHEEP, MATING_SHEEP, BUY_TERRAIN_CARD, BUYING_TERRAIN_CARD, MOVE_SHEEP, MOVING_SHEEP, MARKET_SELL, MARKET_BUY

}
