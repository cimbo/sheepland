package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * A simple image drawing component with a fixed size.
 * 
 * @author Alessandro Cimbelli
 */
public class ImageComponent extends JPanel {
    private static final long serialVersionUID = 1L;
    private final Image img;

    /**
     * Construct a new image component, loading the specified image resource and
     * with the given size.
     * 
     * @param xSize
     *            the width
     * @param ySize
     *            the height
     * @param path
     *            the path of the image to load
     */
    public ImageComponent(int xSize, int ySize, String path) {
        img = ImageLoader.loadImageSync(this, path);
        setMaximumSize(new Dimension(xSize, ySize));
        setMinimumSize(new Dimension(xSize, ySize));
        this.setSize(xSize, ySize);
        setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintComponent(Graphics g) {
        setOpaque(false);
        g.drawImage(img, 0, 0, null);
        super.paintComponent(g);
    }
}
