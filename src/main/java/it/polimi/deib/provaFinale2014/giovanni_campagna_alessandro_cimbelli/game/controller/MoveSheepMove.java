package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * This class represents a "move sheep" move. See the base class {@link Move}
 * for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class MoveSheepMove extends ShepherdMove {
    private static final long serialVersionUID = 1L;

    private final SheepTileIdentifier fromId;
    private final SheepTileIdentifier toId;
    private final SheepIdentifier sheepId;

    /**
     * Construct a new "move sheep" move
     * 
     * @param who
     *            the player executing the move
     * @param which
     *            the shepherd he is using
     * @param from
     *            the origin of the move
     * @param to
     *            the destination of the move
     * @param sheep
     *            the sheep to move
     */
    public MoveSheepMove(PlayerIdentifier who, ShepherdIdentifier which,
            SheepTileIdentifier from, SheepTileIdentifier to,
            SheepIdentifier sheep) {
        super(who, which);
        fromId = from;
        toId = to;
        sheepId = sheep;
    }

    /**
     * Retrieve the actual from tile object, by resolving the identifier through
     * the passed in engine.
     * 
     * @param engine
     * @return the from tile as game object
     * @throws InvalidIdentifierException
     */
    protected SheepTile getFrom(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getSheepTileFromId(fromId);
    }

    /**
     * Retrieve the actual to tile object, by resolving the identifier through
     * the passed in engine.
     * 
     * @param engine
     * @return the to tile as game object
     * @throws InvalidIdentifierException
     */
    protected SheepTile getTo(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getSheepTileFromId(toId);
    }

    /**
     * Retrieve the actual sheep object, by resolving the identifier through the
     * passed in engine.
     * 
     * @param engine
     * @return the sheep as game object
     * @throws InvalidIdentifierException
     */
    protected Sheep getSheep(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getSheepFromId(sheepId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.moveSheep(getWho(engine), getWhich(engine), getFrom(engine),
                getTo(engine), getSheep(engine));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + fromId.hashCode();
        result = prime * result + sheepId.hashCode();
        result = prime * result + toId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof MoveSheepMove)) {
            return false;
        }
        MoveSheepMove other = (MoveSheepMove) obj;
        if (!fromId.equals(other.fromId)) {
            return false;
        }
        if (!sheepId.equals(other.sheepId)) {
            return false;
        }
        if (!toId.equals(other.toId)) {
            return false;
        }
        return true;
    }
}