package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerSender;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The socket implementation of ServerConnection
 * 
 * @author Alessandro Cimbelli
 */
public class SocketServerConnection implements ServerConnection, ServerSender {

    private final Socket socket;
    private final InputStream socketInputStream;
    private final ObjectOutputStream socketOutputStream;
    private ServerReaderThread readerThread;

    /**
     * Construct a SocketServerConnection for the given already connected
     * socket.
     * 
     * @param socket
     *            the accepted socket
     * @throws IOException
     *             if the initial handshake failed
     */
    public SocketServerConnection(Socket socket) throws IOException {
        this.socket = socket;
        socketInputStream = socket.getInputStream();
        socketOutputStream = new ObjectOutputStream(socket.getOutputStream());
        // flush immediately to unblock the reader thread on the other side
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public ServerSender getServerSender() {
        return this;
    }

    /**
     * Set the server receiver for this connection.
     * 
     * This will also ensure that a reader thread exists, feeding input. Because
     * access to the reader thread is not synchronized (and cannot be), calls to
     * this method after the first time MUST happen from the reader thread
     * itself.
     */
    public void setServerReceiver(ServerReceiver receiver) {
        if (readerThread == null) {
            readerThread = new ServerReaderThread(this, receiver,
                    socketInputStream);
            readerThread.start();
        } else {
            readerThread.setServerReceiver(receiver);
        }
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void close() {
        try {
            socketInputStream.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, "IO exception closing", e);
        }
        try {
            socketOutputStream.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, "IO exception closing", e);
        }
        try {
            socket.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, "IO exception closing", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void sendSetup(RuleSet rules, GameBoard board, PlayerIdentifier self)
            throws IOException {
        // Initial send
        presetCommand(ClientOperation.SETUP);

        // Send arguments
        socketOutputStream.writeObject(rules);
        socketOutputStream.writeObject(board);
        socketOutputStream.writeObject(self);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendMoveAcknowledged(Exception result) throws IOException {
        // Initial send
        presetCommand(ClientOperation.MOVE_ACKNOWLEDGED);

        // Send arguments
        socketOutputStream.writeObject(result);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendForceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) throws IOException {
        // Initial send
        presetCommand(ClientOperation.FORCE_SYNCHRONIZE);

        // Send arguments
        socketOutputStream.writeObject(rules);
        socketOutputStream.writeObject(newBoard);
        socketOutputStream.writeObject(self);
        socketOutputStream.writeObject(newState);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendAdvanceNextTurn(int turn, GamePhase phase)
            throws IOException {
        // Initial send
        presetCommand(ClientOperation.ADVANCE_NEXT_TURN);

        // Send arguments
        socketOutputStream.writeInt(turn);
        socketOutputStream.writeObject(phase);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendBlackSheepMoved(SheepTileIdentifier destination)
            throws IOException {
        // Initial send
        presetCommand(ClientOperation.BLACK_SHEEP_MOVED);

        // Send arguments
        socketOutputStream.writeObject(destination);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendWolfMoved(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) throws IOException {
        // Initial send
        presetCommand(ClientOperation.WOLF_MOVED);

        // Send arguments
        socketOutputStream.writeObject(destination);
        socketOutputStream.writeObject(eatenSheep);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendLambsGrew(SheepTileIdentifier where, Sheep[] newAdultSheep)
            throws IOException {
        // Initial send
        presetCommand(ClientOperation.LAMBS_GREW);

        // Send arguments
        socketOutputStream.writeObject(where);
        socketOutputStream.writeObject(newAdultSheep);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendDidExecuteMove(Move move) throws IOException {
        // Initial send
        presetCommand(ClientOperation.DID_EXECUTE_MOVE);

        // Send arguments
        socketOutputStream.writeObject(move);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendLoginAnswer(boolean answer) throws IOException {
        // Initial send
        presetCommand(ClientOperation.LOGIN_ANSWER);

        // Send arguments
        socketOutputStream.writeObject(answer);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendPing() throws IOException {
        // Initial send
        presetCommand(ClientOperation.PING);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendEndGame(Map<PlayerIdentifier, Integer> scores)
            throws IOException {
        // Initial send
        presetCommand(ClientOperation.END_GAME);

        // Send arguments
        socketOutputStream.writeObject(scores);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendPlayerLeft(PlayerIdentifier who, boolean clean)
            throws IOException {
        // Initial send
        presetCommand(ClientOperation.PLAYER_LEFT);

        // Send arguments
        socketOutputStream.writeObject(who);
        socketOutputStream.writeObject(clean);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendPlayerJoined(PlayerIdentifier who) throws IOException {
        // Initial send
        presetCommand(ClientOperation.PLAYER_JOINED);

        // Send arguments
        socketOutputStream.writeObject(who);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendAbandonComplete() throws IOException {
        // Initial send
        presetCommand(ClientOperation.ABANDON_COMPLETE);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    // This is a function that sends the typical bits of a function's call
    private void presetCommand(ClientOperation operation) throws IOException {

        // Now i send to the client the informations according to the network
        // protocol

        // First 8 bit for the version of the protocol
        socketOutputStream.writeByte(Protocol.V1.getVersion());
        // Bits for future changes
        socketOutputStream.writeInt(0);
        // 32 Bits for the kind of the move
        socketOutputStream.writeInt(operation.ordinal());
    }

}
