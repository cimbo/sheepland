package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * A button that activates one of the game moves.
 * 
 * @author Alessandro Cimbelli
 * 
 */
public class MoveButton extends JButton {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 50;
    private static final int Y_SIZE = 50;
    private final GraphicMove move;

    /**
     * Construct a new move button, with the given label and move enumeration.
     * 
     * @param text
     *            the label on the button
     * @param move
     *            the associated move
     */
    public MoveButton(String text, GraphicMove move) {
        JLabel textLabel = new JLabel("<html>" + text.replace("\n", "<br/>")
                + "</html>");
        textLabel.setHorizontalAlignment(SwingConstants.CENTER);
        textLabel.setHorizontalTextPosition(SwingConstants.CENTER);
        textLabel.setVerticalAlignment(SwingConstants.CENTER);
        textLabel.setFont(new Font("Arial", Font.PLAIN, 9));
        textLabel.setSize(Y_SIZE, Y_SIZE);
        textLabel.setForeground(new Color(0, 0, 0));
        textLabel.setLocation(0, 0);
        this.add(textLabel);
        setLayout(null);

        this.setSize(X_SIZE, Y_SIZE);
        setBackground(new Color(255, 255, 0));

        this.move = move;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(new Color(255, 255, 0));
        g.fillRect(5, 5, getWidth() - 10, getHeight() - 10);
    }

    /**
     * Retrieve the move associated with this button
     * 
     * @return the move
     */
    public GraphicMove getMove() {
        return move;
    }
}
