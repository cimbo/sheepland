package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

/**
 * An icon of a coin.
 * 
 * @author Alessandro Cimbelli
 */
public class Coin extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 30;
    private static final int Y_SIZE = 30;

    /**
     * Construct a new coin icon.
     */
    public Coin() {
        super(X_SIZE, Y_SIZE, "coin.png");
    }
}
