package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import java.io.IOException;

/**
 * A small Runnable-like interface to represent an item queued in an
 * AbstractSenderThread.
 * 
 * @author Giovanni Campagna
 * 
 */
public interface IORunnable {
    /**
     * Run this queued item.
     * 
     * @throws IOException
     */
    void run() throws IOException;
}
