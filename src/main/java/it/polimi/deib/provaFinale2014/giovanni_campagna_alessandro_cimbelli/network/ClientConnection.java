package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import java.io.Closeable;

/**
 * An interface for a connection to the server (a connection from the point of
 * view of the client).
 * 
 * @author Giovanni Campagna
 */
public interface ClientConnection extends Closeable {
    /**
     * Retrieves the writing side of the connection.
     * 
     * @return a ClientSender, which can be used to send commands to the server.
     */
    ClientSender getClientSender();

    /**
     * Sets a receiver to handle commands on the connections. Methods on
     * ClientReceiver will be invoked when commands are received from the
     * server.
     * 
     * It is unspecified from which thread the methods will be invoked.
     * 
     * @param receiver
     *            the ClientReceiver implementation
     */
    void setClientReceiver(ClientReceiver receiver);

    /**
     * Close the connection.
     * 
     * Closing the connection will never fail.
     */
    void close();
}
