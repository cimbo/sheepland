package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;

/**
 * Identifies a road square.
 * 
 * The numerical ID of a road square is constructed from the tile codes of the
 * incident tiles, taking the minimum among the two, shifting by 8 and adding
 * the maximum code.
 * 
 * The result is thus (MSB): [0 -0- 15] [16 -minCode- 23] [24 -maxCode -31]
 * 
 * This means that road squares in the same position will always have the same
 * ID, even across different games.
 * 
 * @author Giovanni Campagna
 */
public class RoadSquareIdentifier extends GameObjectIdentifier {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a road square identifier given the tile codes (numerical IDs)
     * of the two incident tiles.
     * 
     * The order of the arguments is irrelevant.
     * 
     * @param tile1
     * @param tile2
     */
    public RoadSquareIdentifier(int tile1, int tile2) {
        super(RoadSquare.class, (Math.min(tile1, tile2) << 8)
                + Math.max(tile1, tile2));
    }
}
