package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;

/**
 * This class create a sheep icon to put on a tile
 * 
 * @author Alessandro Cimbelli
 * 
 */

public class SheepIcon extends AnimalIcon {
    private static final long serialVersionUID = 1L;
    private final SheepType type;

    /**
     * Construct a sheep icon of the given type.
     * 
     * @param type
     *            the sheep type to represent.
     */
    public SheepIcon(SheepType type) {
        super(type.name().toLowerCase() + ".png");
        this.type = type;
    }

    /**
     * Retrieve the type of sheep this icon represents.
     * 
     * @return the sheep type
     */
    public SheepType getType() {
        return type;
    }
}
