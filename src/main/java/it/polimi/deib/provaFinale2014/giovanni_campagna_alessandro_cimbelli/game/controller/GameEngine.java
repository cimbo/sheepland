package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the main class of the game engine, or controller. It takes care of a
 * single game of Sheepland, and is essentially a state machine that goes
 * through the several phases of the game, feed through the public API which is
 * called by the IO layer above.
 * 
 * It is an abstract class of which two implementation exists, ServerGameEngine
 * and ClientGameEngine, whose main difference is the amount of knowledge of the
 * game state they have and the ability to make arbitrary (random) choices.
 * 
 * @author Giovanni Campagna
 * 
 */
public abstract class GameEngine {
    private RuleSet rules;
    private GameBoard board;
    private GameState state;

    /**
     * Construct a game engine.
     */
    protected GameEngine() {
        state = new GameState();
    }

    /**
     * Initialize the engine with a given game board.
     * 
     * This will be called on the server to set the newly constructed board, and
     * on the client to setup the engine for the board received from the server.
     * 
     * @param newboard
     *            the board to use.
     */
    protected void setGameBoard(GameBoard newboard) {
        board = newboard;
    }

    /**
     * Retrieves the game board currently in use.
     * 
     * Callers must be aware that, because of synchronization, the board can
     * change at any time, and therefore it or any component game object must
     * not be stored outside
     * 
     * @return the board currently in use.
     */
    public GameBoard getGameBoard() {
        return board;
    }

    /**
     * Returns the rules of the game, as configured before starting, or null if
     * not yet set.
     * 
     * @return the rules
     */
    public RuleSet getRules() {
        return rules;
    }

    /**
     * Returns the current phase of the game.
     * 
     * See {@link GamePhase} for the meaning of this return value, and the
     * allowed moves and calls at each time.
     * 
     * @return the current phase
     */
    public GamePhase getGamePhase() {
        return state.getCurrentPhase();
    }

    /**
     * Retrieves the full game state object.
     * 
     * This method is provided only to allow full game serialization. Any
     * interaction with it other than sending it over a channel must be avoided.
     * 
     * @return the full game state
     */
    public GameState getGameState() {
        return state;
    }

    /**
     * Convert a player identifier into a player game object
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not valid
     */
    public Player getPlayerFromId(PlayerIdentifier id)
            throws InvalidIdentifierException {
        return board.getPlayerFromId(id);
    }

    /**
     * Convert a shepherd identifier (valid for a given player) into a shepherd
     * game object
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not valid
     */
    public Shepherd getShepherdFromId(Player owner, ShepherdIdentifier id)
            throws InvalidIdentifierException {
        return owner.getShepherdFromId(id);
    }

    /**
     * Convert a tile identifier into a tile game object
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not valid
     */
    public SheepTile getSheepTileFromId(SheepTileIdentifier id)
            throws InvalidIdentifierException {
        return board.getMap().getSheepTileFromId(id);
    }

    /**
     * Convert a road square identifier into a road square game object
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not valid
     */
    public RoadSquare getRoadSquareFromId(RoadSquareIdentifier id)
            throws InvalidIdentifierException {
        return board.getMap().getRoadSquareFromId(id);
    }

    /**
     * Convert a sheep identifier into a sheep game object
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not valid
     */
    public Sheep getSheepFromId(SheepIdentifier id)
            throws InvalidIdentifierException {
        return board.getSheepFromId(id);
    }

    /**
     * Convert a card identifier into a card game object
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not valid
     */
    public TerrainCard getTerrainCardFromId(TerrainCardIdentifier id)
            throws InvalidIdentifierException {
        return board.getTerrainCardFromId(id);
    }

    /**
     * Retrieves the Player expected to play in the current turn.
     * 
     * @return the currently playing Player
     */
    public Player getCurrentPlayer() {
        return getPlayingOrder()[state.getCurrentTurn()];
    }

    /**
     * Retrieves the full playing order, that is the sequence of all players.
     * 
     * The first player is always result[0].
     * 
     * For performance reasons, the array is not copied, and therefore must not
     * be modified by the caller.
     * 
     * @return the playing order
     */
    public Player[] getPlayingOrder() {
        if (board == null) {
            throw new GameInvariantViolationException(
                    "playing order is not initialized yet");
        }

        return board.getPlayingOrder();
    }

    /**
     * Fully synchronize the state of the game.
     * 
     * This should only be called after external changes happened that cannot be
     * reconstructed (for example the connection to the server was lost).
     * 
     * The game engine will start again at the beginning of the turn of the
     * player indexed by turn.
     * 
     * The old game state (GameBoard) is discarded, and should not be used
     * again.
     * 
     * @param newboard
     *            the new GameBoard, replacing the old one
     * @param newstate
     *            the new state of the game
     */
    public void forceSynchronizeState(GameBoard newboard, GameState newstate)
            throws InvalidIdentifierException {
        if (state.getCurrentPhase() == GamePhase.END) {
            throw new GameInvariantViolationException(
                    "State cannot be synchronized after the end of the game");
        }
        if (newstate.getCurrentPhase() == GamePhase.PREINIT) {
            throw new GameInvariantViolationException(
                    "State cannot be synchronized from a PREINIT state object");
        }

        state = newstate;
        board = newboard;
        state.setRules(rules);
    }

    // Helpers

    private void checkTurn(Player who) throws RuleViolationException {
        if (who != board.getPlayingOrder()[state.getCurrentTurn()]) {
            throw new RuleViolationException("Player is playing out of turn");
        }
    }

    private void checkStateAndTurn(GamePhase expected, Player who)
            throws RuleViolationException {
        checkStateAndTurn(expected, expected, who);
    }

    private void checkStateAndTurn(GamePhase expected, GamePhase alternative,
            Player who) throws RuleViolationException {
        if (state.getCurrentPhase() != expected
                && state.getCurrentPhase() != alternative) {
            throw new RuleViolationException(
                    "This move is not allowed at this point of the game");
        }
        checkTurn(who);
    }

    private void checkBeforeMove(Player who, MoveType move)
            throws RuleViolationException {
        checkStateAndTurn(GamePhase.PLAY, GamePhase.FINAL, who);
        state.checkBeforeMove(move);
    }

    private void checkBeforeMove(Player who, Shepherd which, MoveType move)
            throws RuleViolationException {
        checkBeforeMove(who, move);
        state.checkBeforeMove(which, move);

        if (!who.ownsShepherd(which)) {
            throw new RuleViolationException("Player does not control shepherd");
        }
    }

    // PREINIT state

    /**
     * Set the rules of the game.
     * 
     * It is allowed to call this at any point of the game, but it will throw an
     * exception if called twice with two rulesets that are not equal.
     * 
     * @param newrules
     *            the new rules of the game.
     */
    public void setRules(RuleSet newrules) throws RuleViolationException {
        if (!newrules.isSealed()) {
            throw new IllegalArgumentException(
                    "RuleSet must be sealed when the game engine is configured");
        }

        if (rules != null && !rules.equals(newrules)) {
            throw new RuleViolationException("Cannot change the rules midgame");
        } else if (rules == null
                && newrules.getVersion() != RuleSet.CURRENT_VERSION) {
            throw new RuleViolationException(
                    "This version of the rules is not supported");
        }

        rules = newrules;
        state.setRules(newrules);
    }

    // INIT state

    /**
     * Moves the Shepherd \which (belonging to Player \who) to the RoadSquare
     * \to, before the beginning of the game, if allowed by the rules.
     * 
     * @param who
     *            the Player who decides to move one of his shepherds to @to
     * @param which
     *            the Shepherd to move
     * @param to
     *            the destination of this movement
     * @throws RuleViolationException
     */
    public void initiallyPlaceShepherd(Player who, Shepherd which, RoadSquare rs)
            throws RuleViolationException {
        checkStateAndTurn(GamePhase.INIT, who);
        if (!who.ownsShepherd(which)) {
            throw new RuleViolationException("Player does not control shepherd");
        }

        if (rs.getCurrentShepherd() != null) {
            throw new RuleViolationException(
                    "Square is already occupied with another shepherd");
        }
        if (which.isPositioned()) {
            throw new RuleViolationException(
                    "Shepherd is already initially positioned");
        }

        which.setCurrentPosition(rs);
        rs.addShepherd(which);

        state.recordMove(which, MoveType.INITIALLY_PLACE);
    }

    // PLAY->FINAL transition

    private void checkAndAdvanceFinalState() {
        if (state.getCurrentPhase() == GamePhase.PLAY
                && board.getRemainingFences() == 0) {
            state.advanceFinalState();
        }
    }

    // PLAY+FINAL states

    /**
     * Moves the Shepherd \which (belonging to Player \who) to the RoadSquare
     * \to, if allowed by the rules of the game, and potentially subtracting
     * money from \who in the process. The origin of the movement is always the
     * current position of \who
     * 
     * @param who
     *            the Player who decides to move one of his shepherds to @to
     * @param which
     *            the Shepherd to move
     * @param to
     *            the destination of this movement
     * @throws RuleViolationException
     */
    public void moveShepherd(Player who, Shepherd which, RoadSquare to)
            throws RuleViolationException {
        checkBeforeMove(who, which, MoveType.MOVE_SHEPHERD);

        if (to.getHasFence() || to.getCurrentShepherd() != null) {
            throw new RuleViolationException(
                    "Destination square is already occupied");
        }

        RoadSquare from = which.getCurrentPosition();
        GameMap map = board.getMap();

        if (!map.areSquaresAdjacent(from, to)) {
            if (who.getRemainingCoins() == 0) {
                throw new RuleViolationException(
                        "Movement requires one coin, and player has none");
            }

            who.payCoins(1);
        }

        from.removeShepherd(which);
        if (state.getCurrentPhase() == GamePhase.PLAY) {
            from.placeFence(false);
        } else {
            assert state.getCurrentPhase() == GamePhase.FINAL;
            from.placeFence(true);
        }
        if (state.getCurrentPhase() == GamePhase.PLAY) {
            board.markFencePlaced();
        } else {
            board.markFinalFencePlaced();
        }

        to.addShepherd(which);
        which.setCurrentPosition(to);

        state.recordMove(which, MoveType.MOVE_SHEPHERD);
        checkAndAdvanceFinalState();
    }

    /**
     * Executes a move on behalf of Player who and Shepherd which, and moves one
     * sheep from the SheepTile from to the SheepTile to
     */
    public void moveSheep(Player who, Shepherd which, SheepTile from,
            SheepTile to, Sheep sheep) throws RuleViolationException {
        checkBeforeMove(who, which, MoveType.MOVE_SHEEP);

        GameMap map = board.getMap();
        RoadSquare playerPosition = which.getCurrentPosition();
        if (!map.isSquareAdjacentToTile(from, playerPosition)
                || !map.isSquareAdjacentToTile(to, playerPosition)) {
            throw new RuleViolationException(
                    "Sheep can only be moved to/from tiles adjacent to the shepherd");
        }
        if (from == to) {
            throw new RuleViolationException(
                    "Sheep cannot be moved on the same region");
        }

        if (!from.containsSheep(sheep)) {
            throw new RuleViolationException(
                    "The origin tile does not contain this sheep");
        }

        if (sheep.getType() == SheepType.BLACK_SHEEP) {
            if (from != map.getCurrentBlackSheepPosition()) {
                // This is a GIVE, not a RVE, because we checked
                // that the from tile contains this sheep
                throw new GameInvariantViolationException(
                        "The black sheep is not where we expected it to be");
            }

            map.moveBlackSheep(sheep, to);
        } else {
            from.removeSheep(sheep);
            to.addSheep(sheep);
        }

        state.recordMove(which, MoveType.MOVE_SHEEP);
        checkAndAdvanceFinalState();
    }

    /**
     * Buy the terrain card @card for Player @who (provided that enough cards
     * exist already, and the player has enough money)
     */
    public void buyTerrainCard(Player who, Shepherd which, TerrainCard card)
            throws RuleViolationException {
        checkBeforeMove(who, which, MoveType.BUY_TERRAIN);

        RoadSquare playerPosition = which.getCurrentPosition();
        SheepTile[] incidentTiles = board.getMap().getIncidentTiles(
                playerPosition);
        boolean correctType = false;
        for (SheepTile tile : incidentTiles) {
            if (tile.getTerrainType() == card.getType()) {
                correctType = true;
                break;
            }
        }
        if (!correctType) {
            throw new RuleViolationException(
                    "Only a card of the same type as one of the adjacent regions can be bought");
        }
        if (!card.isForSale() || card.getOwner() != null) {
            throw new RuleViolationException("This card is not for sale");
        }

        if (who.getRemainingCoins() < card.getCost()) {
            throw new RuleViolationException(
                    "Player does not have enough coins left");
        }

        who.acquireCard(card);
        card.markAcquired(who);
        board.markCardAcquired(card);

        state.recordMove(which, MoveType.BUY_TERRAIN);
        checkAndAdvanceFinalState();
    }

    /**
     * Mate two sheep on SheepTile tile (controlled by Shepherd which),
     * producing a new lamb on that tile.
     * 
     * This does not actually execute the mating, call completeMateSheep() on a
     * ClientGameEngine or realMateSheep() on a ServerGameEngine to do so.
     */
    public void mateSheep(Player who, Shepherd which, SheepTile tile)
            throws RuleViolationException {
        checkBeforeMove(who, which, MoveType.MATE);

        RoadSquare currentPosition = which.getCurrentPosition();
        GameMap map = getGameBoard().getMap();

        if (!map.isSquareAdjacentToTile(tile, currentPosition)) {
            throw new RuleViolationException(
                    "The chosen tile is not adjacent to the sheperd");
        }
        if (tile.getNumberOfSheep(SheepType.FEMALE_SHEEP) == 0
                || tile.getNumberOfSheep(SheepType.RAM) == 0) {
            throw new RuleViolationException(
                    "Mating requires a ram and a female sheep on the tile");
        }

        state.recordMove(which, MoveType.MATE);
        checkAndAdvanceFinalState();
    }

    protected List<Shepherd> getAllAdjacentOpponentShepherds(Player who,
            Shepherd which) {
        GameMap map = board.getMap();
        RoadSquare currentPosition = which.getCurrentPosition();
        List<Shepherd> result = new ArrayList<Shepherd>();

        for (Player player : board.getPlayingOrder()) {
            if (player == who) {
                continue;
            }

            for (Shepherd shepherd : player.getAllShepherds()) {
                if (map.areSquaresAdjacent(currentPosition,
                        shepherd.getCurrentPosition())) {
                    result.add(shepherd);
                }
            }
        }

        return result;
    }

    /**
     * Kill a sheep of SheepType type on SheepTile tile (controlled by Shepherd
     * which), and return that players that will be paid for it.
     * 
     * This does not actually execute the mating, call completeMateSheep() on a
     * ClientGameEngine or realMateSheep() on a ServerGameEngine to do so.
     */
    public void killSheep(Player who, Shepherd which, SheepTile tile,
            Sheep sheep) throws RuleViolationException {
        checkBeforeMove(who, which, MoveType.KILL);

        RoadSquare currentPosition = which.getCurrentPosition();
        GameMap map = getGameBoard().getMap();

        if (!map.isSquareAdjacentToTile(tile, currentPosition)) {
            throw new RuleViolationException(
                    "The chosen tile is not adjacent to the sheperd");
        }
        if (sheep.getType() == SheepType.BLACK_SHEEP) {
            throw new RuleViolationException(
                    "Killing the black sheep is not allowed");
        }
        if (!tile.containsSheep(sheep)) {
            throw new RuleViolationException(
                    "The chosen sheep is not on this tile");
        }

        List<Shepherd> opponentShepherds = getAllAdjacentOpponentShepherds(who,
                which);
        int worstCaseRequiredPayment = 2 * opponentShepherds.size();
        if (who.getRemainingCoins() < worstCaseRequiredPayment) {
            throw new RuleViolationException(
                    "Not enough coins are available for killing here");
        }

        state.recordMove(which, MoveType.KILL);
        checkAndAdvanceFinalState();
    }

    private RoadSquare getAnyFreeRoadSquare() {
        for (RoadSquare rs : board.getMap().getAllRoadSquares()) {
            assert !rs.getHasFence();
            if (rs.getCurrentShepherd() == null) {
                return rs;
            }
        }

        throw new GameInvariantViolationException(
                "At least one road square must be free");
    }

    private void forceEndInitTurn(Player who) throws RuleViolationException {
        for (Shepherd shepherd : who.getAllShepherds()) {
            if (!shepherd.isPositioned()) {
                initiallyPlaceShepherd(who, shepherd, getAnyFreeRoadSquare());
                return;
            }
        }
    }

    private void forceEndPlayingTurn(Player who) throws RuleViolationException {
        if (!state.getCurrentPhase().isPlaying()
                && !state.getCurrentPhase().isMarket()) {
            throw new RuleViolationException(
                    "This move is not allowed at this point of the game");
        }
        checkTurn(who);

        state.forceEndTurn();
    }

    /**
     * Force the end of the turn, leaving the game to the next player.
     * 
     * Note that this does not start the next turn, it merely brings the engine
     * in a state in which no moves are allowed by the rules.
     * 
     * @param who
     *            the player ending the turn.
     * @return
     */
    public void forceEndTurn(Player who) throws RuleViolationException {
        if (state.getCurrentPhase() == GamePhase.INIT) {
            forceEndInitTurn(who);
        } else {
            forceEndPlayingTurn(who);
        }
    }

    /**
     * Mark a terrain card owned by the current player as available for sale at
     * a given cost.
     * 
     * @param who
     *            the player selling the card
     * @param card
     *            the card being sold
     * @param cost
     *            the requested price
     * @throws RuleViolationException
     */
    public void sellMarketTerrainCard(Player who, TerrainCard card, int cost)
            throws RuleViolationException {
        checkStateAndTurn(GamePhase.MARKET_SELL, who);

        if (card.getOwner() != who) {
            throw new RuleViolationException(
                    "Player is not the owner of this card");
        }
        if (card == who.getInitialCard()) {
            throw new RuleViolationException(
                    "Selling the initial card is not allowed");
        }
        if (cost > rules.getMaximumCardPrice() || cost < 1) {
            throw new RuleViolationException(
                    "The price of the card is outside the allowed limits");
        }
        if (card.isForSale()) {
            throw new RuleViolationException("Card is already for sale");
        }

        card.markForSale(cost);
        board.markCardForSale(card);

        state.recordMove(null, MoveType.MARKET_SELL);
        checkAndAdvanceFinalState();
    }

    /**
     * Buy a card that was previously made available for sale by another player,
     * if allowed by the rules of the game, and possibly transferring the coins.
     * 
     * @param who
     *            the player buying the card
     * @param card
     *            the card being bought.
     * @throws RuleViolationException
     */
    public void buyMarketTerrainCard(Player who, TerrainCard card)
            throws RuleViolationException {
        checkStateAndTurn(GamePhase.MARKET_BUY, who);

        if (!card.isForSale() || card.getOwner() == null) {
            throw new RuleViolationException("This card is not for sale");
        }
        if (who.getRemainingCoins() < card.getCost()) {
            throw new RuleViolationException(
                    "Player does not have enough coins to buy the card");
        }

        Player seller = card.getOwner();
        seller.releaseCard(card);
        seller.earnCoins(card.getCost());

        who.acquireCard(card);
        card.markAcquired(who);
        board.markCardAcquired(card);

        state.recordMove(null, MoveType.MARKET_BUY);
        checkAndAdvanceFinalState();
    }

    /**
     * Give terrain cards that were marked for sale and not bought back to the
     * owner player.
     * 
     * This method must be called prior to ending the MARKET_BUY phase.
     */
    protected void giveBackTerrainCardsForSale() {
        if (state.getCurrentPhase() != GamePhase.MARKET_BUY) {
            throw new GameInvariantViolationException(
                    "Cards are only given back to players after the market buy phase");
        }

        board.releaseAllTerrainCardsForSale();
    }
}
