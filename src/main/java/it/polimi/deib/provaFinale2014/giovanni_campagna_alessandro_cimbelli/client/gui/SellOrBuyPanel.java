package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * The overlay panel used for market operations. Contains buttons for ending the
 * market phase (with or without a transaction).
 * 
 * @author Alessandro Cimbelli
 */

public class SellOrBuyPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 527;
    private static final int Y_SIZE = 150;
    private final JButton backButton;
    private final JButton actionButton;

    /**
     * Construct a new sellorbuypanel, with the given title text, and with the
     * given label for the action button.
     * 
     * @param text
     *            the primary text
     * @param action
     *            the label of the OK button
     */
    public SellOrBuyPanel(String text, String action) {
        setLayout(null);
        setSize(X_SIZE, Y_SIZE);
        setBackground(new Color(0, 0, 0));

        JLabel label = new JLabel(text);
        label.setForeground(new Color(255, 255, 0));
        label.setLocation(0, 5);
        label.setSize(X_SIZE, 40);
        label.setFont(new Font("Arial", Font.BOLD, 14));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        add(label);

        backButton = new JButton();
        JComponent backIcon = new ImageComponent(25, 25, "undo.png");
        backIcon.setSize(25, 25);
        backIcon.setLocation(0, 0);
        backButton.setLayout(null);
        backButton.setBounds(369 - 25 - 12, 106 + (145 - 106 - 25) / 2, 25, 25);
        backButton.add(backIcon);
        backButton.setContentAreaFilled(false);
        add(backButton);
        backButton.addActionListener(new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                ((GameBoardPanel) getParent()).goBackMarket();
            }
        });

        JButton finishButton = new JButton("Finish");
        finishButton.setFont(new Font("Arial", Font.PLAIN, 9));
        finishButton.setBounds(445, 106, 513 - 445, 145 - 106);
        finishButton.addActionListener(new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                ((GameBoardPanel) getParent()).endMarket();
            }
        });
        finishButton.setBackground(new Color(255, 0, 0));
        add(finishButton);

        actionButton = new JButton(action);
        actionButton.setFont(new Font("Arial", Font.PLAIN, 9));
        actionButton.addActionListener(new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                ((GameBoardPanel) getParent()).performMarketAction();
            }
        });
        add(actionButton);
        actionButton.setBounds(369, 106, 423 - 369, 145 - 106);
        actionButton.setEnabled(false);
        actionButton.setBackground(new Color(0, 255, 0));
    }

    /**
     * Change the visibility of the back button
     * 
     * @param visible
     *            if the back button should be visible
     */
    public void setBackVisible(boolean visible) {
        backButton.setVisible(visible);
    }

    /**
     * Change the enabled status of the primary button
     * 
     * @param enabled
     *            if the primary button should be enabled
     */
    public void setActionEnabled(boolean enabled) {
        actionButton.setEnabled(enabled);
    }
}
