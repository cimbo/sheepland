package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;

/**
 * The "mate" command (called MateSheepCommand because it is always followed by
 * "sheep"), implementing the mating move.
 * 
 * @author Giovanni Campagna
 */
class MateSheepCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("mate sheep on <REGION>: mate a female sheep and a ram on this region, to produce a new lamb");
        baseHelpRegion(ui);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, InvalidIdentifierException,
            RuleViolationException {
        if (ui.getChosenShepherd() == null) {
            ui.showMessage("A businessman like you would never touch a sheep directly (let alone for such disgusting practices!)");
            if (GameOptions.get().getShowHints()) {
                ui.showMessage("[Use the be command to choose a shepherd before mating]");
            }
            return;
        }

        Game game = ui.getCurrentGame();
        tokenizer.expect("sheep");
        tokenizer.expect("on");
        SheepTile tile = game.getSheepTileFromId(tokenizer.readSheepTile());

        game.mateSheep(ui.getChosenShepherd(), tile);
    }
}