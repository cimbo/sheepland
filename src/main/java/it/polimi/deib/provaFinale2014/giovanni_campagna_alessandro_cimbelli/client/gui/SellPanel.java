package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * This class is an extension of the more general SellOrBuyPanel. The extension
 * consists in the addiction of the coin buttons to express the value of a card
 * that the player wants to sell.
 * 
 * @author Alessandro Cimbelli
 */

public class SellPanel extends SellOrBuyPanel {
    private static final long serialVersionUID = 1L;
    private static final int Y_STAR = 113;
    private static final int Y_STAR_SIZE = 145 - 113;
    private static final int[] X_STAR = { 181, 222, 262, 305 };
    private static final int X_STAR_SIZE = 211 - 181;
    private final JButton[] coins;

    /**
     * Construct a new sellpanel, with the given title text, and with the given
     * label for the action button.
     * 
     * @param text
     *            the primary text
     * @param action
     *            the label of the OK button
     */
    public SellPanel(String text, String action) {
        super(text, action);

        coins = new JButton[4];
        for (int i = 0; i < 4; i++) {
            JButton starButton = new CoinButton(Integer.toString(i + 1));
            starButton.setBounds(X_STAR[i], Y_STAR, X_STAR_SIZE, Y_STAR_SIZE);

            final int coinNumber = i + 1;
            starButton.addActionListener(new ActionListener() {
                /**
                 * {@inheritDoc}
                 */
                public void actionPerformed(ActionEvent e) {
                    ((GameBoardPanel) getParent())
                            .chooseMarketCoins(coinNumber);
                }
            });

            add(starButton);
            coins[i] = starButton;
        }
    }

    /**
     * Set the enabled status of the coin buttons.
     * 
     * @param b
     *            if the coin buttons should be enabled
     */
    public void setCoinsEnabled(boolean b) {
        for (JButton button : coins) {
            button.setEnabled(b);
        }
    }
}
