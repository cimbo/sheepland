package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.io.Serializable;

/**
 * This class encapsulates all the state related to the game state machine
 * (turns, moves, current shepherd, etc.), in other words all state which is not
 * strictly part of the game model.
 * 
 * Besides serialization, it should be considered an implementation detail of
 * {@link GameEngine}.
 * 
 * @author Giovanni Campagna
 */
public class GameState implements Serializable {
    private static final long serialVersionUID = 1L;

    private transient RuleSet rules;
    private GamePhase currentPhase;
    private int currentTurn;
    private int currentMoves;
    private ShepherdIdentifier lastShepherd;
    private MoveType lastMove;
    private boolean didMovePlayer;

    /**
     * Construct a new game state.
     */
    public GameState() {
        currentPhase = GamePhase.PREINIT;
        currentTurn = 0;
    }

    /**
     * @return the current phase of the game
     */
    public GamePhase getCurrentPhase() {
        return currentPhase;
    }

    /**
     * Jump to the given phase of the game.
     * 
     * This method will check that the transition is possible (ie, a transition
     * from init to market_sell is not possible and will fail), but will not
     * check that the transition is allowed or consistent with the rules of the
     * game.
     * 
     * Use this method on the client side when the phase is chosen by the
     * server.
     * 
     * @param phase
     *            the new game phase
     */
    public void setCurrentPhase(GamePhase phase) {
        if (phase != currentPhase) {
            switch (phase) {
            case PREINIT:
            case INIT:
                throw new GameInvariantViolationException(
                        "Cannot reenter PREINIT or INIT state");
            case PLAY:
                advancePlayState();
                break;
            case FINAL:
                advanceFinalState();
                break;
            case MARKET_SELL:
                advanceMarketSellState();
                break;
            case MARKET_BUY:
                advanceMarketBuyState();
                break;
            case END:
                throw new GameInvariantViolationException(
                        "Cannot jump to END state");
            default:
                // is it even possible in java to have enum values
                // we don't know about?
                // anyway, sonar complains without it...
                throw new AssertionError();
            }
        }
    }

    /**
     * @return the current turn (index of player expected to play next)
     */
    public int getCurrentTurn() {
        return currentTurn;
    }

    /**
     * Jump to a given turn
     * 
     * It is undefined behavior to pass a negative turn or a turn that exceeds
     * the number of players.
     */
    public void setCurrentTurn(int turn) {
        if (turn < 0) {
            throw new IllegalArgumentException();
        }
        currentTurn = turn;
    }

    /**
     * Set the rules governing the game.
     * 
     * Unlike GameEngine's version, this method does not check for overwriting,
     * and therefore must not be called directly.
     * 
     * @param rules
     *            the new rules of the game.
     */
    public void setRules(RuleSet rules) {
        this.rules = rules;
    }

    /**
     * Check that a move of the given type would be allowed at this point of the
     * game.
     * 
     * This method does not check the game phase, only the remaining moves.
     * Don't use it directly.
     * 
     * @param move
     *            the type of move
     * @throws RuleViolationException
     */
    public void checkBeforeMove(MoveType move) throws RuleViolationException {
        // Can only happen on the client side, because the server immediately
        // advances to the next turn (and therefore only with a client that has
        // been tampered with)
        if (currentMoves == 0) {
            throw new RuleViolationException(
                    "All moves for this turn have been completed");
        }

        if (move != MoveType.MOVE_SHEPHERD) {
            if (lastMove == move) {
                throw new RuleViolationException(
                        "Cannot do the same move twice in succession, must move shepherd first");
            }
            if (currentMoves == 1 && !didMovePlayer) {
                throw new RuleViolationException(
                        "At least one move in the turn must be to move the shepherd");
            }
        }
    }

    /**
     * Check that a move of the given type would be allowed at this point of the
     * game.
     * 
     * This method does not check the game phase, only the remaining moves.
     * Don't use it directly.
     * 
     * @param move
     *            the type of move
     * @throws RuleViolationException
     */
    public void checkBeforeMove(Shepherd which, MoveType move)
            throws RuleViolationException {
        checkBeforeMove(move);

        if (lastShepherd != null && !lastShepherd.equals(which.getId())) {
            throw new RuleViolationException(
                    "Cannot use two different shepherds in the same turn");
        }
    }

    /**
     * Advance to the init phase.
     * 
     * This method checks that the current phase is valid for the advancement,
     * but does not check or reset other state. Do not use directly.
     */
    public void advanceInitState() {
        if (currentPhase != GamePhase.PREINIT) {
            throw new GameInvariantViolationException(
                    "Game was already started");
        }

        currentPhase = GamePhase.INIT;
    }

    /**
     * Advance to the play phase.
     * 
     * This method checks that the current phase is valid for the advancement,
     * but does not check or reset other state. Do not use directly.
     */
    public void advancePlayState() {
        if (currentPhase != GamePhase.INIT
                && currentPhase != GamePhase.MARKET_BUY) {
            throw new GameInvariantViolationException("Game is already playing");
        }

        currentPhase = GamePhase.PLAY;
    }

    /**
     * Advance to the market sell phase.
     * 
     * This method checks that the current phase is valid for the advancement,
     * but does not check or reset other state. Do not use directly.
     */
    public void advanceMarketSellState() {
        if (currentPhase != GamePhase.PLAY && currentPhase != GamePhase.FINAL) {
            throw new GameInvariantViolationException(
                    "Market not while playing");
        }

        currentPhase = GamePhase.MARKET_SELL;
    }

    /**
     * Advance to the market buy phase.
     * 
     * This method checks that the current phase is valid for the advancement,
     * but does not check or reset other state. Do not use directly.
     */
    public void advanceMarketBuyState() {
        if (currentPhase != GamePhase.MARKET_SELL) {
            throw new GameInvariantViolationException(
                    "Market buy not after selling");
        }

        currentPhase = GamePhase.MARKET_BUY;
    }

    /**
     * Advance to the final phase.
     * 
     * This method checks that the current phase is valid for the advancement,
     * but does not check or reset other state. Do not use directly.
     */
    public void advanceFinalState() {
        if (currentPhase != GamePhase.PLAY
                && currentPhase != GamePhase.MARKET_BUY) {
            throw new GameInvariantViolationException(
                    "Game is not yet playing, or is already finishing");
        }

        currentPhase = GamePhase.FINAL;
    }

    /**
     * Advance to the end phase (end of the game).
     * 
     * This method checks that the current phase is valid for the advancement,
     * but does not check or reset other state. Do not use directly.
     */
    public void advanceEndState() {
        if (currentPhase != GamePhase.MARKET_BUY) {
            throw new GameInvariantViolationException(
                    "Game finished without going through the market");
        }

        currentPhase = GamePhase.END;
    }

    /**
     * Check if the current state is ready for advancing to the next turn (that
     * is, the current player completed his allowed moves, or forced the end of
     * his turn).
     * 
     * @return true, if the game should advance to a different player, false
     *         otherwise
     */
    public boolean checkShouldAdvanceNextTurn() {
        return currentMoves == 0;
    }

    /**
     * Reset the game state (remaining moves, shepherd, etc.) at the beginning
     * of the new turn.
     */
    public void prepareNextTurn() {
        if (currentPhase.isPlaying()) {
            currentMoves = rules.getMovesPerTurn();
        } else if (currentPhase == GamePhase.INIT) {
            // XXX: in what order are shepherds initially placed?
            currentMoves = 1;
        } else if (currentPhase.isMarket()) {
            currentMoves = -1;
        }

        lastShepherd = null;
        lastMove = MoveType.INVALID;
        didMovePlayer = false;
    }

    /**
     * Record that a move was executed successfully by a given shepherd.
     * 
     * @param which
     *            the shepherd that executed the move
     * @param move
     *            the type of move executed
     */
    public void recordMove(Shepherd which, MoveType move) {
        lastMove = move;
        lastShepherd = which != null ? which.getId() : null;
        if (move == MoveType.MOVE_SHEPHERD) {
            didMovePlayer = true;
        }

        if (currentMoves >= 0) {
            assert currentMoves > 0;
            currentMoves--;
        }
    }

    /**
     * Force the end of the turn.
     * 
     * After calling this method, checkShouldAdvanceNextTurn() will always
     * return true.
     */
    public void forceEndTurn() {
        currentMoves = 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + currentMoves;
        result = prime * result
                + (currentPhase == null ? 0 : currentPhase.hashCode());
        result = prime * result + currentTurn;
        result = prime * result + (didMovePlayer ? 1231 : 1237);
        result = prime * result + (lastMove == null ? 0 : lastMove.hashCode());
        result = prime * result
                + (lastShepherd == null ? 0 : lastShepherd.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof GameState)) {
            return false;
        }
        GameState other = (GameState) obj;
        if (currentMoves != other.currentMoves) {
            return false;
        }
        if (currentPhase != other.currentPhase) {
            return false;
        }
        if (currentTurn != other.currentTurn) {
            return false;
        }
        if (didMovePlayer != other.didMovePlayer) {
            return false;
        }
        if (lastMove != other.lastMove) {
            return false;
        }
        if (lastShepherd == null) {
            if (other.lastShepherd != null) {
                return false;
            }
        } else if (!lastShepherd.equals(other.lastShepherd)) {
            return false;
        }
        return true;
    }
}