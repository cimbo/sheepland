package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import java.io.Serializable;

/**
 * Small helper class representing an Edge in the graph (which is stored as a
 * neighbor list). Includes both the destination vertex and the actual, mutable
 * and shared, edge object, the RoadSquare.
 * 
 * @author Giovanni Campagna
 * 
 */
class Edge implements Serializable {
    private static final long serialVersionUID = 1L;
    private final RoadSquare roadSquare;
    private final SheepTile destination;

    /**
     * Construct the edge connecting to the given tile, through the given road
     * 
     * @param to
     *            the destination
     * @param via
     *            the crossed road
     */
    public Edge(SheepTile to, RoadSquare via) {
        roadSquare = via;
        destination = to;
    }

    /**
     * Retrieve the destination of this edge
     * 
     * @return the destination tile
     */
    public SheepTile getDestination() {
        return destination;
    }

    /**
     * Retrieve the square crossed by this edge
     * 
     * @return the crossed road squarez
     */
    public RoadSquare getRoadSquare() {
        return roadSquare;
    }
}
