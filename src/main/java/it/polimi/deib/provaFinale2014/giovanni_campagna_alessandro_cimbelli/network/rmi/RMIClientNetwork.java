package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * The RMI implementation of ClientNetwork.
 * 
 * @author Alessandro Cimbelli
 */
public class RMIClientNetwork implements ClientNetwork {

    private final InetSocketAddress registryAddress;

    /**
     * Construct a RMI client network for the default registry on the local
     * host.
     */
    public RMIClientNetwork() {
        this(new InetSocketAddress("127.0.0.1", Registry.REGISTRY_PORT));
    }

    /**
     * Construct a RMI client network for the registry at the given address.
     * 
     * @param address
     *            the address (hostname and port) of the remote registry
     */
    public RMIClientNetwork(InetSocketAddress address) {
        registryAddress = address;
    }

    /**
     * {@inheritDoc}
     */
    public ClientConnection connectToServer() throws IOException {
        Registry registry = LocateRegistry.getRegistry(
                registryAddress.getHostName(), registryAddress.getPort());
        Server remoteServer;

        try {
            remoteServer = (Server) registry.lookup(Server.SERVER_NAME);
        } catch (NotBoundException e) {
            throw new IOException("Could not connect to server", e);
        }

        return new RMIClientConnection(remoteServer);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "RMIClientNetwork";
    }
}
