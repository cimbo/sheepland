package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;

/**
 * An exception indicating a identifier that has no corresponding game object in
 * the board.
 * 
 * @author Giovanni Campagna
 * 
 */
public class InvalidIdentifierException extends InvalidMoveException {
    private static final long serialVersionUID = 1L;
    private final GameObjectIdentifier invalidId;

    /**
     * Construct a new InvalidIdentifierException for the given invalid
     * GameObjectIdentifier.
     * 
     * @param id
     *            the invalid id
     */
    public InvalidIdentifierException(GameObjectIdentifier id) {
        super("Invalid identifier: " + id);
        invalidId = id;
    }

    /**
     * Retrieve the invalid identifier that triggered this exception.
     * 
     * @return the identifier
     */
    public GameObjectIdentifier getInvalidId() {
        return invalidId;
    }
}
