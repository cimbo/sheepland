package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A player in the game.
 * 
 * This contains the state of one playing character during a specific match.
 * 
 * @author Giovanni Campagna
 * 
 */
public class Player extends GameObject implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String name;
    private final Set<TerrainCard> cardsInHand;
    private int remainingCoins;
    private Shepherd[] shepherds;
    private TerrainCard initialCard;

    /**
     * Construct a new player. The player initially holds no cards and owns no
     * shepherds.
     */
    public Player(String name) {
        cardsInHand = new HashSet<TerrainCard>();
        initialCard = null;
        this.name = name;
    }

    /**
     * @return the name of the player
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PlayerIdentifier getId() {
        return (PlayerIdentifier) super.getId();
    }

    /**
     * Initialize ruleset dependent fields of Player. This will called by the
     * GameBoard when you initialize it.
     * 
     * Among other things, this method will create all the shepherds owned by
     * this player.
     * 
     * @param rules
     *            the set of game rules
     */
    public void initializeForRules(RuleSet rules) {
        remainingCoins = rules.getInitialCoinsPerPlayer();

        int nShepherds = rules.getNumberOfShepherdsPerPlayer();
        shepherds = new Shepherd[nShepherds];
        for (int i = 0; i < nShepherds; i++) {
            shepherds[i] = new Shepherd(this);
            shepherds[i].setId(new ShepherdIdentifier(i));
        }
    }

    /**
     * Retrieves the shepherd matching the passed id (which needs to be equal to
     * the id of shepherd, but need to be a ShepherdIdentifier)
     * 
     * Note that shepherd identifiers are only meaningful within one player, so
     * calling this method on two different players might succeed both times
     * (but will give two different results)
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             no such object could be retrieved
     */
    public Shepherd getShepherdFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        if (id == null) {
            throw new InvalidIdentifierException(id);
        }

        for (Shepherd s : shepherds) {
            if (s.getId().equals(id)) {
                return s;
            }
        }

        throw new InvalidIdentifierException(id);
    }

    /**
     * Retrieves the list of all shepherds owned by this player.
     * 
     * This method returns the internal array directly. It must not be modified
     * in any way.
     * 
     * @return the shepherd owned by this player
     */
    public Shepherd[] getAllShepherds() {
        return shepherds;
    }

    /**
     * Checks if the player is already positioned on the board.
     * 
     * A player is positioned iff all his shepherds are positioned.
     * 
     * It is invalid and undefined behavior to call this method before the
     * player is initialized for the rule set.
     * 
     * @return true if the player is positioned, false otherwise
     */
    public boolean isPositioned() {
        if (shepherds == null) {
            throw new GameInvariantViolationException(
                    "Player is not initialized yet");
        }

        for (int i = 0; i < shepherds.length; i++) {
            if (!shepherds[i].isPositioned()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Retrieve the initial card of this player.
     * 
     * Because the initial card is a secret, on the client side this method will
     * return null on all players that are not the human player.
     * 
     * On the server side, this method will also return null if the initial card
     * was not allocated to the player by the game engine yet (ie, before the
     * engine is started).
     * 
     * @return the initial card, or null if not known.
     */
    public TerrainCard getInitialCard() {
        return initialCard;
    }

    /**
     * Allocate the initial card to the player.
     * 
     * This marks that the player owns the given card as the initial card, and
     * modifies the state of the card accordingly.
     * 
     * @param card
     *            the initial card
     */
    public void setInitialCard(TerrainCard card) {
        initialCard = card;
        card.setOwner(this);
    }

    /**
     * Clear the initial card of this player.
     * 
     * This marks that the player does not have an initial card.
     * 
     * It can be used to hide the initial card from the board that is sent to
     * other player clients.
     */
    public void clearInitialCard() {
        if (initialCard == null) {
            return;
        }

        initialCard.setOwner(null);
        initialCard = null;
    }

    /**
     * Acquire a card. The cost of the card will be automatically scaled from
     * the remaining coins. The card must be for sale.
     * 
     * @param card
     *            the card to acquire
     * @param cost
     *            the cost of the card
     */
    public void acquireCard(TerrainCard card) {
        payCoins(card.getCost());
        cardsInHand.add(card);
    }

    /**
     * Release a card.
     * 
     * This simply removes the card from the set of cards held by the player, it
     * does not change the owner of the card or mark it for sale.
     * 
     * Attempting to release the initial card will throw, even when the initial
     * card is known.
     * 
     * @param card
     *            the card to release
     */
    public void releaseCard(TerrainCard card) {
        if (!cardsInHand.contains(card)) {
            throw new GameInvariantViolationException(
                    "Player does not currently hold card");
        }

        cardsInHand.remove(card);
    }

    /**
     * Retrieves any terrain card of the given type from the deck.
     * 
     * This method will return null if no card of the given type is present, and
     * will throw if type is sheepsburg.
     * 
     * Unlike getNumberOfRemainingCards(), this method does not consider the
     * initial card.
     * 
     * This method is linear in the number of cards.
     * 
     * @param type
     *            a terrain type
     * @return a card of the given type, owned by this player
     */
    public TerrainCard getAnyCardOfType(TerrainType type) {
        if (type == TerrainType.SHEEPSBURG) {
            throw new IllegalArgumentException(
                    "Sheepsburg is not a valid terrain card");
        }

        for (TerrainCard card : cardsInHand) {
            if (card.getType() == type) {
                return card;
            }
        }

        return null;
    }

    /**
     * Computes and retrieves the number of cards of a given type.
     * 
     * This method will consider all cards owned by the player and return the
     * number of those that are of the given type, including the initial card,
     * if that is known.
     * 
     * This method is linear in the number of all cards owned by the player.
     * 
     * Calling this method with a terrain type of sheepsburg will throw an
     * exception.
     * 
     * @param type
     *            the type of cards to filter on
     * @return the number of cards of the given type
     */
    public int getNumberOfCards(TerrainType type) {
        if (type == TerrainType.SHEEPSBURG) {
            throw new IllegalArgumentException(
                    "Sheepsburg is not a valid terrain card");
        }

        int counter = 0;
        for (TerrainCard card : cardsInHand) {
            if (card.getType() == type) {
                counter++;
            }
        }

        if (initialCard != null && initialCard.getType() == type) {
            counter++;
        }

        return counter;
    }

    /**
     * Pay (subtract) a given amount of money from the remaining coins of the
     * player.
     * 
     * This method should only be called when there is no other specific methods
     * (eg. acquireCard()).
     * 
     * The cost must not be negative (an exception is thrown in that case), but
     * can be 0. An exception is also thrown if the player does not have enough
     * coins left.
     * 
     * @param cost
     *            the cost to pay.
     */
    public void payCoins(int cost) {
        if (cost < 0) {
            throw new IllegalArgumentException(
                    "Cannot pay a negative amount of money");
        }
        if (remainingCoins < cost) {
            throw new GameInvariantViolationException(
                    "Player does not have enough coins to pay");
        }

        remainingCoins -= cost;
    }

    /**
     * Earn (add) a given amount of money to the remaining coins of the player.
     * 
     * The earning must not be negative (an exception is thrown in that case),
     * but can be 0.
     * 
     * @param earning
     *            the earning to get
     */
    public void earnCoins(int earning) {
        if (earning < 0) {
            throw new IllegalArgumentException(
                    "Cannot earn a negative amount of money");
        }

        remainingCoins += earning;
    }

    /**
     * @return the number of remaining coins owned by this player
     */
    public int getRemainingCoins() {
        return remainingCoins;
    }

    /**
     * Compute the score of this player.
     * 
     * The score is computed considering the map score values as an array
     * indexed by terrain type, and multiplying each value by the number of
     * terrain cards of that type. The sum of those results and the remaining
     * coins gives the final score.
     * 
     * This method is only meaningful after the end of the game, and only on the
     * server (as the client does not know the right number of cards for all
     * players).
     * 
     * @param mapScoreValues
     *            the map score values
     * @return the final score of the player.
     */
    public int getScore(int[] mapScoreValues) {
        int score = remainingCoins;
        TerrainType[] allTypes = TerrainType.values();

        for (int i = 0; i < mapScoreValues.length; i++) {
            score += getNumberOfCards(allTypes[i]) * mapScoreValues[i];
        }

        return score;
    }

    /**
     * Checks that this player is the owner (controller) of Shepherd which.
     * 
     * @param which
     * @return true, if which belongs to this player, and false otherwise.
     */
    public boolean ownsShepherd(Shepherd which) {
        for (Shepherd s : shepherds) {
            if (s == which) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve an iterator for all cards currently in this player's hand.
     * 
     * The iteration order is unspecified.
     * 
     * @return an iterator for cards in hand
     */
    public Iterator<TerrainCard> iterateCards() {
        return cardsInHand.iterator();
    }
}
