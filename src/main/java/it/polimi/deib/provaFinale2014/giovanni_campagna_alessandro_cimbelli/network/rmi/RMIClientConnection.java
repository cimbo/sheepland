package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientSender;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The RMI implementation of ClientConnection
 * 
 * @author Alessandro Cimbelli
 */
public class RMIClientConnection extends UnicastRemoteObject implements Client,
        ClientConnection, ClientSender {
    private static final long serialVersionUID = 1L;

    private ClientReceiver receiver;
    private final Game remoteGame;

    /**
     * Construct a new RMIClientConnection for the given server.
     * 
     * @param remoteServer
     *            the server to connect to
     * @throws RemoteException
     */
    public RMIClientConnection(Server remoteServer) throws RemoteException {
        super();

        remoteGame = remoteServer.connect(this);
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
        try {
            unexportObject(this, true);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.FINE,
                    "Exception unexporting RMI object", e);
        }

        receiver.connectionClosed();
    }

    /**
     * {@inheritDoc}
     */
    public ClientSender getClientSender() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void setClientReceiver(ClientReceiver receiver) {
        this.receiver = receiver;
    }

    /**
     * {@inheritDoc}
     */
    public void sendAbandonGame() throws IOException {
        remoteGame.abandonGame();
    }

    /**
     * {@inheritDoc}
     */
    public void sendForceSynchronizeMe() throws IOException {
        remoteGame.forceSynchronizeMe();
    }

    /**
     * {@inheritDoc}
     */
    public void sendExecuteMove(Move move) throws IOException {
        remoteGame.executeMove(move);
    }

    /**
     * {@inheritDoc}
     */
    public void sendLoginToServer(String username, String password)
            throws IOException {
        remoteGame.loginToServer(username, password);
    }

    /**
     * {@inheritDoc}
     */
    public void sendPong() throws IOException {
        remoteGame.pong();
    }

    /**
     * {@inheritDoc}
     */
    public void setup(RuleSet rules, GameBoard board, PlayerIdentifier self) {
        receiver.setup(rules, board, self);
    }

    /**
     * {@inheritDoc}
     */
    public void moveAcknowledged(Exception result) {
        receiver.moveAcknowledged(result);
    }

    /**
     * {@inheritDoc}
     */
    public void forceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) {
        receiver.forceSynchronize(rules, newBoard, self, newState);
    }

    /**
     * {@inheritDoc}
     */
    public void advanceNextTurn(int turn, GamePhase phase) {
        receiver.advanceNextTurn(turn, phase);
    }

    /**
     * {@inheritDoc}
     */
    public void blackSheepMoved(SheepTileIdentifier destination) {
        receiver.blackSheepMoved(destination);
    }

    /**
     * {@inheritDoc}
     */
    public void wolfMoved(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) {
        receiver.wolfMoved(destination, eatenSheep);
    }

    /**
     * {@inheritDoc}
     */
    public void lambsGrew(SheepTileIdentifier where, Sheep[] newAdultSheep) {
        receiver.lambsGrew(where, newAdultSheep);
    }

    /**
     * {@inheritDoc}
     */
    public void didExecuteMove(Move move) {
        receiver.didExecuteMove(move);
    }

    /**
     * {@inheritDoc}
     */
    public void loginAnswer(boolean answer) {
        receiver.loginAnswer(answer);
    }

    /**
     * {@inheritDoc}
     */
    public void ping() {
        receiver.ping();
    }

    /**
     * {@inheritDoc}
     */
    public void endGame(Map<PlayerIdentifier, Integer> scores) {
        receiver.endGame(scores);
    }

    /**
     * {@inheritDoc}
     */
    public void playerLeft(PlayerIdentifier who, boolean clean) {
        receiver.playerLeft(who, clean);
    }

    /**
     * {@inheritDoc}
     */
    public void playerJoined(PlayerIdentifier who) {
        receiver.playerJoined(who);
    }

    /**
     * {@inheritDoc}
     */
    public void abandonComplete() {
        receiver.abandonComplete();
    }
}
