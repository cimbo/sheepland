package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.CompletableMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ForceEndTurnMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ServerGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class wraps and represents a single running game on the server.
 * 
 * It handles locking around moves and operations that affect all clients at the
 * same time.
 * 
 * @author Alessandro Cimbelli
 */
public class Game {
    private static AtomicInteger counter = new AtomicInteger();

    private final int id;
    private final ServerGameEngine engine;
    private final Collection<ServerPlayer> serverPlayers;

    /**
     * Construct a new game, given a ServerGameEngine in PREINIT phase and a
     * list of players that are playing in this game.
     * 
     * @param engine
     *            the game engine
     * @param players
     *            the list of players
     */
    public Game(ServerGameEngine engine, Collection<ServerPlayer> players) {
        id = counter.getAndIncrement();
        this.engine = engine;
        serverPlayers = players;
    }

    /**
     * Retrieve the ID of this Game.
     * 
     * The ID of a game is an increasing sequence number that uniquely
     * identifies a Game on the server. It is useful for debug and logging
     * purposes only.
     * 
     * This method is threadsafe.
     * 
     * @return the game ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Check if the game is currently abandoned.
     * 
     * A game is abandoned iff all players have abandoned.
     * 
     * This method must be called under Game's lock.
     * 
     * @return true, if all players are abandoned, false otherwise
     */
    public boolean isAbandoned() {
        assert Thread.holdsLock(this);

        for (ServerPlayer player : serverPlayers) {
            if (!player.isAbandoned()) {
                return false;
            }
        }

        return true;
    }

    private void setPlayerOnGameBoard() {
        for (ServerPlayer sPlayer : serverPlayers) {
            engine.addPlayer(sPlayer.getPlayer());
        }
    }

    private void giveGameToPlayers() {
        for (ServerPlayer player : serverPlayers) {
            player.setServerGame(this);
        }
    }

    private void setupAllPlayers() {
        for (ServerPlayer player : serverPlayers) {
            player.setYourTurn(player.getPlayer() == engine.getCurrentPlayer());
            player.sendSetup();
        }
    }

    private void checkDisconnectedAtSetup() {
        for (ServerPlayer player : serverPlayers) {
            if (player.hasLeft()) {
                playerLeft(player, player.isAbandoned());
            }
        }
    }

    /**
     * Start the game.
     * 
     * Starting a game will associate the players with the new game, start the
     * engine and send the "setup" command to all of them.
     * 
     * This is a method to ensure proper synchronization, and to avoid
     * reentrancy from the constructor, which is unsafe.
     */
    public synchronized void start() {
        setPlayerOnGameBoard();
        giveGameToPlayers();
        engine.start();
        setupAllPlayers();
        checkDisconnectedAtSetup();

        Logger.getGlobal().info(
                "New game with " + serverPlayers.size()
                        + " players started, id " + id);
    }

    /**
     * Retrieve the game engine associated with this game.
     * 
     * This method must be called under Game's lock, and usage of the engine is
     * only valid under the Game's lock.
     * 
     * @return the game engine
     */
    public ServerGameEngine getEngine() {
        return engine;
    }

    private void advanceNextTurn(int nextTurn) {
        for (ServerPlayer player : serverPlayers) {
            player.setYourTurn(player.getPlayer() == engine.getCurrentPlayer());
            player.sendAdvanceNextTurn(nextTurn, engine.getGamePhase());
        }
    }

    private void endGame() {
        Map<PlayerIdentifier, Integer> scores = engine.getScores();

        for (ServerPlayer player : serverPlayers) {
            player.setYourTurn(false);
            player.endGame(scores);
        }

        Logger.getGlobal().info("Game " + id + " is over");
    }

    private void checkAfterMove() {
        int nextTurn = engine.checkAndAdvanceNextTurn();
        if (nextTurn < 0) {
            return;
        }

        if (engine.getGamePhase() == GamePhase.END) {
            endGame();
        } else {
            advanceNextTurn(nextTurn);
        }
    }

    /**
     * Report that the turn timeout for the given player fired.
     * 
     * This method should be fired at the end of the turn timeout for a player.
     * It will force the advancement to the next turn for the player.
     * 
     * @param from
     *            the player whose turn was current
     */
    public synchronized void turnTimeout(ServerPlayer from) {
        if (from.getPlayer() != engine.getCurrentPlayer()) {
            // race condition, the player completed his turn just before
            // the timeout fired
            return;
        }

        Move move = new ForceEndTurnMove(from.getPlayer().getId(), true);
        executeMove(from, move, false);
    }

    /**
     * Inform all players that the given player left the game.
     * 
     * This method should be called when the player leaves (abandons) the game,
     * or when the connection for the player is closed because of network
     * errors.
     * 
     * This method must be called under Game's lock.
     * 
     * @param who
     *            the player that left
     * @param clean
     *            true, if the player left the game on purpose, false for
     *            network issues
     */
    public void playerLeft(ServerPlayer who, boolean clean) {
        assert Thread.holdsLock(this);

        for (ServerPlayer player : serverPlayers) {
            if (player != who) {
                player.sendPlayerLeft(who.getPlayer().getId(), clean);
            }
        }
    }

    /**
     * Inform all players that the given player rejoined the game after
     * temporary disconnection.
     * 
     * This method must be called after a successful login, if the user is
     * associated with a not abandoned player, just before resynchronizing.
     * 
     * This method must be called under Game's lock.
     * 
     * @param who
     *            the player that reconnected
     */
    public void playerJoined(ServerPlayer who) {
        assert Thread.holdsLock(this);

        for (ServerPlayer player : serverPlayers) {
            if (player != who) {
                player.sendPlayerJoined(who.getPlayer().getId());
            }
        }
    }

    /**
     * Execute the given move.
     * 
     * The move is executed on the engine, and if successful all players are
     * informed of the outcome, the engine state machine is advanced and the
     * player is sent the proper acknowledgment. If the move fails the
     * acknowledgment includes the failure.
     * 
     * @param from
     *            the player executing the move
     * @param move
     *            the move
     */
    public synchronized void executeMove(ServerPlayer from, Move move) {
        executeMove(from, move, true);
    }

    private synchronized void executeMove(ServerPlayer from, Move move,
            boolean sendAck) {
        try {
            move.execute(engine);

            if (move instanceof CompletableMove) {
                ((CompletableMove) move).computeResult(engine);
            }
        } catch (InvalidMoveException e) {
            Logger.getGlobal().log(Level.INFO,
                    "Move invalid exception from client move", e);

            if (sendAck) {
                from.sendAck(e);
            }
            return;
        }

        for (ServerPlayer player : serverPlayers) {
            if (sendAck && player == from && !(move instanceof CompletableMove)) {
                continue;
            }

            player.sendMove(move);
        }

        checkAfterMove();
        if (sendAck) {
            from.sendAck(null);
        }
    }
}
