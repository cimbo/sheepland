package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.AbstractUI;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The CLI implementation of the Sheepland user interface.
 */
public class CLIUIManager extends AbstractUI {
    private final ClientNetwork network;
    private final CommandParser parser;
    private Game currentGame;
    private boolean waiting;
    private boolean myTurn;
    private ShepherdIdentifier nextShepherd;

    /**
     * Construct a new CLIUIManager with the given network
     * 
     * @param net
     *            the network implementation
     */
    public CLIUIManager(ClientNetwork net) {
        this(net, new CommandParser());
    }

    // For testing
    CLIUIManager(ClientNetwork net, CommandParser input) {
        network = net;
        waiting = true;
        parser = input;
        parser.setUI(this);

        // disable most logging, we don't want spam on the console
        Logger.getGlobal().setLevel(Level.SEVERE);
    }

    /**
     * Retrieve the current game.
     * 
     * @return the current game
     */
    public Game getCurrentGame() {
        return currentGame;
    }

    /**
     * Show a line of output.
     * 
     * @param message
     *            the line to write
     */
    public void showMessage(String message) {
        System.out.println(message);
    }

    /**
     * Show a line of output.
     * 
     * @param builder
     *            the builder constructing the output line.
     */
    public void showMessage(StringBuilder builder) {
        showMessage(builder.toString());
    }

    /**
     * Show a line of output, constructed with the given format and arguments.
     * 
     * @param format
     *            the printf-style format
     * @param arguments
     *            the arguments
     */
    public void showMessage(String format, Object... arguments) {
        showMessage(String.format(format, arguments));
    }

    /**
     * Show an empty line.
     */
    public void skipLine() {
        showMessage("");
    }

    /**
     * Retrieve the shepherd for this turn, or null if none was chosen.
     * 
     * @return the current turn shepherd
     */
    public Shepherd getChosenShepherd() {
        try {
            if (nextShepherd != null) {
                return currentGame.getShepherdFromId(
                        currentGame.getSelfPlayer(), nextShepherd);
            } else {
                return null;
            }
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.WARNING,
                    "Invalid identifier retrieving the current shepherd", e);

            return null;
        }
    }

    /**
     * Choose the given shepherd for this turn
     * 
     * @param shepherd
     *            the shepherd for the turn
     */
    public void chooseShepherd(Shepherd shepherd) {
        if (shepherd != null) {
            nextShepherd = shepherd.getId();
        } else {
            nextShepherd = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void show() {
        showMessage("Welcome to Sheepland!");
        showMessage("You are a proud farm owner in Sheepland, a joyful land"
                + " of promiscuous shepherds and silent sheep.");
        showMessage("But even though you're rich, powerful and respected "
                + "it is only today that you will be given a name. What "
                + "will it be?");

        currentGame = new Game(network, this);
        login();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void quit() {
        showMessage("You find yourself in a sudden rush to be somewhere else, and you just be.");
        showMessage("See you some other day in the land of Sheepland!");

        currentGame.abandon(true);
        currentGame = null;
    }

    private void login() {
        String username = parser.readCustom("Enter username: ");
        String password = parser.readCustom("Enter password: ");
        showMessage("Please wait until I establish a connection...");

        currentGame.joinGame(username, password);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loginSuccess() {
        showMessage("And now please wait until the game starts...");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loginFailed() {
        showMessage("Unfortunately, the gods did not recognize your name. You decide to try again.");
        login();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playerLeft(Player player, boolean clean) {
        if (clean) {
            showMessage("Enemy %s decided to leave Sheepland",
                    Representation.playerToString(player));
            showMessage("You and other survivors decide to continue without him.");
        } else {
            showMessage("Enemy %s disappeared from the land",
                    Representation.playerToString(player));
            showMessage("You are confident that he will come back soon.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playerJoined(Player player) {
        showMessage("Enemy %s is back on sight!",
                Representation.playerToString(player));
        showMessage("It seems getting rid of him is not as easy as you thought.");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setWaiting(boolean wait) {
        waiting = wait;
        inputLoop();
    }

    // For testing
    void beginTurn() {
        nextShepherd = null;

        GamePhase currentPhase = currentGame.getGamePhase();
        if (currentPhase.isPlaying() || currentPhase == GamePhase.INIT) {
            showMessage("It is now your turn. What will you do?");
        } else if (currentPhase == GamePhase.MARKET_SELL) {
            showMessage("It is now your turn, and you're at the market. What do you want to sell?");
        } else if (currentPhase == GamePhase.MARKET_BUY) {
            showMessage("It is now your turn, and all your enemies exposed their goods for sale. What will you do?");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setYourTurn(boolean myTurn) {
        this.myTurn = myTurn;

        if (myTurn) {
            beginTurn();
        }

        inputLoop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reportError(String error) {
        showMessage(error);
    }

    private void handleOneCommand() {
        try {
            parser.parseCommand();
        } catch (InvalidInputException e) {
            Logger.getGlobal().log(Level.FINE, "Invalid input command", e);

            showMessage(e.getMessage());
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);

            showMessage("I'm afraid I can't allow that: %s", e.getMessage());
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.FINE, "Invalid identifier in command",
                    e);

            GameObjectIdentifier id = e.getInvalidId();

            String what;
            if (id instanceof PlayerIdentifier) {
                what = " player";
            } else if (id instanceof ShepherdIdentifier) {
                what = " shepherd";
            } else if (id instanceof SheepTileIdentifier
                    || id instanceof RoadSquareIdentifier) {
                what = " place";
            } else {
                what = "";
            }

            showMessage("You don't know that%s. You look around confused.",
                    what);
        }
    }

    private void inputLoop() {
        while (myTurn && !waiting) {
            handleOneCommand();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        showMessage("And you're ready to start!");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void forceSynchronize() {
        showMessage("And you're ready to start again, just from where you left!");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameOver(Map<PlayerIdentifier, Integer> scores) {
        showMessage("The game is over!");
        showRanking(scores);

        currentGame = null;
        maybePlayAgain();
    }

    private void showRanking(final Map<PlayerIdentifier, Integer> scores) {
        Player[] ranking = Arrays.copyOf(currentGame.getAllPlayers(),
                currentGame.getAllPlayers().length);
        Arrays.sort(ranking, new Comparator<Player>() {
            /**
             * {@inheritDoc}
             */
            public int compare(Player one, Player two) {
                return scores.get(two.getId()) - scores.get(one.getId());
            }
        });

        showMessage("Final ranking:");
        for (int i = 0; i < ranking.length; i++) {
            String ordinal;
            switch (i + 1) {
            case 1:
                ordinal = "st";
                break;
            case 2:
                ordinal = "nd";
                break;
            case 3:
                ordinal = "rd";
                break;
            default:
                ordinal = "th";
                break;
            }

            showMessage("%d%s - %s with %d points", i + 1, ordinal,
                    Representation.playerToString(ranking[i]),
                    scores.get(ranking[i].getId()));
        }
    }

    private void maybePlayAgain() {
        String answer = parser.readCustom("Do you want to play again [y/N]? ");
        while (true) {
            if (answer.isEmpty() || "n".equalsIgnoreCase(answer)) {
                return;
            } else if ("y".equalsIgnoreCase(answer)) {
                show();
                return;
            } else {
                showMessage("Please answer y or n.");
                answer = parser.readCustom("Do you want to play again [y/N]? ");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBlackSheepMoved(SheepTile from, SheepTile to) {
        showMessage("Tonf! And the black sheep jumped to %s.",
                Representation.sheepTileToString(to));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateWolfMoved(SheepTile from, SheepTile to, Sheep eaten) {
        String eating;
        if (eaten != null) {
            eating = "eating a " + Representation.sheepToString(eaten);
        } else {
            eating = "but no harm was done";
        }

        showMessage("A wild animal was seen in %s, %s.",
                Representation.sheepTileToString(to), eating);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateShepherdMoved(Player who, Shepherd which, RoadSquare to) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage("You successfully jump %s.",
                    Representation.roadSquareToString(to));
        } else {

            showMessage("%s, working for enemy %s, jumped %s.",
                    Representation.shepherdToString(which, true),
                    Representation.playerToString(who),
                    Representation.roadSquareToString(to));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateSheepMoved(Player who, Shepherd which, SheepTile from,
            SheepTile to, Sheep sheep) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage("You successfully move a %s from %s to %s.",
                    Representation.sheepToString(sheep),
                    Representation.sheepTileToString(from),
                    Representation.sheepTileToString(to));
        } else {
            showMessage("%s, working for enemy %s, moved a %s from %s to %s.",
                    Representation.shepherdToString(which, true),
                    Representation.playerToString(who),
                    Representation.sheepToString(sheep),
                    Representation.sheepTileToString(from),
                    Representation.sheepTileToString(to));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBoughtTerrainCard(Player who, Shepherd which,
            TerrainCard card) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage("You successfully buy a %s.",
                    Representation.terrainCardToString(card));
        } else {
            showMessage("Enemy %s bought a %s.",
                    Representation.playerToString(who),
                    Representation.terrainCardToString(card));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBeginSheepMating(Player who, Shepherd which,
            SheepTile tile) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage("You attempt to mate a female sheep and a ram on %s.",
                    Representation.sheepTileToString(tile));
        } else {
            showMessage(
                    "%s, working for enemy %s, is attempting to mate a female sheep and a ram on %s.",
                    Representation.shepherdToString(which, true),
                    Representation.playerToString(who),
                    Representation.sheepTileToString(tile));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateEndSheepMating(Player who, Shepherd which,
            SheepTile tile, Sheep newLamb) {
        if (who == currentGame.getSelfPlayer()) {
            if (newLamb == null) {
                showMessage("You fail.");
            } else {
                showMessage("You succeed, and a beautiful lamb is born.");
            }
        } else {
            if (newLamb == null) {
                showMessage("He fails.");
            } else {
                showMessage("He succeeds, and a beautiful lamb is born.");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBeginSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage("You attempt to kill a %s on %s.",
                    Representation.sheepToString(sheep),
                    Representation.sheepTileToString(tile));
        } else {
            showMessage(
                    "%s, working for enemy %s, is attempting to kill a %s on %s.",
                    Representation.shepherdToString(which, true),
                    Representation.playerToString(who),
                    Representation.sheepToString(sheep),
                    Representation.sheepTileToString(tile));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateEndSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep, Map<Player, Integer> paidPlayers) {
        if (who == currentGame.getSelfPlayer()) {
            if (paidPlayers == null) {
                showMessage("You fail. Lucky %s!",
                        Representation.sheepToString(sheep));
            } else {
                if (paidPlayers.isEmpty()) {
                    showMessage("You succeed, and no one bats an eye.");
                } else {
                    showMessage("You succeed, but it does not come for free.");
                    StringBuilder payment = new StringBuilder("You pay ");
                    boolean first = true;
                    for (Map.Entry<Player, Integer> entry : paidPlayers
                            .entrySet()) {
                        if (first) {
                            first = false;
                        } else {
                            payment.append(", ");
                        }

                        payment.append(String.format("%d coins to %s", entry
                                .getValue().intValue(), Representation
                                .playerToString(entry.getKey())));
                    }
                    payment.append(".");
                    showMessage(payment);
                }
            }
        } else {
            if (paidPlayers == null) {
                showMessage("He fails.");
            } else {
                StringBuilder payment = new StringBuilder(
                        "He succeeds, and pays ");

                if (paidPlayers.isEmpty()) {
                    payment.append("nothing for his misdeeds.");
                } else {
                    boolean first = true;
                    for (java.util.Map.Entry<Player, Integer> entry : paidPlayers
                            .entrySet()) {
                        if (first) {
                            first = false;
                        } else {
                            payment.append(", ");
                        }

                        String player;
                        if (entry.getKey() == currentGame.getSelfPlayer()) {
                            player = "you";
                        } else {
                            player = Representation.playerToString(entry
                                    .getKey());
                        }
                        payment.append(String.format("%d coins to %s", entry
                                .getValue().intValue(), player));
                    }
                    payment.append(".");
                }

                showMessage(payment);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateSoldMarketCard(Player who, TerrainCard card, int cost) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage(
                    "Your %s is now for sale at the market, for only %d coins!",
                    Representation.terrainCardToString(card), cost);
        } else {
            showMessage(
                    "A %s from %s is now for sale at the market, for %d coins.",
                    Representation.terrainCardToString(card),
                    Representation.playerToString(who), cost);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBoughtMarketCard(Player who, TerrainCard card) {
        if (who == currentGame.getSelfPlayer()) {
            showMessage("You successfully buy a %s from the market.",
                    Representation.terrainCardToString(card));
        } else {
            showMessage("Enemy %s bought a %s from the market.",
                    Representation.playerToString(who),
                    Representation.terrainCardToString(card));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateLambsGrew(SheepTile tile) {
        // nothing to notify here
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void syncShepherdPositions() {
        // nothing to do here
    }
}
