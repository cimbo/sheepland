package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The background input thread for a SocketClientConnection.
 * 
 * @author Alessandro Cimbelli
 */
public class ClientReaderThread extends Thread {

    private final SocketClientConnection connection;
    private final ClientReceiver receiver;
    private final InputStream socketInputStream;
    private ObjectInputStream inputStream;
    private boolean running;

    /**
     * Construct a ClientReaderThread, given the connection and the receiver
     * handling the commands.
     * 
     * @param connection
     *            the connection associated with this input thread
     * @param receiver
     *            the receiver handling the command
     * @param socketInputStream
     *            the actual socket input stream to read from
     */
    public ClientReaderThread(SocketClientConnection connection,
            ClientReceiver receiver, InputStream socketInputStream) {

        this.connection = connection;
        this.receiver = receiver;
        this.socketInputStream = socketInputStream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        running = true;
        try {
            inputStream = new ObjectInputStream(socketInputStream);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE,
                    "IO exception opening input stream", e);

            close();
            return;
        }

        while (running) {
            checkInput();
        }
    }

    private void checkInput() {

        try {
            byte version = inputStream.readByte();

            // I check that the protocol's version that the server is using is
            // equals to the client's protocol version
            if (Protocol.V1.getVersion() != version) {
                throw new ProtocolException("Unsupported version");
            }

            int futureBits = inputStream.readInt();
            if (futureBits != 0) {
                throw new ProtocolException("Unsupported flags");
            }

            // I take the Number of the operation that the server wants to call

            int operationCode = inputStream.readInt();
            ClientOperation[] operations = ClientOperation.values();
            if (operationCode < 0 || operationCode >= operations.length) {
                throw new ProtocolException("Invalid operation code "
                        + operationCode);
            }

            safeInvokeMethod(operations[operationCode]);
        } catch (ProtocolException e) {
            Logger.getGlobal().log(Level.SEVERE,
                    "Protocol error reading input channel", e);

            close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE,
                    "IO exception reading input channel", e);

            close();
        }
    }

    private void safeInvokeMethod(ClientOperation operation) throws IOException {
        try {
            invokeMethod(operation);
        } catch (ClassCastException e) {
            throw new ProtocolException("Invalid message arguments", e);
        } catch (ClassNotFoundException e) {
            throw new ProtocolException("Invalid message arguments", e);
        }
    }

    private void doSetup() throws IOException, ClassNotFoundException {
        RuleSet rules;
        GameBoard board;
        PlayerIdentifier self;

        rules = (RuleSet) inputStream.readObject();
        board = (GameBoard) inputStream.readObject();
        self = (PlayerIdentifier) inputStream.readObject();

        receiver.setup(rules, board, self);
    }

    private void doMoveAcknowledged() throws IOException,
            ClassNotFoundException {
        Exception result;

        result = (Exception) inputStream.readObject();

        receiver.moveAcknowledged(result);
    }

    private void doForceSynchronize() throws IOException,
            ClassNotFoundException {
        RuleSet newRules;
        GameBoard newBoard;
        PlayerIdentifier newSelf;
        GameState newState;

        newRules = (RuleSet) inputStream.readObject();
        newBoard = (GameBoard) inputStream.readObject();
        newSelf = (PlayerIdentifier) inputStream.readObject();
        newState = (GameState) inputStream.readObject();

        receiver.forceSynchronize(newRules, newBoard, newSelf, newState);
    }

    private void doAdvanceNextTurn() throws IOException, ClassNotFoundException {
        int nextTurn;
        GamePhase phase;

        nextTurn = inputStream.readInt();
        phase = (GamePhase) inputStream.readObject();

        receiver.advanceNextTurn(nextTurn, phase);
    }

    private void doBlackSheepMoved() throws IOException, ClassNotFoundException {
        SheepTileIdentifier destination;

        destination = (SheepTileIdentifier) inputStream.readObject();

        receiver.blackSheepMoved(destination);
    }

    private void doWolfMoved() throws IOException, ClassNotFoundException {
        SheepTileIdentifier destination;
        SheepIdentifier eatenSheep;

        destination = (SheepTileIdentifier) inputStream.readObject();
        eatenSheep = (SheepIdentifier) inputStream.readObject();

        receiver.wolfMoved(destination, eatenSheep);
    }

    private void doLambsGrew() throws IOException, ClassNotFoundException {
        SheepTileIdentifier where;
        Sheep[] newAdultSheep;

        where = (SheepTileIdentifier) inputStream.readObject();
        newAdultSheep = (Sheep[]) inputStream.readObject();

        receiver.lambsGrew(where, newAdultSheep);
    }

    private void doDidExecuteMove() throws IOException, ClassNotFoundException {
        Move move;

        move = (Move) inputStream.readObject();

        receiver.didExecuteMove(move);
    }

    private void doLoginAnswer() throws IOException, ClassNotFoundException {
        boolean answer;

        answer = (Boolean) inputStream.readObject();

        receiver.loginAnswer(answer);
    }

    private void doPing() throws IOException, ClassNotFoundException {
        receiver.ping();
    }

    @SuppressWarnings("unchecked")
    private void doEndGame() throws IOException, ClassNotFoundException {
        Map<PlayerIdentifier, Integer> scores;

        scores = (Map<PlayerIdentifier, Integer>) inputStream.readObject();

        receiver.endGame(scores);
    }

    private void doPlayerLeft() throws IOException, ClassNotFoundException {
        PlayerIdentifier who;
        boolean clean;

        who = (PlayerIdentifier) inputStream.readObject();
        clean = (Boolean) inputStream.readObject();

        receiver.playerLeft(who, clean);
    }

    private void doPlayerJoined() throws IOException, ClassNotFoundException {
        PlayerIdentifier who;

        who = (PlayerIdentifier) inputStream.readObject();

        receiver.playerJoined(who);
    }

    private void doAbandonComplete() throws IOException, ClassNotFoundException {
        receiver.abandonComplete();
    }

    private void invokeMethod(ClientOperation operation) throws IOException,
            ClassNotFoundException {
        switch (operation) {
        case SETUP:
            doSetup();
            break;
        case MOVE_ACKNOWLEDGED:
            doMoveAcknowledged();
            break;
        case FORCE_SYNCHRONIZE:
            doForceSynchronize();
            break;
        case ADVANCE_NEXT_TURN:
            doAdvanceNextTurn();
            break;
        case BLACK_SHEEP_MOVED:
            doBlackSheepMoved();
            break;
        case WOLF_MOVED:
            doWolfMoved();
            break;
        case LAMBS_GREW:
            doLambsGrew();
            break;
        case DID_EXECUTE_MOVE:
            doDidExecuteMove();
            break;
        case LOGIN_ANSWER:
            doLoginAnswer();
            break;
        case PING:
            doPing();
            break;
        case END_GAME:
            doEndGame();
            break;
        case PLAYER_LEFT:
            doPlayerLeft();
            break;
        case PLAYER_JOINED:
            doPlayerJoined();
            break;
        case ABANDON_COMPLETE:
            doAbandonComplete();
            break;
        default:
            throw new AssertionError();
        }
    }

    private void close() {
        receiver.connectionClosed();
        connection.close();
        running = false;
    }
}
