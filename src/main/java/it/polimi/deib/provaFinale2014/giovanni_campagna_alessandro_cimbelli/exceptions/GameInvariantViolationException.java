package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions;

/**
 * Thrown when an invariant of the game is violated (for example the game engine
 * attempts to remove a sheep from an empty tile).
 * 
 * In a well-behaving execution of the program, this will never be generated
 * (and as such it's a RuntimeException). It's presence is an indication of a
 * bug in the game engine (which ought to check for rule violations before
 * attempting unsafe operations).
 * 
 * @author Giovanni Campagna
 * 
 */
public class GameInvariantViolationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a GameInvariantViolationException with the given message.
     * 
     * @param message
     *            the exception message
     */
    public GameInvariantViolationException(String message) {
        super(message);
    }
}
