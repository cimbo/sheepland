package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions;

/**
 * An exception during command line parsing, before actually starting the
 * application.
 * 
 * @author Giovanni Campagna
 */
public class CommandLineParseException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a command line parse exception with the given message.
     * 
     * @param message
     *            the message
     */
    public CommandLineParseException(String message) {
        super(message);
    }

    /**
     * Construct a command line parse exception with the given message and cause
     * 
     * @param message
     *            the message
     * @param cause
     *            the inner exception triggering this one
     */
    public CommandLineParseException(String message, Exception cause) {
        super(message, cause);
    }
}
