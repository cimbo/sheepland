package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import java.util.HashMap;
import java.util.Map;

/**
 * The user database singleton.
 * 
 * This class holds all users (login/passwords) known to the server, and their
 * associated current players/games.
 * 
 * This class is threadsafe.
 * 
 * @author Giovanni Campagna
 * @author Alessandro Cimbelli
 */
public class UserDatabase {
    private static UserDatabase instance;

    private final Map<String, User> users;

    private UserDatabase() {
        users = new HashMap<String, User>();
        users.put("binosi", new User("binosi", "nero"));
    }

    /**
     * Retrieve the user database singleton.
     * 
     * @return the user database
     */
    public static synchronized UserDatabase get() {
        if (instance == null) {
            instance = new UserDatabase();
        }

        return instance;
    }

    /**
     * Retrieve or create a user for the given username/password couple.
     * 
     * If the user exists, the stored password is checked against the provided
     * value. Otherwise, a new user is created with the user/pass pair.
     * 
     * @param username
     *            the username
     * @param password
     *            the password
     * @return the user for this username
     * @throws InvalidPasswordException
     *             the user existed and the password was not valid
     */
    public synchronized User getOrCreateUser(String username, String password)
            throws InvalidPasswordException {
        if (username.isEmpty() || password.isEmpty()) {
            throw new InvalidPasswordException();
        }

        User candidate = users.get(username);

        if (candidate == null) {
            candidate = new User(username, password);
            users.put(username, candidate);
        } else {
            candidate.checkPassword(password);
        }

        return candidate;
    }
}
