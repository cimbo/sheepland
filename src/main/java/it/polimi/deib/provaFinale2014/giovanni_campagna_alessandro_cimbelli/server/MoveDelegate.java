package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ServerDelegate;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.util.Collection;

/**
 * This is a helper class that deals with communicating to players all server
 * initiated moves (moving the black sheep, the wolf, growing the lambs)
 * 
 * All methods of this class must be called under Game's lock (and in general
 * must not be called directly - it is the game engine responsability to call
 * them when appropriate)
 * 
 * @author Alessandro Cimbelli
 */
public class MoveDelegate implements ServerDelegate {

    private final Collection<ServerPlayer> listOfPlayerToServe;

    /**
     * Construct a move delegate for the given players
     * 
     * @param list
     *            the list of players that will receive the moves
     */
    public MoveDelegate(Collection<ServerPlayer> list) {
        listOfPlayerToServe = list;

    }

    /**
     * {@inheritDoc}
     */
    public void didMoveBlackSheep(SheepTileIdentifier destination) {
        for (ServerPlayer player : listOfPlayerToServe) {
            synchronized (player) {
                if (player.getServerThread() != null) {
                    player.getServerThread().sendBlackSheepMoved(destination);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void didMoveWolf(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) {
        for (ServerPlayer player : listOfPlayerToServe) {
            synchronized (player) {
                if (player.getServerThread() != null) {
                    player.getServerThread().sendWolfMoved(destination,
                            eatenSheep);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void didGrowLambs(SheepTileIdentifier tile, Sheep[] newAdultSheep) {
        for (ServerPlayer player : listOfPlayerToServe) {
            synchronized (player) {
                if (player.getServerThread() != null) {
                    player.getServerThread().sendLambsGrew(tile, newAdultSheep);
                }
            }
        }
    }

}
