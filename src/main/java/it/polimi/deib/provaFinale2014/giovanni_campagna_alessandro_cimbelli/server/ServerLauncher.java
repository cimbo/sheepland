package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.CommandLineParseException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIServerNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.NetworkUtil;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketServerNetwork;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The entry point for the server application. Instantiates a new Server and
 * starts it.
 * 
 * @author Giovanni Campagna
 */
public class ServerLauncher {
    private final Collection<ServerNetwork> networks;

    /**
     * Construct a server launcher with the given command line arguments
     * 
     * @param args
     *            the server command line arguments
     * @throws CommandLineParseException
     *             if arguments where not recognized
     */
    public ServerLauncher(String[] args) throws CommandLineParseException {
        networks = new ArrayList<ServerNetwork>();

        parseArguments(args);
        maybeAddDefaultNetworks();
    }

    private void parseArguments(String[] args) throws CommandLineParseException {
        boolean inListen = false;
        String listenOption = null;
        for (String arg : args) {
            if (inListen && arg.startsWith("-")) {
                throw new CommandLineParseException(listenOption
                        + " requires an argument");
            }

            if (inListen) {
                try {
                    networks.add(new SocketServerNetwork(NetworkUtil
                            .parseHostPort(arg, true)));
                    inListen = false;
                } catch (NumberFormatException e) {
                    throw new CommandLineParseException(
                            "Could not parse listen address", e);
                } catch (UnknownHostException e) {
                    throw new CommandLineParseException(
                            "Could not resolve listen address", e);
                }
            } else {
                if ("-L".equals(arg) || "--listen".equals(arg)) {
                    listenOption = arg;
                    inListen = true;
                } else if ("--socket".equals(arg)) {
                    networks.add(new SocketServerNetwork());
                } else if ("--rmi".equals(arg)) {
                    try {
                        networks.add(new RMIServerNetwork());
                    } catch (IOException e) {
                        throw new CommandLineParseException(
                                "RMI is not supported", e);
                    }
                } else {
                    throw new CommandLineParseException("Unknown option " + arg);
                }
            }
        }
    }

    private void maybeAddDefaultNetworks() {
        if (networks.isEmpty()) {
            networks.add(new SocketServerNetwork());
        }
    }

    /**
     * Run the server application.
     */
    public void run() {
        Logger.getGlobal().setLevel(Level.CONFIG);

        Server server;

        try {
            server = new Server(networks);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE,
                    "Failed to initialize network", e);
            return;
        }

        server.start();
    }
}
