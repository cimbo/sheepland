package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

/**
 * An abstract class that takes care of applying the effect on an animation
 * (given as a progress factor between 0.0 and 1.0) onto the view.
 * 
 * @author Giovanni Campagna
 */
public abstract class Animator {
    /**
     * Update the view for the given progress.
     * 
     * A value of 0.0 represents the state at the beginning of the animation,
     * and 1.0 represents the state at the end, but the animator should be able
     * to handle values outside, to allow for bouncing effects
     * 
     * @param progress
     *            the space progress of the animation
     */
    protected abstract void update(double progress);
}
