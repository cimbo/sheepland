package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

import java.io.Serializable;

/**
 * This class represents an abstract game move, that is, an operation that can
 * be sent over the wire and executed on multiple game engines.
 * 
 * @author Giovanni Campagna
 * 
 */
public abstract class Move implements Serializable {
    private static final long serialVersionUID = 1L;

    private final PlayerIdentifier whoId;

    /**
     * Construct a move on behalf of a player.
     * 
     * @param who
     *            the player executing the move
     */
    protected Move(PlayerIdentifier who) {
        whoId = who;
    }

    /**
     * Retrieve the player executing the move
     * 
     * @return the player as identifier
     */
    public PlayerIdentifier getWhoId() {
        return whoId;
    }

    /**
     * Retrieve the actual player object, by resolving the identifier through
     * the passed in engine.
     * 
     * @param engine
     * @return the player as game object
     * @throws InvalidIdentifierException
     */
    protected Player getWho(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getPlayerFromId(whoId);
    }

    /**
     * Execute this move on the given engine.
     * 
     * @param engine
     *            the game engine
     * @throws InvalidMoveException
     *             if executing the move failed, or the move itself was
     *             corrupted (contained invalid identifiers)
     */
    public abstract void execute(GameEngine engine) throws InvalidMoveException;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + whoId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Move)) {
            return false;
        }
        Move other = (Move) obj;
        if (!whoId.equals(other.whoId)) {
            return false;
        }
        return true;
    }
}
