package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageLoader;

import java.awt.Graphics;
import java.awt.Image;

/**
 * An icon of a shepherd (player pawn on the board)
 * 
 * @author Alessandro Cimbelli
 */
public class PlayerPawn extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 29;
    private static final int Y_SIZE = 29;
    private boolean draggable = false;
    private boolean current = false;
    private final Image crown;

    /**
     * Construct a new player pawn, with the given index.
     * 
     * @param index
     *            the index of the player owning this pawn, used to pick the
     *            color matching his player icon
     */
    public PlayerPawn(int index) {
        super(X_SIZE, Y_SIZE, "player" + (index + 1) + ".png");
        crown = ImageLoader.loadImageSync(this, "crown.png");
    }

    /**
     * Sets this pawn as draggable.
     * 
     * @param b
     *            if the pawn is draggable
     */
    public void setDraggable(boolean b) {
        draggable = b;
    }

    /**
     * Check if this pawn is draggable.
     * 
     * @return true, if the pawn is draggable, false otherwise
     */
    public boolean isDraggable() {
        return draggable;
    }

    /**
     * Check if this pawn is current (ie, represents the shepherd that was
     * chosen by the player for the current turn)
     * 
     * @return true, if the pawn is current, false otherwise
     */
    public boolean isCurrent() {
        return current;
    }

    /**
     * Sets the pawn as current.
     * 
     * The current pawn has a crown icon drawn on top.
     * 
     * @param b
     *            if the pawn ought to be current.
     */
    public void setCurrent(boolean b) {
        current = b;
        repaint();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (current) {
            g.drawImage(crown, (getWidth() - crown.getWidth(null)) / 2, 0, null);
        }
    }
}
