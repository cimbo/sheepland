package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

/**
 * The sidebar holding the cards currently in the hand of the player.
 * 
 * @author Alessandro Cimbelli
 */
public class CardPanel extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 120;
    private static final int Y_SIZE = 700;
    private final Card[] allCards;

    /**
     * Construct a new empty card panel.
     */
    public CardPanel() {
        super(X_SIZE, Y_SIZE, "cardsPanel.png");
        setLayout(null);
        allCards = new Card[TerrainType.NUMBER_OF_TERRAINS];
        addCards();
    }

    private void addCards() {
        Card grain = new Card(TerrainType.GRAIN);

        double sumHeightCards = grain.getHeight() * 6;
        double singleCardHeight = grain.getHeight();
        double freeHeightSpece = getHeight() - sumHeightCards;
        double singleFreeSpace = freeHeightSpece / 7;

        int xCardLocation = (int) (getWidth() / 2 - grain.getWidth() * 0.5);
        int yGeneralForAll = (int) (getHeight() - sumHeightCards);

        TerrainType[] order = { TerrainType.GRAIN, TerrainType.MOUNTAIN,
                TerrainType.DESERT, TerrainType.WATER, TerrainType.FOREST,
                TerrainType.LAND };
        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            Card card = new Card(order[i]);

            add(card);
            card.setLocation(xCardLocation, (int) (yGeneralForAll
                    - singleFreeSpace * (6 - i) + singleCardHeight * i));
            allCards[order[i].ordinal()] = card;
        }
    }

    /**
     * Update the number of cards of the given type.
     * 
     * @param type
     *            the type to update
     * @param i
     *            the new number of cards
     */
    public void updateCard(TerrainType type, int i) {
        allCards[type.ordinal()].setNumberOfCard(i);
    }
}
