package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.Animation;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.AnimationAlpha;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.AnimationListener;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.PositionAnimator;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JPanel;

/**
 * The main game board component.
 * 
 * This class is responsible for drawing the map and handling input on it, as
 * well as taking input from the various buttons and overlay panels during the
 * game.
 * 
 * @author Alessandro Cimbelli
 */
public class GameBoardPanel extends ImageComponent {
    private static final long serialVersionUID = 1L;
    // Mantenere sempre questo rapporto!--->700/527 i centri delle regioni sono
    // piazzati rispetto a questo
    private static final int X_SIZE = 527;
    private static final int Y_SIZE = 700;

    private ColorGameBoard colorMap;
    private Map<SheepTileIdentifier, TileIcon> animalTilePosition;
    private final Map<Player, PlayerIcon> playerIcon = new HashMap<Player, PlayerIcon>();
    private Map<Shepherd, PlayerPawn> shepherdMap;
    private GraphicPhase currentPhase = new GraphicPhase();
    private final List<Fence> arrayFence = new ArrayList<Fence>();
    private PlayerPawn draggingPawn;
    private SheepIcon draggingAnimal;
    private SheepIcon killingAnimal;
    private JPanel buyPanel;
    private JPanel matePanel;
    private int cardLeftX;
    private int cardLeftY;
    private int cardRightX;
    private int cardRightY;
    private Dimension cardSize;
    private SheepTile leftTile;
    private SheepTile rightTile;
    private boolean cardLeft = false;
    private boolean cardRight = false;
    private Card cardToSellOrBuy = null;
    private PlayerIcon playerSeller = null;
    private TerrainCard effectiveCardToBuy = null;
    private boolean readyToSellCard = false;
    private int starNumber = 0;
    private SellOrBuyPanel marketPanel;
    private Shepherd turnShepherd;
    private int placedFence = 0;
    private int xInitialDrag;
    private int yInitialDrag;
    private GameFrame myFrame;

    // FOR THE DRAG AND DROP
    private boolean correctlyPressed = false;
    private int xInitialDragClick;
    private int yInitialDragClick;

    /**
     * Construct a new game board panel.
     * 
     * @param frame
     *            the owner window
     * @throws IOException
     *             if the map failed to load
     */
    public GameBoardPanel(GameFrame frame) throws IOException {
        super(X_SIZE, Y_SIZE, "GameBoard.png");
        // PreInit hashMap tile
        colorMap = new ColorGameBoard();
        myFrame = frame;
        animalTilePosition = new HashMap<SheepTileIdentifier, TileIcon>();
        fillHashMapAnimalPosition();
        shepherdMap = new HashMap<Shepherd, PlayerPawn>();

        GameBoardMouseInput input = new GameBoardMouseInput();
        addMouseMotionListener(input);
        addMouseListener(input);
        setLayout(null);
        addMoveButtons();
    }

    private class GameBoardMouseInput implements MouseListener,
            MouseMotionListener {
        /**
         * {@inheritDoc}
         */
        public void mouseMoved(MouseEvent e) {
            // we don't need to handle this event
        }

        /**
         * {@inheritDoc}
         */
        public void mouseDragged(MouseEvent e) {
            if (!correctlyPressed) {
                return;
            }

            switch (currentPhase.getPhase()) {
            case MOVING_SHEPHERD:
                draggingPawn.setLocation(
                        (int) (e.getX() - draggingPawn.getWidth() * 0.5),
                        (int) (e.getY() - draggingPawn.getHeight() * 0.5));
                repaint();
                break;
            case MOVING_SHEEP:
                draggingAnimal.setLocation(
                        (int) (e.getX() - draggingAnimal.getWidth() * 0.5),
                        (int) (e.getY() - draggingAnimal.getHeight() * 0.5));
                repaint();
                break;

            default:
                break;
            }
        }

        /**
         * {@inheritDoc}
         */
        public void mouseEntered(MouseEvent e) {
            // we don't need to handle this event
        }

        /**
         * {@inheritDoc}
         */
        public void mouseExited(MouseEvent e) {
            // we don't need to handle this event
        }

        /**
         * {@inheritDoc}
         */
        public void mousePressed(MouseEvent e) {
            if (currentPhase.isFrozen()) {
                return;
            }

            switch (currentPhase.getPhase()) {

            case MOVING_SHEPHERD:
                startShepherdDrag(e.getX(), e.getY());
                break;

            case MOVING_SHEEP:
                startSheepDrag(e.getX(), e.getY());
                break;

            default:
                break;
            }
        }

        /**
         * {@inheritDoc}
         */
        public void mouseClicked(MouseEvent e) {
            if (!currentPhase.isFrozen()) {

                switch (currentPhase.getPhase()) {
                case NULL:
                case MOVING_SHEPHERD:
                case MOVING_SHEEP:
                    break;

                case OPPONENT_TURN:
                    moveDuringOpponent();
                    break;

                case PLACE_SHEPHERD:
                    placeShepherd(e.getX(), e.getY());
                    break;

                case CHOOSE_SHEPHERD:
                    chooseShepherd(e.getX(), e.getY());
                    break;

                case BUYING_TERRAIN_CARD:
                    buyingTerrainCard(e.getX(), e.getY());
                    break;

                case MATING_SHEEP:
                    matingSheep(e.getX(), e.getY());
                    break;

                case KILLING_SHEEP:
                    chooseSheepToKill(e.getX(), e.getY());
                    break;

                case MARKET_SELL:
                    sellCard(e.getX(), e.getY());
                    break;

                case MARKET_BUY:
                    buyCardsMarket(e.getX(), e.getY());
                    break;

                default:
                    throw new AssertionError();
                }
            }
        }

        /**
         * {@inheritDoc}
         */
        public void mouseReleased(MouseEvent e) {
            if (!correctlyPressed) {
                return;
            }

            switch (currentPhase.getPhase()) {
            case MOVING_SHEPHERD:
                endShepherdDrag(e.getX(), e.getY());
                break;

            case MOVING_SHEEP:
                endSheepDrag(e.getX(), e.getY());
                break;

            default:
                break;
            }
        }

    }

    private void moveDuringOpponent() {
        myFrame.showMessage("It is not your turn");
    }

    /**
     * Clear (close) any remaining open panels or overlays.
     * 
     * This method should be called when the turn switches to an opponent
     * player.
     */
    public void clearOpenPanels() {
        if (marketPanel != null) {
            remove(marketPanel);
            marketPanel = null;
        }
        if (buyPanel != null) {
            remove(buyPanel);
            buyPanel = null;
        }
        if (matePanel != null) {
            remove(matePanel);
            matePanel = null;
        }
        repaint();
    }

    private void moveButtonClicked(GraphicMove move) {
        if (currentPhase.isFrozen()
                || currentPhase.getPhase().equals(GraphicMove.MARKET_SELL)
                || currentPhase.getPhase().equals(GraphicMove.MARKET_BUY)) {
            return;
        }
        clearOpenPanels();

        if (currentPhase.getPhase() == GraphicMove.PLACE_SHEPHERD) {
            myFrame.placeYourShepherd();
            return;
        }
        if (currentPhase.getPhase() == GraphicMove.CHOOSE_SHEPHERD) {
            myFrame.showChooseShepherd();
            return;
        }
        if (currentPhase.getPhase() == GraphicMove.OPPONENT_TURN) {
            moveDuringOpponent();
            return;
        }

        currentPhase.setPhase(move);

        switch (currentPhase.getPhase()) {

        case JUMP_TURN:
            try {
                myFrame.getCurrentGame().forceEndTurn();
            } catch (RuleViolationException e) {
                Logger.getGlobal().log(Level.FINE, "Rule violation exception",
                        e);
                myFrame.moveNotAllowed(e);
                return;
            }
            break;

        case MOVE_SHEPHERD:
            currentPhase.setPhase(GraphicMove.MOVING_SHEPHERD);
            break;

        case MOVE_SHEEP:
            currentPhase.setPhase(GraphicMove.MOVING_SHEEP);
            break;

        case BUY_TERRAIN_CARD:
            showAvailableCards();
            break;

        case MATE_SHEEP:
            showAvailableMates();
            break;

        case KILL_SHEEP:
            currentPhase.setPhase(GraphicMove.KILLING_SHEEP);
            break;

        default:
            throw new AssertionError();
        }
    }

    /**
     * Initialize the fence icons from the rules of the current game.
     */
    public void fillFenceArray() {
        RuleSet rule = myFrame.getCurrentGame().getRules();

        for (int i = 0; i < rule.getNumberOfFences(); i++) {
            arrayFence.add(new Fence("fence.png"));
        }

        for (int i = 0; i < rule.getNumberOfFinalFences(); i++) {
            arrayFence.add(new Fence("finalFence.png"));
        }

    }

    private void setButtonLocation(MoveButton x, double i, double j) {
        add(x);
        x.setLocation((int) (X_SIZE / (527.0 / i) - x.getWidth() / 2),
                (int) (Y_SIZE / (700.0 / j) - x.getHeight() / 2));
    }

    private void addMoveButtons() {
        MoveButton moveShepherd = new MoveButton("Move\nShepherd",
                GraphicMove.MOVE_SHEPHERD);
        MoveButton moveSheep = new MoveButton("Move\nSheep",
                GraphicMove.MOVE_SHEEP);
        MoveButton mateSheep = new MoveButton("Mate\nSheep",
                GraphicMove.MATE_SHEEP);
        MoveButton killSheep = new MoveButton("Kill\nSheep",
                GraphicMove.KILL_SHEEP);
        MoveButton jumpTurn = new MoveButton("Skip\nTurn",
                GraphicMove.JUMP_TURN);
        MoveButton buyCard = new MoveButton("Buy\nCard",
                GraphicMove.BUY_TERRAIN_CARD);

        setButtonLocation(moveShepherd, 302.0, 667.0);
        setButtonLocation(moveSheep, 490.0, 603.0);
        setButtonLocation(mateSheep, 366.0, 667.0);
        setButtonLocation(buyCard, 490.0, 667.0);
        setButtonLocation(killSheep, 428.0, 667.0);
        setButtonLocation(jumpTurn, 428.0, 603.0);

        moveShepherd.addActionListener(new MoveButtonListener());
        moveSheep.addActionListener(new MoveButtonListener());
        mateSheep.addActionListener(new MoveButtonListener());
        killSheep.addActionListener(new MoveButtonListener());
        jumpTurn.addActionListener(new MoveButtonListener());
        buyCard.addActionListener(new MoveButtonListener());
    }

    private class MoveButtonListener implements ActionListener {
        /**
         * {@inheritDoc}
         */
        public void actionPerformed(ActionEvent e) {
            moveButtonClicked(((MoveButton) e.getSource()).getMove());
        }
    }

    private SheepIcon getAnimalToDrag(int x, int y) {
        Component drag;

        drag = this.getComponentAt(x, y);

        if (drag instanceof SheepIcon) {
            return (SheepIcon) drag;
        }

        return null;
    }

    private SheepIcon getAnimalToKill(int x, int y) {
        SheepIcon kill = getAnimalToDrag(x, y);

        if (kill != null && kill.getType() != SheepType.BLACK_SHEEP) {
            return kill;
        }

        return null;
    }

    /**
     * Show an overlay panel allowing the user to choose which cards to sell
     * during the market phase.
     */
    public void sellCards() {
        cardToSellOrBuy = null;

        if (marketPanel != null) {
            remove(marketPanel);
        }
        marketPanel = new SellPanel("Choose the cards that you want to sell",
                "Sell");
        ((SellPanel) marketPanel).setCoinsEnabled(false);
        marketPanel.setActionEnabled(false);
        marketPanel.setLayout(null);
        marketPanel.setBackVisible(false);

        Player selfPlayer = myFrame.getCurrentGame().getSelfPlayer();

        int space = (int) ((getWidth() - TerrainType.NUMBER_OF_TERRAINS * 60) / (float) (TerrainType.NUMBER_OF_TERRAINS + 1));

        TerrainType[] types = { TerrainType.GRAIN, TerrainType.MOUNTAIN,
                TerrainType.DESERT, TerrainType.WATER, TerrainType.FOREST,
                TerrainType.LAND };

        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            Card c = new Card(types[i]);
            marketPanel.add(c);
            c.setLocation(i * c.getWidth() + (i + 1) * space, 38);

            int number = 0;
            Iterator<TerrainCard> iterator = selfPlayer.iterateCards();
            while (iterator.hasNext()) {
                TerrainCard card = iterator.next();
                if (card.getType() == types[i] && !card.isForSale()) {
                    number++;
                }
            }
            if (number == 0) {
                c.setDrawMask(true);
            } else {
                c.setNumberOfCard(number);
            }
        }

        this.add(marketPanel, 0);
        marketPanel.setLocation(0, (int) (X_SIZE / (X_SIZE / 210.0)));
        marketPanel.setVisible(true);
        currentPhase.setPhase(GraphicMove.MARKET_SELL);
    }

    private void buyCardsChooseOpponent() {
        Player selfPlayer = myFrame.getCurrentGame().getSelfPlayer();

        if (marketPanel != null) {
            remove(marketPanel);
        }
        marketPanel = new SellOrBuyPanel("Choose the seller of this card!",
                "Buy");
        marketPanel.setActionEnabled(false);
        marketPanel.setLayout(null);
        marketPanel.setBackVisible(true);

        Player[] allPlayers = myFrame.getCurrentGame().getAllPlayers();

        int space = (int) ((getWidth() - allPlayers.length * 65) / (float) (allPlayers.length + 1));

        int j = 0;
        for (int i = 0; i < allPlayers.length; i++) {
            Player player = allPlayers[i];
            if (player == selfPlayer) {
                continue;
            }

            PlayerIcon p = new PlayerIcon(i, player.getName());
            marketPanel.add(p);
            p.setLocation(j * p.getWidth() + (j + 1) * space, 38);

            int number = 0;
            for (TerrainCard card : myFrame.getCurrentGame().getGameBoard()
                    .getTerrainCardsOnSale(player)) {
                if (card.getType() == cardToSellOrBuy.getType()) {
                    number++;
                }
            }

            if (number == 0) {
                p.setDrawMask(true);
            } else {
                p.updateMoney(number);
            }

            j++;
        }

        this.add(marketPanel, 0);
        marketPanel.setLocation(0, (int) (X_SIZE / (X_SIZE / 210.0)));
        marketPanel.setVisible(true);
        currentPhase.setPhase(GraphicMove.MARKET_BUY);
    }

    private void buyCardsChooseCard() {
        if (marketPanel != null) {
            remove(marketPanel);
        }
        marketPanel = new SellOrBuyPanel(
                "Choose the card that you want to buy!", "Buy");
        marketPanel.setActionEnabled(false);
        marketPanel.setLayout(null);
        marketPanel.setBackVisible(true);

        int maxNumber = myFrame.getCurrentGame().getRules().getBuyableCards();
        int space = (int) ((getWidth() - maxNumber * 60) / (float) (maxNumber + 1));

        Player seller = myFrame.getCurrentGame().getAllPlayers()[playerSeller
                .getIndex()];
        TerrainCard[] allSoldCards = myFrame.getCurrentGame().getGameBoard()
                .getTerrainCardsOnSale(seller);
        List<TerrainCard> soldCards = new ArrayList<TerrainCard>();
        for (TerrainCard card : allSoldCards) {
            if (card.getType() == cardToSellOrBuy.getType()) {
                soldCards.add(card);
            }
        }

        for (int i = 0; i < soldCards.size(); i++) {
            TerrainCard card = soldCards.get(i);
            Card c = new Card(card.getType());
            marketPanel.add(c);
            c.setLocation(i * c.getWidth() + (i + 1) * space, 38);

            c.setNumberOfCard(card.getCost());
        }

        this.add(marketPanel, 0);
        marketPanel.setLocation(0, (int) (X_SIZE / (X_SIZE / 210.0)));
        marketPanel.setVisible(true);
        currentPhase.setPhase(GraphicMove.MARKET_BUY);
    }

    private void sellCard(int xClicked, int yClicked) {
        checkSellOrBuyCard(xClicked, yClicked);

        ((SellPanel) marketPanel).setCoinsEnabled(cardToSellOrBuy != null);
        marketPanel.setActionEnabled(readyToSellCard);
    }

    private void buyCardsMarket(int xClicked, int yClicked) {
        if (playerSeller != null) {
            // sto mostrando il pannello con le carte di un giocatore
            checkBuyEffectiveCard(xClicked, yClicked);
            marketPanel.setActionEnabled(effectiveCardToBuy != null);
        } else if (cardToSellOrBuy != null) {
            // sto mostrando il pannello con i giocatori
            checkBuyOpponent(xClicked, yClicked);
            if (playerSeller != null) {
                buyCardsChooseCard();
            }
        } else {
            // sto mostrando il pannello iniziale
            checkSellOrBuyCard(xClicked, yClicked);
            if (cardToSellOrBuy != null) {
                buyCardsChooseOpponent();
            }
        }
    }

    /**
     * Show an overlay panel allowing the user to choose which cards to buy
     * (which type, first of all) during the market phase.
     */
    public void buyCardsChooseType() {
        effectiveCardToBuy = null;
        playerSeller = null;
        cardToSellOrBuy = null;

        if (marketPanel != null) {
            remove(marketPanel);
        }
        marketPanel = new SellOrBuyPanel(
                "Choose the card that you want to buy!", "Buy");
        marketPanel.setActionEnabled(false);
        marketPanel.setLayout(null);
        marketPanel.setBackVisible(false);

        Player selfPlayer = myFrame.getCurrentGame().getSelfPlayer();
        int space = (int) ((getWidth() - TerrainType.NUMBER_OF_TERRAINS * 60) / (float) (TerrainType.NUMBER_OF_TERRAINS + 1));

        TerrainType[] types = { TerrainType.GRAIN, TerrainType.MOUNTAIN,
                TerrainType.DESERT, TerrainType.WATER, TerrainType.FOREST,
                TerrainType.LAND };

        for (int i = 0; i < TerrainType.NUMBER_OF_TERRAINS; i++) {
            Card c = new Card(types[i]);
            marketPanel.add(c);
            c.setLocation(i * c.getWidth() + (i + 1) * space, 38);

            int number = 0;
            for (TerrainCard card : myFrame.getCurrentGame().getGameBoard()
                    .getTerrainCardsOnSale(types[i])) {
                if (card.getOwner() != selfPlayer) {
                    number++;
                }
            }

            if (number == 0) {
                c.setDrawMask(true);
            } else {
                c.setNumberOfCard(number);
            }
        }

        this.add(marketPanel, 0);
        marketPanel.setLocation(0, (int) (X_SIZE / (X_SIZE / 210.0)));
        marketPanel.setVisible(true);
        currentPhase.setPhase(GraphicMove.MARKET_BUY);

    }

    /**
     * Forcibly end the market phase, moving forward with the game.
     */
    public void endMarket() {
        try {
            myFrame.getCurrentGame().forceEndTurn();
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
        }

        remove(marketPanel);
        marketPanel = null;
        repaint();
    }

    /**
     * Go back to the previous panel, during a multi-panel market operation
     * (buying only)
     */
    public void goBackMarket() {
        assert currentPhase.getPhase() == GraphicMove.MARKET_BUY;
        assert cardToSellOrBuy != null;

        if (playerSeller != null) {
            playerSeller = null;
            buyCardsChooseOpponent();
        } else {
            buyCardsChooseType();
        }
    }

    /**
     * Confirm the current market action (buying or selling as appropriate)
     */
    public void performMarketAction() {
        if (currentPhase.getPhase() == GraphicMove.MARKET_BUY) {
            doBuy();
        } else {
            assert currentPhase.getPhase() == GraphicMove.MARKET_SELL;
            doSell();
        }
    }

    private void doSell() {
        try {
            Iterator<TerrainCard> iterator = myFrame.getCurrentGame()
                    .getSelfPlayer().iterateCards();

            TerrainCard card = null;
            while (iterator.hasNext()) {
                TerrainCard candidate = iterator.next();
                if (candidate.getType() == cardToSellOrBuy.getType()
                        && !candidate.isForSale()) {
                    card = candidate;
                    break;
                }
            }
            myFrame.getCurrentGame().sellMarketCard(card, starNumber);
            remove(marketPanel);
            repaint();
            sellCards();

        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
        }
    }

    private void doBuy() {
        try {
            myFrame.getCurrentGame().buyMarketCard(effectiveCardToBuy);
            remove(marketPanel);
            repaint();
            buyCardsChooseType();
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
        }
    }

    /**
     * Select the given number of coins as the price of the selected card.
     * 
     * @param coins
     *            the price
     */
    public void chooseMarketCoins(int coins) {
        if (cardToSellOrBuy == null) {
            return;
        }

        if (coins != 0) {
            starNumber = coins;
            readyToSellCard = true;
        }

        marketPanel.setActionEnabled(readyToSellCard);
    }

    private void checkSellOrBuyCard(int xClicked, int yClicked) {
        Component clicked = marketPanel.getComponentAt(
                xClicked - marketPanel.getX(), yClicked - marketPanel.getY());

        if (clicked instanceof Card && !((Card) clicked).getDrawMask()) {
            cardToSellOrBuy = (Card) clicked;
        }
    }

    private void checkBuyOpponent(int xClicked, int yClicked) {
        Component clicked = marketPanel.getComponentAt(
                xClicked - marketPanel.getX(), yClicked - marketPanel.getY());

        if (clicked instanceof PlayerIcon
                && !((PlayerIcon) clicked).getDrawMask()) {
            playerSeller = (PlayerIcon) clicked;
        }
    }

    private void checkBuyEffectiveCard(int xClicked, int yClicked) {
        Component clicked = marketPanel.getComponentAt(
                xClicked - marketPanel.getX(), yClicked - marketPanel.getY());

        if (clicked instanceof Card) {
            Card icon = (Card) clicked;

            TerrainCard[] allForSale = myFrame.getCurrentGame().getGameBoard()
                    .getTerrainCardsOnSale(icon.getType());

            for (TerrainCard card : allForSale) {
                if (icon.getNumberOfCard() == card.getCost()) {
                    effectiveCardToBuy = card;
                    break;
                }
            }
        }
    }

    private void showAvailableCards() {

        if (turnShepherd == null) {
            currentPhase.setPhase(GraphicMove.CHOOSE_SHEPHERD);
            return;
        }

        RoadSquare myRoad = turnShepherd.getCurrentPosition();
        TerrainType leftType = null;
        TerrainType rightType = null;

        buyPanel = new JPanel();

        SheepTile[] array = myFrame.getCurrentGame().getGameBoard().getMap()
                .getIncidentTiles(myRoad);

        leftTile = array[0];
        rightTile = array[1];

        if (leftTile != null) {
            leftType = leftTile.getTerrainType();
        }
        if (rightTile != null) {
            rightType = rightTile.getTerrainType();
        }

        if (isBuyable(leftType) || isBuyable(rightType)) {
            currentPhase.setPhase(GraphicMove.BUYING_TERRAIN_CARD);
        } else {
            myFrame.moveNotAllowed(new RuleViolationException(
                    "Card is not buyable"));
            currentPhase.setPhase(GraphicMove.NULL);
            return;
        }

        buyPanel.setLayout(null);
        buyPanel.setSize((int) (X_SIZE / (527.0 / 240)),
                (int) (Y_SIZE / (700.0 / 85)));
        buyPanel.setMaximumSize(new Dimension((int) (X_SIZE / (527.0 / 330)),
                (int) (Y_SIZE / (700.0 / 120))));
        buyPanel.setMinimumSize(new Dimension((int) (X_SIZE / (527.0 / 330)),
                (int) (Y_SIZE / (700.0 / 120))));

        buyPanel.setBackground(new Color(255, 255, 0xA0));

        if (isBuyable(leftType)) {
            cardLeft = true;
            Card tmpLeft = new Card(leftType);
            cardSize = new Dimension(tmpLeft.getWidth(), tmpLeft.getHeight());
            buyPanel.add(tmpLeft);
            cardLeftX = (int) (buyPanel.getWidth() / 7.0);
            cardLeftY = (int) ((buyPanel.getHeight() - tmpLeft.getHeight()) * 0.5);
            tmpLeft.setLocation(cardLeftX, cardLeftY);
        }

        if (isBuyable(rightType)) {
            cardRight = true;
            Card tmpRight = new Card(rightType);
            cardSize = new Dimension(tmpRight.getWidth(), tmpRight.getHeight());
            buyPanel.add(tmpRight);
            cardRightX = (int) (buyPanel.getWidth() / 7.0 * 4);
            cardRightY = (int) ((buyPanel.getHeight() - tmpRight.getHeight()) * 0.5);
            tmpRight.setLocation(cardRightX, cardRightY);

        }

        add(buyPanel, 0);
        buyPanel.setLocation((int) (X_SIZE / (527.0 / 144)),
                (int) (Y_SIZE * 0.3));
        buyPanel.setVisible(true);

        currentPhase.setPhase(GraphicMove.BUYING_TERRAIN_CARD);

    }

    private boolean isBuyable(TerrainType type) {
        return myFrame.getCurrentGame().getGameBoard()
                .getNumberOfTerrainCards(type) != 0;
    }

    private void buyingTerrainCard(int xClicked, int yClicked) {

        int yPanel = (int) buyPanel.getLocation().getY();
        int xPanel = (int) buyPanel.getLocation().getX();

        if (!(yPanel + cardLeftY < yClicked && yClicked < yPanel + cardLeftY
                + cardSize.getHeight())) {
            remove(buyPanel);
            buyPanel = null;
            repaint();
            currentPhase.setPhase(GraphicMove.NULL);
            return;
        }

        if (xClicked > xPanel + cardLeftX
                && xClicked < xPanel + cardLeftX + cardSize.getWidth()
                && cardLeft) {
            TerrainCard buy = myFrame.getCurrentGame().getGameBoard()
                    .getCheapestTerrainCard(leftTile.getTerrainType(), null);

            try {
                myFrame.getCurrentGame().buyTerrainCard(turnShepherd, buy);
            } catch (RuleViolationException e) {
                Logger.getGlobal().log(Level.FINE, "Rule violation exception",
                        e);
                remove(buyPanel);
                buyPanel = null;
                repaint();
                myFrame.moveNotAllowed(e);
                return;
            }
        }

        if (xClicked > xPanel + cardRightX
                && xClicked < xPanel + cardRightX + cardSize.getWidth()
                && cardRight) {
            TerrainCard buy = myFrame.getCurrentGame().getGameBoard()
                    .getCheapestTerrainCard(rightTile.getTerrainType(), null);
            try {
                myFrame.getCurrentGame().buyTerrainCard(turnShepherd, buy);
            } catch (RuleViolationException e) {
                Logger.getGlobal().log(Level.FINE, "Rule violation exception",
                        e);
                remove(buyPanel);
                buyPanel = null;
                repaint();
                myFrame.moveNotAllowed(e);
                return;
            }

        }

        remove(buyPanel);
        buyPanel = null;
        cardLeft = false;
        cardRight = false;
        repaint();
        currentPhase.setPhase(GraphicMove.NULL);

    }

    private void showAvailableMates() {
        assert turnShepherd != null;
        assert turnShepherd.isPositioned();

        RoadSquare myRoad = turnShepherd.getCurrentPosition();

        SheepTile[] array = myFrame.getCurrentGame().getGameBoard().getMap()
                .getIncidentTiles(myRoad);

        TerrainType leftType = null;
        TerrainType rightType = null;

        matePanel = new JPanel();

        leftTile = array[0];
        rightTile = array[1];

        leftType = leftTile.getTerrainType();
        rightType = rightTile.getTerrainType();

        matePanel.setLayout(null);
        matePanel.setSize((int) (X_SIZE / (527.0 / 240)),
                (int) (Y_SIZE / (700.0 / 85)));
        matePanel.setMaximumSize(new Dimension((int) (X_SIZE / (527.0 / 330)),
                (int) (Y_SIZE / (700.0 / 120))));
        matePanel.setMinimumSize(new Dimension((int) (X_SIZE / (527.0 / 330)),
                (int) (Y_SIZE / (700.0 / 120))));

        matePanel.setBackground(new Color(255, 255, 0xA0));

        if (leftTile.getNumberOfSheep(SheepType.FEMALE_SHEEP) >= 1
                && leftTile.getNumberOfSheep(SheepType.RAM) >= 1) {
            Card tmpLeft = new Card(leftType);
            cardSize = new Dimension(tmpLeft.getWidth(), tmpLeft.getHeight());
            matePanel.add(tmpLeft);
            cardLeftX = (int) (matePanel.getWidth() / 7.0);
            cardLeftY = (int) ((matePanel.getHeight() - tmpLeft.getHeight()) * 0.5);
            tmpLeft.setLocation(cardLeftX, cardLeftY);
        } else {
            leftTile = null;
        }

        if (rightTile.getNumberOfSheep(SheepType.FEMALE_SHEEP) >= 1
                && rightTile.getNumberOfSheep(SheepType.RAM) >= 1) {
            Card tmpRight = new Card(rightType);
            cardSize = new Dimension(tmpRight.getWidth(), tmpRight.getHeight());
            matePanel.add(tmpRight);
            cardRightX = (int) (matePanel.getWidth() / 7.0 * 4);
            cardRightY = (int) ((matePanel.getHeight() - tmpRight.getHeight()) * 0.5);
            tmpRight.setLocation(cardRightX, cardRightY);
        } else {
            rightTile = null;
        }

        if (rightTile == null && leftTile == null) {
            currentPhase.setPhase(GraphicMove.NULL);
            myFrame.moveNotAllowed(new RuleViolationException(
                    "No mating is possible here"));
            return;
        }

        add(matePanel, 0);
        matePanel.setLocation((int) (X_SIZE / (527.0 / 144)),
                (int) (Y_SIZE * 0.3));
        matePanel.setVisible(true);

        currentPhase.setPhase(GraphicMove.MATING_SHEEP);
    }

    private void matingSheep(int xClicked, int yClicked) {
        int yPanel = (int) matePanel.getLocation().getY();
        int xPanel = (int) matePanel.getLocation().getX();

        if (!(yPanel + cardLeftY < yClicked && yClicked < yPanel + cardLeftY
                + cardSize.getHeight())) {
            remove(matePanel);
            matePanel = null;
            repaint();
            currentPhase.setPhase(GraphicMove.NULL);
            return;
        }

        try {
            if (xClicked > xPanel + cardLeftX
                    && xClicked < xPanel + cardLeftX + cardSize.getWidth()
                    && leftTile != null) {
                myFrame.getCurrentGame().mateSheep(turnShepherd, leftTile);
            }

            if (xClicked > xPanel + cardRightX
                    && xClicked < xPanel + cardRightX + cardSize.getWidth()
                    && rightTile != null) {
                myFrame.getCurrentGame().mateSheep(turnShepherd, rightTile);
            }
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
        }

        remove(matePanel);
        matePanel = null;
        repaint();
        currentPhase.setPhase(GraphicMove.NULL);
    }

    private void chooseSheepToKill(int xClicked, int yClicked) {
        try {
            assert turnShepherd != null;

            SheepTileIdentifier tileId = colorMap.takeTileElement(new Point(
                    xClicked, yClicked));
            SheepTile tile = myFrame.getCurrentGame()
                    .getSheepTileFromId(tileId);

            killingAnimal = getAnimalToKill(xClicked, yClicked);
            if (killingAnimal == null) {
                throw new InvalidIdentifierException(null);
            }

            Sheep sheep = tile.getAnySheepOfType(killingAnimal.getType());
            assert sheep != null;

            myFrame.getCurrentGame().killSheep(turnShepherd, tile, sheep);
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.FINE, "Wrong click exception", e);
            myFrame.moveNotAllowed(new RuleViolationException(
                    "You must click on a sheep"));
        }
    }

    private void chooseShepherd(int x, int y) {
        RoadSquare roadClicked = null;
        try {
            roadClicked = myFrame.getCurrentGame().getRoadSquareFromId(
                    colorMap.takeRoadElement(new Point(x, y)));
        } catch (InvalidIdentifierException e1) {
            Logger.getGlobal().log(Level.INFO,
                    "Invalid identifier choosing shepherd for turn", e1);
            return;
        }
        Shepherd shepherd = roadClicked.getCurrentShepherd();

        if (myFrame.getCurrentGame().getSelfPlayer().ownsShepherd(shepherd)) {
            setTurnShepherd(shepherd);
            currentPhase.setPhase(GraphicMove.NULL);
        } else {
            return;
        }
    }

    private int getTurnNumber(Player mySelf) {
        Player[] array = myFrame.getCurrentGame().getAllPlayers();

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(mySelf)) {
                return i;
            }
        }

        return 0;
    }

    /**
     * Update the position of the given shepherd to the given road square.
     * 
     * If the shepherd icon was already visible on screen, and was not actively
     * dragged by the user, it is animated.
     * 
     * @param who
     *            the player owning the shepherd
     * @param which
     *            the shepherd being moved
     * @param to
     *            the destination of the move
     */
    public void drawShepherd(Player who, Shepherd which, RoadSquare to) {
        int myTurnNumber = getTurnNumber(who);
        Point roadCenterPosition = colorMap.getRoadCenter(to.getId());
        Point shepherdPosition = new Point((int) roadCenterPosition.getX(),
                (int) (roadCenterPosition.getY() + 3));

        PlayerPawn pawn = null;

        boolean isMyShepherd = myFrame.getCurrentGame().getSelfPlayer()
                .ownsShepherd(which);
        boolean isMove;

        if (shepherdMap.get(which) == null) {
            pawn = new PlayerPawn(myTurnNumber);
            shepherdMap.put(which, pawn);
            isMove = false;
        } else {
            pawn = shepherdMap.get(which);
            Fence fence = arrayFence.get(placedFence);
            placedFence++;
            add(fence);
            if (isMyShepherd) {
                fence.setLocation(xInitialDrag, yInitialDrag);
            } else {
                fence.setLocation(pawn.getX(), pawn.getY());
            }
            isMove = true;
        }
        add(pawn);

        Point finalShepherdPosition = new Point(
                (int) (shepherdPosition.getX() - pawn.getWidth() / 2),
                (int) (shepherdPosition.getY() - pawn.getHeight() / 2));
        if (isMyShepherd || !isMove) {
            pawn.setLocation(finalShepherdPosition);
        } else {
            Animation a = new Animation(new PositionAnimator(pawn,
                    pawn.getLocation(), finalShepherdPosition),
                    AnimationAlpha.EASE_OUT_QUAD, 1000);
            myFrame.getUIManager().getMasterClock().addAnimation(a);
        }
    }

    /**
     * Update the money counters for all players.
     */
    public void updateMoney() {
        Player[] array = myFrame.getCurrentGame().getAllPlayers();

        for (Player element : array) {
            playerIcon.get(element).updateMoney(element.getRemainingCoins());
        }
    }

    private Shepherd getDraggableSheperd() {
        Shepherd[] arraySheperd = myFrame.getCurrentGame().getSelfPlayer()
                .getAllShepherds();

        PlayerPawn pawn;
        for (Shepherd element : arraySheperd) {
            pawn = shepherdMap.get(element);
            if (pawn.isDraggable()) {
                return element;
            }
        }
        return null;
    }

    private void placeShepherd(int x, int y) {
        RoadSquareIdentifier roadToGo;
        Game game = myFrame.getCurrentGame();
        Shepherd[] array = game.getSelfPlayer().getAllShepherds();
        Shepherd shepherdToPlace = null;

        for (Shepherd s : array) {
            if (!s.isPositioned()) {
                shepherdToPlace = s;
                break;
            }
        }

        if (shepherdToPlace == null) {
            myFrame.moveNotAllowed(new RuleViolationException(
                    "You already placed all your shepherds"));
            return;
        }

        roadToGo = colorMap.takeRoadElement(new Point(x, y));

        try {
            game.initiallyPlaceShepherd(shepherdToPlace,
                    game.getRoadSquareFromId(roadToGo));
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
            return;
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(new RuleViolationException(
                    "You must click on a road"));
            return;
        }

    }

    private void startShepherdDrag(int x, int y) {
        RoadSquareIdentifier rsId = colorMap.takeRoadElement(new Point(x, y));
        try {
            RoadSquare square = myFrame.getCurrentGame().getRoadSquareFromId(
                    rsId);
            Shepherd shepherd = square.getCurrentShepherd();

            assert turnShepherd != null;
            if (shepherd != turnShepherd) {
                throw new InvalidIdentifierException(null);
            }

            PlayerPawn pawn = shepherdMap.get(shepherd);
            assert pawn != null;

            draggingPawn = pawn;
            xInitialDrag = draggingPawn.getX();
            yInitialDrag = draggingPawn.getY();
            pawn.setDraggable(true);
            correctlyPressed = true;
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.FINE,
                    "Invalid identifier in choosing shepherd to drag", e);
            myFrame.moveNotAllowed(new RuleViolationException(
                    "You must click on your shepherd"));
            return;
        }
    }

    private void endShepherdDrag(int x, int y) {
        try {
            RoadSquare roadToGo = myFrame.getCurrentGame().getRoadSquareFromId(
                    colorMap.takeRoadElement(new Point(x, y)));

            myFrame.getCurrentGame().moveShepherd(getDraggableSheperd(),
                    roadToGo);

            PlayerPawn pawn = shepherdMap.get(getDraggableSheperd());

            Point centerRoadToGoLocation = colorMap.getRoadCenter(roadToGo
                    .getId());

            pawn.setLocation(
                    (int) (centerRoadToGoLocation.getX() - pawn.getWidth() / 2),
                    (int) (centerRoadToGoLocation.getY() - pawn.getHeight() / 2));
            updateMoney();

            this.repaint();

            correctlyPressed = false;
            currentPhase.setPhase(GraphicMove.NULL);
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            correctlyPressed = false;
            draggingPawn.setLocation(xInitialDrag, yInitialDrag);
            myFrame.moveNotAllowed(e);
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.FINE,
                    "Invalid identifier on shepherd drop", e);
            currentPhase.setPhase(GraphicMove.NULL);
            correctlyPressed = false;
            draggingPawn.setLocation(xInitialDrag, yInitialDrag);
            myFrame.moveNotAllowed(new RuleViolationException(
                    "You must drag to a road"));
        }
    }

    private void startSheepDrag(int x, int y) {
        draggingAnimal = getAnimalToDrag(x, y);
        if (draggingAnimal == null) {
            return;
        }

        xInitialDrag = draggingAnimal.getX();
        yInitialDrag = draggingAnimal.getY();
        xInitialDragClick = x;
        yInitialDragClick = y;
        correctlyPressed = true;
    }

    private void endSheepDrag(int x, int y) {
        try {
            SheepTile from = myFrame.getCurrentGame().getSheepTileFromId(
                    colorMap.takeTileElement(new Point(xInitialDragClick,
                            yInitialDragClick)));

            SheepTile to = myFrame.getCurrentGame().getSheepTileFromId(
                    colorMap.takeTileElement(new Point(x, y)));

            Sheep sheep = from.getAnySheepOfType(draggingAnimal.getType());

            if (sheep == null) {
                throw new RuleViolationException(
                        "There is no sheep of this type");
            }

            assert turnShepherd != null;
            myFrame.getCurrentGame().moveSheep(turnShepherd, from, to, sheep);
            draggingAnimal.setLocation(xInitialDrag, yInitialDrag);

            correctlyPressed = false;
            draggingAnimal = null;
            repaint();
        } catch (RuleViolationException e) {
            Logger.getGlobal().log(Level.FINE, "Rule violation exception", e);
            myFrame.moveNotAllowed(e);
            correctlyPressed = false;
            draggingAnimal.setLocation(xInitialDrag, yInitialDrag);
            return;

        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.FINE, "Invalid identifier exception",
                    e);
            myFrame.moveNotAllowed(new RuleViolationException(
                    "You must drag to a region"));
            correctlyPressed = false;
            draggingAnimal.setLocation(xInitialDrag, yInitialDrag);
            repaint();
            return;
        }
    }

    /**
     * Initialize the player icons at the top of the component from the current
     * data in the model.
     */
    public void placePlayerIcon() {
        Player[] array = myFrame.getCurrentGame().getAllPlayers();

        for (int i = 0; i < array.length; i++) {
            PlayerIcon p = new PlayerIcon(i, array[i].getName());
            playerIcon.put(array[i], p);
            this.add(p, 0);
            p.setLocation((int) (p.getWidth() * i + p.getWidth() / 5.0 * i), 0);
            p.updateMoney(array[i].getRemainingCoins());
            repaint();
        }
    }

    /**
     * Update the current turn indication on the player icons.
     */
    public void updateCurrentTurn() {
        Player currentPlayer = myFrame.getCurrentGame().getCurrentPlayer();
        for (Map.Entry<Player, PlayerIcon> e : playerIcon.entrySet()) {
            e.getValue().setCurrent(e.getKey() == currentPlayer);
        }
    }

    /**
     * Retrieve the current graphic state object, containing the current UI
     * state machine state.
     * 
     * @return the graphic state (phase) object
     */
    public GraphicPhase getCurrentPhase() {
        return currentPhase;
    }

    /**
     * Reset the draggability of all shepherds to false.
     * 
     * This method should be called at the beginning of each turn, before a
     * shepherd is chosen.
     */
    public void resetShepherdDrag() {
        Shepherd[] arraySheperd = myFrame.getCurrentGame().getSelfPlayer()
                .getAllShepherds();

        PlayerPawn pawn;

        for (Shepherd element : arraySheperd) {
            pawn = shepherdMap.get(element);
            pawn.setDraggable(false);
        }
    }

    /**
     * Draw (animate) the motion of the wolf from the given tile to the given
     * tile, possibly eating the given sheep.
     * 
     * The eaten sheep is only used as a hint as whether the number of animals
     * needs updating or not
     * 
     * @param from
     *            the origin of the move
     * @param to
     *            the destination of the move
     * @param eatenSheep
     *            the eaten sheep, or null if none
     */
    public void drawMoveWolf(SheepTile from, SheepTile to, Sheep eatenSheep) {
        TileIcon from3 = animalTilePosition.get(from.getId());
        final TileIcon to3 = animalTilePosition.get(to.getId());

        final AnimalIcon animated = new AnimalIcon("wolf.png");
        add(animated);

        Animation animation = new Animation(new PositionAnimator(animated,
                from3.getWolfPosition(), to3.getWolfPosition()),
                AnimationAlpha.EASE_OUT_QUAD, 1000);
        animation.addAnimationListener(new AnimationListener() {
            /**
             * {@inheritDoc}
             */
            public void onComplete() {
                remove(animated);
                to3.setWolf(true);
            }
        });
        myFrame.getUIManager().getMasterClock().addAnimation(animation);
        from3.setWolf(false);
        to3.setWolf(false);

        if (eatenSheep != null) {
            updateNumberAnimals(to.getId());
        }
        repaint();
    }

    private void fillHashMapAnimalPosition() {
        for (int i = 0; i < 19; i++) {
            SheepTileIdentifier id = new SheepTileIdentifier(i);
            animalTilePosition.put(id, new TileIcon(colorMap.getTileCenter(id),
                    this));
        }
    }

    private void updateNumberAnimals(SheepTileIdentifier on) {
        try {
            SheepTile tile = myFrame.getCurrentGame().getGameBoard().getMap()
                    .getSheepTileFromId(on);
            TileIcon icon = animalTilePosition.get(on);

            SheepType[] allTypes = SheepType.values();
            for (SheepType type : allTypes) {
                icon.setNumberSheep(type, tile.getNumberOfSheep(type));
            }
        } catch (InvalidIdentifierException e) {
            Logger.getGlobal().log(Level.SEVERE, "Mistake più del solito", e);
        }
    }

    /**
     * Draw (animate) the motion of a sheep of the given type between the given
     * tiles, as operated by the given player.
     * 
     * The mover can be null, if the motion happened sponteanously during the
     * game (only matters for the black sheep).
     * 
     * No animation will be performed if the sheep was already moved by a
     * drag&drop operation.
     * 
     * @param mover
     *            the player that moved the sheep
     * @param from
     *            the origin of the move
     * @param to
     *            the destination of the move
     * @param sheepType
     *            the type of the moved sheep
     */
    public void drawMoveSheep(Player mover, SheepTileIdentifier from,
            final SheepTileIdentifier to, SheepType sheepType) {
        updateNumberAnimals(from);

        if (mover == myFrame.getCurrentGame().getSelfPlayer()) {
            updateNumberAnimals(to);
            return;
        }

        final SheepIcon animated = new SheepIcon(sheepType);
        add(animated);
        TileIcon fromIcon = animalTilePosition.get(from);
        TileIcon toIcon = animalTilePosition.get(to);

        Animation animation = new Animation(new PositionAnimator(animated,
                fromIcon.getSheepPosition(sheepType),
                toIcon.getSheepPosition(sheepType)),
                AnimationAlpha.EASE_OUT_QUAD, 1000);
        animation.addAnimationListener(new AnimationListener() {
            /**
             * {@inheritDoc}
             */
            public void onComplete() {
                remove(animated);
                updateNumberAnimals(to);
            }
        });
        myFrame.getUIManager().getMasterClock().addAnimation(animation);
    }

    /**
     * Initialize the animal counters for the given tile.
     * 
     * This method will update the icons for all sheep and for the wolf
     * 
     * @param sheepTile
     *            the tile to initialize
     */
    public void drawAnimals(SheepTile sheepTile) {
        updateNumberAnimals(sheepTile.getId());
        animalTilePosition.get(sheepTile.getId()).setWolf(
                sheepTile.getHasWolf());
    }

    /**
     * Draw (report) that the given player bought a card, either during a normal
     * turn or at the market.
     * 
     * @param player
     *            the player buying the card
     * @param card
     *            the card that was bought
     */
    public void drawCardBought(Player player, TerrainCard card) {
        if (player.equals(myFrame.getCurrentGame().getSelfPlayer())) {
            updateMoney();
            return;
        }

        updateMoney();
        repaint();
        PlayerIcon pawn = playerIcon.get(player);
        PlayerIcon pawnClone = new PlayerIcon(pawn.getIndex(), player.getName());
        Card cardIcon = new Card(card.getType());
        cardIcon.setVisible(true);

        myFrame.showBoughtOrSoldCardMessage(pawnClone, cardIcon,
                "bought this card");
    }

    /**
     * Draw (report) that the given player succeeded in a mating operation on
     * the given tile.
     * 
     * @param player
     *            the player doing the mating
     * @param tile
     *            where the mating is happening
     * @param newLamb
     *            the new born lamb (must not be null)
     */
    public void drawMateCompleted(Player player, SheepTile tile, Sheep newLamb) {
        PlayerIcon pawn = playerIcon.get(player);
        PlayerIcon pawnClone = new PlayerIcon(pawn.getIndex(), player.getName());
        SheepIcon lamb = new SheepIcon(SheepType.LAMB);
        lamb.setVisible(true);

        TileIcon tileIcon = animalTilePosition.get(tile.getId());
        tileIcon.setNumberSheep(SheepType.LAMB,
                tile.getNumberOfSheep(SheepType.LAMB));

        myFrame.showMateCompletedMessage(pawnClone, lamb);
    }

    /**
     * Draw (report) that the given player successfully killed the given sheep
     * on the given tile.
     * 
     * @param who
     *            the player killing the sheep
     * @param tile
     *            the tile where the operation is happening
     * @param sheep
     *            the sheep being killed
     * @param paidPlayers
     *            the map with the players being paid for and the amount
     */
    public void drawSheepKilled(Player who, SheepTile tile, Sheep sheep,
            Map<Player, Integer> paidPlayers) {
        PlayerIcon pawn1 = playerIcon.get(who);
        PlayerIcon pawn = new PlayerIcon(pawn1.getIndex(), who.getName());
        SheepTileIdentifier tileId = tile.getId();
        updateNumberAnimals(tileId);
        SheepIcon sheepIcon = new SheepIcon(sheep.getType());

        myFrame.showKillCompletedMessage(pawn, sheepIcon, paidPlayers);
    }

    /**
     * Draw (report) that as part of a killing operation, the previously drawn
     * player paid the given players the corresponding amount in the map.
     * 
     * @param paidPlayers
     *            same as in sheepKilled
     */
    public void drawBuySilence(Map<Player, Integer> paidPlayers) {
        Player[] gamePlayer = myFrame.getCurrentGame().getAllPlayers();
        Map<PlayerIcon, Integer> money = new HashMap<PlayerIcon, Integer>();

        for (int i = 0; i < gamePlayer.length; i++) {
            PlayerIcon pawn1 = playerIcon.get(gamePlayer[i]);
            pawn1.updateMoney(gamePlayer[i].getRemainingCoins());

            if (paidPlayers.containsKey(gamePlayer[i])) {
                PlayerIcon pawn = new PlayerIcon(i, gamePlayer[i].getName());
                money.put(pawn, paidPlayers.get(gamePlayer[i]));
            }
        }

        myFrame.showMoneySpentForSilenceMessage(money);
    }

    /**
     * Draw (report) that the given player is selling the given card for the
     * given amount.
     * 
     * Despite the name, the card is not sold yet, it is only available for
     * sale.
     * 
     * @param who
     *            the player selling the card
     * @param card
     *            the card
     * @param cost
     *            the amount of money asked for
     */
    public void drawSoldMarketCard(Player who, TerrainCard card, int cost) {
        if (who.equals(myFrame.getCurrentGame().getSelfPlayer())) {
            return;
        }

        PlayerIcon pawn = playerIcon.get(who);
        PlayerIcon pawnClone = new PlayerIcon(pawn.getIndex(), who.getName());
        pawnClone.updateMoney(cost);

        Card cardIcon = new Card(card.getType());
        cardIcon.setVisible(true);

        myFrame.showBoughtOrSoldCardMessage(pawnClone, cardIcon, "is selling");
    }

    /**
     * Choose the given shepherd for the rest of the turn.
     * 
     * This method will make sure to mark the shepherd icon appropriately.
     * 
     * This method does not check that the given shepherd belongs to the self
     * player.
     * 
     * @param shepherd
     *            the shepherd for the turn.
     */
    public void setTurnShepherd(Shepherd shepherd) {
        if (turnShepherd != null) {
            shepherdMap.get(turnShepherd).setCurrent(false);
        }
        turnShepherd = shepherd;
        if (shepherd != null) {
            shepherdMap.get(turnShepherd).setCurrent(true);
        }
    }

    /**
     * Draw (report) the completion of the game, with the given final scores.
     * 
     * @param scores
     *            the scores, as a mapping between players and points
     */
    public void drawGameOver(Map<PlayerIdentifier, Integer> scores) {
        Player[] gamePlayer = myFrame.getCurrentGame().getAllPlayers();
        Map<PlayerIcon, Integer> scoreIcons = new HashMap<PlayerIcon, Integer>();

        for (int i = 0; i < gamePlayer.length; i++) {
            PlayerIcon pawn = new PlayerIcon(i, gamePlayer[i].getName());
            scoreIcons.put(pawn, scores.get(gamePlayer[i].getId()));
        }

        myFrame.showGameOverMessage(scoreIcons);
    }

    /**
     * Update the number of sheep on the given lamb as a side of effect of lambs
     * growing on the given tile.
     * 
     * @param tile
     *            the tile where lambs are growing
     */
    public void lambsGrew(SheepTile tile) {
        updateNumberAnimals(tile.getId());
    }

    /**
     * Sync the position of all shepherd icons.
     * 
     * Under normal conditions, this method will be called only if the server
     * forcibly ends a player turn due to timeout.
     */
    public void syncShepherdPositions() {
        int i = 0;
        for (Player player : myFrame.getCurrentGame().getAllPlayers()) {
            for (Shepherd shepherd : player.getAllShepherds()) {
                if (shepherd.isPositioned()
                        && !shepherdMap.containsKey(shepherd)) {
                    PlayerPawn pawn = new PlayerPawn(i);

                    RoadSquare currentPosition = shepherd.getCurrentPosition();
                    Point position = colorMap.getRoadCenter(currentPosition
                            .getId());
                    Point shepherdPosition = new Point(
                            (int) (position.getX() - pawn.getWidth() * .5),
                            (int) (position.getY() - pawn.getHeight() * .5));
                    pawn.setLocation(shepherdPosition);

                    shepherdMap.put(shepherd, pawn);
                    add(pawn);
                }
            }

            i++;
        }
    }

    /**
     * Synchronize the view with the model after a reconnection or server error.
     * 
     * This method will recreate any derived view state from the model, that
     * would normally be built as part of the normal game. This includes fences
     * and shepherd positions.
     * 
     */
    public void forceSynchronize() {
        GameBoard gameBoard = myFrame.getCurrentGame().getGameBoard();
        Collection<RoadSquare> allRoadSquare = gameBoard.getMap()
                .getAllRoadSquares();

        fillFenceArray();
        int startingFinalFences = myFrame.getCurrentGame().getRules()
                .getNumberOfFences();
        int placedFinalFences = 0;
        int placedNormalFences = 0;
        placedFence = 0;

        for (RoadSquare road : allRoadSquare) {
            if (road.getHasFinalFence()) {
                Fence fence = arrayFence.get(startingFinalFences
                        + placedFinalFences);
                add(fence);
                Point position = colorMap.getRoadCenter(road.getId());
                Point fencePosition = new Point(
                        (int) (position.getX() - fence.getWidth() * .5),
                        (int) (position.getY() - fence.getHeight() * .5));
                fence.setLocation(fencePosition);
                placedFinalFences++;
                placedFence++;
            } else if (road.getHasFence()) {
                Fence fence = arrayFence.get(placedNormalFences);
                add(fence);
                Point position = colorMap.getRoadCenter(road.getId());
                Point fencePosition = new Point(
                        (int) (position.getX() - fence.getWidth() * .5),
                        (int) (position.getY() - fence.getHeight() * .5));
                fence.setLocation(fencePosition);
                placedNormalFences++;
                placedFence++;
            }
        }

        assert placedFence == placedNormalFences + placedFinalFences;

        syncShepherdPositions();
    }

}
