package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

import java.awt.Component;
import java.awt.Point;

/**
 * An animator that moves a component across its parent.
 * 
 * @author Giovanni Campagna
 * 
 */
public class PositionAnimator extends Animator {
    private final Component component;
    private final Point initialPosition;
    private final Point finalPosition;

    /**
     * Construct a new position animator, moving the given component between the
     * given points.
     * 
     * @param comp
     *            the component to move
     * @param from
     *            the start position, in parent's coordinates
     * @param to
     *            the end position, in parent's coordinates
     */
    public PositionAnimator(Component comp, Point from, Point to) {
        component = comp;
        initialPosition = from;
        finalPosition = to;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void update(double progress) {
        assert 0.0 <= progress && progress <= 1.0;
        Point interpolated = new Point();
        interpolated.setLocation(Util.interpolate(initialPosition.getX(),
                finalPosition.getX(), progress), Util.interpolate(
                initialPosition.getY(), finalPosition.getY(), progress));

        component.setLocation(interpolated);
    }
}
