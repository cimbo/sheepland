package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

import java.awt.Component;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An utility class for synchrously loading an image from resources.
 * 
 * @author Alessandro Cimbelli
 */
public class ImageLoader {
    private ImageLoader() {
        // utility class
    }

    /**
     * Load the image given the path, and wait until it is fully decoded and in
     * memory.
     * 
     * @param component
     *            the component on behalf of which the image is loaded
     * @param path
     *            the resource path to load the image from
     * @return
     */
    public static Image loadImageSync(Component component, String path) {
        Image img = Toolkit.getDefaultToolkit().createImage(
                ImageLoader.class.getClassLoader().getResource(path));

        MediaTracker track = new MediaTracker(component);
        track.addImage(img, 0);
        while (!track.checkAll()) {
            try {
                track.waitForID(0);
            } catch (InterruptedException e) {
                Logger.getGlobal().log(Level.FINEST,
                        "Interrupted exception while loading image", e);
            }
        }

        return img;
    }
}
