package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

/**
 * The identifier of an operation sent to the server in the command header.
 * 
 * @author Alessandro Cimbelli
 */
public enum ServerOperation {

    /**
     * There is a logic connection between the order of this Enum and
     * ServerReciver methods
     * 
     * @author Alessandro Cimbelli
     */

    ABANDON_GAME, FORCE_SYNCHRONIZE_ME, EXECUTE_MOVE, LOGIN_TO_SERVER, PONG;
}
