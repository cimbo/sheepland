package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Game Board is the central class of the model.
 * 
 * It has two purposes:
 * 
 * 1) It holds the map and the database of objects, allowing mapping from and to
 * identifiers. For this, it provides also the translation methods in the ID to
 * object direction.
 * 
 * 2) It holds all objects, such as available terrain cards and dead sheep, that
 * are not specifically owned by a player or by the map
 * 
 * The game board is fully serializable, and designed such that it is possible
 * to store it on the disk or send it across a network channel to freeze the
 * state of the game at any given time
 * 
 * @author Giovanni Campagna
 */
public class GameBoard implements Serializable {
    private static final long serialVersionUID = 1L;

    private final java.util.Map<GameObjectIdentifier, Player> players;
    private final java.util.Map<GameObjectIdentifier, Sheep> allSheep;
    private TerrainCard[] allCards;
    private final Set<TerrainCard> availableCards;
    private Sheep blackSheep;
    private Player[] playingOrder;
    private boolean initialized;

    private final GameMap map;
    private int remainingFences;
    private int remainingFinalFences;

    /**
     * Construct a new, empty game board.
     * 
     * The map will be constructed but empty, and so will be the object
     * identifiers databases, until the board is initialized for a specific rule
     * set.
     */
    public GameBoard() {
        map = new GameMap();
        players = new HashMap<GameObjectIdentifier, Player>();
        allSheep = new HashMap<GameObjectIdentifier, Sheep>();
        availableCards = new HashSet<TerrainCard>();
        initialized = false;
    }

    /**
     * Retrieve the map associated with this game board.
     */
    public GameMap getMap() {
        return map;
    }

    /**
     * Deep clone the board for a given player.
     * 
     * This produces a copy of this object, which is filtered of all the
     * information the requestor player should not be aware of (namely, the
     * initial cards of the other players).
     * 
     * The resulting copy is a deep copy. To relate the new objects to the old
     * ones, identifier equality must be used.
     * 
     * As an implementation detail, this method uses a temporary serialization
     * of the board to a byte array stream.
     * 
     * @param requestor
     *            the player who is requesting the clone
     * @return a cloned and filtered board.
     */
    public GameBoard cloneForPlayer(Player requestor) {
        // We want a "deep" clone here, and the simplest way is to
        // run the serialization algorithm
        GameBoard clone;

        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutput = new ObjectOutputStream(byteOutput);
            try {
                objectOutput.writeObject(this);
                objectOutput.flush();
                byteOutput.flush();

                byte[] output = byteOutput.toByteArray();
                ByteArrayInputStream byteInput = new ByteArrayInputStream(
                        output);
                ObjectInputStream objectInput = new ObjectInputStream(byteInput);
                try {
                    clone = (GameBoard) objectInput.readObject();
                } finally {
                    objectInput.close();
                }
            } finally {
                objectOutput.close();
            }
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "Failed to clone board", e);
            return null;
        }

        for (Player cloned : clone.playingOrder) {
            if (cloned.getId().equals(requestor.getId())) {
                continue;
            }

            cloned.clearInitialCard();
        }

        return clone;
    }

    /**
     * Retrieves the sheep matching the passed id (which needs to be equal to
     * the id of some sheep, but need to be a SheepIdentifier)
     * 
     * For static type safety, you will probably want to use
     * {@link GameEngine.getSheepFromId} instead.
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             no such object could be retrieved
     */
    public Sheep getSheepFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        if (id == null) {
            throw new InvalidIdentifierException(id);
        }

        Sheep s = allSheep.get(id);

        if (s == null) {
            throw new InvalidIdentifierException(id);
        }

        return s;
    }

    /**
     * Add a sheep to the board, generating a new identifier.
     * 
     * At the end of the method, the sheep identifier will be available using
     * sheep.getId().
     * 
     * Because sheep identifiers are automatically generated, this method should
     * only be used on the server side. This is really a convenience for
     * addSheep(new SheepIdentifier(), sheep)
     * 
     * @param sheep
     *            the new sheep
     */
    public void addSheep(Sheep sheep) {
        SheepIdentifier id = new SheepIdentifier();
        sheep.setId(id);
        addSheep(id, sheep);
    }

    /**
     * Add a sheep to the board, with an explicitly given identifier.
     * 
     * The sheep and the identifier must match, and the sheep must have been
     * assigned the identifier already.
     * 
     * @param sheep
     *            the new sheep
     */
    public void addSheep(SheepIdentifier id, Sheep sheep) {
        assert sheep.getId().equals(id);

        if (sheep.getType() == SheepType.BLACK_SHEEP) {
            if (blackSheep != null) {
                throw new GameInvariantViolationException(
                        "More than one black sheep registered with the board");
            }

            blackSheep = sheep;
        }

        allSheep.put(id, sheep);
    }

    /**
     * Retrieves the black sheep of this board.
     * 
     * This is only valid after a black sheep was added through addSheep(Sheep)
     * or addSheep(SheepIdentifier, Sheep)
     * 
     * @return the black sheep
     */
    public Sheep getBlackSheep() {
        return blackSheep;
    }

    // For testing
    boolean terrainCardStorageOk() {
        for (TerrainCard allCard : allCards) {
            if (allCard == null) {
                return false;
            }
        }

        return true;
    }

    /**
     * Retrieves the terrain card matching the passed id (which needs to be
     * equal to the id of some card, but need not be a TerrainCardIdentifier)
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             no such object could be retrieved
     */
    public TerrainCard getTerrainCardFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        if (id == null) {
            throw new InvalidIdentifierException(id);
        }

        int content = id.getContent();

        if (content < 0 || content >= allCards.length) {
            throw new InvalidIdentifierException(id);
        }

        TerrainCard c = allCards[content];

        if (!c.getId().equals(id)) {
            throw new InvalidIdentifierException(id);
        }

        return c;
    }

    /**
     * Retrieves the initial terrain card of a given type.
     * 
     * This method is only allowed after the board was initialized.
     * 
     * @param type
     *            the terrain type (which must not be sheepsburg)
     * @return the initial card
     */
    public TerrainCard getInitialTerrainCard(TerrainType type) {
        if (!initialized) {
            throw new GameInvariantViolationException(
                    "Board was not initialized");
        }
        if (type == TerrainType.SHEEPSBURG) {
            throw new IllegalArgumentException(
                    "There is no sheepsburg initial card");
        }

        int cardCode = type.ordinal();
        return allCards[cardCode];
    }

    private TerrainCard createTerrainCard(RuleSet rules, TerrainType type,
            int subcode) {
        int cardCode = subcode * (rules.getBuyableCards() + 1) + type.ordinal();
        TerrainCard card = new TerrainCard(type);
        TerrainCardIdentifier cardId = new TerrainCardIdentifier(cardCode);
        card.setId(cardId);
        assert allCards[cardCode] == null;
        allCards[cardCode] = card;

        return card;
    }

    /**
     * Initialize ruleset dependent fields of GameBoard. You must call this
     * after all players have joined the game, and the right ruleset (for 2 or 6
     * players) can be decided. Failing to call this method will likely result
     * in GameInvariantViolationExceptions as the game progresses.
     * 
     * @param r
     *            the set of game rules
     */
    public void initializeForRules(RuleSet rules) {
        remainingFences = rules.getNumberOfFences();
        remainingFinalFences = rules.getNumberOfFinalFences();

        int initialCards = TerrainType.NUMBER_OF_TERRAINS;
        int buyableCards = TerrainType.NUMBER_OF_TERRAINS
                * rules.getBuyableCards();
        allCards = new TerrainCard[initialCards + buyableCards];

        for (TerrainType type : TerrainType.values()) {
            if (type == TerrainType.SHEEPSBURG) {
                continue;
            }

            // initial card
            createTerrainCard(rules, type, 0);

            // buyable cards
            int number = rules.getBuyableCards();
            for (int j = 0; j < number; j++) {
                TerrainCard card = createTerrainCard(rules, type, j + 1);
                // First card costs 0, second costs 1, etc.
                card.markForSale(j);
                availableCards.add(card);
            }
        }

        for (Player p : players.values()) {
            p.initializeForRules(rules);
        }

        initialized = true;
    }

    /**
     * Joins a new (already initialized) Player to the Game
     * 
     * @param p
     *            the player
     */
    public void addPlayer(Player p) {
        if (initialized) {
            throw new GameInvariantViolationException(
                    "Cannot add players after the game has started");
        }

        PlayerIdentifier id = new PlayerIdentifier();
        p.setId(id);
        players.put(id, p);
    }

    /**
     * Retrieves the player matching the passed id (which needs to be equal to
     * the id of some player, but need not be a PlayerIdentifier)
     * 
     * For static type safety, you will probably want to use
     * {@link GameEngine.getSheepFromId} instead.
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             no such object could be retrieved
     */
    public Player getPlayerFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        if (id == null) {
            throw new InvalidIdentifierException(id);
        }

        Player player = players.get(id);

        if (player == null) {
            throw new InvalidIdentifierException(id);
        }

        return player;
    }

    /**
     * Retrieve the number of players currently in the board.
     * 
     * This is guaranteed to be a constant after the game is started
     * 
     * @return the number of players
     */
    public int getNumberOfPlayers() {
        return players.size();
    }

    /**
     * Retrieve all players, as an array. This method is only valid after the
     * game board is initialized for the rules.
     * 
     * This method will always return a new array, so feel free to modify it or
     * sort it as you wish.
     * 
     * The order of the players in the returned array is unspecified, and might
     * change between successive invocation to this method.
     * 
     * @return
     */
    public Player[] getAllPlayers() {
        if (!initialized) {
            throw new GameInvariantViolationException(
                    "Cannot acquire player list before the game has started");
        }

        Player[] array = new Player[players.size()];
        return players.values().toArray(array);
    }

    /**
     * Retrieves the playing order, or null if no such playing order was set.
     * 
     * @return the playing order, or null
     */
    public Player[] getPlayingOrder() {
        return playingOrder;
    }

    private boolean checkPlayingOrder(Player[] order) {
        Player[] allPlayers = getAllPlayers();

        if (order.length != allPlayers.length) {
            return false;
        }

        Comparator<Player> comp = new Comparator<Player>() {
            /**
             * Compare two players according to their hash code (which is based
             * on the unique GC id)
             * 
             * @param p1
             *            the first player
             * @param p2
             *            the second player
             * @return -1, if p1's GC id is smaller, 1 if it is greater, 0 if
             *         they are equal
             */
            public int compare(Player p1, Player p2) {
                return p1.hashCode() - p2.hashCode();
            }
        };
        Arrays.sort(allPlayers, comp);
        Player[] orderClone = order.clone();
        Arrays.sort(orderClone, comp);

        for (int i = 0; i < orderClone.length; i++) {
            if (allPlayers[i] != orderClone[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Set the playing order on the board.
     * 
     * The playing order must be a permutation of the set of all players, but
     * this is only checked if assertions are enabled.
     * 
     * @param order
     *            the order of players
     */
    public void setPlayingOrder(Player[] order) {
        assert checkPlayingOrder(order);

        playingOrder = order.clone();
    }

    /**
     * Record that a given card, which was previously available for sale, was
     * now acquired by a player.
     * 
     * This method does not change the state of the card it self (which
     * therefore must be changed with card.markAcquired()), it just removes the
     * card from the set of available cards.
     * 
     * @param card
     *            the card
     */
    public void markCardAcquired(TerrainCard card) {
        if (!availableCards.contains(card)) {
            throw new GameInvariantViolationException(
                    "This card was not available");
        }

        availableCards.remove(card);
    }

    /**
     * Record that a given card is now available for sale.
     * 
     * This method does not change the state of the card, the price or the
     * owner, it just adds the card to the set of available cards. As such, it
     * is appropriate for the board cards (null owner) and market sold cards
     * (non-null owner)
     * 
     * @param card
     *            the card
     */
    public void markCardForSale(TerrainCard card) {
        if (availableCards.contains(card)) {
            throw new GameInvariantViolationException("Card is already on sale");
        }
        if (!card.isForSale()) {
            throw new GameInvariantViolationException("Card is not for sale");
        }

        availableCards.add(card);
    }

    /**
     * Return the number of not yet bought cards for the terrain of type @type.
     * 
     * @param type
     *            the interesting TerrainType
     * @return the number of cards
     */
    public int getNumberOfTerrainCards(TerrainType type) {
        int counter = 0;
        for (TerrainCard card : availableCards) {
            if (card.getType() == type) {
                counter++;
            }
        }

        return counter;
    }

    /**
     * Returns the cheapest terrain card of a given type and owner
     * 
     * This method considers all cards that are available for sale (according to
     * the board, that is, according to markCardAcquired() and
     * markCardForSale(), not according to the state on the card itself),
     * filters for the owner (which can be null to indicate a board card) and
     * returns the cheapest.
     * 
     * This method returns null if there is no card of the given type on sale
     * from the given player (or from the board, if the player is null).
     * 
     * This method is linear in the number of available cards.
     * 
     * @param type
     *            the terrain type
     * @param from
     *            the owner of the card to filter on
     * @return the cheapest terrain card that satisfies the filter, or null
     */
    public TerrainCard getCheapestTerrainCard(TerrainType type, Player from) {
        TerrainCard chosen = null;
        for (TerrainCard card : availableCards) {
            if (card.getType() != type || card.getOwner() != from) {
                continue;
            }

            if (chosen == null || chosen.getCost() > card.getCost()) {
                chosen = card;
            }
        }

        return chosen;
    }

    /**
     * Retrieves all terrain cards on sale from the given player.
     * 
     * The order of the returned cards is unspecified, and should not be assumed
     * to be consistent from one method call to another.
     * 
     * This method returns a new array, which can be modified freely.
     * 
     * @param from
     *            the owner of the card to filer on
     * @return the list of cards on sale
     */
    public TerrainCard[] getTerrainCardsOnSale(Player from) {
        List<TerrainCard> list = new ArrayList<TerrainCard>();

        for (TerrainCard card : availableCards) {
            if (card.getOwner() == from) {
                list.add(card);
            }
        }

        TerrainCard[] cardArray = new TerrainCard[list.size()];
        list.toArray(cardArray);
        return cardArray;
    }

    /**
     * Retrieves all terrain cards on sale at the market of the given type.
     * 
     * The order of the returned cards is unspecified, and should not be assumed
     * to be consistent from one method call to another.
     * 
     * This method returns a new array, which can be modified freely.
     * 
     * @param type
     *            the type of the card to filer on
     * @return the list of cards on sale
     */
    public TerrainCard[] getTerrainCardsOnSale(TerrainType type) {
        List<TerrainCard> list = new ArrayList<TerrainCard>();

        for (TerrainCard card : availableCards) {
            if (card.getType() == type && card.getOwner() != null) {
                list.add(card);
            }
        }

        TerrainCard[] cardArray = new TerrainCard[list.size()];
        list.toArray(cardArray);
        return cardArray;
    }

    private boolean availableCardsStorageOk() {
        for (TerrainCard card : availableCards) {
            if (!card.isForSale()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Release (give back to the owner) all cards that are for sale in the
     * board.
     */
    public void releaseAllTerrainCardsForSale() {
        assert availableCardsStorageOk();

        Iterator<TerrainCard> iterator = availableCards.iterator();

        while (iterator.hasNext()) {
            TerrainCard card = iterator.next();
            if (card.getOwner() != null) {
                card.markAcquired(card.getOwner());
                iterator.remove();
            }
        }
    }

    /**
     * Record that a fence was placed. This method will be called by the game
     * engine after placing a fence on a RoadSquare during the normal execution
     * of the game.
     */
    public void markFencePlaced() {
        if (remainingFences == 0) {
            throw new GameInvariantViolationException(
                    "There were no fence to place");
        }
        remainingFences--;
    }

    /**
     * Retrieve the number of non-final fences remaining in the board (ie, not
     * yet placed on a road).
     * 
     * @return the number of remaining non-final fences
     */
    public int getRemainingFences() {
        return remainingFences;
    }

    /**
     * Record that a final fence was placed. This is similar to
     * markFencePlaced(), but will be called only during the final part of the
     * game.
     */
    public void markFinalFencePlaced() {
        if (remainingFinalFences == 0) {
            throw new GameInvariantViolationException(
                    "There were no final fences to place");
        }
        remainingFinalFences--;
    }

    /**
     * Retrieve the number of final fences remaining in the board (ie, not yet
     * placed on a road).
     * 
     * @return the number of remaining final fences
     */
    public int getRemainingFinalFences() {
        return remainingFinalFences;
    }
}
