package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.io.Serializable;

/**
 * This class represents a shepherd, that is, a single emissary of the player on
 * the map.
 * 
 * The shepherd is the primary way through which the player interacts with
 * Sheepland, as most moves are bound to the shepherd position or conditions.
 * 
 * @author Giovanni Campagna
 */
public class Shepherd extends GameObject implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Player owner;
    private RoadSquare currentPosition;

    /**
     * Create a shepherd with a given owner.
     */
    public Shepherd(Player owner) {
        this.owner = owner;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShepherdIdentifier getId() {
        return (ShepherdIdentifier) super.getId();
    }

    /**
     * @return the owner of this shepherd
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * Returns the current position of the shepherd.
     * 
     * This method will throw if the shepherd is not positioned yet.
     * 
     * @return the current position
     */
    public RoadSquare getCurrentPosition() {
        if (currentPosition == null) {
            throw new GameInvariantViolationException(
                    "Player is not positioned yet");
        }
        return currentPosition;
    }

    /**
     * Checks if the shepherd was positioned or not.
     * 
     * @return true, if the shepherd has a non-null position, false otherwise
     */
    public boolean isPositioned() {
        return currentPosition != null;
    }

    /**
     * Set the new position of the shepherd.
     * 
     * This method will throw if called with a null argument.
     * 
     * @param rs
     *            the new position
     */
    public void setCurrentPosition(RoadSquare rs) {
        if (rs == null) {
            throw new IllegalArgumentException();
        }

        currentPosition = rs;
    }
}
