package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * The UI counterpart of a move sheep move.
 * 
 * @author Giovanni Campagna
 * 
 */
public class MoveSheepUIMove extends MoveSheepMove implements UIMove {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a move sheep move.
     * 
     * @param who
     *            the player doing it
     * @param which
     *            the shepherd doing it
     * @param from
     *            the origin of the move
     * @param to
     *            the destination of the move
     * @param sheep
     *            the affected sheep
     */
    public MoveSheepUIMove(PlayerIdentifier who, ShepherdIdentifier which,
            SheepTileIdentifier from, SheepTileIdentifier to,
            SheepIdentifier sheep) {
        super(who, which, from, to, sheep);
    }

    /**
     * {@inheritDoc}
     */
    public void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateSheepMoved(getWho(engine), getWhich(engine), getFrom(engine),
                getTo(engine), getSheep(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        // This is not a completable move, so we have nothing to do at the end
        //
        // (this comment courtesy of sonar)
    }

}
