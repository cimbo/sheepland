package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.AbstractUI;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.MasterClock;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.UIManager;

/**
 * The central class of the graphic UI controller, and the AbstractUI
 * implementation for the GUI.
 * 
 * @author Alessandro Cimbelli
 */
public class GraphicUIManager extends AbstractUI {
    private final ClientNetwork network;
    private final MasterClock clock;
    private Game currentGame;
    private final GameFrame frame;

    /**
     * Construct a new GraphicUIManager with the given network implementation
     * (which is used to construct a Game).
     * 
     * @param net
     *            the network implementation of choice.
     */
    public GraphicUIManager(ClientNetwork net) {
        try {
            UIManager.setLookAndFeel(UIManager
                    .getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            Logger.getGlobal().log(Level.WARNING,
                    "Failed to set cross-platform l&f", e);
        }

        network = net;
        clock = new MasterClock();
        frame = new GameFrame(this);
        currentGame = new Game(network, this);
    }

    /**
     * Retrieve the master clock associated with this UI.
     * 
     * @return the master clock
     */
    public MasterClock getMasterClock() {
        return clock;
    }

    /**
     * Retrieve the current game.
     * 
     * @return the current game, or null if game was already over.
     */
    public Game getCurrentGame() {
        return currentGame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void show() {
        frame.login();
        frame.setVisible(true);

        clock.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void quit() {
        if (currentGame != null) {
            currentGame.abandon(true);
        }
        clock.stop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loginSuccess() {
        frame.gameBoard();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void loginFailed() {
        frame.wrongLogin();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        frame.initialize(currentGame);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setWaiting(boolean waiting) {
        frame.setWaiting(waiting);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reportError(String error) {
        frame.showMessage(error);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateShepherdMoved(Player who, Shepherd which, RoadSquare to) {
        frame.drawShepherd(who, which, to);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBoughtTerrainCard(Player who, Shepherd which,
            TerrainCard card) {
        frame.drawBoughtCard(who, card);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBeginSheepMating(Player who, Shepherd which,
            SheepTile tile) {
        // no animation
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setYourTurn(boolean yourTurn) {
        frame.updateCurrentTurn();

        if (yourTurn) {
            frame.yourTurn(currentGame.getGamePhase());
        } else {
            frame.opponentTurn();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateSheepMoved(Player who, Shepherd which, SheepTile from,
            SheepTile to, Sheep sheep) {
        frame.drawMoveSheep(who, from, to, sheep);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateEndSheepMating(Player who, Shepherd which,
            SheepTile tile, Sheep newLamb) {
        if (newLamb != null) {
            frame.drawMateCompleted(who, which, tile, newLamb);
        } else {
            frame.showFailureMessage("Failed mating attempt");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBeginSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep) {
        // no animation
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateEndSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep, Map<Player, Integer> paidPlayers) {
        if (paidPlayers == null) {
            frame.showFailureMessage("Failed assassination attempt");
            return;
        }
        frame.drawSheepKilled(who, tile, sheep, paidPlayers);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void forceSynchronize() {
        frame.forceSynchronize(currentGame);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void syncShepherdPositions() {
        frame.syncShepherdPositions();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateSoldMarketCard(Player who, TerrainCard card, int cost) {
        frame.drawSoldMarketCard(who, card, cost);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBoughtMarketCard(Player who, TerrainCard card) {
        frame.drawBoughtCard(who, card);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameOver(Map<PlayerIdentifier, Integer> scores) {
        frame.drawGameOver(scores);
        currentGame = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playerLeft(Player who, boolean clean) {
        if (clean) {
            frame.showMessage(String.format("Player %s abandoned the game",
                    who.getName()));
        } else {
            frame.showMessage(String.format("Player %s disconnected",
                    who.getName()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playerJoined(Player who) {
        frame.showMessage(String.format("Player %s connected again",
                who.getName()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateBlackSheepMoved(SheepTile from, SheepTile to) {
        frame.drawMoveSheep(null, from, to, currentGame.getGameBoard()
                .getBlackSheep());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateWolfMoved(SheepTile from, SheepTile to, Sheep eatenSheep) {
        frame.drawMoveWolf(from, to, eatenSheep);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void animateLambsGrew(SheepTile tile) {
        frame.lambsGrew(tile);
    }
}
