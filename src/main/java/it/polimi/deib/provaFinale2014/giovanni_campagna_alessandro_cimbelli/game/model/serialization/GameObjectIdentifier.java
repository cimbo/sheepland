package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameObject;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A GameObjectIdentifier is a serializable object that can be used to identify
 * a particular game object across different game boards.
 * 
 * It is essentially a string holding a canonical class name (of the
 * corresponding GameObject) and a numeric ID. The meaning of the ID differs
 * between GameObjects, and in general is meaningful only if the subclass
 * specific constructors has it among the arguments. See the documentation of
 * subclasses for details.
 * 
 * GameObjectIdentifier should always compared by equality, never by identity.
 * 
 * @author Giovanni Campagna
 */
public class GameObjectIdentifier implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String className;
    private final int content;

    private static AtomicInteger consecutiveIds = new AtomicInteger();

    /**
     * Construct a game object identifier for the given class of game objects.
     * 
     * This method will use an automatically increasing number for the actual
     * numeric ID.
     * 
     * @param classObject
     *            the class object
     */
    protected GameObjectIdentifier(Class<? extends GameObject> classObject) {
        this(classObject, consecutiveIds.getAndIncrement());
    }

    /**
     * Construct a game object identifier for a given class and explicit ID.
     * 
     * @param classObject
     *            the class object
     * @param explicit
     *            the numeric ID.
     */
    protected GameObjectIdentifier(Class<? extends GameObject> classObject,
            int explicit) {
        className = classObject.getCanonicalName();
        content = explicit;
    }

    /**
     * Compare two GameObjectIdentifier for equality.
     * 
     * Two identifiers can be equal even if they are of different classes, as
     * long as the class of the game object they represent is equal. This should
     * not be relied upon though.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GameObjectIdentifier)) {
            return false;
        }

        GameObjectIdentifier id = (GameObjectIdentifier) obj;
        return className.equals(id.className) && content == id.content;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return className.hashCode() ^ content;
    }

    /**
     * Retrieves the numerical ID held by this identifier object.
     * 
     * How useful this method is depends on the particular class of game objects
     * being identified.
     * 
     * @return the numerical ID
     */
    public int getContent() {
        return content;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("GameObjectIdentifier for %s:%d", className,
                content);
    }
}
