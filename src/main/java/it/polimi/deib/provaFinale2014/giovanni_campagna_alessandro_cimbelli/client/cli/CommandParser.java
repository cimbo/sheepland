package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The central class for input handling in the CLI implementation.
 * 
 * @author Giovanni Campagna
 * 
 */
class CommandParser {
    private CLIUIManager ui;
    private final LineReader input;

    /**
     * Construct a new CommandParser, using the most appropriate input
     * implementation for the current environment.
     */
    public CommandParser() {
        this(createReader());
    }

    // For testing
    CommandParser(LineReader in) {
        input = in;
    }

    /**
     * Set the UI manager for this CommandParser
     * 
     * @param uiManager
     */
    public void setUI(CLIUIManager uiManager) {
        ui = uiManager;
    }

    private static LineReader createReader() {
        try {
            return new JLineLineReader();
        } catch (Exception e) {
            Logger.getGlobal().log(Level.FINEST, "Failed to initialize JLine",
                    e);
            return new ScannerLineReader();
        }
    }

    /**
     * Read a custom line from input, with the given prompt.
     * 
     * @param prompt
     *            the prompt to show
     * @return the read line
     */
    public String readCustom(String prompt) {
        return input.nextLine(prompt);
    }

    /**
     * Read and execute a command from the user.
     * 
     * @throws InvalidInputException
     *             if the command was not recognized
     * @throws RuleViolationException
     *             if the command violated the game rules
     * @throws InvalidIdentifierException
     *             if the command referenced invalid game objects
     */
    public void parseCommand() throws InvalidInputException,
            RuleViolationException, InvalidIdentifierException {
        Tokenizer tokenizer = new Tokenizer(input.nextLine("> "));
        BaseCommand command = tokenizer.readCommand();
        command.parseAndDo(tokenizer, ui);
    }
}
