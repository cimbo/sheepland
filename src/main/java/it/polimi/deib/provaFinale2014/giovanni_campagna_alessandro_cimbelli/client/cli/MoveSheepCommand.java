package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;

/**
 * The "move" command (called MoveSheepCommand because it is always followed by
 * a sheep type), which moves a sheep between two adjacent regions.
 * 
 * @author Giovanni Campagna
 * 
 */
class MoveSheepCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("move <SHEEP TYPE> from <REGION> to <REGION>: move a sheep");
        baseHelpSheepType(ui);
        baseHelpRegion(ui);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException,
            InvalidIdentifierException {
        if (ui.getChosenShepherd() == null) {
            ui.showMessage("A businessman like you would never touch a sheep directly!");
            if (GameOptions.get().getShowHints()) {
                ui.showMessage("[Use the be command to choose a shepherd before moving]");
            }
            return;
        }

        Game game = ui.getCurrentGame();
        SheepType type = tokenizer.readSheepType();
        tokenizer.expect("from");
        SheepTile from = game.getSheepTileFromId(tokenizer.readSheepTile());
        tokenizer.expect("to");
        SheepTile to = game.getSheepTileFromId(tokenizer.readSheepTile());

        Sheep sheep = from.getAnySheepOfType(type);
        if (sheep == null) {
            ui.showMessage("You don't see anything like that there.");
            return;
        }

        game.moveSheep(ui.getChosenShepherd(), from, to, sheep);
    }
}
