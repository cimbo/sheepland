package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi.RMIClientNetwork;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket.SocketClientNetwork;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An implementation of ClientNetwork that proxies through to real
 * ClientNetworks, using the first one that does not fail.
 * 
 * @author Giovanni Campagna
 * 
 */
public class AutomaticClientNetwork implements ClientNetwork {
    private final Collection<ClientNetwork> slaveNetworks;

    /**
     * Construct a new automatic client network.
     */
    public AutomaticClientNetwork() {
        slaveNetworks = new ArrayList<ClientNetwork>();

        // In the future we might want to add an official
        // server (like sheepland-online.net), or a mDNS
        // server discovery
        slaveNetworks.add(new SocketClientNetwork());
        slaveNetworks.add(new RMIClientNetwork());
    }

    /**
     * {@inheritDoc}
     */
    public ClientConnection connectToServer() throws IOException {
        for (ClientNetwork slave : slaveNetworks) {
            try {
                return slave.connectToServer();
            } catch (IOException e) {
                Logger.getGlobal().log(Level.FINE,
                        "Automatic connection to " + slave + " failed", e);
            }
        }

        throw new IOException("All connection methods failed");
    }
}
