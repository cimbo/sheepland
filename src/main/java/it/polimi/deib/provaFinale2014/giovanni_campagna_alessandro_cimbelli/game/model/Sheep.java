package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;

import java.io.Serializable;

/**
 * This class represents a sheep.
 * 
 * A sheep has a type, and a position, which is only implicitly stored as the
 * sheep tile references the sheep.
 * 
 * As a game object, a sheep has an identifier, which is created on the server
 * when the sheep is instantiated.
 * 
 * @author Giovanni Campagna
 */
public class Sheep extends GameObject implements Serializable {
    private static final long serialVersionUID = 1L;

    private final SheepType type;

    /**
     * Construct a sheep of the given type
     * 
     * @param type
     *            the sheep type
     */
    public Sheep(SheepType type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SheepIdentifier getId() {
        return (SheepIdentifier) super.getId();
    }

    /**
     * @return the type of the sheep
     */
    public SheepType getType() {
        return type;
    }
}
