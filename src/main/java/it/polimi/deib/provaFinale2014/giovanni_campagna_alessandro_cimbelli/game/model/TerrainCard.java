package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

import java.io.Serializable;

/**
 * This class represents a single terrain card, which can be exchanged during
 * the game and is used to compute the final score.
 * 
 * The set of all terrain cards across the game board is static, and all terrain
 * cards are created when the board is initialized. As such, the numerical IDs
 * of cards are meaningful but depend on the rule set in use.
 * 
 * @author Giovanni Campagna
 */
public class TerrainCard extends GameObject implements Serializable {
    private static final long serialVersionUID = 1L;

    private final TerrainType terrainType;
    private Player owner;
    private int cost;
    private boolean onSale;

    /**
     * Construct a terrain card of a given type.
     * 
     * The card is initially unowned and not for sale.
     * 
     * Attempting to create a terrain card of type sheepsburg will throw an
     * exception.
     * 
     * @param type
     *            the type of card to create.
     */
    public TerrainCard(TerrainType type) {
        if (type == TerrainType.SHEEPSBURG) {
            throw new IllegalArgumentException(
                    "Sheepsburg is not a valid terrain card");
        }

        terrainType = type;
        owner = null;
        cost = -1;
        onSale = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TerrainCardIdentifier getId() {
        return (TerrainCardIdentifier) super.getId();
    }

    /**
     * @return the type of this card.
     */
    public TerrainType getType() {
        return terrainType;
    }

    /**
     * Checks if this card is for sale, that is, markForSale() was last called
     * on this card and markAcquired() was not called.
     * 
     * @return true, if the card is for sale, false otherwise
     */
    public boolean isForSale() {
        return onSale;
    }

    /**
     * Retrieves the current owner of the card.
     * 
     * This method will return null if the card is unowned (for example it is an
     * available card that was not bought by a player, or an initial card that
     * was not allocated), or if the owner is not known (for example, an initial
     * card on the client side)
     * 
     * @return the card owner, or null
     */
    public Player getOwner() {
        return owner;
    }

    /**
     * Change the card owner to the given player.
     * 
     * No checks are made on the card, allowing to switch the owner without
     * completing the sale if needed (which it should never be).
     * 
     * @param who
     *            the new owner of the card
     */
    public void setOwner(Player who) {
        owner = who;
    }

    /**
     * Retrieves the cost of the card, as set in the last markForSale() card.
     * 
     * This method will throw if the card is not for sale.
     * 
     * @return the cost of the card.
     */
    public int getCost() {
        if (!onSale) {
            throw new GameInvariantViolationException(
                    "A card not for sale does not have a cost");
        }

        return cost;
    }

    /**
     * Marks the card for sale at a given cost.
     * 
     * Attempting to sell the card at a negative cost will throw, but it is
     * otherwise allowed to mark a card for sale more than once, and the cost is
     * not bound checked according to the rules of the game.
     * 
     * @param cost
     *            the new cost of the card
     */
    public void markForSale(int cost) {
        if (cost < 0) {
            throw new IllegalArgumentException();
        }

        this.cost = cost;
        onSale = true;
    }

    /**
     * Mark that the card was acquired by the given player.
     * 
     * This method will not throw if the player is null, or if the card was not
     * for sale previously.
     * 
     * @param by
     *            the new owner of the card (or null for no owner)
     */
    public void markAcquired(Player by) {
        owner = by;
        onSale = false;
        cost = -1;
    }
}
