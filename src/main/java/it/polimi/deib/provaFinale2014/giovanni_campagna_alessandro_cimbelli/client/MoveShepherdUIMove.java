package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.MoveShepherdMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * The UI counterpart of a move shepherd move.
 * 
 * @author Giovanni Campagna
 * 
 */
public class MoveShepherdUIMove extends MoveShepherdMove implements UIMove {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a move shepherd move.
     * 
     * @param who
     *            the player doing it
     * @param which
     *            the shepherd doing it
     * @param to
     *            the destination of the move
     */
    public MoveShepherdUIMove(PlayerIdentifier who, ShepherdIdentifier which,
            RoadSquareIdentifier to) {
        super(who, which, to);
    }

    /**
     * {@inheritDoc}
     */
    public void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateShepherdMoved(getWho(engine), getWhich(engine), getTo(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        // This is not a completable move, so we have nothing to do at the end
        //
        // (this comment courtesy of sonar)
    }
}
