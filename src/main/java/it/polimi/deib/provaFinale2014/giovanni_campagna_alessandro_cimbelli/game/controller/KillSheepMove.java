package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.util.HashMap;
import java.util.Map;

/**
 * This class represents a "kill sheep" move. See the base class {@link Move}
 * for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class KillSheepMove extends ShepherdMove implements CompletableMove {
    private static final long serialVersionUID = 1L;

    private final SheepTileIdentifier tileId;
    private final SheepIdentifier sheepId;
    private Map<PlayerIdentifier, Integer> paidPlayersId;
    private transient Map<Player, Integer> paidPlayers;

    /**
     * Construct a new "kill sheep" move
     * 
     * @param who
     *            the player executing the move
     * @param which
     *            the shepherd he is using
     * @param tile
     *            the tile where the move is happening
     * @param sheep
     *            the sheep to kill
     */
    public KillSheepMove(PlayerIdentifier who, ShepherdIdentifier which,
            SheepTileIdentifier tile, SheepIdentifier sheep) {
        super(who, which);
        tileId = tile;
        sheepId = sheep;
    }

    /**
     * Retrieve the actual tile object, by resolving the identifier through the
     * passed in engine.
     * 
     * @param engine
     * @return the tile as game object
     * @throws InvalidIdentifierException
     */
    protected SheepTile getTile(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getSheepTileFromId(tileId);
    }

    /**
     * Retrieve the actual sheep object, by resolving the identifier through the
     * passed in engine.
     * 
     * @param engine
     * @return the sheep as game object
     * @throws InvalidIdentifierException
     */
    protected Sheep getSheep(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getSheepFromId(sheepId);
    }

    /**
     * Retrieve the players that were paid for killing the sheep, and the amount
     * each.
     * 
     * @param engine
     * @return a hash map associating a player game object to a integer amount
     *         of coins
     * @throws InvalidIdentifierException
     */
    protected Map<Player, Integer> getPaidPlayers(GameEngine engine)
            throws InvalidIdentifierException {
        if (paidPlayersId == null) {
            return null;
        }

        if (paidPlayers != null) {
            return paidPlayers;
        }

        paidPlayers = new HashMap<Player, Integer>();
        for (java.util.Map.Entry<PlayerIdentifier, Integer> entry : paidPlayersId
                .entrySet()) {
            paidPlayers.put(engine.getPlayerFromId(entry.getKey()),
                    entry.getValue());
        }

        return paidPlayers;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.killSheep(getWho(engine), getWhich(engine), getTile(engine),
                getSheep(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void computeResult(ServerGameEngine engine)
            throws InvalidIdentifierException {
        paidPlayers = engine.realKillSheep(getWho(engine), getWhich(engine),
                getTile(engine), getSheep(engine));

        if (paidPlayers == null) {
            paidPlayersId = null;
        } else {
            paidPlayersId = new HashMap<PlayerIdentifier, Integer>();
            for (java.util.Map.Entry<Player, Integer> entry : paidPlayers
                    .entrySet()) {
                paidPlayersId.put(entry.getKey().getId(), entry.getValue());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void complete(ClientGameEngine engine)
            throws InvalidIdentifierException {
        engine.completeKillSheep(getWho(engine), getTile(engine),
                getSheep(engine), getPaidPlayers(engine));
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + sheepId.hashCode();
        result = prime * result + tileId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof KillSheepMove)) {
            return false;
        }
        KillSheepMove other = (KillSheepMove) obj;
        if (!sheepId.equals(other.sheepId)) {
            return false;
        }
        if (!tileId.equals(other.tileId)) {
            return false;
        }
        return true;
    }
}