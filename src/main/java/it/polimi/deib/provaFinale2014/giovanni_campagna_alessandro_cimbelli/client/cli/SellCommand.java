package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

import java.util.Iterator;

/**
 * The "sell" command, makes a card available at the market.
 * 
 * @author Giovanni Campagna
 */
class SellCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("sell <TERRAIN TYPE> for <COINS>: sell a terrain card from your deck");
        baseHelpTerrainType(ui);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException {
        Player self = ui.getCurrentGame().getSelfPlayer();
        TerrainType type = tokenizer.readTerrainType();
        Iterator<TerrainCard> iterator = self.iterateCards();

        TerrainCard card = null;
        while (iterator.hasNext()) {
            TerrainCard candidate = iterator.next();
            if (candidate.getType() == type && !candidate.isForSale()) {
                card = candidate;
                break;
            }
        }
        if (card == null) {
            ui.showMessage("It seems that you don't have a card of this type.");
            if (self.getInitialCard().getType() == type
                    && GameOptions.get().getShowHints()) {
                ui.showMessage("[The card you see in your inventory is the initial card. You cannot sell that]");
            }
            return;
        }

        tokenizer.expect("for");
        ui.getCurrentGame().sellMarketCard(card, tokenizer.readInt());
    }
}
