package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import jline.console.ConsoleReader;

/**
 * A LineReader that uses jline2 to obtain user input, supporting history and
 * line editing.
 * 
 * @author Giovanni Campagna
 * 
 */
public class JLineLineReader implements LineReader {
    private final ConsoleReader reader;

    /**
     * Construct a new JLineLineReader
     * 
     * @throws IOException
     *             if the terminal could not be setup properly
     */
    public JLineLineReader() throws IOException {
        reader = new ConsoleReader();
    }

    private String getLine(String prompt) {
        try {
            return reader.readLine(prompt);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINEST,
                    "IO exception reading from terminal", e);
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    public String nextLine(String prompt) {
        reader.setHistoryEnabled("> ".equals(prompt));

        String line = getLine(prompt);
        while (line == null || line.isEmpty()) {
            line = getLine(prompt);
        }
        return line;
    }
}
