package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

/**
 * The socket implementation of ClientNetwork.
 * 
 * @author Alessandro Cimbelli
 */
public class SocketClientNetwork implements ClientNetwork {
    private final SocketAddress address;

    /**
     * Construct a new SocketClientNetwork connecting to the given address.
     * 
     * @param address
     *            the server address
     */
    public SocketClientNetwork(SocketAddress address) {
        this.address = address;
    }

    /**
     * Construct a new SocketClientNetwork connecting to the default server
     * address.
     */
    public SocketClientNetwork() {
        this(new InetSocketAddress("127.0.0.1",
                SocketServerNetwork.DEFAULT_PORT));
    }

    /**
     * {@inheritDoc}
     */
    public ClientConnection connectToServer() throws IOException {
        return new SocketClientConnection(address);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "SocketClientNetwork to " + address;
    }
}
