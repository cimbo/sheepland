package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

import java.util.Map;

/**
 * A base class for the central class in the UI layer. Includes all abstract
 * operations that will be invoked by the application layer (Application and
 * Game) as the game progresses.
 * 
 * @author Giovanni Campagna
 * 
 */
public abstract class AbstractUI {
    /**
     * Show the first window, and begin interacting with the user. It is
     * expected that first of all the user will be presented with a choice of
     * available servers or games, but this is not required.
     */
    public abstract void show();

    /**
     * Terminate the application, possibly leaving the current game if playing.
     */
    public abstract void quit();

    /**
     * Initialize the UI for the game (which can be retrieved with
     * getApplication().getCurrentGame()), which is about to start. Players are
     * configured now, but they are not placed yet.
     */
    public abstract void initialize();

    /**
     * Reset the UI after a forced synchronization from the server (or after
     * logging in again into an existing game).
     */
    public abstract void forceSynchronize();

    /**
     * Record that the game is over.
     * 
     * @param scores
     *            the final scores of the game
     */
    public abstract void gameOver(Map<PlayerIdentifier, Integer> scores);

    /**
     * Mark that the application is waiting (processing an asynchronous request
     * to the server). If true, further interaction with the application layer
     * must be avoided, until setWaiting is called with false.
     * 
     * @param waiting
     *            the current "waiting" state
     */
    public abstract void setWaiting(boolean waiting);

    /**
     * Mark that it is now the human turn, or not (enabling and disabling the UI
     * as appropriate). Outside the human turn, all game moves will fail with
     * {@link RuleViolationException}. @
     */
    public abstract void setYourTurn(boolean yourTurn);

    /**
     * Mark that the given player left the game, either because of connectivity
     * problems / client failure, or because it decided to quit the game.
     * 
     * The parameter clean indicates if this was a conscious user action or not.
     * 
     * @param who
     *            the player that left the game
     * @param clean
     *            true if it was a user action, false otherwise
     */
    public abstract void playerLeft(Player who, boolean clean);

    /**
     * Mark that the given player reentered the game after temporary network
     * issues, and he is now playing correctly.
     * 
     * @param who
     *            the player that joined the game again
     */
    public abstract void playerJoined(Player who);

    /**
     * Report a generic error message to the user.
     * 
     * @param error
     *            the human readable message
     */
    public abstract void reportError(String error);

    /**
     * Report that the login succeeded, and we're now waiting for the game to
     * start.
     */
    public abstract void loginSuccess();

    /**
     * Report that the login failed (and that it should be attempted again).
     */
    public abstract void loginFailed();

    /**
     * Animate that the black sheep moved spontaneously to the tile
     * \destination. This method is called after updating the game model.
     * 
     * @param from
     *            the old tile where the black sheep was.
     * @param to
     *            the new tile where the black sheep is.
     */
    public abstract void animateBlackSheepMoved(SheepTile from, SheepTile to);

    /**
     * Animate that the wolf moved spontaneously to the tile \destination. This
     * method is called after updating the game model.
     * 
     * @param from
     *            the old tile where the wolf was.
     * @param to
     *            the new tile where the wolf is.
     * @param eaten
     *            the sheep that was eaten by the wolf.
     */
    public abstract void animateWolfMoved(SheepTile from, SheepTile to,
            Sheep eatenSheep);

    /**
     * Animate that the shepherd \which, belonging to player \who (who may be
     * the human player or remote), moved to the square \to. This method is
     * called after updating the game model.
     * 
     * @param who
     *            the player that moved the shepherd
     * @param which
     *            the shepherd that moved
     * @param destination
     *            the new tile where the shepherd is.
     */
    public abstract void animateShepherdMoved(Player who, Shepherd which,
            RoadSquare to);

    /**
     * Animate that a sheep close to the shepherd \which, belonging to player
     * \who (who may be the human player or remote), moved from the tile \source
     * to the tile \destination. This method is called after updating the game
     * model.
     * 
     * @param who
     *            the player that moved the sheep
     * @param which
     *            the shepherd close to whom the sheep moved
     * @param from
     *            the tile from which the sheep was taken
     * @param to
     *            the tile where the sheep was added
     * @param black
     *            whether this move affects a black sheep or a white sheep
     * */
    public abstract void animateSheepMoved(Player who, Shepherd which,
            SheepTile from, SheepTile to, Sheep sheep);

    /**
     * Animate that the shepherd \which, belonging to player \who (who may be
     * the human player or remote), bought a card of type \type. This method is
     * called after updating the game model.
     * 
     * @param who
     *            the player that moved the sheep
     * @param which
     *            the shepherd close to whom the sheep moved
     * @param card
     *            the card bought
     * */
    public abstract void animateBoughtTerrainCard(Player who, Shepherd which,
            TerrainCard card);

    /**
     * Animate that the shepherd \which, belonging to player \who (who may be
     * the human player or remote), began to mate a female sheep and a ram on
     * tile \tile. This method is called after updating the game model.
     * 
     * @param who
     *            the player that is mating the sheep
     * @param which
     *            the shepherd close to whom the sheep are mating
     * @param tile
     *            the tile on which the mating is happening
     */
    public abstract void animateBeginSheepMating(Player who, Shepherd which,
            SheepTile tile);

    /**
     * Animate that the shepherd \which, belonging to player \who (who may be
     * the human player or remote), finished mating a female sheep and a ram on
     * tile \tile. This method is called after updating the game model.
     * 
     * @param who
     *            the player that is mating the sheep
     * @param which
     *            the shepherd close to whom the sheep are mating
     * @param tile
     *            the tile on which the mating is happening
     * @param success
     *            if the mating operation succeeded (resulting in a new lamb) or
     *            not
     */
    public abstract void animateEndSheepMating(Player who, Shepherd which,
            SheepTile tile, Sheep newLamb);

    /**
     * Animate that the shepherd \which, belonging to player \who (who may be
     * the human player or remote), began to kill a sheep of type \type on tile
     * \tile. This method is called after updating the game model.
     * 
     * @param who
     *            the player that is killing the sheep
     * @param which
     *            the shepherd close to whom the sheep are killing
     * @param tile
     *            the tile on which the killing is happening
     */
    public abstract void animateBeginSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep);

    /**
     * Animate that the shepherd \which, belonging to player \who (who may be
     * the human player or remote), finished killing a sheep of type \type on
     * tile \tile. This method is called after updating the game model.
     * 
     * @param who
     *            the player that is killing the sheep
     * @param which
     *            the shepherd close to whom the sheep are killing
     * @param tile
     *            the tile on which the killing is happening
     * @param paidPlayers
     *            the players that were paid for this operation (and the amount)
     */
    public abstract void animateEndSheepKilling(Player who, Shepherd which,
            SheepTile tile, Sheep sheep, Map<Player, Integer> paidPlayers);

    /**
     * Animate that player \who (who may be the human player or remote), put the
     * card \card up for sale. This method is called after updating the game
     * model.
     * 
     * @param who
     *            the player that moved the sheep
     * @param card
     *            the card put on sale
     * @param cost
     *            the required cost to buy the card
     * */
    public abstract void animateSoldMarketCard(Player who, TerrainCard card,
            int cost);

    /**
     * Animate that player \who (who may be the human player or remote), bought
     * the card \card. This method is called after updating the game model.
     * 
     * @param who
     *            the player that moved the sheep
     * @param card
     *            the card that was bought
     * */
    public abstract void animateBoughtMarketCard(Player who, TerrainCard card);

    /**
     * Animate that the lambs on the given tile grew.
     * 
     * @param tile
     */
    public abstract void animateLambsGrew(SheepTile tile);

    /**
     * Synchronize the positions of the shepherds after the server forced the
     * end of one's turn.
     */
    public abstract void syncShepherdPositions();
}
