package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.CompletableMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ForceEndTurnMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientNetwork;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Timer;

/**
 * A Game, at the application layer. Includes a connection to the server and a
 * game engine maintaining local state.
 * 
 * Methods of this class must be accessed only from the UI thread. See AWT
 * documentation for details.
 * 
 * @author Giovanni Campagna
 * 
 */
public class Game {
    private final AbstractUI uiDelegate;
    private final ClientGameEngine engine;
    private final ClientNetwork network;
    private PlayerIdentifier playerId;
    private Player player;
    private String username;
    private String password;
    private boolean joined;

    // protected by this lock (used to run pings from the receiver
    // thread, so they don't wait on the UI, which is blocking for
    // the CLI)
    private ClientConnection connection;
    private NetworkSender networkOut;
    private final Timer reconnectionTimer;

    /**
     * Instantiate a new Game, using network as the network implementation and
     * ui as the interface to the UI layer. This will implicitly connect to the
     * server.
     * 
     * @param network
     *            the network implementation
     * @param ui
     *            the UI
     */
    public Game(ClientNetwork net, AbstractUI ui) {
        network = net;
        uiDelegate = ui;
        uiDelegate.setWaiting(true);

        engine = new ClientGameEngine();

        reconnectionTimer = new Timer(500, new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                joinGame();
            }
        });
        reconnectionTimer.setRepeats(false);
    }

    /**
     * Retrieves the rules of this game.
     * 
     * This method is only valid after the game has started.
     * 
     * @return the rules of the game
     */
    public RuleSet getRules() {
        return engine.getRules();
    }

    /**
     * Retrieve the user player.
     * 
     * Because of synchronization, it is strongly discouraged to reference this
     * object, as it is only valid until the next setup or forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @return the player
     */
    public Player getSelfPlayer() {
        return player;
    }

    /**
     * Retrieve the list of all players.
     * 
     * The list is guaranteed to be in playing order.
     * 
     * Because of synchronization, it is strongly discouraged to reference this
     * object, as it is only valid until the next setup or forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @return the list of all players
     */
    public Player[] getAllPlayers() {
        return engine.getPlayingOrder();
    }

    /**
     * Retrieve the current game board.
     * 
     * Because of synchronization, it is strongly discouraged to reference this
     * object, as it is only valid until the next setup or forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @return the game board
     */
    public GameBoard getGameBoard() {
        return engine.getGameBoard();
    }

    /**
     * Retrieve the current game phase.
     * 
     * This method is only valid after the game has started.
     * 
     * @return the game phase
     */
    public GamePhase getGamePhase() {
        return engine.getGamePhase();
    }

    /**
     * Retrieve the current player (the player expected to play now).
     * 
     * Because of synchronization, it is strongly discouraged to reference this
     * object, as it is only valid until the next setup or forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return engine.getCurrentPlayer();
    }

    // FOR TESTING ONLY - use it in production code and
    // I will hunt you with a pitchfork
    // - Giovanni
    ClientGameEngine getEngine() {
        return engine;
    }

    /**
     * Translate a player identifier into a player, for the current game board.
     * 
     * Because of synchronization, it is strongly discouraged to reference the
     * returned object, as it is only valid until the next setup or
     * forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not recognized
     */
    public Player getPlayerFromId(PlayerIdentifier id)
            throws InvalidIdentifierException {
        return engine.getPlayerFromId(id);
    }

    /**
     * Translate a shepherd identifier into a shepherd, for the current game
     * board.
     * 
     * Because of synchronization, it is strongly discouraged to reference the
     * returned object, as it is only valid until the next setup or
     * forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not recognized
     */
    public Shepherd getShepherdFromId(Player owner, ShepherdIdentifier id)
            throws InvalidIdentifierException {
        return engine.getShepherdFromId(owner, id);
    }

    /**
     * Translate a sheep tile identifier into a sheep tile, for the current game
     * board.
     * 
     * Because of synchronization, it is strongly discouraged to reference the
     * returned object, as it is only valid until the next setup or
     * forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not recognized
     */
    public SheepTile getSheepTileFromId(SheepTileIdentifier id)
            throws InvalidIdentifierException {
        return engine.getSheepTileFromId(id);
    }

    /**
     * Translate a road square identifier into a road square, for the current
     * game board.
     * 
     * Because of synchronization, it is strongly discouraged to reference the
     * returned object, as it is only valid until the next setup or
     * forceSynchronize.
     * 
     * This method is only valid after the game has started.
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             the identifier was not recognized
     */
    public RoadSquare getRoadSquareFromId(RoadSquareIdentifier id)
            throws InvalidIdentifierException {
        return engine.getRoadSquareFromId(id);
    }

    // Interface from the UI layer (AbstractUI and inheritors)

    private void assertJoined() {
        if (!joined) {
            throw new IllegalStateException(
                    "Cannot interact with the game without a connection to the server");
        }
    }

    private void assertNotJoined() {
        if (joined) {
            throw new IllegalStateException(
                    "Method cannot be called while already connected to a server");
        }
    }

    /**
     * Ask the server to join the new game.
     * 
     * This method will connect to the server.
     */
    public void joinGame(String user, String pass) {
        username = user;
        password = pass;
        assertNotJoined();
        joinGame();
    }

    private void joinGame() {
        assert username != null;
        assert password != null;

        try {
            synchronized (this) {
                if (connection == null) {
                    connection = network.connectToServer();
                    connection.setClientReceiver(new NetworkReceiver(this));

                    assert networkOut == null;
                    networkOut = new NetworkSender(this,
                            connection.getClientSender());
                    networkOut.start();
                }
            }

            uiDelegate.setWaiting(true);
            networkOut.sendLoginToServer(username, password);
            joined = true;
        } catch (IOException e) {
            Logger.getGlobal()
                    .log(Level.INFO, "Failed to connect to server", e);

            uiDelegate.reportError("Failed to connect to server: "
                    + e.getMessage());
        }
    }

    private synchronized void closeConnection() {
        if (connection != null) {
            networkOut.close();
            networkOut = null;
            connection.close();
            connection = null;
        }
    }

    private void resetReconnection() {
        reconnectionTimer.stop();
        reconnectionTimer.setInitialDelay(500);
    }

    /**
     * Abandon the current game. This method has no effect if called before
     * joining the game, or after abandoning it.
     */
    public void abandon(boolean wait) {
        if (!joined) {
            return;
        }

        uiDelegate.setWaiting(true);
        networkOut.sendAbandonGame();
        if (wait) {
            networkOut.flushQueue();
        }
    }

    private void executeMove(Move move) throws RuleViolationException {
        assertJoined();

        try {
            // First, run the move locally, to see if it is allowed
            move.execute(engine);

            // Cool, it worked fine. So freeze the UI and send the move to the
            // server
            if (move instanceof UIMove) {
                ((UIMove) move).animateBegin(engine, uiDelegate);
            }
            uiDelegate.setWaiting(true);
            networkOut.sendExecuteMove(move);
        } catch (RuleViolationException e) {
            throw e;
        } catch (InvalidMoveException e) {
            Logger.getGlobal().log(Level.WARNING,
                    "Unexpected invalid move in execute move", e);
        }
    }

    /**
     * Execute a "initially place shepherd" move, for shepherd which and moving
     * it to to.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @param which
     *            the shepherd to place
     * @param to
     *            the destination to place it on
     * @throws RuleViolationException
     */
    public void initiallyPlaceShepherd(Shepherd which, RoadSquare to)
            throws RuleViolationException {
        executeMove(new InitiallyPlaceShepherdUIMove(playerId, which.getId(),
                to.getId()));
    }

    /**
     * Execute a "move sheperd" move, for shepherd which and moving it to to.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @param which
     *            the shepherd to move
     * @param to
     *            the destination of the move
     * @throws RuleViolationException
     */
    public void moveShepherd(Shepherd which, RoadSquare to)
            throws RuleViolationException {
        executeMove(new MoveShepherdUIMove(playerId, which.getId(), to.getId()));
    }

    /**
     * Execute a "move sheep" move, moving a black or white sheep (according to
     * the black argument) from \from to \to.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @param which
     *            the shepherd to move
     * @param from
     *            the origin of the move
     * @param to
     *            the destination of the move
     * @param black
     *            if true, this will move the black sheep, otherwise any white
     *            one
     * @throws RuleViolationException
     */
    public void moveSheep(Shepherd which, SheepTile from, SheepTile to,
            Sheep sheep) throws RuleViolationException {
        executeMove(new MoveSheepUIMove(playerId, which.getId(), from.getId(),
                to.getId(), sheep.getId()));
    }

    /**
     * Buy a terrain card \card, close to shepherd \which.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @param which
     *            the shepherd buying the card
     * @param card
     *            the card to buy.
     * @throws RuleViolationException
     */
    public void buyTerrainCard(Shepherd which, TerrainCard card)
            throws RuleViolationException {
        executeMove(new BuyTerrainCardUIMove(playerId, which.getId(),
                card.getId()));
    }

    /**
     * Attempt to mate a female sheep and a ram on SheepTile \tile, close to
     * shepherd \which.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely. The outcome is not immediately available though. It will be
     * available before the next moveAcknowledged call.
     * 
     * @param which
     *            the shepherd buying the card
     * @param type
     *            the type of card to buy.
     * @throws RuleViolationException
     */
    public void mateSheep(Shepherd which, SheepTile tile)
            throws RuleViolationException {
        executeMove(new MateSheepUIMove(playerId, which.getId(), tile.getId()));
    }

    /**
     * Attempt to kill a sheep of SheepType \type on SheepTile \tile, close to
     * shepherd \which.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely. The outcome is not immediately available though. It will be
     * available before the next moveAcknowledged call.
     * 
     * @param which
     *            the shepherd buying the card
     * @param type
     *            the type of card to buy.
     * @throws RuleViolationException
     */
    public void killSheep(Shepherd which, SheepTile tile, Sheep sheep)
            throws RuleViolationException {
        executeMove(new KillSheepUIMove(playerId, which.getId(), tile.getId(),
                sheep.getId()));
    }

    /**
     * Attempt to sell a card to the market, at a given cost.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @param card
     *            the card to sell
     * @param cost
     *            the money asked for the card
     * @throws RuleViolationException
     */
    public void sellMarketCard(TerrainCard card, int cost)
            throws RuleViolationException {
        executeMove(new SellMarketCardUIMove(playerId, card.getId(), cost));
    }

    /**
     * Attempt to buy a card from the market (at whatever cost it was put on
     * sale).
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @param card
     *            the card to buy
     * @throws RuleViolationException
     */
    public void buyMarketCard(TerrainCard card) throws RuleViolationException {
        executeMove(new BuyMarketCardUIMove(playerId, card.getId()));
    }

    /**
     * Force the end of the turn.
     * 
     * This will throw a RuleViolationException if the move was not allowed by
     * the rules of the game. Otherwise, the move is executed locally and
     * remotely.
     * 
     * @throws RuleViolationException
     */
    public void forceEndTurn() throws RuleViolationException {
        executeMove(new ForceEndTurnMove(playerId, false));
    }

    // Interface from NetworkReceiver (through the AWT EventQueue)

    /**
     * Record the completion of the login procedure, with the given outcome.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param answer
     *            true, if login was successful, and false otherwise
     */
    public void loginAnswer(boolean answer) {
        resetReconnection();

        if (answer) {
            // we don't setWaiting(false) here, we're still
            // waiting for more from the server
            uiDelegate.loginSuccess();
        } else {
            joined = false;
            uiDelegate.setWaiting(false);
            uiDelegate.loginFailed();
        }
    }

    /**
     * Record the initial setup information from the server, and kickstart the
     * game engine.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param rules
     *            the rules of the game
     * @param board
     *            the fully initialized game board (including the playing order
     *            and all players)
     * @param self
     *            the identifier of the human player
     */
    public void setup(RuleSet rules, GameBoard board, PlayerIdentifier self)
            throws ServerException {
        try {
            engine.setRules(rules);
            engine.start(board);
        } catch (RuleViolationException e) {
            throw new ServerRuleViolationException(e);
        }

        try {
            playerId = self;
            player = engine.getPlayerFromId(self);
        } catch (InvalidIdentifierException exc) {
            throw new ServerException(exc);
        }

        uiDelegate.initialize();
        uiDelegate.setYourTurn(engine.getCurrentPlayer() == player);
        uiDelegate.setWaiting(false);
    }

    /**
     * Record the end of the game, with the given final scores.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param scores
     *            the final scores.
     */
    public void endGame(Map<PlayerIdentifier, Integer> scores) {
        engine.end();
        closeConnection();
        joined = false;

        uiDelegate.setYourTurn(false);
        uiDelegate.setWaiting(false);
        uiDelegate.gameOver(scores);
    }

    /**
     * Acknowledge a previous move.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     */
    public void moveAcknowledged() throws ServerException {
        uiDelegate.setWaiting(false);
    }

    /**
     * Report an error from a previous move.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param reason
     *            the error reported by the server
     */
    public void moveFailed(Exception reason) throws ServerException {
        uiDelegate.reportError("Server rejected our move: "
                + reason.getMessage());
        handleServerException();
    }

    /**
     * Report an error from the server, or a failure of a previous move.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param exc
     *            the detailed error.
     */
    public void reportError(ServerException exc) {
        uiDelegate.reportError(exc.getMessage());
        handleServerException();
    }

    /**
     * Report an IO error.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param exc
     *            the detailed error.
     */
    public void reportError(IOException exc) {
        closeConnection();
        if (!joined) {
            return;
        }

        uiDelegate.setWaiting(true);
        uiDelegate.reportError("Lost connection to the server");

        // Try again at joining the game
        // This will create a new NetworkSender, as well as a new
        // ClientConnection
        reconnectionTimer.setInitialDelay(2 * reconnectionTimer
                .getInitialDelay());
        if (reconnectionTimer.getInitialDelay() >= 30000) {
            uiDelegate
                    .reportError("Too many failures connecting to the server.");
        } else {
            uiDelegate.reportError("Attempting reconnection in "
                    + reconnectionTimer.getInitialDelay() / 1000 + " seconds");
            reconnectionTimer.restart();
        }
    }

    /**
     * Report an IO error while writing.
     * 
     * This method should only be called by {@link NetworkSender}.
     * 
     * @param sender
     *            the faulty sender
     * @param exc
     *            the detailed error.
     */
    public void reportError(NetworkSender sender, IOException exc) {
        // Check for race conditions due to old senders being flushed
        // or still handling errors from queued items
        if (sender != networkOut) {
            return;
        }

        reportError(exc);
    }

    private void handleServerException() {
        uiDelegate
                .reportError("Attempting synchronization with the server again.");

        uiDelegate.setWaiting(true);
        networkOut.sendForceSynchronizeMe();
    }

    /**
     * Acknowledge a synchronization or reconnection, with the full state
     * received from the server.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     */
    public void forceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) throws ServerException {
        if (playerId != null && !playerId.equals(self)) {
            Logger.getGlobal().log(Level.SEVERE,
                    "Server changed our player identifier");
        }

        playerId = self;
        try {
            engine.setRules(rules);
            engine.forceSynchronizeState(newBoard, newState);
            player = engine.getPlayerFromId(playerId);
        } catch (InvalidIdentifierException e) {
            throw new ServerException(e);
        } catch (RuleViolationException e) {
            throw new ServerRuleViolationException(e);
        }

        uiDelegate.forceSynchronize();
        uiDelegate.setYourTurn(engine.getCurrentPlayer() == player);
        uiDelegate.setWaiting(false);
    }

    /**
     * Advance to the next turn (as instructed by the server).
     * 
     * This method should only be called by {@link NetworkReceiver}.
     */
    public void advanceNextTurn(int turn, GamePhase phase) {
        engine.advanceNextTurn(turn, phase);
        uiDelegate.setYourTurn(engine.getCurrentPlayer() == player);
    }

    /**
     * Report that the black sheep moved to tile destination.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param destination
     */
    public void didMoveBlackSheep(SheepTileIdentifier destinationId)
            throws ServerException {
        SheepTile destination;

        try {
            destination = engine.getSheepTileFromId(destinationId);
        } catch (InvalidIdentifierException exc) {
            throw new ServerException(exc);
        }

        SheepTile previousTile = engine.getGameBoard().getMap()
                .getCurrentBlackSheepPosition();
        engine.moveBlackSheep(destination);
        uiDelegate.animateBlackSheepMoved(previousTile, destination);
    }

    /**
     * Report that the wolf moved to tile destination.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param destination
     */
    public void didMoveWolf(SheepTileIdentifier destinationId,
            SheepIdentifier eatenSheepId) throws ServerException {
        SheepTile destination;
        Sheep eatenSheep;
        try {
            destination = engine.getSheepTileFromId(destinationId);
            if (eatenSheepId != null) {
                eatenSheep = engine.getSheepFromId(eatenSheepId);
            } else {
                eatenSheep = null;
            }
        } catch (InvalidIdentifierException exc) {
            throw new ServerException(exc);
        }

        SheepTile previousTile = engine.getGameBoard().getMap()
                .getCurrentWolfPosition();
        engine.moveWolf(destination, eatenSheep);
        uiDelegate.animateWolfMoved(previousTile, destination, eatenSheep);
    }

    /**
     * Report that the lamb in SheepTile where grown to be adult sheep, and
     * newAdultSheep sheep of each adult type were created.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param destination
     */
    public void didGrowLambs(SheepTileIdentifier whereId, Sheep[] newAdultSheep)
            throws ServerException {
        SheepTile where;

        try {
            where = engine.getSheepTileFromId(whereId);
        } catch (InvalidIdentifierException exc) {
            throw new ServerException(exc);
        }

        engine.growLambs(where, newAdultSheep);
        uiDelegate.animateLambsGrew(where);
    }

    private void didCompleteMyMove(Move move) throws InvalidMoveException {
        if (move instanceof CompletableMove) {
            ((CompletableMove) move).complete(engine);
            if (move instanceof UIMove) {
                ((UIMove) move).animateEnd(engine, uiDelegate);
            }
        } else if (move instanceof ForceEndTurnMove
                && ((ForceEndTurnMove) move).isForced()) {
            move.execute(engine);
            uiDelegate.reportError("The turn was ended by timeout");
            uiDelegate.syncShepherdPositions();
        } else {
            throw new InvalidMoveException(
                    "The server is sending us our moves?");
        }
    }

    private void didExecuteForeignMove(Move move) throws InvalidMoveException {
        move.execute(engine);
        if (move instanceof UIMove) {
            ((UIMove) move).animateBegin(engine, uiDelegate);
        } else if (move instanceof ForceEndTurnMove
                && ((ForceEndTurnMove) move).isForced()) {
            uiDelegate.syncShepherdPositions();
        }

        if (move instanceof CompletableMove) {
            ((CompletableMove) move).complete(engine);
            if (move instanceof UIMove) {
                ((UIMove) move).animateEnd(engine, uiDelegate);
            }
        }
    }

    /**
     * Report that a game move was executed or completed.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param move
     *            the move
     */
    public void didExecuteMove(Move move) throws ServerException {
        try {
            if (move.getWhoId().equals(playerId)) {
                didCompleteMyMove(move);
            } else {
                didExecuteForeignMove(move);
            }
        } catch (RuleViolationException e) {
            throw new ServerRuleViolationException(e);
        } catch (InvalidMoveException e) {
            throw new ServerException(e);
        }
    }

    /**
     * Report a ping request from the server.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     */
    public synchronized void didPing() {
        if (networkOut != null) {
            networkOut.sendPong();
        }
    }

    /**
     * Report that a player left the game.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param who
     *            the player who left
     * @param clean
     *            true, if he left on purpose, false, if he left due to
     *            transient network issues
     */
    public void playerLeft(PlayerIdentifier who, boolean clean)
            throws ServerException {
        try {
            uiDelegate.playerLeft(engine.getPlayerFromId(who), clean);
        } catch (InvalidIdentifierException e) {
            throw new ServerException(e);
        }
    }

    /**
     * Report that a player reconnected to the game.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     * 
     * @param who
     *            the player who connected again
     */
    public void playerJoined(PlayerIdentifier who) throws ServerException {
        try {
            uiDelegate.playerJoined(engine.getPlayerFromId(who));
        } catch (InvalidIdentifierException e) {
            throw new ServerException(e);
        }
    }

    /**
     * Signal the end of the abandon process.
     * 
     * This method should only be called by {@link NetworkReceiver}.
     */
    public void abandonComplete() {
        closeConnection();
        joined = false;
        resetReconnection();

        uiDelegate.setYourTurn(false);
        uiDelegate.setWaiting(false);
    }
}
