package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;

/**
 * The "jump" command, moves the chosen shepherd to the given road.
 * 
 * @author Giovanni Campagna
 */
class JumpCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("jump between <REGION> and <REGION>: jump with your shepherd to this road");
        baseHelpRegion(ui);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException,
            InvalidIdentifierException {
        if (ui.getChosenShepherd() == null) {
            ui.showMessage("As the farm owner, you have no intention of ever moving away from Sheepsburg.");
            if (GameOptions.get().getShowHints()) {
                ui.showMessage("[Use the be command to choose a shepherd before jumping]");
            }
            return;
        }

        Game game = ui.getCurrentGame();
        RoadSquare destination = game.getRoadSquareFromId(tokenizer
                .readRoadSquare());

        if (game.getGamePhase() == GamePhase.INIT) {
            game.initiallyPlaceShepherd(ui.getChosenShepherd(), destination);
            ui.chooseShepherd(null);
        } else {
            game.moveShepherd(ui.getChosenShepherd(), destination);
        }
    }
}
