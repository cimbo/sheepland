package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerSender;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The RMI implementation of ServerConnection
 * 
 * @author Alessandro Cimbelli
 */
public class RMIServerConnection extends UnicastRemoteObject implements Game,
        ServerConnection, ServerSender {
    private static final long serialVersionUID = 1L;

    private final Client remoteClient;
    private ServerReceiver receiver;

    /**
     * Construct a new RMIServerConnection for the given client.
     * 
     * @param client
     *            the connecting client
     * @throws RemoteException
     */
    public RMIServerConnection(Client client) throws RemoteException {
        super();
        remoteClient = client;
    }

    /**
     * {@inheritDoc}
     */
    public ServerSender getServerSender() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void setServerReceiver(ServerReceiver receiver) {
        this.receiver = receiver;
    }

    /**
     * {@inheritDoc}
     */
    public void close() {
        try {
            unexportObject(this, true);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.FINE,
                    "Exception unexporting RMI object", e);
        }

        receiver.connectionClosed();
    }

    /**
     * {@inheritDoc}
     */
    public void sendSetup(RuleSet rules, GameBoard board, PlayerIdentifier self)
            throws IOException {
        remoteClient.setup(rules, board, self);
    }

    /**
     * {@inheritDoc}
     */
    public void sendMoveAcknowledged(Exception result) throws IOException {
        remoteClient.moveAcknowledged(result);
    }

    /**
     * {@inheritDoc}
     */
    public void sendForceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) throws IOException {
        remoteClient.forceSynchronize(rules, newBoard, self, newState);
    }

    /**
     * {@inheritDoc}
     */
    public void sendAdvanceNextTurn(int turn, GamePhase phase)
            throws IOException {
        remoteClient.advanceNextTurn(turn, phase);
    }

    /**
     * {@inheritDoc}
     */
    public void sendBlackSheepMoved(SheepTileIdentifier destination)
            throws IOException {
        remoteClient.blackSheepMoved(destination);
    }

    /**
     * {@inheritDoc}
     */
    public void sendWolfMoved(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) throws IOException {
        remoteClient.wolfMoved(destination, eatenSheep);
    }

    /**
     * {@inheritDoc}
     */
    public void sendLambsGrew(SheepTileIdentifier where, Sheep[] newAdultSheep)
            throws IOException {
        remoteClient.lambsGrew(where, newAdultSheep);
    }

    /**
     * {@inheritDoc}
     */
    public void sendDidExecuteMove(Move move) throws IOException {
        remoteClient.didExecuteMove(move);
    }

    /**
     * {@inheritDoc}
     */
    public void sendLoginAnswer(boolean answer) throws IOException {
        remoteClient.loginAnswer(answer);
    }

    /**
     * {@inheritDoc}
     */
    public void sendPing() throws IOException {
        remoteClient.ping();
    }

    /**
     * {@inheritDoc}
     */
    public void sendEndGame(Map<PlayerIdentifier, Integer> scores)
            throws IOException {
        remoteClient.endGame(scores);
    }

    /**
     * {@inheritDoc}
     */
    public void sendPlayerLeft(PlayerIdentifier who, boolean clean)
            throws IOException {
        remoteClient.playerLeft(who, clean);
    }

    /**
     * {@inheritDoc}
     */
    public void sendPlayerJoined(PlayerIdentifier who) throws IOException {
        remoteClient.playerJoined(who);
    }

    /**
     * {@inheritDoc}
     */
    public void sendAbandonComplete() throws IOException {
        remoteClient.abandonComplete();
    }

    /**
     * {@inheritDoc}
     */
    public void abandonGame() {
        receiver.doAbandonGame();
    }

    /**
     * {@inheritDoc}
     */
    public void forceSynchronizeMe() {
        receiver.doForceSynchronizeMe();
    }

    /**
     * {@inheritDoc}
     */
    public void executeMove(Move move) {
        receiver.doExecuteMove(move);
    }

    /**
     * {@inheritDoc}
     */
    public void loginToServer(String username, String password) {
        receiver.doLoginToServer(username, password);
    }

    /**
     * {@inheritDoc}
     */
    public void pong() {
        receiver.doPong();
    }
}
