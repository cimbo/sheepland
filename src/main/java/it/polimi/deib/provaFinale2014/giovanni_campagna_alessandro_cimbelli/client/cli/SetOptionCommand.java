package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

/**
 * The "set" command, used to configure the game.
 * 
 * @author Giovanni Campagna
 */
class SetOptionCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("set <OPTION> <VALUE>: change the game configuration");
        ui.showMessage("Currently, the only recognized option is \"hints\", with value \"on\" or \"off\", that will affect the hints shown with square brackets");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException {
        GameOptions.get().setOption(tokenizer.readAny(), tokenizer.readAny());
    }
}
