package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;

import java.util.Map;

/**
 * A game engine implementation that relies on an external source to implement
 * the methods that require additional knowledge (such as a random source).
 * 
 * @author Giovanni Campagna
 * 
 */
public class ClientGameEngine extends GameEngine {
    public ClientGameEngine() {
    }

    /**
     * Starts the game, moving from the PREINIT to the INIT phase.
     */
    public void start(GameBoard board) {
        GameState state = getGameState();
        if (state.getCurrentPhase() != GamePhase.PREINIT) {
            throw new GameInvariantViolationException(
                    "Game has already started");
        }

        setGameBoard(board);
        state.advanceInitState();
        state.prepareNextTurn();
    }

    /**
     * Finish the game.
     * 
     * After this method, all calls on the game engine will fail.
     */
    public void end() {
        getGameState().advanceEndState();
    }

    /**
     * Advance to the given turn, and prepare the game state for the next moves.
     * 
     * @param turn
     *            the next player to play, in order
     */
    public void advanceNextTurn(int turn, GamePhase phase) {
        GameState state = getGameState();

        if (state.getCurrentPhase() == GamePhase.MARKET_BUY
                && phase != GamePhase.MARKET_BUY) {
            giveBackTerrainCardsForSale();
        }

        state.setCurrentPhase(phase);
        state.setCurrentTurn(turn);
        state.prepareNextTurn();
    }

    /**
     * Record that the black sheep moved to destination.
     * 
     * This should only be called in response to a command from the server.
     * 
     * @param destination
     */
    public void moveBlackSheep(SheepTile destination) {
        GameMap map = getGameBoard().getMap();

        map.moveBlackSheep(getGameBoard().getBlackSheep(), destination);
    }

    /**
     * Record that the wolf moved to destination, and ate a sheep of type
     * eatenSheep.
     * 
     * This should only be called in response to a command from the server.
     * 
     * @param destination
     * @param eatenSheep
     */
    public void moveWolf(SheepTile destination, Sheep eatenSheep) {
        GameMap map = getGameBoard().getMap();

        if (eatenSheep != null) {
            destination.removeSheep(eatenSheep);
        }
        map.moveWolf(destination);
    }

    /**
     * Record that the lambs of SheepTile where grew, and there are new adult
     * sheep.
     * 
     * @param where
     *            the affected tile
     * @param newAdultSheep
     *            the count of new adult sheep, for each adult type
     */
    public void growLambs(SheepTile where, Sheep[] newAdultSheep) {
        where.growLambs();

        for (Sheep newAdult : newAdultSheep) {
            getGameBoard().addSheep(newAdult.getId(), newAdult);
            where.addSheep(newAdult);
        }
    }

    /**
     * Report completion of a "mate sheep" move from the server.
     * 
     * @param tile
     *            the tile on which the move is happening
     * @param success
     *            if the move was successful
     */
    public void completeMateSheep(SheepTile tile, Sheep newLamb) {
        if (newLamb != null) {
            getGameBoard().addSheep(newLamb.getId(), newLamb);
            tile.addSheep(newLamb);
        }
    }

    /**
     * Report completion of a "kill sheep" move from the server.
     * 
     * @param tile
     *            the tile on which the move is happening
     * @param type
     *            type of sheep to kill
     * @param result
     *            the players that were paid for this operation (with their
     *            earning), or null if it failed
     */
    public void completeKillSheep(Player who, SheepTile tile, Sheep sheep,
            Map<Player, Integer> paidPlayers) {
        if (paidPlayers == null) {
            return;
        }

        tile.removeSheep(sheep);
        for (Map.Entry<Player, Integer> entry : paidPlayers.entrySet()) {
            int amount = entry.getValue();
            entry.getKey().earnCoins(amount);
            who.payCoins(amount);
        }
    }
}
