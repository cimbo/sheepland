package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.io.Serializable;

/**
 * This class extends {@link Move} to represent a move that involves a shepherd.
 * 
 * @author Giovanni Campagna
 */
public abstract class ShepherdMove extends Move implements Serializable {
    private static final long serialVersionUID = 1L;

    private final ShepherdIdentifier whichId;

    /**
     * Construct a move on behalf of a player, using a specific shepherd
     * 
     * @param who
     *            the player executing the move
     * @param which
     *            the shepherd being used for the move
     */
    protected ShepherdMove(PlayerIdentifier who, ShepherdIdentifier which) {
        super(who);
        whichId = which;
    }

    /**
     * Retrieve the actual shepherd object, by resolving the identifier through
     * the passed in engine.
     * 
     * @param engine
     * @return the shepherd as game object
     * @throws InvalidIdentifierException
     */
    protected Shepherd getWhich(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getShepherdFromId(getWho(engine), whichId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + whichId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof ShepherdMove)) {
            return false;
        }
        ShepherdMove other = (ShepherdMove) obj;
        if (!whichId.equals(other.whichId)) {
            return false;
        }
        return true;
    }
}
