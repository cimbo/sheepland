package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * A panel used to show market and terrain card operations.
 * 
 * @author Alessandro Cimbelli
 */
public class BoughtOrSoldCardPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 527;
    private static final int Y_SIZE = 100;

    /**
     * Construct a boughtorsoldcardpanel with the given player icon, card icon
     * and message.
     */
    public BoughtOrSoldCardPanel(PlayerIcon pawn, Card card, String text) {
        this.setSize(X_SIZE, Y_SIZE);
        setLayout(null);
        setBackground(new Color(255, 255, 0));

        JLabel label = new JLabel(text);
        add(label);
        label.setSize(X_SIZE, Y_SIZE);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);
        label.setFont(new Font("Arial", Font.BOLD, 16));

        this.add(pawn);
        this.add(card);
        pawn.setLocation((int) (getWidth() * 0.14),
                (int) ((getHeight() - pawn.getHeight()) * 0.5));
        card.setLocation((int) (getWidth() * 0.70),
                (int) ((getHeight() - card.getHeight()) * 0.5));
    }
}
