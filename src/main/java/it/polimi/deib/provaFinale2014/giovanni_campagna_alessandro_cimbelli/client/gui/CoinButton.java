package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageLoader;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * A button with a coin image and some text.
 * 
 * @author Alessandro Cimbelli
 * @author Giovanni Campagna
 */
public class CoinButton extends JButton {
    private static final long serialVersionUID = 1L;
    private final Image img;
    private static final int X_SIZE = 30;
    private static final int Y_SIZE = 30;

    /**
     * Construct a new coin button with the given text.
     * 
     * @param text
     *            the number on top of the coin
     */
    public CoinButton(String text) {
        super();
        setLayout(null);
        setSize(X_SIZE, Y_SIZE);

        img = ImageLoader.loadImageSync(this, "coin.png");
        setContentAreaFilled(false);

        JLabel label = new JLabel(text);
        add(label);
        label.setLocation(0, 0);
        label.setSize(X_SIZE, Y_SIZE);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, null);
    }
}
