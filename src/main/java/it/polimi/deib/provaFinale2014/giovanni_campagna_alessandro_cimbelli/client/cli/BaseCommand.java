package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;

import java.util.HashMap;
import java.util.Map;

/**
 * The base class for all CLI commands. Has methods to execute them and to print
 * help.
 * 
 * @author Giovanni Campagna
 */
abstract class BaseCommand {
    private static Map<String, BaseCommand> allCommands;

    /**
     * Print help for this command.
     * 
     * @param ui
     *            the UI manager through which output should happen
     */
    public abstract void printHelp(CLIUIManager ui);

    /**
     * Complete parsing of this command and execute it.
     * 
     * @param tokenizer
     *            the tokenizer used for parsing
     * @param ui
     *            the UI manager through which output should happen
     * @throws InvalidInputException
     *             if parsing fails
     * @throws RuleViolationException
     *             if the command fails due to rule violation
     * @throws InvalidIdentifierException
     *             if the command references unknown objects
     */
    public abstract void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException,
            InvalidIdentifierException;

    private static void ensureCommands() {
        if (allCommands != null) {
            return;
        }

        allCommands = new HashMap<String, BaseCommand>();
        allCommands.put("help", new HelpCommand());
        allCommands.put("look", new LookCommand());
        allCommands.put("be", new BeShepherdCommand());
        allCommands.put("jump", new JumpCommand());
        allCommands.put("move", new MoveSheepCommand());
        allCommands.put("mate", new MateSheepCommand());
        allCommands.put("kill", new KillSheepCommand());
        allCommands.put("buy", new BuyCommand());
        allCommands.put("sell", new SellCommand());
        allCommands.put("end", new EndTurnCommand());
        allCommands.put("set", new SetOptionCommand());
        allCommands.put("quit", new QuitCommand());
    }

    /**
     * A helper function for help implementations of commands that use regions.
     * 
     * @param ui
     *            the UI manager through which output should happen
     */
    protected static void baseHelpRegion(CLIUIManager ui) {
        ui.showMessage("<REGION> can be \"sheepsburg\" or a terrain type followed by a number, as shown on the map.");
    }

    /**
     * A helper function for help implementations of commands that use sheep
     * types.
     * 
     * @param ui
     *            the UI manager through which output should happen
     */
    protected static void baseHelpSheepType(CLIUIManager ui) {
        ui.showMessage("<SHEEP TYPE> can be \"black sheep\", \"female sheep\", \"ram\" or \"lamb\"");
    }

    /**
     * A helper function for help implementations of commands that use terrain
     * types.
     * 
     * @param ui
     *            the UI manager through which output should happen
     */
    protected static void baseHelpTerrainType(CLIUIManager ui) {
        ui.showMessage("<TERRAIN TYPE> can be \"land\", \"desert\", \"mountain\", \"grain\", \"water\" or \"forest\"");
    }

    /**
     * Retrieve the named command.
     * 
     * @param name
     *            the command name
     * @return the BaseCommand implementation of the given command.
     */
    public static BaseCommand getCommand(String name) {
        ensureCommands();
        return allCommands.get(name);
    }
}
