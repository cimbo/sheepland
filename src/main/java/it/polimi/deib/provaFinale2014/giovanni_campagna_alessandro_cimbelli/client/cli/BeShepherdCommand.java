package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import static it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli.Representation.shepherdToString;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.Game;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The "be" command (called "BeShepherd" because "be" needs to be always
 * followed by "shepherd"). Switches the current shepherd.
 * 
 * @author Giovanni Campagna
 * 
 */
class BeShepherdCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("be shepherd <NUMBER>: be this shepherd for the rest of this turn");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, InvalidIdentifierException {
        tokenizer.expect("shepherd");
        ShepherdIdentifier which = tokenizer.readShepherd();

        if (ui.getChosenShepherd() != null
                && ui.getCurrentGame().getGamePhase() != GamePhase.INIT) {
            ui.showMessage("You already chose which shepherd to be for this turn.");
        } else {
            try {
                Game game = ui.getCurrentGame();
                ui.chooseShepherd(game.getShepherdFromId(game.getSelfPlayer(),
                        which));

                ui.showMessage("You are now %s.", shepherdToString(which));
            } catch (InvalidIdentifierException e) {
                Logger.getGlobal().log(Level.FINE,
                        "Invalid identifier in be shepherd", e);

                ui.showMessage(
                        "You try to be %s, but you fail. You probably don't know how to be him.",
                        shepherdToString(which));
            }
        }
    }
}