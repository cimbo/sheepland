package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

/**
 * A collection of miscellaneous options affecting the CLI.
 * 
 * @author Giovanni Campagna
 */
public class GameOptions {
    private static GameOptions options;

    private boolean hints;
    private boolean debug;

    private GameOptions() {
        hints = true;
    }

    /**
     * Retrieve the GameOptions singleton.
     * 
     * @return the singleton
     */
    public static GameOptions get() {
        if (options == null) {
            options = new GameOptions();
        }

        return options;
    }

    /**
     * Check if hints are to be shown to the user
     * 
     * @return true, if hints should be shown, false otherwise
     */
    public boolean getShowHints() {
        return hints;
    }

    /**
     * Check if debug commands are enabled.
     * 
     * @return true, if debug commands are enabled, false otherwise
     */
    public boolean getDebug() {
        return debug;
    }

    private static boolean isTrue(String value) {
        return "1".equals(value) || "on".equals(value) || "true".equals(value)
                || "yes".equals(value);
    }

    private static boolean isFalse(String value) {
        return "0".equals(value) || "off".equals(value)
                || "false".equals(value) || "no".equals(value);
    }

    private static boolean toBoolean(String value) throws InvalidInputException {
        if (isTrue(value)) {
            return true;
        }
        if (isFalse(value)) {
            return false;
        }

        throw new InvalidInputException(value);
    }

    /**
     * Change the given option to the given value.
     * 
     * The meaning of value depends on the specific option
     * 
     * @param option
     *            the option name
     * @param value
     *            the new value
     * @throws InvalidInputException
     *             if the option was not recognized, or the value was not valid
     */
    public void setOption(String option, String value)
            throws InvalidInputException {
        if ("hints".equals(option)) {
            hints = toBoolean(value);
        } else if ("debug".equals(option)) {
            debug = toBoolean(value);
        } else {
            throw new InvalidInputException(option);
        }
    }
}