package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;

/**
 * Identifies a sheep.
 * 
 * As sheep are created and destroyed on demand as the game progresses, the
 * numerical IDs are generated and not meaningful outside a specific game board
 * or its clones/serializations.
 * 
 * @author Giovanni Campagna
 */
public class SheepIdentifier extends GameObjectIdentifier {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a sheep identifier with a new generated ID.
     */
    public SheepIdentifier() {
        super(Sheep.class);
    }
}
