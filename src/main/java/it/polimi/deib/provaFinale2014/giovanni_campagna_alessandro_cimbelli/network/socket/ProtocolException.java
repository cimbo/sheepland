package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import java.io.IOException;

/**
 * An exception that represents any problem reading from the input channel,
 * caused by a protocol violation or incompatibility.
 * 
 * @author Giovanni Campagna
 */
public class ProtocolException extends IOException {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new protocol exception with the given message.
     * 
     * @param message
     *            the exception message
     */
    public ProtocolException(String message) {
        super(message);
    }

    /**
     * Construct a new protocol exception with the given message and inner
     * exception
     * 
     * @param message
     * @param cause
     */
    public ProtocolException(String message, Exception cause) {
        super(message, cause);
    }
}
