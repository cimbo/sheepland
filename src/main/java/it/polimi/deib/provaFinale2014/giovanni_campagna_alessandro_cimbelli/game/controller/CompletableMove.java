package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;

/**
 * This interface is implemented by all {@link Move}s that require some
 * additional processing on the server side.
 * 
 * The processing happens inside {@link CompletableMove.computeResult}, which
 * saves the state on the move object, and is transferred to the client's game
 * engine through {@link CompletableMove.complete}.
 * 
 * @author Giovanni Campagna
 * 
 */
public interface CompletableMove {
    /**
     * Complete the move, applying the result computed on the server to the game
     * engine.
     * 
     * This method is only legal for a move received from the server. It is
     * undefined behavior to call it for a locally generated move.
     * 
     * @param engine
     * @throws InvalidIdentifierException
     */
    void complete(ClientGameEngine engine) throws InvalidIdentifierException;

    /**
     * Compute the result of this move, using the passed engine to compute it.
     * 
     * The result is then prepared for serialization and stored for sending to
     * clients.
     * 
     * @param engine
     * @throws InvalidIdentifierException
     */
    void computeResult(ServerGameEngine engine)
            throws InvalidIdentifierException;
}
