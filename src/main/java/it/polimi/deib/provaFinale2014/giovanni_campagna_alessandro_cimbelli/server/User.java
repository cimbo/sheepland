package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

/**
 * A user in the database.
 * 
 * A user has a username and password which are immutable and set when the user
 * is registered (created).
 * 
 * It also has a current player, which is the player (game/network entity) the
 * user is currently controlling - providing a link to the running game, if any.
 * 
 * This class is threadsafe.
 * 
 * @author Giovanni Campagna
 */
public class User {
    private final String username;
    private final String password;
    private ServerPlayer currentPlayer;

    /**
     * Construct a new user, with the given username and password.
     * 
     * This should be called only by the {@link UserDatabase}
     * 
     * @param username
     *            the username
     * @param password
     *            the password
     */
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Check that the given password is valid at login.
     * 
     * @param password
     *            the provided password
     * @throws InvalidPasswordException
     *             the password is not valid.
     */
    public void checkPassword(String password) throws InvalidPasswordException {
        if (!this.password.equals(password)) {
            throw new InvalidPasswordException();
        }
    }

    /**
     * Return the name of this user.
     * 
     * @return
     */
    public String getUserName() {
        return username;
    }

    /**
     * Return (canonicalize) the given player.
     * 
     * This method will atomically (with respect to the user) check if the user
     * is associated with a player, and if not, will associate it to the given
     * caller player (and return that)
     * 
     * @param caller
     *            the calling player
     * @return the canonical player for this user at this moment
     */
    public synchronized ServerPlayer getCurrentPlayer(ServerPlayer caller) {
        if (currentPlayer == null) {
            currentPlayer = caller;
        }

        return currentPlayer;
    }

    // For testing
    ServerPlayer getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Detach the given player from the user.
     * 
     * This method will atomically check that the given player is canonical for
     * the user, and if so will clear the canonical user, allowing a new
     * connection to become canonical in the future.
     * 
     * @param caller
     *            the calling player that wishes to detach
     */
    public synchronized void detachPlayer(ServerPlayer caller) {
        if (currentPlayer == caller) {
            currentPlayer = null;
        }
    }

    // For testing
    synchronized void detachPlayer() {
        currentPlayer = null;
    }
}
