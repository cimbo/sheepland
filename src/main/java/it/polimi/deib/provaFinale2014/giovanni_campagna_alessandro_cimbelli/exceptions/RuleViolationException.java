package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions;

/**
 * An exception indicating a generic violation of the game rules (for example
 * violating the requirements on consecutive moves, or changing shepherd during
 * the turn, or moving too far, or trying to spend money the player doesn't
 * have...)
 * 
 * @author Giovanni Campagna
 * 
 */
public class RuleViolationException extends InvalidMoveException {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new RuleViolationException with the given (human readable)
     * message.
     * 
     * @param message
     *            the exception message
     */
    public RuleViolationException(String message) {
        super(message);
    }
}
