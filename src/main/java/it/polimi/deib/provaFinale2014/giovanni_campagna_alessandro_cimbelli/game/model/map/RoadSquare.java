package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameObject;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;

import java.io.Serializable;

/**
 * Represents a square between two tiles, where a player or a fence can be.
 * 
 * @author Giovanni Campagna
 * 
 */
public class RoadSquare extends GameObject implements Serializable {
    private static final long serialVersionUID = 1;
    private Shepherd currentShepherd;
    private boolean hasFence;
    private boolean isFinal;
    private final int value;
    private SheepTile from;
    private SheepTile to;

    /**
     * Constructs a RoadSquare of value @v. The RoadSquare is initialized with
     * no player and no fence.
     * 
     * @param v
     *            the value of the square (the number that needs to be rolled by
     *            the wolf or
     */
    public RoadSquare(int v) {
        super();

        value = v;
        currentShepherd = null;
        hasFence = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RoadSquareIdentifier getId() {
        return (RoadSquareIdentifier) super.getId();
    }

    /**
     * Mark that this RoadSquare connects @st1 and @st2 This is a friendly
     * method only called by Graph. Note that the public API deals with origin
     * and destination tiles only because java does not allow multiple return
     * values. In reality, a RoadSquare is a bidirectional edge.
     * 
     * @param st1
     *            A SheepTile
     * @param st2
     *            Another, adjacent, SheepTile
     */
    void connect(SheepTile st1, SheepTile st2) {
        from = st1;
        to = st2;
    }

    SheepTile getOriginTile() {
        return from;
    }

    SheepTile getDestinationTile() {
        return to;
    }

    /**
     * Returns the "other end" of this RoadSquare, given one of the SheepTiles
     * connected to it.
     * 
     * @param which
     * @return
     */
    public SheepTile getMatchingTile(SheepTile which) {
        assert which == from || which == to;

        if (which == from) {
            return to;
        } else {
            return from;
        }
    }

    /**
     * Place the shepherd @s on this square.
     * 
     * @param s
     *            the new shepherd
     */
    public void addShepherd(Shepherd s) {
        if (hasFence) {
            throw new GameInvariantViolationException(
                    "Shepherd cannot be a on square with a fence");
        }
        if (currentShepherd != null) {
            throw new GameInvariantViolationException(
                    "Player cannot be on the same square as another one");
        }
        currentShepherd = s;
    }

    /**
     * Removes the shepherd @s from this square.
     * 
     * @s is provided only for invariant checking, and is otherwise unused.
     * 
     * @param s
     *            the shepherd which is expected to be on this tile.
     */
    public void removeShepherd(Shepherd s) {
        if (currentShepherd != s) {
            throw new GameInvariantViolationException(
                    "Shepherd is not currently on this square");
        }
        currentShepherd = null;
    }

    /**
     * Obtains the shepherd currently occupying this square, or null if the
     * square is empty (or has a fence)
     * 
     * @return
     */
    public Shepherd getCurrentShepherd() {
        return currentShepherd;
    }

    /**
     * Place a fence on this square. This must be called after removing the
     * player that triggered the fence building, or an exception is raised (but
     * do not count on that). Calling this method twice will also trigger an
     * exception (but again this is not part of the API contract). the boolean
     * isFinal indicate was placed a normal or a final fence.
     */
    public void placeFence(boolean isFinal) {
        if (hasFence) {
            throw new GameInvariantViolationException(
                    "Cannot place a fence on a square that already has one");
        }
        if (currentShepherd != null) {
            throw new GameInvariantViolationException(
                    "Cannot place a fence on a square with a shepherd");
        }
        hasFence = true;
        this.isFinal = isFinal;
    }

    /**
     * Checks if this square has a fence or not.
     * 
     * @return if the square has a fence
     */
    public boolean getHasFence() {
        return hasFence;
    }

    /**
     * 
     * Checks if this square has a final fence or not.
     * 
     * @return if the square has a final fence
     */
    public boolean getHasFinalFence() {
        assert isFinal ? hasFence : true;
        return isFinal;
    }

    /**
     * Obtains the "dice value" of this square, ie the value printed on the map
     * and that the wolf and black sheep need to make with a dice to cross this
     * square.
     * 
     * @return the dice value of this square
     */
    public int getDiceValue() {
        return value;
    }

    @Override
    public String toString() {
        return "RoadSquare connecting " + from + " to " + to;
    }

}
