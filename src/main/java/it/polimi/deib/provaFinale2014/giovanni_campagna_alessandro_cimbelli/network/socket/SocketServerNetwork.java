package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnectionHandler;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerNetwork;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The socket implementation of ServerNetwork
 * 
 * @author Alessandro Cimbelli
 */

public class SocketServerNetwork implements ServerNetwork, Runnable {
    /**
     * The default port of the server.
     */
    public static final int DEFAULT_PORT = 5000;

    private final SocketAddress listenAddress;
    private ServerConnectionHandler handler;
    private ServerSocket serverSocket;
    private boolean closed;

    /**
     * Construct a SocketServerNetwork listening on the given address.
     * 
     * @param listenAddress
     *            the address to listen on
     */
    public SocketServerNetwork(SocketAddress listenAddress) {
        this.listenAddress = listenAddress;
    }

    /**
     * Construct a SocketServerNetwork listening on all addresses on the default
     * port
     */
    public SocketServerNetwork() {
        listenAddress = new InetSocketAddress("0.0.0.0", DEFAULT_PORT);
    }

    /**
     * {@inheritDoc}
     */
    public void setConnectionHandler(ServerConnectionHandler h) {
        handler = h;
    }

    /**
     * {@inheritDoc}
     */
    public void run() {
        while (true) {
            ServerConnection c;

            try {
                Socket socket = serverSocket.accept();
                c = new SocketServerConnection(socket);
            } catch (SocketException e) {
                Logger.getGlobal().log(Level.FINE, "Server closed", e);
                break;
            } catch (IOException e) {
                Logger.getGlobal().log(Level.INFO,
                        "IO exception accepting new connection", e);
                continue;
            }

            // Here i'm sending to the handler the connection created with a
            // client
            handler.handleNewConnection(c);
        }

        synchronized (this) {
            closed = true;
            notify();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void startListening() throws IOException {
        serverSocket = new ServerSocket();
        serverSocket.bind(listenAddress);
        closed = false;
        new Thread(this).start();

        Logger.getGlobal().info("Server listening on " + listenAddress);
    }

    /**
     * {@inheritDoc}
     */
    public void stopListening() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            Logger.getGlobal()
                    .log(Level.FINE, "Error closing server socket", e);
        }

        synchronized (this) {
            while (!closed) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    Logger.getGlobal().log(Level.FINEST,
                            "Interrupted exception", e);
                }
            }
        }
    }
}
