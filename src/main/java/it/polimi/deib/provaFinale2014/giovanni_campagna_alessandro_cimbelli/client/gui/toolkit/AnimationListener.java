package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

/**
 * An interface to receive events from an animation.
 * 
 * @author Giovanni Campagna
 */
public interface AnimationListener {
    /**
     * Signals that the animation was completed.
     */
    void onComplete();
}
