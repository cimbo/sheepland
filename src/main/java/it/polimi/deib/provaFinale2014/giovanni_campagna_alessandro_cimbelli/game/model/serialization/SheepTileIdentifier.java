package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;

/**
 * Identifies a sheep tile.
 * 
 * The identifier for Sheepsburg is always 0. Identifiers for other regions are
 * built by adding 1 (which is used to disambiguate 0 being Sheepsburg) to 3
 * times the ordinal of the terrain type plus the subcode (between 0 and 2
 * inclusive).
 * 
 * This results in codes between 0 and 18 inclusive.
 * 
 * @author giovanni
 * 
 */
public class SheepTileIdentifier extends GameObjectIdentifier {
    private static final long serialVersionUID = 1L;

    /**
     * Create a sheep tile identifier for a given tile code.
     */
    public SheepTileIdentifier(int code) {
        super(SheepTile.class, code);
    }
}
