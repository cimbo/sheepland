package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.io.IOException;
import java.util.Map;

/**
 * The interface to send commands from the server to the client.
 * 
 * @author Giovanni Campagna
 */
public interface ServerSender {
    /**
     * Send a "setup" command.
     * 
     * This command is sent when the game is about to start, after all players
     * have joined.
     * 
     * @param rules
     *            the rules of the game
     * @param board
     *            the fully initialized game board
     * @param self
     *            the identifier of the self player
     */
    void sendSetup(RuleSet rules, GameBoard board, PlayerIdentifier self)
            throws IOException;

    /**
     * Send a "move acknowledged" command.
     * 
     * This command is sent after executing a move command ("move shepherd",
     * "move sheep", "buy terrain card").
     * 
     * If the server was able to verify it successfully, result will be null,
     * otherwise it will be the exception that was internally generated by the
     * game engine.
     * 
     * @param result
     *            null, if no error, or an error if the move failed.
     */
    void sendMoveAcknowledged(Exception result) throws IOException;

    /**
     * Send a "force synchronize" command.
     * 
     * This command is sent after the client asked for synchronization
     * explicitly (for example due to a failure), or after the client
     * reconnected to a running game.
     * 
     * It contains the full state of the game, enough for the client to recover
     * any missed command.
     */
    void sendForceSynchronize(RuleSet rules, GameBoard newBoard,
            PlayerIdentifier self, GameState newState) throws IOException;

    /**
     * Send a "advance next turn" command.
     * 
     * This command is sent after any player executed the last move of his turn,
     * and after sending out all actions that happen between turns (moving the
     * black sheep and the wolf, growing the lambs).
     */
    void sendAdvanceNextTurn(int turn, GamePhase phase) throws IOException;

    /**
     * Send a "black sheep moved" command.
     * 
     * This command is sent after the server determined the next random position
     * of the black sheep. The meaning of the arguments is as usual.
     */
    void sendBlackSheepMoved(SheepTileIdentifier destination)
            throws IOException;

    /**
     * Send a "wolf moved" command.
     * 
     * This command is sent after the server determined the next random position
     * of the wolf. The meaning of the arguments is as usual.
     */
    void sendWolfMoved(SheepTileIdentifier destination,
            SheepIdentifier eatenSheep) throws IOException;

    /**
     * Send a "lambs grew" command.
     * 
     * This command is sent after the server determined the new adult sheep
     * count for the tile. The meaning of the arguments is as usual.
     */
    void sendLambsGrew(SheepTileIdentifier where, Sheep[] newAdultSheep)
            throws IOException;

    /**
     * Send a "did execute move" command.
     * 
     * This command is sent after another player executed a "execute move"
     * command, and the server verified it successfully.
     * 
     * @param move
     *            the move that was executed (with all the necessary details)
     */
    void sendDidExecuteMove(Move move) throws IOException;

    /**
     * Send a "login answer" command.
     * 
     * This command is sent to indicate the result of a login attempt
     * 
     * @param answer
     *            true, if login succeeded, false otherwise
     */
    void sendLoginAnswer(boolean answer) throws IOException;

    /**
     * Send a "ping" command.
     * 
     * This command is periodically sent by the server to verify that the client
     * is still connected.
     */
    void sendPing() throws IOException;

    /**
     * Send a "end game" command.
     * 
     * This command is sent at the end of the game. The only valid operation on
     * the client after receiving this command is to close the connection.
     * 
     * @param scores
     *            the final scores
     */
    void sendEndGame(Map<PlayerIdentifier, Integer> scores) throws IOException;

    /**
     * Send a "player left" command.
     * 
     * This command is sent after an other player disconnected or abandoned the
     * game.
     * 
     * @param who
     *            the player that left the game
     * @param clean
     *            true, if the player abandoned the game on purpose, false
     *            otherwise (if false, the player might rejoin the game at a
     *            later time)
     */
    void sendPlayerLeft(PlayerIdentifier who, boolean clean) throws IOException;

    /**
     * Send a "player joined" command.
     * 
     * This command is sent after another player that was previously
     * disconnected rejoined the game.
     * 
     * @param who
     *            the player that rejoined the game
     */
    void sendPlayerJoined(PlayerIdentifier who) throws IOException;

    /**
     * Send a "abandon complete" command.
     * 
     * This command is sent after abandoning the game.
     * 
     * It signals that the connection can now be safely closed.
     * 
     * It is a protocol error for the client to close the connection before
     * receiving this command (and may result in other players not getting
     * appropriate notification of leaving).
     */
    void sendAbandonComplete() throws IOException;
}
