package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

/**
 * The superclass of all exceptions caused by faulty server behavior (or by a
 * client that got out of sync with the server)
 * 
 * @author Giovanni Campagna
 * 
 */
public class ServerException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new ServerException with the given message and cause
     * 
     * @param message
     *            the message (that will be appended to a default prefix)
     * @param origin
     *            the cause of the exception
     */
    public ServerException(String message, Exception origin) {
        super("Detected unexpected server behavior: " + message, origin);
    }

    /**
     * Construct a new ServerException with a default message and the given
     * cause
     * 
     * @param origin
     *            the inner exception
     */
    public ServerException(Exception origin) {
        super("Detected unexpected server behavior: " + origin.getMessage(),
                origin);
    }
}
