package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

/**
 * An exception indicating that a user command could not be parsed.
 * 
 * @author Giovanni Campagna
 * 
 */
class InvalidInputException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a new InvalidInputException for the given invalid command or
     * fragment.
     * 
     * @param command
     *            the command element that triggered the error
     */
    public InvalidInputException(String command) {
        super("Cannot recognize command " + command);
    }

    /**
     * Construct a new InvalidInputException for the given cause
     * 
     * @param cause
     *            the inner exception
     */
    public InvalidInputException(Exception cause) {
        super("Cannot recognize command: " + cause.getMessage(), cause);
    }
}
