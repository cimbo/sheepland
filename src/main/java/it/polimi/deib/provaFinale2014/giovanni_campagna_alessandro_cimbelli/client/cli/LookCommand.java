package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;

/**
 * The "look" command, gives the user information on the current state of the
 * game.
 * 
 * @author Giovanni Campagna
 * 
 */
class LookCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("look [<SUBCOMMAND>]: look around");
        ui.showMessage("Without <SUBCOMMAND>, it will show who you are and where you are");
        ui.showMessage("\"look inventory\" will show you your cards and coins");
        ui.showMessage("\"look shepherd\" will show your shepherd(s) and his surroundings");
        ui.showMessage("\"look map [<REGION>]\" will show you the entire map, or a specific region");
        ui.showMessage("\"look emporium\" will show you the items you can buy");
        ui.showMessage("\"look opponents\" will show your opponents, their coins and cards");
        ui.showMessage("\"look market\" will show you the cards on sale at the market");
        baseHelpRegion(ui);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, InvalidIdentifierException {
        if (tokenizer.hasNext()) {
            LookView.lookAround(ui, tokenizer, tokenizer.readLookSubCommand());
        } else {
            LookView.lookAround(ui);
        }
    }
}
