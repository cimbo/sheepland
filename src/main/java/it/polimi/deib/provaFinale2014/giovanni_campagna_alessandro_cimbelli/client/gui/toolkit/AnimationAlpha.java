package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

/**
 * An interface for easing functions (adapters that modulate conversion from
 * normalized time (0.0 to 1.0) into normalized space, according to a given
 * acceleration behavior)
 * 
 * @author Giovanni Campagna
 * 
 */
public interface AnimationAlpha {
    /**
     * A linear acceleration function.
     */
    static AnimationAlpha LINEAR = new AnimationAlpha() {
        /**
         * {@inheritDoc}
         */
        public double ease(double linearProgress) {
            return linearProgress;
        }
    };

    /**
     * A decelerating quadratic acceleration function.
     * 
     * This is the recommended behavior for most animation, as it gives the
     * impression of speed while focusing on the final state (which is likely to
     * be more interesting to the user)
     */
    static AnimationAlpha EASE_OUT_QUAD = new AnimationAlpha() {
        /**
         * {@inheritDoc}
         */
        public double ease(double linearProgress) {
            return -linearProgress * (linearProgress - 2);
        }
    };

    /**
     * Apply the acceleration function
     * 
     * @param linearProgress
     *            the time progress (between 0.0 and 1.0)
     * @return the space progress (with 0.0 being the beginning and 1.0 the end
     *         of the path, but values outside are also allowed, to give a
     *         bouncy effect)
     */
    double ease(double linearProgress);
}
