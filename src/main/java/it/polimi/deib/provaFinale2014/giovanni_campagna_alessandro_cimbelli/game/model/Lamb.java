package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;

import java.io.Serializable;

/**
 * This class represents a lamb.
 * 
 * A lamb is a particular sheep that has the ability to grow into an adult sheep
 * after some time in the game. As such, the lamb has an age (in the number of
 * rounds).
 * 
 * @author Giovanni Campagna
 */
public class Lamb extends Sheep implements Serializable {
    /**
     * The age (in rounds) at which the lamb should become an adult.
     */
    public static final int GROWING_AGE = 2;

    private static final long serialVersionUID = 1L;

    private int age;

    /**
     * Construct a new lamb.
     * 
     * The lamb is 0 rounds old.
     */
    public Lamb() {
        this(0);
    }

    /**
     * Construct a new lamb with a specific age.
     * 
     * @param age
     *            the age of the lamb
     */
    public Lamb(int age) {
        super(SheepType.LAMB);
        this.age = age;
    }

    /**
     * @return the age of the lamb
     */
    public int getAge() {
        return age;
    }

    /**
     * Increment the age of the lamb
     * 
     * This method will fail if the lamb is already at its growing age.
     */
    public void grow() {
        if (age >= GROWING_AGE) {
            throw new GameInvariantViolationException(
                    "Cannot grow lambs over their growing age");
        }

        age++;
    }
}
