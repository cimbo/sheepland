package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * This class represents a "mate sheep" move. See the base class {@link Move}
 * for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class MateSheepMove extends ShepherdMove implements CompletableMove {
    private static final long serialVersionUID = 1L;

    private final SheepTileIdentifier tileId;
    private Sheep newLamb;

    /**
     * Construct a new "mate sheep" move
     * 
     * @param who
     *            the player executing the move
     * @param which
     *            the shepherd he is using
     * @param tile
     *            the tile where the operation is happening
     */
    public MateSheepMove(PlayerIdentifier who, ShepherdIdentifier which,
            SheepTileIdentifier tile) {
        super(who, which);
        tileId = tile;
    }

    /**
     * Retrieve the actual tile object, by resolving the identifier through the
     * passed in engine.
     * 
     * @param engine
     * @return the tile as game object
     * @throws InvalidIdentifierException
     */
    protected SheepTile getTile(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getSheepTileFromId(tileId);
    }

    /**
     * Retrieve the newly born lamb.
     * 
     * @return the new sheep as game object
     */
    protected Sheep getResult() {
        return newLamb;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.mateSheep(getWho(engine), getWhich(engine), getTile(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void computeResult(ServerGameEngine engine)
            throws InvalidIdentifierException {
        newLamb = engine.realMateSheep(getWhich(engine), getTile(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void complete(ClientGameEngine engine)
            throws InvalidIdentifierException {
        engine.completeMateSheep(getTile(engine), getResult());
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + tileId.hashCode();
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof MateSheepMove)) {
            return false;
        }
        MateSheepMove other = (MateSheepMove) obj;
        if (!tileId.equals(other.tileId)) {
            return false;
        }
        return true;
    }
}