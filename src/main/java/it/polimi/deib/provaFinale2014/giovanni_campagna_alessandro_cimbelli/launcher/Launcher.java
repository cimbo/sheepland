package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.launcher;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.ClientLauncher;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.CommandLineParseException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server.ServerLauncher;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The entry point of the Sheepland package (client, server and installer).
 * Discriminates between the client and the server application according to the
 * first command line argument.
 * 
 * @author Giovanni Campagna
 * 
 */
public class Launcher {
    private Launcher() {
    }

    private static void launchServer(String[] args)
            throws CommandLineParseException {
        int cutArg = 1;
        ServerLauncher launcher = new ServerLauncher(Arrays.copyOfRange(args,
                cutArg, args.length));
        launcher.run();
    }

    private static void launchClient(String[] args)
            throws CommandLineParseException {
        int cutArg = args.length >= 1 ? 1 : 0;
        ClientLauncher launcher = new ClientLauncher(Arrays.copyOfRange(args,
                cutArg, args.length));
        launcher.run();
    }

    private static void launchAny(String[] args)
            throws CommandLineParseException {
        if (args.length >= 1 && "--server".equals(args[0])) {
            launchServer(args);
        } else if (args.length == 0 || "--client".equals(args[0])) {
            launchClient(args);
        } else {
            throw new CommandLineParseException("Invalid launch command "
                    + args[0]);
        }
    }

    /**
     * The entry point of every Java application.
     * 
     * @param args
     *            command line arguments.
     */
    public static void main(String[] args) {
        try {
            launchAny(args);
        } catch (CommandLineParseException e) {
            Logger.getGlobal().setLevel(Level.SEVERE);
            Logger.getGlobal().log(Level.SEVERE,
                    "Failed to parse command line: " + e.getMessage(), e);
        }
    }

}
