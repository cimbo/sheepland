package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import java.io.Closeable;

/**
 * An interface representing the server-side of a connection.
 * 
 * @author Giovanni Campagna
 */
public interface ServerConnection extends Closeable {
    /**
     * Set the server receiver for this connection.
     * 
     * This will also enable and start feeding input. Because access to the
     * input thread(s) is not synchronized (and cannot be), calls to this method
     * after the first time MUST happen from an input thread, that is,
     * synchronously from within a call to a ServerReceiver method.
     * 
     * @param receiver
     *            the ServerReceiver handling input on this connection
     */
    void setServerReceiver(ServerReceiver receiver);

    /**
     * Retrieve the server sender, which is the writing end of this server
     * connection.
     * 
     * @return the server sender
     */
    ServerSender getServerSender();

    /**
     * Close the connection.
     * 
     * Closing the connection will never fail.
     */
    void close();
}
