package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * A collection of helpers for representing objects in the UI. It does the
 * opposite of Tokenizer.
 * 
 * @author Giovanni Campagna
 * 
 */
public class Representation {
    private Representation() {
    }

    /**
     * Turn a player into a human readable string that identifies him.
     * 
     * @param player
     *            a player
     * @return a human readable representation
     */
    public static String playerToString(Player player) {
        return player.getName();
    }

    private static String sheepTileToString(int code) {
        if (code == 0) {
            return "sheepsburg";
        } else {
            return TerrainType.values()[(code - 1) / 3].name().toLowerCase()
                    + "/" + Integer.toString((code - 1) % 3);
        }
    }

    /**
     * Turn a sheep tile into a human readable string that identifies it.
     * 
     * @param tile
     *            a sheep tile
     * @return a human readable representation
     */
    public static String sheepTileToString(SheepTile tile) {
        return sheepTileToString(tile.getId().getContent());
    }

    /**
     * Turn a road square into a human readable string that identifies it.
     * 
     * @param square
     *            a road square
     * @return a human readable representation
     */
    public static String roadSquareToString(RoadSquare square) {
        RoadSquareIdentifier id = square.getId();
        return "between " + sheepTileToString(id.getContent() & 0xFF) + " and "
                + sheepTileToString(id.getContent() >> 8);
    }

    /**
     * Turn a shepherd into a human readable string that identifies it.
     * 
     * @param id
     *            a shepherd identifier
     * @param capitalize
     *            if the string should be capitalized
     * @return a human readable representation
     */
    public static String shepherdToString(ShepherdIdentifier id,
            boolean capitalize) {
        return (capitalize ? "S" : "s") + "hepherd " + (id.getContent() + 1);
    }

    /**
     * Turn a shepherd into a human readable string that identifies it.
     * 
     * @param shepherd
     *            a shepherd
     * @param capitalize
     *            if the string should be capitalized
     * @return a human readable representation
     */
    public static String shepherdToString(Shepherd shepherd, boolean capitalize) {
        return shepherdToString(shepherd.getId(), capitalize);
    }

    /**
     * Turn a shepherd into a human readable string that identifies it.
     * 
     * @param id
     *            a shepherd identifier
     * @return a human readable representation
     */
    public static String shepherdToString(ShepherdIdentifier id) {
        return shepherdToString(id, false);
    }

    /**
     * Turn a shepherd into a human readable string that identifies it.
     * 
     * @param shepherd
     *            a shepherd identifier
     * @return a human readable representation
     */
    public static String shepherdToString(Shepherd shepherd) {
        return shepherdToString(shepherd, false);
    }

    /**
     * Turn a sheep type into a human readable string that identifies it.
     * 
     * @param type
     *            a sheep type
     * @return a human readable representation
     */
    public static String sheepTypeToString(SheepType type) {
        return type.name().toLowerCase().replace('_', ' ');
    }

    /**
     * Turn a sheep into a human readable string that identifies it.
     * 
     * @param sheep
     *            a sheep
     * @return a human readable representation
     */
    public static String sheepToString(Sheep sheep) {
        if (!(sheep instanceof Lamb)) {
            return sheepTypeToString(sheep.getType());
        } else {
            return ((Lamb) sheep).getAge() + " round old lamb";
        }
    }

    /**
     * Turn a terrain type into a human readable string that identifies it.
     * 
     * @param type
     *            a terrain type
     * @return a human readable representation
     */
    public static String terrainTypeToString(TerrainType type) {
        return type.name().toLowerCase();
    }

    /**
     * Turn a terrain card into a human readable string that identifies it.
     * 
     * @param card
     *            a terrain card
     * @return a human readable representation
     */
    public static String terrainCardToString(TerrainCard card) {
        return terrainTypeToString(card.getType());
    }
}
