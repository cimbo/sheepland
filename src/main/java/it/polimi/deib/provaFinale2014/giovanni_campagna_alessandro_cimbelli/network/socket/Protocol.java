package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

/**
 * The identifier of the protocol version in the command header.
 * 
 * @author Alessandro Cimbelli
 */
public enum Protocol {

    V1((byte) 1);

    private byte version;

    private Protocol(byte version) {
        this.version = version;
    }

    public byte getVersion() {
        return version;
    }

}
