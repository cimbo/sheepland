package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * A graph-like object representing the map of the game. The state lives on the
 * SheepTiles (nodes of the graph) and on the RoadSquares (edges of the graph),
 * but should be accessed only through the public API of GameMap.
 * 
 * @author Giovanni Campagna
 * 
 */
public class GameMap implements Serializable {
    private static final long serialVersionUID = 1L;

    private final Graph graph;
    private SheepTile blackSheepTile;
    private SheepTile wolfTile;

    /**
     * Construct a new game map.
     */
    public GameMap() {
        this(new Graph());
    }

    // For testing
    GameMap(Graph g) {
        graph = g;
        blackSheepTile = null;
        wolfTile = null;
    }

    /**
     * Retrieves the tile representing Sheepsburg.
     * 
     * @return sheepsburg
     */
    public SheepTile getSheepsburg() {
        return graph.getRoot();
    }

    /**
     * Retrieves any random sheep tile (but sheepsburg).
     * 
     * @param dice
     *            the random generator to use
     * @return a random non-sheepsburg tile
     */
    public SheepTile getRandomSheepTile(Random dice) {
        SheepTile[] tiles = graph.getSheepTiles();
        return tiles[dice.nextInt(tiles.length)];
    }

    /**
     * Retrieves the tile matching the passed id (which needs to be equal to the
     * id of some sheep tile, but need not be a SheepTileIdentifier)
     * 
     * @param id
     *            the identifier
     * @return the game object
     * @throws InvalidIdentifierException
     *             no such object could be retrieved
     */
    public SheepTile getSheepTileFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        return graph.getSheepTileFromId(id);
    }

    /**
     * Retrieves the square matching the passed id (which needs to be equal to
     * the id of some road square, but need not be a RoadSquareIdentifier)
     * 
     * @param id
     *            the identifier
     * @return the road square
     * @throws InvalidIdentifierException
     *             no such object could be retrieved
     */
    public RoadSquare getRoadSquareFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        return graph.getRoadSquareFromId(id);
    }

    /**
     * Move the black sheep to the specified position
     * 
     * @param to
     *            the tile where the black sheep will be next (must not be null)
     */
    public void moveBlackSheep(Sheep blackSheep, SheepTile to) {
        if (blackSheepTile != null) {
            blackSheepTile.removeSheep(blackSheep);
        }
        to.addSheep(blackSheep);
        blackSheepTile = to;
    }

    /**
     * Retrieve the current black sheep position (or null if the black sheep was
     * not positioned yet)
     * 
     * @return the current black sheep position
     */
    public SheepTile getCurrentBlackSheepPosition() {
        return blackSheepTile;
    }

    /**
     * Move the wolf to the specified position
     * 
     * @param to
     *            the tile where the wolf will be next (must not be null)
     */
    public void moveWolf(SheepTile to) {
        if (wolfTile != null) {
            wolfTile.removeWolf();
        }
        to.addWolf();
        wolfTile = to;
    }

    /**
     * Retrieve the current wolf position (or null if the wolf was not
     * positioned yet)
     * 
     * @return the current wolf position
     */
    public SheepTile getCurrentWolfPosition() {
        return wolfTile;
    }

    /**
     * Return the RoadSquare with value @value adjacent to the SheepTile st, if
     * one exists, and null otherwise. If multiple such RoadSquares exist, it is
     * undefined which one is returned.
     * 
     * @param st
     *            the sheep tile to check
     * @param value
     *            the looked for dice value
     * @return a RoadSquare with dice value @value, or null
     */
    public RoadSquare getAdjacentSquareForValue(SheepTile st, int value) {
        List<Edge> neigh = st.getNeighbors();

        for (Edge e : neigh) {
            RoadSquare rs = e.getRoadSquare();

            if (rs.getDiceValue() == value) {
                return rs;
            }
        }

        return null;
    }

    /**
     * Returns all RoadSquares adjacent to the SheepTile st
     * 
     * @param st
     *            a SheepTile in the Map
     * @return
     */
    public RoadSquare[] getAllAdjacentSquares(SheepTile st) {
        List<Edge> neigh = st.getNeighbors();
        RoadSquare[] array = new RoadSquare[neigh.size()];

        for (int i = 0; i < neigh.size(); i++) {
            array[i] = neigh.get(i).getRoadSquare();
        }

        return array;
    }

    /**
     * Return all the tiles adjacent to @st (connected by one and exactly one
     * RoadSquare)
     * 
     * @param st
     *            a SheepTile to check
     * @return
     */
    public SheepTile[] getAllAdjacentTiles(SheepTile st) {
        List<Edge> neigh = st.getNeighbors();
        SheepTile[] array = new SheepTile[neigh.size()];

        for (int i = 0; i < neigh.size(); i++) {
            array[i] = neigh.get(i).getDestination();
        }

        return array;
    }

    /**
     * Returns an array of two elements, the two tiles that share the RoadSquare
     * rs. The order of the two tiles is unspecified.
     * 
     * @param rs
     * @return
     */
    public SheepTile[] getIncidentTiles(RoadSquare rs) {
        SheepTile[] array = new SheepTile[2];
        array[0] = rs.getOriginTile();
        array[1] = rs.getDestinationTile();

        return array;
    }

    /**
     * Check if @st1 and @st2 are adjacent tiles (that is, they are connected by
     * one and exactly one RoadSquare)
     * 
     * @param st1
     * @param st2
     * @return
     */
    public boolean areTilesAdjacent(SheepTile st1, SheepTile st2) {
        List<Edge> neigh = st1.getNeighbors();

        for (Edge e : neigh) {
            if (e.getDestination() == st2) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retrieve the list of all road squares in the map.
     * 
     * The list must not be modified by the caller.
     * 
     * This method is O(1)
     * 
     * @return the list of squares
     */
    public Collection<RoadSquare> getAllRoadSquares() {
        return graph.getRoadSquares();
    }

    /**
     * Retrieves the list of all road squares adjacent to a given square.
     * 
     * The list must not be modified by the caller.
     * 
     * This method is linear in the number of all roads.
     * 
     * @param reference
     *            the interesting square
     * @return the list of adjacent squares
     */
    // FIXME the complexity of this method is bad, and you should feel bad
    public Collection<RoadSquare> getAllAdjacentRoadSquares(RoadSquare reference) {
        List<RoadSquare> result = new ArrayList<RoadSquare>();

        for (RoadSquare square : graph.getRoadSquares()) {
            if (areSquaresAdjacent(square, reference)) {
                result.add(square);
            }
        }

        return result;
    }

    /**
     * Check if @rs1 and @rs2 are adjacent squares (that is, they are connected
     * through a road on the map)
     * 
     * @param rs1
     *            a RoadSquare
     * @param rs2
     *            another RoadSquare
     * @return true if the two RoadSquares are directly connected, and false
     *         otherwise
     */
    public boolean areSquaresAdjacent(RoadSquare rs1, RoadSquare rs2) {
        /*
         * We don't represent roads in memory (because doing so would introduce
         * a lot of redundancy and would make the hardcoded map completely
         * unreadable and untestable). Instead, we check if two squares are
         * adjacent (squares) by checking that they have a tile in common, and
         * the other two are adjacent (tiles) This results in a little of
         * complication here, because we don't know which of the various tiles
         * is the shared one, so have to check 2 * 2 combinations.
         * 
         * The number in variable names refers to the road square, from/to is
         * the RS field, so comparisons should always be between a *1 and a *2.
         */
        SheepTile from1, to1, from2, to2;

        from1 = rs1.getOriginTile();
        to1 = rs1.getDestinationTile();
        from2 = rs2.getOriginTile();
        to2 = rs2.getDestinationTile();

        if (from1 == from2) {
            return areTilesAdjacent(to1, to2);
        } else {
            if (from1 == to2) {
                return areTilesAdjacent(to1, from2);
            } else {
                if (to1 == from2) {
                    return areTilesAdjacent(from1, to2);
                } else {
                    if (to1 == to2) {
                        return areTilesAdjacent(from1, from2);
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    /**
     * Checks if a RoadSquare square is adjacent to a SheepTile tile (ie, it is
     * used to connect this SheepTile to other neighbor tiles)
     * 
     * @param tile
     * @param square
     * @return
     */
    public boolean isSquareAdjacentToTile(SheepTile tile, RoadSquare square) {
        for (Edge e : tile.getNeighbors()) {
            if (e.getRoadSquare() == square) {
                return true;
            }
        }

        return false;
    }

    /**
     * Computes the score values of all the tiles in the map (1 for each white
     * sheep, 2 for the black sheep), aggregated by terrain type. The inner
     * product of this vector with the vector of terrain cards held by the
     * player, plus his remaining coins, will give his final score.
     * 
     * @return the score values of the map
     */
    public int[] getScoreValues() {
        ScoreVisitor visitor = new ScoreVisitor();

        graph.depthFirstVisit(visitor);

        return visitor.getScoreValues();
    }

    /**
     * Execute a generic visit on the map.
     * 
     * The order of visit is implementation defined and should not be relied
     * upon.
     */
    public void forEachTile(GraphVisitor visitor) {
        graph.depthFirstVisit(visitor);
    }
}
