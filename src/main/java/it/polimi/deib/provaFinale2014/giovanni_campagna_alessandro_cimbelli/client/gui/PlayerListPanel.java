package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import java.awt.Color;
import java.awt.Font;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * An overlay panel that holds a list of players and numbers, used by the
 * killing operation and by the final game over screen.
 * 
 * @author Alessandro Cimbelli
 */
public class PlayerListPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 527;
    private static final int Y_SIZE = 150;

    /**
     * Construct a new player list panel, with the given player icons and the
     * given primary text.
     * 
     * @param players
     *            the player icons to show on this panel
     * @param text
     *            the title of this panel
     */
    public PlayerListPanel(Map<PlayerIcon, Integer> players, String text) {
        setBackground(new Color(21, 170, 255));
        this.setSize(X_SIZE, Y_SIZE);
        setLayout(null);

        JLabel label = new JLabel(text);
        label.setSize(X_SIZE, Y_SIZE - 5);
        label.setLocation(0, 5);
        label.setFont(new Font("Arial", Font.BOLD, 16));
        label.setVerticalAlignment(SwingConstants.TOP);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        add(label);

        int n = players.size() * 2;
        double space = (double) (X_SIZE - n * 65) / (n + 1);

        int i = 0;
        for (Map.Entry<PlayerIcon, Integer> e : players.entrySet()) {
            this.add(e.getKey());
            e.getKey().setLocation((int) ((i + 1) * space + i * 65),
                    (int) ((Y_SIZE - e.getKey().getHeight()) * .5));
            e.getKey().updateMoney(e.getValue());
            i++;
        }
    }
}
