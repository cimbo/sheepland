package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

/**
 * The class holding all the state of the UI state machine.
 * 
 * @author Alessandro Cimbelli
 * 
 */
public class GraphicPhase {

    private boolean waiting;
    private int freezeCount;
    private GraphicMove phase;

    /**
     * Construct a new graphic phase.
     * 
     * The phase is initially null, and the state machine is not frozen or
     * waiting.
     */
    public GraphicPhase() {
        phase = GraphicMove.NULL;
        waiting = false;
        freezeCount = 0;
    }

    /**
     * Retrieve the current phase of the state machine.
     * 
     * @return the current phase
     */
    public GraphicMove getPhase() {
        return phase;
    }

    /**
     * Jump to the given state machine phase.
     * 
     * @param phase
     *            the new phase
     */
    public void setPhase(GraphicMove phase) {
        this.phase = phase;
    }

    /**
     * Freeze the state machine, preventing user interaction.
     * 
     * An equal number of thaw() as of freeze() need to happen for interaction
     * to be recognized again.
     */
    public void freeze() {
        freezeCount++;
    }

    /**
     * Thaw the state machine, restoring the ability to interact with the user.
     * 
     * If the thaw() is not matching a previous freeze(), an exception is
     * generated.
     */
    public void thaw() {
        if (freezeCount == 0) {
            throw new IllegalStateException("Unmatched thaw");
        }

        freezeCount--;
    }

    /**
     * Check if the state machine is currently frozen, either because frozen
     * explicitly or because in the waiting state.
     * 
     * @return
     */
    public boolean isFrozen() {
        return waiting || freezeCount > 0;
    }

    /**
     * Set the waiting flag on the state machine.
     * 
     * This has the same effect as calling freeze/thaw, but it does not
     * cumulate.
     * 
     * @param b
     *            the waiting flag.
     */
    public void setWaiting(boolean b) {
        waiting = b;
    }
}
