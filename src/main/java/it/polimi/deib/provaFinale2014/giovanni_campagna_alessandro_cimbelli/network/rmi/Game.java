package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The RMI interface implemented by the server for a specific client.
 * 
 * @author Alessandro Cimbelli
 */
public interface Game extends Remote {
    /**
     * Send/Handle an "abandon game" command.
     * 
     * This command is sent when then client wishes to abandon the current game.
     */
    void abandonGame() throws RemoteException;

    /**
     * Send/Handle a "force synchronize me" command.
     * 
     * This command is sent when the client recognized to be out of sync and
     * wants a full resynchronization with the server.
     */
    void forceSynchronizeMe() throws RemoteException;

    /**
     * Send/Handle a "execute move" command.
     * 
     * This command is sent when the client wishes to execute a game move.
     * 
     * @param move
     *            the move to execute.
     */
    void executeMove(Move move) throws RemoteException;

    /**
     * Send/Handle a "login to server" command.
     * 
     * This command is sent immediately after connection, to identify the client
     * to the server.
     * 
     * @param username
     *            the username
     * @param password
     *            the password
     * @throws IOException
     */
    void loginToServer(String username, String password) throws RemoteException;

    /**
     * Send a "pong" command.
     * 
     * This command is sent in reply to a "ping" command.
     * 
     * @throws IOException
     */
    void pong() throws RemoteException;
}
