package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

/**
 * This class is responsible for holding the colored map, which is the trick
 * used to recognize roads and regions, and their centers.
 * 
 * @author Alessandro Cimbelli
 */
public class ColorGameBoard {
    private static final String IMAGE_PATH = "ColorGameBoard.png";
    private final BufferedImage imgColored;
    private static final int X_SIZE = 527;
    private static final int Y_SIZE = 700;
    private final Map<Color, SheepTileIdentifier> mapTile = new HashMap<Color, SheepTileIdentifier>();
    private final Map<Color, RoadSquareIdentifier> mapRoad = new HashMap<Color, RoadSquareIdentifier>();
    private final Map<RoadSquareIdentifier, Point> centerRoadMap = new HashMap<RoadSquareIdentifier, Point>();
    private final Map<SheepTileIdentifier, Point> animalTilePosition = new HashMap<SheepTileIdentifier, Point>();

    /**
     * Construct a new color game board.
     * 
     * @throws IOException
     *             if the image failed to load
     */
    public ColorGameBoard() throws IOException {
        imgColored = loadImage(IMAGE_PATH);
        fillMapTile();
        fillMapRoadSquare();
        fillCenterRoadMap();
        fillHashMapAnimalPosition();
    }

    /**
     * Return the center of the given road.
     * 
     * @param rsId
     *            the road
     * @return the center position
     */
    public Point getRoadCenter(RoadSquareIdentifier rsId) {
        return centerRoadMap.get(rsId);
    }

    /**
     * Return the center of the given region (that is, the point around which
     * animals are positioned)
     * 
     * @param tileId
     *            the region
     * @return the center position
     */
    public Point getTileCenter(SheepTileIdentifier tileId) {
        return animalTilePosition.get(tileId);
    }

    private void placeRoadSquare(Map<RoadSquareIdentifier, Point> map, int st1,
            int st2, int x, int y) {
        map.put(new RoadSquareIdentifier(st1, st2), new Point(
                (int) (X_SIZE / (527.0 / x)), (int) (Y_SIZE / (700.0 / y))));
    }

    private void fillHashMapAnimalPosition() {
        animalTilePosition.put(new SheepTileIdentifier(0), new Point(
                (int) (X_SIZE / 1.882142857), (int) (Y_SIZE / 2.180685358)));
        animalTilePosition.put(new SheepTileIdentifier(1), new Point(
                (int) (X_SIZE / 6.057471264), (int) (Y_SIZE / 4.794520548)));
        animalTilePosition.put(new SheepTileIdentifier(2), new Point(
                (int) (X_SIZE / (X_SIZE / 140.0)),
                (int) (Y_SIZE / (Y_SIZE / 321.0))));
        animalTilePosition.put(new SheepTileIdentifier(3), new Point(
                (int) (X_SIZE / 2.533653846), (int) (Y_SIZE / 1.785714286)));
        animalTilePosition.put(new SheepTileIdentifier(4), new Point(
                (int) (X_SIZE / 4.284552846), (int) (Y_SIZE / 1.535087719)));
        animalTilePosition.put(new SheepTileIdentifier(5), new Point(
                (int) (X_SIZE / 2.759162304), (int) (Y_SIZE / 1.340996169)));
        animalTilePosition.put(new SheepTileIdentifier(6), new Point(
                (int) (X_SIZE / 1.862190813), (int) (Y_SIZE / 1.565995526)));
        animalTilePosition.put(new SheepTileIdentifier(7), new Point(
                (int) (X_SIZE / 1.43989071), (int) (Y_SIZE / 1.842105263)));
        animalTilePosition.put(new SheepTileIdentifier(8), new Point(
                (int) (X_SIZE / 1.43989071), (int) (Y_SIZE / 1.335877863)));
        animalTilePosition.put(new SheepTileIdentifier(9), new Point(
                (int) (X_SIZE / 1.959107807), (int) (Y_SIZE / 1.219512195)));
        animalTilePosition.put(new SheepTileIdentifier(10), new Point(
                (int) (X_SIZE / 2.5215311), (int) (Y_SIZE / 4.320987654)));
        animalTilePosition.put(new SheepTileIdentifier(11), new Point(
                (int) (X_SIZE / 2.497630332), (int) (Y_SIZE / 2.681992337)));
        animalTilePosition.put(new SheepTileIdentifier(12), new Point(
                (int) (X_SIZE / 1.497159091), (int) (Y_SIZE / 7.865168539)));
        animalTilePosition.put(new SheepTileIdentifier(13), new Point(
                (int) (X_SIZE / 1.248815166), (int) (Y_SIZE / 5.34351145)));
        animalTilePosition.put(new SheepTileIdentifier(14), new Point(
                (int) (X_SIZE / 1.745033113), (int) (Y_SIZE / 3.181818182)));
        animalTilePosition.put(new SheepTileIdentifier(15), new Point(
                (int) (X_SIZE / 1.160792952), (int) (Y_SIZE / 3.271028037)));
        animalTilePosition.put(new SheepTileIdentifier(16), new Point(
                (int) (X_SIZE / (527.0 / 367.0)),
                (int) (Y_SIZE / (700.0 / 272))));
        animalTilePosition.put(new SheepTileIdentifier(17), new Point(
                (int) (X_SIZE / (527.0 / 444)),
                (int) (Y_SIZE / (700.0 / 316.0))));
        animalTilePosition.put(new SheepTileIdentifier(18), new Point(
                (int) (X_SIZE / 1.19772727), (int) (Y_SIZE / 1.59817352)));
    }

    private void fillCenterRoadMap() {
        Map<RoadSquareIdentifier, Point> map = centerRoadMap;

        placeRoadSquare(map, 1, 10, 137, 179);
        placeRoadSquare(map, 1, 2, 90, 256);
        placeRoadSquare(map, 1, 11, 168, 227);
        placeRoadSquare(map, 1, 2, 90, 256);
        placeRoadSquare(map, 2, 11, 167, 284);
        placeRoadSquare(map, 2, 3, 167, 350);
        placeRoadSquare(map, 2, 4, 127, 383);
        placeRoadSquare(map, 3, 4, 172, 418);
        placeRoadSquare(map, 3, 5, 205, 464);
        placeRoadSquare(map, 4, 5, 135, 525);
        placeRoadSquare(map, 3, 6, 249, 416);
        placeRoadSquare(map, 3, 11, 202, 325);
        placeRoadSquare(map, 5, 6, 245, 484);
        placeRoadSquare(map, 5, 9, 205, 581);
        placeRoadSquare(map, 6, 9, 282, 506);
        placeRoadSquare(map, 6, 7, 322, 404);
        placeRoadSquare(map, 6, 8, 323, 478);
        placeRoadSquare(map, 7, 8, 354, 451);
        placeRoadSquare(map, 8, 9, 322, 556);
        placeRoadSquare(map, 8, 18, 402, 497);
        placeRoadSquare(map, 7, 18, 397, 409);
        placeRoadSquare(map, 7, 17, 415, 342);
        placeRoadSquare(map, 17, 18, 462, 384);
        placeRoadSquare(map, 7, 16, 358, 324);
        placeRoadSquare(map, 16, 17, 415, 290);
        placeRoadSquare(map, 15, 16, 422, 242);
        placeRoadSquare(map, 15, 17, 474, 254);
        placeRoadSquare(map, 14, 16, 333, 245);
        placeRoadSquare(map, 13, 16, 382, 222);
        placeRoadSquare(map, 13, 15, 442, 175);
        placeRoadSquare(map, 12, 13, 371, 123);
        placeRoadSquare(map, 13, 14, 349, 197);
        placeRoadSquare(map, 12, 14, 314, 171);
        placeRoadSquare(map, 10, 14, 270, 184);
        placeRoadSquare(map, 10, 12, 275, 116);
        placeRoadSquare(map, 10, 11, 218, 206);
        placeRoadSquare(map, 11, 14, 257, 236);
        placeRoadSquare(map, 0, 14, 280, 269);
        placeRoadSquare(map, 0, 16, 319, 293);
        placeRoadSquare(map, 0, 7, 324, 345);
        placeRoadSquare(map, 0, 6, 286, 369);
        placeRoadSquare(map, 0, 3, 241, 346);
        placeRoadSquare(map, 0, 11, 240, 297);
    }

    private Color getColorFromPoint(Point pt) {
        if (pt.getX() < 0 || pt.getX() >= X_SIZE || pt.getY() < 0
                || pt.getY() >= Y_SIZE) {
            return Color.BLACK;
        } else {
            int colorCode = imgColored.getRGB((int) pt.getX(), (int) pt.getY());
            return new Color(colorCode, true);
        }
    }

    /**
     * Retrieve the tile that is underlying the given point.
     * 
     * It is allowed to give locations outside the map, and the function will
     * return null.
     * 
     * @param pt
     *            a point on the map
     * @return the corresponding tile, or null
     */
    public SheepTileIdentifier takeTileElement(Point pt) {
        return mapTile.get(getColorFromPoint(pt));
    }

    /**
     * Retrieve the road that is underlying the given point.
     * 
     * It is allowed to give locations outside the map, and the function will
     * return null.
     * 
     * @param pt
     *            a point on the map
     * @return the corresponding road, or null
     */
    public RoadSquareIdentifier takeRoadElement(Point pt) {
        return mapRoad.get(getColorFromPoint(pt));
    }

    private BufferedImage loadImage(String path) throws IOException {
        BufferedImage bimg = null;
        BufferedImage ret = null;

        bimg = ImageIO.read(getClass().getClassLoader().getResource(path));
        ret = new BufferedImage(bimg.getWidth(), bimg.getHeight(),
                BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = ret.createGraphics();
        g.drawImage(bimg, 0, 0, null);
        g.dispose();

        return ret;
    }

    private void fillMapTile() {

        mapTile.put(new Color(255, 255, 255), new SheepTileIdentifier(0));
        mapTile.put(new Color(140, 253, 78), new SheepTileIdentifier(1));
        mapTile.put(new Color(119, 255, 49), new SheepTileIdentifier(2));
        mapTile.put(new Color(120, 255, 49), new SheepTileIdentifier(2));
        mapTile.put(new Color(119, 255, 51), new SheepTileIdentifier(2));
        mapTile.put(new Color(102, 255, 25), new SheepTileIdentifier(3));
        mapTile.put(new Color(86, 254, 105), new SheepTileIdentifier(4));
        mapTile.put(new Color(61, 255, 80), new SheepTileIdentifier(5));
        mapTile.put(new Color(43, 254, 56), new SheepTileIdentifier(6));
        mapTile.put(new Color(43, 255, 180), new SheepTileIdentifier(7));
        mapTile.put(new Color(60, 254, 192), new SheepTileIdentifier(8));
        mapTile.put(new Color(84, 255, 202), new SheepTileIdentifier(9));
        mapTile.put(new Color(252, 151, 50), new SheepTileIdentifier(10));
        mapTile.put(new Color(253, 168, 78), new SheepTileIdentifier(11));
        mapTile.put(new Color(252, 135, 27), new SheepTileIdentifier(12));
        mapTile.put(new Color(163, 163, 163), new SheepTileIdentifier(13));
        mapTile.put(new Color(108, 108, 108), new SheepTileIdentifier(14));
        mapTile.put(new Color(135, 135, 135), new SheepTileIdentifier(15));
        mapTile.put(new Color(232, 254, 27), new SheepTileIdentifier(16));
        mapTile.put(new Color(233, 255, 50), new SheepTileIdentifier(17));
        mapTile.put(new Color(238, 255, 78), new SheepTileIdentifier(18));
    }

    private void fillMapRoadSquare() {

        mapRoad.put(new Color(38, 38, 38), new RoadSquareIdentifier(1, 10));
        mapRoad.put(new Color(3, 3, 3), new RoadSquareIdentifier(1, 11));
        mapRoad.put(new Color(1, 7, 67), new RoadSquareIdentifier(1, 2));
        mapRoad.put(new Color(0, 7, 108), new RoadSquareIdentifier(2, 11));
        mapRoad.put(new Color(1, 3, 253), new RoadSquareIdentifier(2, 3));
        mapRoad.put(new Color(22, 39, 254), new RoadSquareIdentifier(2, 4));
        mapRoad.put(new Color(49, 69, 254), new RoadSquareIdentifier(3, 4));
        mapRoad.put(new Color(0, 3, 153), new RoadSquareIdentifier(3, 11));
        mapRoad.put(new Color(23, 0, 68), new RoadSquareIdentifier(3, 5));
        mapRoad.put(new Color(80, 100, 254), new RoadSquareIdentifier(4, 5));
        mapRoad.put(new Color(251, 70, 77), new RoadSquareIdentifier(5, 9));
        mapRoad.put(new Color(33, 0, 108), new RoadSquareIdentifier(5, 6));
        mapRoad.put(new Color(46, 1, 152), new RoadSquareIdentifier(3, 6));
        mapRoad.put(new Color(252, 38, 50), new RoadSquareIdentifier(6, 9));
        mapRoad.put(new Color(250, 0, 27), new RoadSquareIdentifier(9, 8));
        mapRoad.put(new Color(250, 1, 6), new RoadSquareIdentifier(6, 8));
        mapRoad.put(new Color(199, 0, 3), new RoadSquareIdentifier(7, 8));
        mapRoad.put(new Color(105, 0, 0), new RoadSquareIdentifier(6, 7));
        mapRoad.put(new Color(154, 1, 0), new RoadSquareIdentifier(8, 18));
        mapRoad.put(new Color(96, 0, 108), new RoadSquareIdentifier(10, 11));
        mapRoad.put(new Color(177, 2, 203), new RoadSquareIdentifier(11, 14));
        mapRoad.put(new Color(60, 1, 69), new RoadSquareIdentifier(10, 12));
        mapRoad.put(new Color(181, 208, 9), new RoadSquareIdentifier(10, 14));
        mapRoad.put(new Color(201, 95, 6), new RoadSquareIdentifier(12, 14));
        mapRoad.put(new Color(107, 0, 54), new RoadSquareIdentifier(12, 13));
        mapRoad.put(new Color(234, 58, 254), new RoadSquareIdentifier(13, 14));
        mapRoad.put(new Color(225, 0, 255), new RoadSquareIdentifier(14, 16));
        mapRoad.put(new Color(151, 1, 74), new RoadSquareIdentifier(13, 15));
        mapRoad.put(new Color(66, 0, 35), new RoadSquareIdentifier(13, 16));
        mapRoad.put(new Color(200, 1, 98), new RoadSquareIdentifier(15, 16));
        mapRoad.put(new Color(251, 0, 120), new RoadSquareIdentifier(15, 17));
        mapRoad.put(new Color(146, 255, 223), new RoadSquareIdentifier(16, 17));
        mapRoad.put(new Color(53, 158, 2), new RoadSquareIdentifier(17, 7));
        mapRoad.put(new Color(251, 65, 167), new RoadSquareIdentifier(17, 18));
        mapRoad.put(new Color(66, 1, 1), new RoadSquareIdentifier(7, 18));
        mapRoad.put(new Color(0, 0, 202), new RoadSquareIdentifier(16, 7));
        mapRoad.put(new Color(136, 1, 155), new RoadSquareIdentifier(0, 14));
        mapRoad.put(new Color(112, 24, 254), new RoadSquareIdentifier(0, 16));
        mapRoad.put(new Color(72, 2, 254), new RoadSquareIdentifier(0, 7));
        mapRoad.put(new Color(59, 1, 202), new RoadSquareIdentifier(0, 6));
        mapRoad.put(new Color(92, 1, 254), new RoadSquareIdentifier(0, 3));
        mapRoad.put(new Color(134, 62, 255), new RoadSquareIdentifier(0, 11));
    }
}