package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerReceiver;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The background input thread for a SocketServerConnection.
 * 
 * @author Alessandro Cimbelli
 */
public class ServerReaderThread extends Thread {

    private final SocketServerConnection connection;
    private final InputStream socketInputStream;
    private ServerReceiver receiver;
    private ObjectInputStream inputStream;
    private boolean running;

    /**
     * Construct a ServerReaderThread, given the connection and the receiver
     * handling the commands.
     * 
     * @param connection
     *            the connection associated with this input thread
     * @param receiver
     *            the receiver handling the command
     * @param socketInputStream
     *            the actual socket input stream to read from
     */
    public ServerReaderThread(SocketServerConnection connection,
            ServerReceiver receiver, InputStream socketInputStream) {
        this.connection = connection;
        this.socketInputStream = socketInputStream;
        setServerReceiver(receiver);
    }

    /**
     * {@inheritDoc}
     */
    public void setServerReceiver(ServerReceiver receiver) {
        this.receiver = receiver;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        running = true;
        try {
            inputStream = new ObjectInputStream(socketInputStream);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE,
                    "IO exception opening input stream", e);

            close();
            return;
        }

        while (running) {
            checkInput();
        }
    }

    private void checkInput() {
        try {
            byte version = inputStream.readByte();

            // I check that the protocol's version that the server is using is
            // equals to the client's protocol version
            if (Protocol.V1.getVersion() != version) {
                throw new ProtocolException("Unsupported version");
            }

            int futureBits = inputStream.readInt();
            if (futureBits != 0) {
                throw new ProtocolException("Unsupported flags");
            }

            // I take the Number of the operation that the server wants to call

            int operationCode = inputStream.readInt();
            ServerOperation[] operations = ServerOperation.values();
            if (operationCode < 0 || operationCode >= operations.length) {
                throw new ProtocolException("Invalid operation code "
                        + operationCode);
            }

            safeInvokeMethod(operations[operationCode]);
        } catch (ProtocolException e) {
            Logger.getGlobal().log(Level.SEVERE,
                    "Protocol error reading input channel", e);

            close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE,
                    "IO exception reading input channel", e);

            close();
        }
    }

    private void safeInvokeMethod(ServerOperation operation) throws IOException {
        try {
            invokeMethod(operation);
        } catch (ClassCastException e) {
            throw new ProtocolException("Invalid message arguments", e);
        } catch (ClassNotFoundException e) {
            throw new ProtocolException("Invalid message arguments", e);
        }
    }

    private void doAbandonGame() throws IOException, ClassNotFoundException {
        receiver.doAbandonGame();
    }

    private void doForceSynchronizeMe() throws IOException,
            ClassNotFoundException {
        receiver.doForceSynchronizeMe();
    }

    private void doExecuteMove() throws IOException, ClassNotFoundException {
        Move move;

        move = (Move) inputStream.readObject();

        receiver.doExecuteMove(move);
    }

    private void doLoginToServer() throws IOException, ClassNotFoundException {
        String username;
        String password;

        username = (String) inputStream.readObject();
        password = (String) inputStream.readObject();

        receiver.doLoginToServer(username, password);
    }

    private void doPong() throws IOException, ClassNotFoundException {
        receiver.doPong();
    }

    private void invokeMethod(ServerOperation operation) throws IOException,
            ClassNotFoundException {
        switch (operation) {
        case ABANDON_GAME:
            doAbandonGame();
            break;
        case FORCE_SYNCHRONIZE_ME:
            doForceSynchronizeMe();
            break;
        case EXECUTE_MOVE:
            doExecuteMove();
            break;
        case LOGIN_TO_SERVER:
            doLoginToServer();
            break;
        case PONG:
            doPong();
            break;

        default:
            throw new AssertionError();
        }
    }

    private void close() {
        receiver.connectionClosed();
        connection.close();
        running = false;
    }

}
