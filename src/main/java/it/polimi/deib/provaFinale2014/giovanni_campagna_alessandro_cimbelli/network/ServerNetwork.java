package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import java.io.IOException;

/**
 * The interface representing a server-side network implementation.
 * 
 * @author Giovanni Campagna
 */
public interface ServerNetwork {
    /**
     * Set the connection handler on this network.
     * 
     * Methods on the connection handler will be called as new connections are
     * accepted.
     * 
     * It is not thread-safe to call this method more than once, or after
     * calling startListening().
     * 
     * @param handler
     *            the connection handler.
     */
    public abstract void setConnectionHandler(ServerConnectionHandler handler);

    /**
     * Begin accepting new connections.
     * 
     * Note that it is possible that the server actually starts listening (ie,
     * binds the address) earlier, but it is guaranteed that new connections are
     * not accepted before this call.
     * 
     * @throws IOException
     */
    public abstract void startListening() throws IOException;

    /**
     * Stop listening for new connections.
     * 
     * After this call, it is guaranteed that new connections will fail.
     */
    public abstract void stopListening();
}
