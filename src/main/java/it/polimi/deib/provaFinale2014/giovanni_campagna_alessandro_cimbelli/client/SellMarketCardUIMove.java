package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.SellMarketCardMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

/**
 * The UI counterpart of a sell market card move.
 * 
 * @author Giovanni Campagna
 * 
 */
public class SellMarketCardUIMove extends SellMarketCardMove implements UIMove {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a sell market card move.
     * 
     * @param who
     *            the player doing it
     * @param card
     *            the card involved
     * @param cost
     *            the cost of the card
     */
    public SellMarketCardUIMove(PlayerIdentifier who,
            TerrainCardIdentifier card, int cost) {
        super(who, card, cost);
    }

    /**
     * {@inheritDoc}
     */
    public void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateSoldMarketCard(getWho(engine), getCard(engine), getCost());
    }

    /**
     * {@inheritDoc}
     */
    public void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        // This is not a completable move, so we have nothing to do at the end
        //
        // (this comment courtesy of sonar)
    }
}
