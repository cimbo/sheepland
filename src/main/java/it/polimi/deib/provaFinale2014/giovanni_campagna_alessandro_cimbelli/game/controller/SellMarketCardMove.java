package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidMoveException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.TerrainCardIdentifier;

/**
 * This class represents a "sell market card" move. See the base class
 * {@link Move} for the meaning of a Move class.
 * 
 * @author Giovanni Campagna
 * 
 */
public class SellMarketCardMove extends Move {
    private static final long serialVersionUID = 1L;

    private final TerrainCardIdentifier cardId;
    private final int cost;

    /**
     * Construct a new "sell market card" move
     * 
     * @param who
     *            the player executing the move
     * @param card
     *            the card to sell
     * @param _cost
     *            the desired cost of the card
     */
    public SellMarketCardMove(PlayerIdentifier who, TerrainCardIdentifier card,
            int cost) {
        super(who);
        cardId = card;
        this.cost = cost;
    }

    /**
     * Retrieve the actual card object, by resolving the identifier through the
     * passed in engine.
     * 
     * @param engine
     * @return the card as game object
     * @throws InvalidIdentifierException
     */
    protected TerrainCard getCard(GameEngine engine)
            throws InvalidIdentifierException {
        return engine.getTerrainCardFromId(cardId);
    }

    /**
     * Retrieve the desired cost of the card.
     * 
     * @return the cost
     */
    protected int getCost() {
        return cost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(GameEngine engine) throws InvalidMoveException {
        engine.sellMarketTerrainCard(getWho(engine), getCard(engine), getCost());
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + cardId.hashCode();
        result = prime * result + cost;
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof SellMarketCardMove)) {
            return false;
        }
        SellMarketCardMove other = (SellMarketCardMove) obj;
        if (!cardId.equals(other.cardId)) {
            return false;
        }
        if (cost != other.cost) {
            return false;
        }
        return true;
    }
}
