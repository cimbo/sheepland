package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageLoader;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * A player icon, showing the player name, player pawn color and amount of coins
 * left (or other money related infos)
 * 
 * @author Alessandro Cimbelli
 */
public class PlayerIcon extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 65;
    private static final int Y_SIZE = 72;
    private static final int X_MONEY = 30;
    private static final int Y_MONEY = 30;
    private static final Color[] PLAYER_BG_COLORS = { new Color(255, 0, 0),
            new Color(0, 0, 255), new Color(101, 255, 26),
            new Color(255, 255, 0) };
    private static final Color[] PLAYER_FG_COLORS = { new Color(255, 255, 255),
            new Color(255, 255, 255), new Color(0, 0, 0), new Color(0, 0, 0) };

    private final JLabel money;
    private final Coin coin;
    private boolean drawMask;
    private boolean current;
    private final int index;
    private final Image star;

    /**
     * Construct a new player icon, with the given index in the playing order
     * (which is used to pick the colors) and name.
     * 
     * @param index
     *            the index
     * @param name
     *            the player name
     */
    public PlayerIcon(int index, String name) {
        this.index = index;
        star = ImageLoader.loadImageSync(this, "star.png");

        setBackground(PLAYER_BG_COLORS[index]);

        JLabel label = new JLabel(name);
        label.setForeground(PLAYER_FG_COLORS[index]);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setLocation(0, 18);
        label.setSize(X_SIZE, 41 - 18);

        add(label);

        money = new JLabel();
        money.setSize(X_MONEY, Y_MONEY);
        money.setVisible(true);
        money.setForeground(PLAYER_FG_COLORS[index]);
        add(money);

        coin = new Coin();
        add(coin);

        money.setLocation(X_SIZE - X_MONEY, Y_SIZE - Y_MONEY);
        coin.setLocation(0, Y_SIZE - Y_MONEY);
        setLayout(null);
        setVisible(true);
        setSize(X_SIZE, Y_SIZE);

        drawMask = false;
        current = false;
    }

    /**
     * Retrieve the index of this player icon, which can be used to obtain the
     * associated player or player identifier.
     * 
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (drawMask) {
            g.setColor(new Color(0f, 0f, 0f, 0.6f));
            g.fillRect(0, 0, X_SIZE, Y_SIZE);
        }
        if (current) {
            g.drawImage(star, 1, 1, null);
        }
    }

    /**
     * Update the amount of money displayed on this player icon.
     * 
     * @param money
     *            the new value
     */
    public void updateMoney(int money) {
        String s = "" + money;
        this.money.setText(s);
        repaint();
    }

    /**
     * Check a mask is drawn on top of this player.
     * 
     * @return true if a mask is drawn, false otherwise
     */
    public boolean getDrawMask() {
        return drawMask;
    }

    /**
     * Set the player to draw a mask.
     * 
     * Passing true will cause a semitrasparent layer to be drawn on top the
     * player, making it appear "disabled".
     * 
     * @param mask
     *            if the mask is to be drawn or not.
     */
    public void setDrawMask(boolean b) {
        drawMask = b;
        repaint();
    }

    /**
     * Set the player as current.
     * 
     * If the player is current, a small star icon will be drawn in the top left
     * corner.
     * 
     * @param b
     *            true, if the player is current, false otherwise
     */
    public void setCurrent(boolean b) {
        current = b;
        repaint();
    }
}
