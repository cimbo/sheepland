package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ClientGameEngine;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.KillSheepMove;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

/**
 * The UI counterpart of a kill sheep move.
 * 
 * @author Giovanni Campagna
 * 
 */
public class KillSheepUIMove extends KillSheepMove implements UIMove {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a kill sheep move.
     * 
     * @param who
     *            the player doing it
     * @param which
     *            the shepherd doing it
     * @param tile
     *            the tile where the killing happens
     * @param sheep
     *            the killed sheep
     */
    public KillSheepUIMove(PlayerIdentifier who, ShepherdIdentifier which,
            SheepTileIdentifier tile, SheepIdentifier sheep) {
        super(who, which, tile, sheep);
    }

    /**
     * {@inheritDoc}
     */
    public void animateBegin(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateBeginSheepKilling(getWho(engine), getWhich(engine),
                getTile(engine), getSheep(engine));
    }

    /**
     * {@inheritDoc}
     */
    public void animateEnd(ClientGameEngine engine, AbstractUI ui)
            throws InvalidIdentifierException {
        ui.animateEndSheepKilling(getWho(engine), getWhich(engine),
                getTile(engine), getSheep(engine), getPaidPlayers(engine));
    }

}
