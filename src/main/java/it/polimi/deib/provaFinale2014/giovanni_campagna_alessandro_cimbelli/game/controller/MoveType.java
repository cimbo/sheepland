package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

/**
 * Represents a type of player move. This is a friendly enum used in the
 * GameBoard state tracking.
 * 
 * @author Giovanni Campagna
 * 
 */
enum MoveType {
    INVALID,

    INITIALLY_PLACE, MOVE_SHEPHERD, MOVE_SHEEP, BUY_TERRAIN, MATE, KILL, MARKET_BUY, MARKET_SELL
}
