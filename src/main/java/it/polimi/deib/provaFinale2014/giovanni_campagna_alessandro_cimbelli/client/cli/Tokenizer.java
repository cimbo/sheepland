package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.ShepherdIdentifier;

import java.util.HashMap;
import java.util.Map;

/**
 * The tokenizer used for parsing the user input.
 * 
 * @author Giovanni Campagna
 */
class Tokenizer {
    private final String[] tokens;
    private int currentToken;

    private static Map<String, TerrainType> terrainTypes;

    /**
     * Construct a new Tokenizer for the given input line
     * 
     * @param line
     *            the input to tokenize
     */
    public Tokenizer(String line) {
        tokens = line.trim().split("[ \t]+");
    }

    private void ensureTerrainTypes() {
        if (terrainTypes == null) {
            terrainTypes = new HashMap<String, TerrainType>();

            TerrainType[] allTypes = TerrainType.values();
            for (TerrainType t : allTypes) {
                terrainTypes.put(t.name().toLowerCase(), t);
            }
        }
    }

    private String nextToken() throws InvalidInputException {
        if (currentToken == tokens.length) {
            throw new InvalidInputException("empty string");
        }

        String token = tokens[currentToken];
        currentToken++;

        return token;
    }

    /**
     * Read and return any next input token
     * 
     * @return the next token
     * @throws InvalidInputException
     *             if the input has finished already
     */
    public String readAny() throws InvalidInputException {
        return nextToken();
    }

    /**
     * Read and return a command.
     * 
     * @return the command to execute
     * @throws InvalidInputException
     *             if the input is finished, or the command could not be
     *             recognized
     */
    public BaseCommand readCommand() throws InvalidInputException {
        String name = nextToken();
        BaseCommand command = BaseCommand.getCommand(name);
        if (command == null) {
            throw new InvalidInputException(name);
        }
        return command;
    }

    /**
     * Read and return a subcommand of "look".
     * 
     * @return the subcommand to execute
     * @throws InvalidInputException
     *             if the input is finished, or the subcommand could not be
     *             recognized
     */
    public LookSubCommand readLookSubCommand() throws InvalidInputException {
        return LookSubCommand.forToken(nextToken());
    }

    /**
     * Expect the given string in the input, failing if the input is empty or
     * the next token does not match the input.
     * 
     * @param expected
     *            the expected token
     * @throws InvalidInputException
     *             the expectation failed
     */
    public void expect(String expected) throws InvalidInputException {
        String token = nextToken();
        if (!token.equals(expected)) {
            throw new InvalidInputException(token);
        }
    }

    /**
     * Check if tokenizer has a next token to read (and thus a readAny() call
     * would not fail)
     * 
     * @return true, if more tokens need reading, false otherwise
     */
    public boolean hasNext() {
        return currentToken < tokens.length;
    }

    private int parseInt(String token) throws InvalidInputException {
        try {
            return Integer.parseInt(token);
        } catch (NumberFormatException e) {
            throw new InvalidInputException(e);
        }
    }

    private TerrainType parseTerrainType(String token)
            throws InvalidInputException {
        ensureTerrainTypes();
        TerrainType type = terrainTypes.get(token);
        if (type == null) {
            throw new InvalidInputException(token);
        }
        return type;
    }

    /**
     * Read and return a integer.
     * 
     * @return the read integer
     * @throws InvalidInputException
     *             if the input has finished, or the next token could not be
     *             parsed as an integer
     */
    public int readInt() throws InvalidInputException {
        return parseInt(nextToken());
    }

    /**
     * Read and return a shepherd.
     * 
     * A shepherd is simply an integer between 1 and the number of shepherds,
     * inclusive, that is converted directly to the corresponding identifier.
     * 
     * @return the read shepherd identifier
     * @throws InvalidInputException
     *             if the input has finished, or the next token could not be
     *             parsed as an integer
     */
    public ShepherdIdentifier readShepherd() throws InvalidInputException {
        return new ShepherdIdentifier(readInt() - 1);
    }

    /**
     * Read and return a sheep tile.
     * 
     * A sheep tile is "sheepsburg" or a terrain type followed by "/" followed
     * by a number between 0 and 2 inclusive.
     * 
     * @return the read sheep tile identifier
     * @throws InvalidInputException
     *             if the input has finished, or the next token could not be
     *             parsed as a sheep tile
     */
    public SheepTileIdentifier readSheepTile() throws InvalidInputException {
        String token = nextToken();
        if ("sheepsburg".equals(token)) {
            return new SheepTileIdentifier(0);
        }

        String[] parsed = token.split("/");

        if (parsed.length != 2) {
            throw new InvalidInputException(token);
        }

        int code = parseInt(parsed[1]);
        if (code < 0 || code >= 3) {
            throw new InvalidInputException(token);
        }

        return new SheepTileIdentifier(1
                + parseTerrainType(parsed[0]).ordinal() * 3 + code);
    }

    /**
     * Read and return a road square.
     * 
     * A road square is comprised by "between", followed by a sheep tile
     * parsable by readSheepTile(), followed by "and", followed by another sheep
     * tile.
     * 
     * @return the read road square identifier
     * @throws InvalidInputException
     *             if the input has finished, or the next token could not be
     *             parsed as a road square
     */
    public RoadSquareIdentifier readRoadSquare() throws InvalidInputException {
        expect("between");
        int tileCode1 = readSheepTile().getContent();
        expect("and");
        int tileCode2 = readSheepTile().getContent();
        return new RoadSquareIdentifier(tileCode1, tileCode2);
    }

    /**
     * Read and return a sheep type.
     * 
     * The sheep types are "female" followed by "sheep", "ram", "lamb" and
     * "black" followed by "sheep".
     * 
     * @return the read sheep type identifier
     * @throws InvalidInputException
     *             if the input has finished, or the next token could not be
     *             parsed as a sheep type
     */
    public SheepType readSheepType() throws InvalidInputException {
        String token = nextToken();

        if ("black".equals(token)) {
            expect("sheep");
            return SheepType.BLACK_SHEEP;
        } else if ("female".equals(token)) {
            expect("sheep");
            return SheepType.FEMALE_SHEEP;
        } else if ("ram".equals(token)) {
            return SheepType.RAM;
        } else if ("lamb".equals(token)) {
            return SheepType.LAMB;
        } else {
            throw new InvalidInputException(token);
        }
    }

    /**
     * Read and return a terrain type.
     * 
     * A terrain type is simply the terrain type enumeration name, in lowercase.
     * 
     * @return the read terrain type
     * @throws InvalidInputException
     *             if the input has finished, or the next token could not be
     *             parsed as a terrain type,
     */
    public TerrainType readTerrainType() throws InvalidInputException {
        return parseTerrainType(nextToken());
    }
}
