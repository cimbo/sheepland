package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class holds game options and configuration of the game.
 * 
 * When first constructed, it will open and load a configuration file. If
 * loading that configuration fails, it will fallback to default values.
 * 
 * @author Giovanni Campagna
 */
public class GameOptions {
    private static final int DEFAULT_GAME_STARTING_TIMEOUT = 120;
    private static final int DEFAULT_GAME_TURN_TIMEOUT = 600;
    private static final int DEFAULT_GAME_TURN_DISCONNECTED_TIMEOUT = 30;
    private static final int DEFAULT_PING_TIMEOUT = 5;
    public static final String USER_CONFIG_FILE = System.getenv("HOME")
            + "/.config/sheepland.conf";
    public static final String DEFAULT_CONFIG_FILE = "/etc/sheepland/sheepland.conf";

    private final int gameStartingTimeout;
    private final int gameTurnTimeout;
    private final int gameTurnDisconnectedTimeout;
    private final int pingTimeout;

    /**
     * Construct a game options instance with default parameters.
     */
    public GameOptions() {
        gameStartingTimeout = DEFAULT_GAME_STARTING_TIMEOUT;
        gameTurnTimeout = DEFAULT_GAME_TURN_TIMEOUT;
        gameTurnDisconnectedTimeout = DEFAULT_GAME_TURN_DISCONNECTED_TIMEOUT;
        pingTimeout = DEFAULT_PING_TIMEOUT;
    }

    // For testing
    GameOptions(Reader reader) {
        int gameStartingTimeoutTmp = DEFAULT_GAME_STARTING_TIMEOUT;
        int gameTurnTimeoutTmp = DEFAULT_GAME_TURN_TIMEOUT;
        int gameTurnDisconnectedTimeoutTmp = DEFAULT_GAME_TURN_DISCONNECTED_TIMEOUT;
        int pingTimeoutTmp = DEFAULT_PING_TIMEOUT;

        try {
            BufferedReader buffer = new BufferedReader(reader);
            Scanner scanner = new Scanner(buffer);

            try {
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    if (line.isEmpty() || line.startsWith("#")) {
                        continue;
                    }

                    String[] split = line.split("=", 2);

                    if (split.length != 2) {
                        throw new ParseException(
                                "Line did not contain an equal sign", 0);
                    }

                    String name = split[0];
                    String value = split[1];

                    if ("GAME_STARTING_TIMEOUT".equals(name)) {
                        gameStartingTimeoutTmp = Integer.parseInt(value);
                    } else if ("GAME_TURN_TIMEOUT".equals(name)) {
                        gameTurnTimeoutTmp = Integer.parseInt(value);
                    } else if ("PING_TIMEOUT".equals(name)) {
                        pingTimeoutTmp = Integer.parseInt(value);
                    } else if ("GAME_TURN_DISCONNECTED_TIMEOUT".equals(name)) {
                        gameTurnDisconnectedTimeoutTmp = Integer
                                .parseInt(value);
                    } else {
                        Logger.getGlobal().log(Level.INFO,
                                "Invalid option " + name);
                    }
                }
            } finally {
                scanner.close();
            }
        } catch (Exception e) {
            Logger.getGlobal().log(Level.INFO, "Failed to load config file", e);
        }

        gameStartingTimeout = gameStartingTimeoutTmp;
        gameTurnTimeout = gameTurnTimeoutTmp;
        gameTurnDisconnectedTimeout = gameTurnDisconnectedTimeoutTmp;
        pingTimeout = pingTimeoutTmp;
    }

    /**
     * Construct a GameOptions to load from the given configuration file.
     */
    public GameOptions(String fileName) throws FileNotFoundException {
        this(new FileReader(fileName));
    }

    /**
     * Retrieve the game starting timeout, in seconds.
     * 
     * This is the timeout between having enough players in the lobby
     * (incubator) and starting the game.
     * 
     * @return the game starting timeout
     */
    public int getGameStartingTimeout() {
        return gameStartingTimeout;
    }

    /**
     * Retrieve the game turn timeout, in seconds.
     * 
     * This is the timeout between beginning the turn (receiving
     * advanceNextTurn() with our turn position, barring network latency) and
     * receiving/processing the last move in the turn, when the connection is
     * running fine.
     * 
     * @return the game turn timeout
     */
    public int getGameTurnTimeout() {
        return gameTurnTimeout;
    }

    /**
     * Retrieve the game turn timeout when disconnected, in seconds.
     * 
     * This is the same timeout as getGameTurnTimeout(), but it applies when the
     * connection is already closed when the turn begins. It is usually smaller.
     * 
     * @return the game turn timeout when disconnected
     */
    public int getGameTurnDisconnectedTimeout() {
        return gameTurnDisconnectedTimeout;
    }

    /**
     * Retrieve the ping timeout, in seconds.
     * 
     * This is the time the client is allotted to reply to a ping before the
     * connection is forcibly closed by the server.
     * 
     * @return the ping timeout
     */
    public int getPingTimeout() {
        return pingTimeout;
    }
}
