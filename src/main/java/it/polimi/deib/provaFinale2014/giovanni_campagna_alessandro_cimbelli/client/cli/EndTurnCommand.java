package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;

/**
 * The "end" command, skips the turn.
 * 
 * @author Giovanni Campagna
 * 
 */
public class EndTurnCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("end: end your turn");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws RuleViolationException {
        ui.getCurrentGame().forceEndTurn();
    }
}
