package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Shepherd;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GameMap;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.GraphVisitor;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.RoadSquare;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map.SheepTile;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * A GameEngine that has full knowledge of the game, and is full control of the
 * random events.
 * 
 * @author Giovanni Campagna
 * 
 */
public class ServerGameEngine extends GameEngine {
    private final ServerDelegate delegate;
    private final Random random;
    private int lastTurnOfRound;

    /**
     * Construct a new server game engine, given a delegate and a set of rules.
     * 
     * @param delegate
     *            the delegate object that will be used for the black sheep,
     *            wolf and lambs
     * @param rules
     *            the rules of the game
     */
    public ServerGameEngine(ServerDelegate delegate, RuleSet rules)
            throws RuleViolationException {
        this(new Random(), delegate, rules);
    }

    // For testing only
    ServerGameEngine(Random random, ServerDelegate delegate, RuleSet rules)
            throws RuleViolationException {
        super();
        this.random = random;
        this.delegate = delegate;
        setRules(rules);
        setGameBoard(new GameBoard());
    }

    /**
     * Add a player to the board, checking that the game is in the right state
     * to allow that. This is protected because it must be called by the
     * appropriate subclasses in the client and in the server, to ensure the
     * player is in the right state wrt the hidden initial card, and to make
     * sure the client doesn't add players that don't exist.
     * 
     * @param p
     * @throws RuleViolationException
     */
    public void addPlayer(Player p) {
        if (getGamePhase() != GamePhase.PREINIT) {
            throw new GameInvariantViolationException(
                    "Cannot add players after the game has started");
        }

        getGameBoard().addPlayer(p);
    }

    private int rollDice(int diceKind) {
        return random.nextInt(diceKind);
    }

    private int rollDice() {
        return rollDice(6) + 1;
    }

    /**
     * Starts the game, moving from the PREINIT to the INIT phase.
     * 
     * This will also initialize the board, according to the rules passed to the
     * constructor.
     */
    public void start() {
        GameState state = getGameState();
        GameBoard board = getGameBoard();

        if (state.getCurrentPhase() != GamePhase.PREINIT) {
            throw new GameInvariantViolationException(
                    "Game has already started");
        }
        if (board.getNumberOfPlayers() < 1) {
            throw new GameInvariantViolationException(
                    "Cannot start a game without players");
        }

        board.initializeForRules(getRules());

        initializeRandomPlayingOrder();
        allocateInitialCards();

        initiallyPopulateAllTiles();

        state.advanceInitState();

        int firstPlayer = advanceNextRound();
        assert firstPlayer == 0;
        state.setCurrentTurn(firstPlayer);
        state.prepareNextTurn();
    }

    private void initializeRandomPlayingOrder() {
        GameBoard board = getGameBoard();
        Player[] playingOrder = board.getAllPlayers();
        Collections.shuffle(Arrays.asList(playingOrder), random);
        board.setPlayingOrder(playingOrder);
    }

    private void allocateInitialCards() {
        Player[] playingOrder = getPlayingOrder();
        TerrainType[] initialCards = Arrays.copyOf(TerrainType.values(),
                TerrainType.NUMBER_OF_TERRAINS);
        Collections.shuffle(Arrays.asList(initialCards), random);

        GameBoard board = getGameBoard();
        for (int i = 0; i < playingOrder.length; i++) {
            TerrainCard card = board.getInitialTerrainCard(initialCards[i]);
            playingOrder[i].setInitialCard(card);
        }

        /*
         * Note: we don't call markCardAcquired() here, because that is for
         * regular cards, while this is for initial cards - they are stored in
         * the same pool in Player, but they come from different decks in the
         * real game.
         */
    }

    private void initiallyPopulateAllTiles() {
        GameMap map = getGameBoard().getMap();

        SheepTile sheepsburg = map.getSheepsburg();
        Sheep blackSheep = new Sheep(SheepType.BLACK_SHEEP);
        getGameBoard().addSheep(blackSheep);
        map.moveBlackSheep(blackSheep, sheepsburg);
        map.moveWolf(sheepsburg);

        map.forEachTile(new GraphVisitor() {
            /**
             * {@inheritDoc}
             */
            public void visit(SheepTile tile) {
                if (tile.getTerrainType() == TerrainType.SHEEPSBURG) {
                    return;
                }

                SheepType[] choices = { SheepType.FEMALE_SHEEP, SheepType.RAM,
                        SheepType.LAMB };

                SheepType choice = choices[random.nextInt(choices.length)];
                Sheep sheep;
                if (choice == SheepType.LAMB) {
                    sheep = new Lamb();
                } else {
                    sheep = new Sheep(choice);
                }
                getGameBoard().addSheep(sheep);
                tile.addSheep(sheep);
            }
        });
    }

    private boolean allPlayersPositioned() {
        for (Player p : getGameBoard().getPlayingOrder()) {
            if (!p.isPositioned()) {
                return false;
            }
        }

        return true;
    }

    private void completeCurrentPlayingRound() {
        moveWolf();
    }

    private void prepareNextPlayingTurn() {
        moveBlackSheep();
    }

    private void prepareNextPlayingRound() {
        growAllLambs();
    }

    private void checkAndAdvanceAfterInit() {
        if (allPlayersPositioned()) {
            getGameState().advancePlayState();
        }
    }

    private void checkAndAdvanceAfterMarketBuy() {
        if (getGameBoard().getRemainingFences() != 0) {
            getGameState().advancePlayState();
        } else {
            // If we are in this function, it means we
            // completed a round (ie, each player played
            // the same amount of turns)
            // if remaining fences is 0, it means we were
            // in the final state before taking the detour
            // through market sell and market buy
            //
            // summing that means we can go straight to
            // the end state - the game is done!
            getGameState().advanceEndState();
        }
    }

    private void checkAndAdvanceNextGamePhase() {
        switch (getGamePhase()) {
        case PREINIT:
            throw new IllegalStateException(
                    "Cannot be in the preinit state after a round");
        case INIT:
            checkAndAdvanceAfterInit();
            break;
        case PLAY:
        case FINAL:
            getGameState().advanceMarketSellState();
            break;
        case MARKET_SELL:
            getGameState().advanceMarketBuyState();
            break;
        case MARKET_BUY:
            giveBackTerrainCardsForSale();
            checkAndAdvanceAfterMarketBuy();
            break;
        case END:
            throw new GameInvariantViolationException(
                    "Cannot perform moves after the end of the game");
        default:
            // like in GameState.setGamePhase(), I'm not sure this
            // is even possible
            throw new AssertionError();
        }
    }

    private int advanceNextTurn() {
        GameState state = getGameState();
        int currentTurn = state.getCurrentTurn();
        int nextTurn = (currentTurn + 1) % getGameBoard().getNumberOfPlayers();

        return nextTurn;
    }

    private int advanceNextRound() {
        int roundBegin;
        int numberOfPlayers = getGameBoard().getNumberOfPlayers();
        if (getGamePhase() == GamePhase.MARKET_BUY) {
            roundBegin = random.nextInt(numberOfPlayers);
        } else {
            roundBegin = 0;
        }

        lastTurnOfRound = (roundBegin + numberOfPlayers - 1) % numberOfPlayers;
        return roundBegin;
    }

    /**
     * Advance to next state after completing a move (and sending the
     * appropriate notification on the server, or receiving the acknowledgment
     * on the client).
     * 
     * If appropriate, this will advance to the next turn, and possibly end the
     * game.
     * 
     * @return the next turn to play, or -1 if no advance happened
     */
    public int checkAndAdvanceNextTurn() {
        GameState state = getGameState();
        boolean shouldAdvance = state.checkShouldAdvanceNextTurn();
        if (!shouldAdvance) {
            return -1;
        }

        boolean lastTurn = state.getCurrentTurn() == lastTurnOfRound;
        if (state.getCurrentPhase().isPlaying() && lastTurn) {
            completeCurrentPlayingRound();
        }

        int nextTurn;
        if (lastTurn) {
            checkAndAdvanceNextGamePhase();
            nextTurn = advanceNextRound();
        } else {
            nextTurn = advanceNextTurn();
        }
        state.setCurrentTurn(nextTurn);
        state.prepareNextTurn();

        if (state.getCurrentPhase().isPlaying()) {
            if (lastTurn) {
                prepareNextPlayingRound();
            }

            prepareNextPlayingTurn();
        }

        return nextTurn;
    }

    private RoadSquare getDiceDestination(RoadSquare[] squares, int diceRoll) {
        for (RoadSquare rs : squares) {
            if (rs.getDiceValue() == diceRoll) {
                return rs;
            }
        }

        return null;
    }

    // Friendly for testing
    void moveBlackSheep() {
        GameMap map = getGameBoard().getMap();
        SheepTile blackSheepTile = map.getCurrentBlackSheepPosition();

        int diceRoll = rollDice();
        RoadSquare[] adjacentSquares = map
                .getAllAdjacentSquares(blackSheepTile);
        RoadSquare rs = getDiceDestination(adjacentSquares, diceRoll);

        if (rs == null) {
            return;
        }
        if (rs.getHasFence() || rs.getCurrentShepherd() != null) {
            return;
        }

        SheepTile jumpDestination = rs.getMatchingTile(blackSheepTile);
        map.moveBlackSheep(getGameBoard().getBlackSheep(), jumpDestination);
        delegate.didMoveBlackSheep(jumpDestination.getId());
    }

    private boolean areAllSquaresClosed(RoadSquare[] squares) {
        for (RoadSquare rs : squares) {
            if (!rs.getHasFence()) {
                return false;
            }
        }

        return true;
    }

    // Friendly for testing
    void moveWolf() {
        GameMap map = getGameBoard().getMap();
        SheepTile wolfTile = map.getCurrentWolfPosition();

        RoadSquare[] adjacentSquares = map.getAllAdjacentSquares(wolfTile);
        RoadSquare chosenSquare = null;

        int diceRoll = rollDice();
        chosenSquare = getDiceDestination(adjacentSquares, diceRoll);
        if (chosenSquare == null) {
            return;
        }
        if (!areAllSquaresClosed(adjacentSquares) && chosenSquare.getHasFence()) {
            return;
        }

        SheepTile jumpDestination = chosenSquare.getMatchingTile(wolfTile);
        Sheep eatenSheep = jumpDestination.getRandomSheep(random);
        if (eatenSheep != null) {
            jumpDestination.removeSheep(eatenSheep);
        }

        map.moveWolf(jumpDestination);
        delegate.didMoveWolf(jumpDestination.getId(),
                eatenSheep != null ? eatenSheep.getId() : null);
    }

    // friendly for testing
    void growLambsOnTile(SheepTile tile) {
        SheepType[] choices = { SheepType.FEMALE_SHEEP, SheepType.RAM };

        Collection<Lamb> grownLambs = tile.growLambs();
        List<Sheep> newAdultSheep = new ArrayList<Sheep>();
        for (int i = 0; i < grownLambs.size(); i++) {
            SheepType choice = choices[random.nextInt(choices.length)];
            Sheep newSheep = new Sheep(choice);
            getGameBoard().addSheep(newSheep);
            tile.addSheep(newSheep);
            newAdultSheep.add(newSheep);
        }

        Sheep[] newAdultSheepArray = new Sheep[newAdultSheep.size()];
        newAdultSheep.toArray(newAdultSheepArray);
        delegate.didGrowLambs(tile.getId(), newAdultSheepArray);
    }

    // friendly for testing
    void growAllLambs() {
        getGameBoard().getMap().forEachTile(new GraphVisitor() {
            /**
             * {@inheritDoc}
             */
            public void visit(SheepTile tile) {
                growLambsOnTile(tile);
            }
        });
    }

    /**
     * Implement the "mate sheep" move.
     * 
     * This is implemented differently in the client and server, because it
     * requires a random outcome. For this reason, the API is provided only on
     * the server side.
     * 
     * This method does not throw, because the validity of the move is checked
     * in mateSheep().
     */
    public Sheep realMateSheep(Shepherd which, SheepTile tile) {
        int diceRoll = rollDice();
        RoadSquare currentPosition = which.getCurrentPosition();

        if (currentPosition.getDiceValue() == diceRoll) {
            Sheep newLamb = new Lamb();
            getGameBoard().addSheep(newLamb);
            tile.addSheep(newLamb);
            return newLamb;
        } else {
            return null;
        }
    }

    /**
     * Implement the "kill sheep" move.
     * 
     * This is implemented differently in the client and server, because it
     * requires a random outcome.For this reason, the API is provided only on
     * the server side.
     * 
     * This method does not throw, because the validity of the move is checked
     * in killSheep().
     * 
     * @return a map of players and their earning if killing succeeded, null if
     *         it failed
     */
    public Map<Player, Integer> realKillSheep(Player who, Shepherd which,
            SheepTile tile, Sheep sheep) {
        int diceRoll = rollDice();
        RoadSquare currentPosition = which.getCurrentPosition();

        if (currentPosition.getDiceValue() != diceRoll) {
            return null;
        }

        tile.removeSheep(sheep);

        Map<Player, Integer> paidPlayers = new HashMap<Player, Integer>();
        List<Shepherd> opponentShepherds = getAllAdjacentOpponentShepherds(who,
                which);
        for (Shepherd shepherd : opponentShepherds) {
            int opponentDiceRoll = rollDice();
            if (opponentDiceRoll >= 5) {
                int amount = 2;
                who.payCoins(amount);

                Player owner = shepherd.getOwner();

                if (!paidPlayers.containsKey(owner)) {
                    paidPlayers.put(owner, 0);
                }

                paidPlayers.put(owner, paidPlayers.get(owner) + amount);
                owner.earnCoins(amount);
            }
        }

        return paidPlayers;
    }

    /**
     * Deep clone the current board for a given player.
     * 
     * This produces a copy of the current game board, which is filtered of all
     * the information the requestor player should not be aware of (namely, the
     * initial cards of the other players).
     * 
     * The resulting copy is a deep copy. To relate the new objects to the old
     * ones, identifier equality must be used.
     * 
     * @param requestor
     *            the player who is requesting the clone
     * @return a cloned and filtered board.
     */
    public GameBoard cloneBoardForPlayer(Player requestor) {
        return getGameBoard().cloneForPlayer(requestor);
    }

    // END state

    /**
     * Retrieves the final ranking of the game.
     * 
     * This method is only allowed after the game has ended (by reaching phase
     * END).
     * 
     * @return
     */
    public Map<PlayerIdentifier, Integer> getScores() {
        if (getGameState().getCurrentPhase() != GamePhase.END) {
            throw new GameInvariantViolationException(
                    "Cannot check the ranking until the game has finished");
        }

        Player[] playingOrder = getGameBoard().getPlayingOrder();
        Map<PlayerIdentifier, Integer> scores = new HashMap<PlayerIdentifier, Integer>();
        final int[] sheep = getGameBoard().getMap().getScoreValues();

        for (Player player : playingOrder) {
            scores.put(player.getId(), player.getScore(sheep));
        }

        return scores;
    }
}
