package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;

/**
 * This class contains the location of all the animal that can be on a tile of the map and its center location.
 * Therefore this class can update the number of the animal that it contains. 
 * 
 * @Alessandro Cimbelli
 */
import java.awt.Point;

/**
 * A pseudo-container holding together the animal icons associated with a
 * specific tile.
 * 
 * @author Alessandro Cimbelli
 */
public class TileIcon {
    private GameBoardPanel gameBoardPanel;

    private Point[] sheepPositions;
    private Point wolfPosition;

    private int[] counters;
    private boolean wolf = false;

    private SheepIcon[] sheepIcons;
    private AnimalIcon currentWolf = new AnimalIcon("wolf.png");

    /**
     * Construct a new tile icon, around the given point, and parented to the
     * given game board panel.
     * 
     * @param center
     *            the center of the tile icon
     * @param gameBoardPanel
     *            the parent of the animal icons
     */
    public TileIcon(Point center, GameBoardPanel gameBoardPanel) {
        this.gameBoardPanel = gameBoardPanel;
        AnimalIcon animal = currentWolf;

        SheepType[] allTypes = SheepType.values();
        counters = new int[allTypes.length];
        sheepIcons = new SheepIcon[allTypes.length];
        sheepPositions = new Point[allTypes.length];
        for (int i = 0; i < allTypes.length; i++) {
            sheepIcons[i] = new SheepIcon(allTypes[i]);
        }

        // Initialize all the animal's position into the tile

        this.sheepPositions[SheepType.FEMALE_SHEEP.ordinal()] = new Point(
                (int) (center.getLocation().getX() - animal.getWidth() * 0.5
                        - animal.getWidth() - 1), (int) (center.getLocation()
                        .getY() - animal.getHeight() * 0.5));

        this.sheepPositions[SheepType.RAM.ordinal()] = new Point((int) (center
                .getLocation().getX()
                - animal.getWidth()
                * 0.5
                + animal.getWidth() + 1),
                (int) (center.getLocation().getY() - animal.getHeight() * 0.5));

        this.sheepPositions[SheepType.LAMB.ordinal()] = new Point((int) (center
                .getLocation().getX() - animal.getWidth() * 0.5), (int) (center
                .getLocation().getY()
                - animal.getHeight()
                * 0.5
                - animal.getHeight() - 1));

        this.sheepPositions[SheepType.BLACK_SHEEP.ordinal()] = new Point(
                new Point((int) (center.getLocation().getX() - animal
                        .getWidth() * 0.5), (int) (center.getLocation().getY()
                        - animal.getHeight() * 0.5 + animal.getHeight() + 1)));

        this.wolfPosition = new Point(
                (int) (center.getLocation().getX() - animal.getWidth() * 0.5),
                (int) (center.getLocation().getY() - animal.getHeight() * 0.5));

    }

    // You can have the position of a specify animal

    /**
     * Retrieve the absolute position the wolf would have on this tile, in game
     * board coordinates
     * 
     * @return the wolf position
     */
    public Point getWolfPosition() {
        return wolfPosition;
    }

    /**
     * Retrieve the absolute position the wolf would have on this tile, in game
     * board coordinates
     * 
     * @return the wolf position
     */
    public Point getSheepPosition(SheepType type) {
        return sheepPositions[type.ordinal()];
    }

    /**
     * Change the number of sheep of the given type.
     * 
     * @param type
     *            the type of sheep affected
     * @param number
     *            the new number
     */
    public void setNumberSheep(SheepType type, int number) {
        if (counters[type.ordinal()] == 0 && number == 0) {
            return;
        }

        SheepIcon icon = sheepIcons[type.ordinal()];
        if (counters[type.ordinal()] != 0 && number == 0) {
            gameBoardPanel.remove(icon);
            gameBoardPanel.repaint();
        } else if (counters[type.ordinal()] == 0 && number != 0) {
            if (type != SheepType.BLACK_SHEEP) {
                icon.updateNumber(number);
            }
            gameBoardPanel.add(icon);
            icon.setLocation(sheepPositions[type.ordinal()]);
        } else {
            if (type != SheepType.BLACK_SHEEP) {
                icon.updateNumber(number);
            }
        }

        counters[type.ordinal()] = number;
    }

    /**
     * Change the visibility of the wolf on this tile
     * 
     * @param bool
     *            if the wolf is on this tile or not
     */
    public void setWolf(boolean bool) {
        if (this.wolf && !bool) {
            gameBoardPanel.remove(currentWolf);
            gameBoardPanel.repaint();
            this.wolf = false;
        } else if (!this.wolf && bool) {
            gameBoardPanel.add(currentWolf);
            currentWolf.setLocation(wolfPosition);
            this.wolf = true;
        }
    }

}
