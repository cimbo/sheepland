package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnectionHandler;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerNetwork;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The central point of the server application, establishes a listening socket
 * and handles new connection from the clients.
 */
public class Server implements ServerConnectionHandler {
    private final GameOptions options;
    private final Collection<ServerNetwork> networks;
    private final GameIncubator gameIncubator;

    /**
     * Instantiate a new Server, using net as the network implementation.
     * 
     * @param net
     */
    public Server(Collection<ServerNetwork> nets) {
        GameOptions optionsTmp;
        try {
            optionsTmp = new GameOptions(GameOptions.USER_CONFIG_FILE);
        } catch (FileNotFoundException e1) {
            Logger.getGlobal().log(Level.CONFIG,
                    "User configuration file not found", e1);
            try {
                optionsTmp = new GameOptions(GameOptions.DEFAULT_CONFIG_FILE);
            } catch (FileNotFoundException e2) {
                Logger.getGlobal().log(Level.CONFIG,
                        "System configuration file not found", e2);
                optionsTmp = new GameOptions();
            }
        }

        options = optionsTmp;
        gameIncubator = new GameIncubator(options);
        networks = nets;
        for (ServerNetwork network : networks) {
            network.setConnectionHandler(this);
        }
    }

    /**
     * Start the server. This will enable new connections from the clients.
     */
    public void start() {
        try {
            for (ServerNetwork network : networks) {
                network.startListening();
            }

            Logger.getGlobal().log(Level.INFO, "Server ready");
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "Failed to start network", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void handleNewConnection(ServerConnection connection) {
        new ServerPlayer(connection, gameIncubator, options);
    }
}
