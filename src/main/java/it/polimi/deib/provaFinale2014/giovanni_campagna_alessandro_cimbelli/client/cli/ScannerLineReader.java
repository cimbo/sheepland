package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A LineReader implementation that uses a standard {@link java.util.Scanner}.
 * To be used in fallback environments where the full line editing capabilities
 * of jline2 are not available.
 * 
 * @author Giovanni Campagna
 */
class ScannerLineReader implements LineReader {
    private final Scanner scanner;

    /**
     * Construct a ScannerLineReader wrapping standard input.
     */
    public ScannerLineReader() {
        scanner = new Scanner(System.in);
    }

    private String getLine() {
        try {
            return scanner.nextLine();
        } catch (NoSuchElementException e) {
            Logger.getGlobal().log(Level.FINE, "EOF reading from input", e);
            return "";
        }
    }

    /**
     * {@inheritDoc}
     */
    public String nextLine(String prompt) {
        System.out.print(prompt);

        String line = getLine();
        while (line.isEmpty()) {
            line = getLine();
        }

        return line;
    }
}
