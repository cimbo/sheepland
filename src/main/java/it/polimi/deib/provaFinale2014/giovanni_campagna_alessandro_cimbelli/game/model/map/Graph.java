package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.RoadSquareIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

class Graph implements Serializable {
    private static final long serialVersionUID = 1L;
    private final SheepTile sheepsburg;
    private final SheepTile[] tiles;
    private final java.util.Map<RoadSquareIdentifier, RoadSquare> squareIdMap;

    Graph() {
        squareIdMap = new HashMap<RoadSquareIdentifier, RoadSquare>();

        // hardcoded map
        tiles = new SheepTile[3 * 6];
        sheepsburg = new SheepTile(TerrainType.SHEEPSBURG, 0);
        SheepTileIdentifier sheepsburgId = new SheepTileIdentifier(0);
        sheepsburg.setId(sheepsburgId);

        TerrainType[] types = TerrainType.values();
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                tiles[i * 3 + j] = new SheepTile(types[i], j);

                SheepTileIdentifier id = new SheepTileIdentifier(1 + i * 3 + j);
                tiles[i * 3 + j].setId(id);
            }
        }

        addTileEdge(tiles[0 * 3 + 0], tiles[0 * 3 + 1], 1);
        addTileEdge(tiles[0 * 3 + 0], tiles[3 * 3 + 0], 2);
        addTileEdge(tiles[0 * 3 + 0], tiles[3 * 3 + 1], 3);
        addTileEdge(tiles[0 * 3 + 1], tiles[3 * 3 + 1], 2);
        addTileEdge(tiles[0 * 3 + 1], tiles[0 * 3 + 2], 3);
        addTileEdge(tiles[0 * 3 + 1], tiles[1 * 3 + 0], 4);
        addTileEdge(tiles[0 * 3 + 2], tiles[3 * 3 + 1], 1);
        addTileEdge(tiles[0 * 3 + 2], tiles[1 * 3 + 2], 6);
        addTileEdge(tiles[0 * 3 + 2], tiles[1 * 3 + 0], 2);
        addTileEdge(tiles[0 * 3 + 2], tiles[1 * 3 + 1], 4);
        addTileEdge(tiles[1 * 3 + 0], tiles[1 * 3 + 1], 1);
        addTileEdge(tiles[1 * 3 + 1], tiles[1 * 3 + 2], 3);
        addTileEdge(tiles[1 * 3 + 1], tiles[2 * 3 + 2], 2);
        addTileEdge(tiles[1 * 3 + 2], tiles[2 * 3 + 0], 5);
        addTileEdge(tiles[1 * 3 + 2], tiles[2 * 3 + 1], 2);
        addTileEdge(tiles[1 * 3 + 2], tiles[2 * 3 + 2], 1);
        addTileEdge(tiles[2 * 3 + 0], tiles[2 * 3 + 1], 6);
        addTileEdge(tiles[2 * 3 + 0], tiles[5 * 3 + 0], 1);
        addTileEdge(tiles[2 * 3 + 0], tiles[5 * 3 + 1], 2);
        addTileEdge(tiles[2 * 3 + 0], tiles[5 * 3 + 2], 4);
        addTileEdge(tiles[2 * 3 + 1], tiles[5 * 3 + 2], 1);
        addTileEdge(tiles[2 * 3 + 1], tiles[2 * 3 + 2], 5);
        addTileEdge(tiles[3 * 3 + 0], tiles[3 * 3 + 1], 4);
        addTileEdge(tiles[3 * 3 + 0], tiles[3 * 3 + 2], 3);
        addTileEdge(tiles[3 * 3 + 0], tiles[4 * 3 + 1], 6);
        addTileEdge(tiles[3 * 3 + 1], tiles[4 * 3 + 1], 5);
        addTileEdge(tiles[3 * 3 + 2], tiles[4 * 3 + 0], 1);
        addTileEdge(tiles[3 * 3 + 2], tiles[4 * 3 + 1], 2);
        addTileEdge(tiles[4 * 3 + 0], tiles[4 * 3 + 1], 3);
        addTileEdge(tiles[4 * 3 + 0], tiles[4 * 3 + 2], 2);
        addTileEdge(tiles[4 * 3 + 0], tiles[5 * 3 + 0], 5);
        addTileEdge(tiles[4 * 3 + 1], tiles[5 * 3 + 0], 4);
        addTileEdge(tiles[4 * 3 + 2], tiles[5 * 3 + 0], 3);
        addTileEdge(tiles[4 * 3 + 2], tiles[5 * 3 + 1], 1);
        addTileEdge(tiles[5 * 3 + 0], tiles[5 * 3 + 1], 6);
        addTileEdge(tiles[5 * 3 + 1], tiles[5 * 3 + 2], 5);
        addTileEdge(sheepsburg, tiles[0 * 3 + 2], 5);
        addTileEdge(sheepsburg, tiles[1 * 3 + 2], 4);
        addTileEdge(sheepsburg, tiles[2 * 3 + 0], 3);
        addTileEdge(sheepsburg, tiles[3 * 3 + 1], 6);
        addTileEdge(sheepsburg, tiles[4 * 3 + 1], 1);
        addTileEdge(sheepsburg, tiles[5 * 3 + 0], 2);
    }

    SheepTile getRoot() {
        return sheepsburg;
    }

    SheepTile[] getSheepTiles() {
        return tiles;
    }

    Collection<RoadSquare> getRoadSquares() {
        return squareIdMap.values();
    }

    SheepTile getSheepTileFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        if (id == null) {
            throw new InvalidIdentifierException(id);
        }

        int content = id.getContent();

        if (content >= tiles.length + 1) {
            throw new InvalidIdentifierException(id);
        }

        SheepTile candidate;
        if (content == 0) {
            candidate = sheepsburg;
        } else {
            candidate = tiles[content - 1];
        }

        if (!candidate.getId().equals(id)) {
            throw new InvalidIdentifierException(id);
        }
        return candidate;
    }

    RoadSquare getRoadSquareFromId(GameObjectIdentifier id)
            throws InvalidIdentifierException {
        if (id == null) {
            throw new InvalidIdentifierException(id);
        }

        RoadSquare square = squareIdMap.get(id);

        if (square == null) {
            throw new InvalidIdentifierException(id);
        }

        return square;
    }

    private void addTileEdge(SheepTile st1, SheepTile st2, int weight) {
        RoadSquare rs = new RoadSquare(weight);

        st1.connect(st2, rs);
        st2.connect(st1, rs);
        rs.connect(st1, st2);

        SheepTileIdentifier id1 = st1.getId();
        SheepTileIdentifier id2 = st2.getId();
        int c1 = id1.getContent();
        int c2 = id2.getContent();
        RoadSquareIdentifier rsId = new RoadSquareIdentifier(c1, c2);
        rs.setId(rsId);
        squareIdMap.put(rsId, rs);
    }

    private void depthFirstHelper(Set<SheepTile> visited, GraphVisitor visitor,
            SheepTile node) {
        if (visited.contains(node)) {
            return;
        }

        visitor.visit(node);
        visited.add(node);

        for (Edge e : node.getNeighbors()) {
            depthFirstHelper(visited, visitor, e.getDestination());
        }
    }

    /**
     * Visit the graph in depth first order.
     * 
     * As detailed in GraphVisitor, the visit() method will be called once for
     * every SheepTile in the map (including Sheepsburg). The starting point is
     * not guaranteed to be Sheepsburg.
     * 
     * @param visitor
     */
    public void depthFirstVisit(GraphVisitor visitor) {
        Set<SheepTile> visited = new HashSet<SheepTile>();

        depthFirstHelper(visited, visitor, sheepsburg);
    }
}
