package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.GameInvariantViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameObject;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Lamb;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.SheepType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Represents a tile in the map where sheep can be placed.
 * 
 * @author Giovanni Campagna
 * 
 */
public class SheepTile extends GameObject implements Serializable {
    private static final long serialVersionUID = 1;
    private final Set<Sheep> sheepSet;
    private final TerrainType terrainType;
    private final int code;
    private boolean hasWolf;
    private final List<Edge> neighbors;

    /**
     * Construct a SheepTile of the given type.
     * 
     * The tile is initially empty. It will be initialized by the game logic
     * when the game is about to start.
     * 
     * @param type
     */
    public SheepTile(TerrainType type, int code) {
        neighbors = new ArrayList<Edge>();
        terrainType = type;
        this.code = code;
        sheepSet = new HashSet<Sheep>();
        hasWolf = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SheepTileIdentifier getId() {
        return (SheepTileIdentifier) super.getId();
    }

    /**
     * @return the type of this tile.
     */
    public TerrainType getTerrainType() {
        return terrainType;
    }

    void connect(SheepTile to, RoadSquare via) {
        neighbors.add(new Edge(to, via));
    }

    List<Edge> getNeighbors() {
        return neighbors;
    }

    // For testing
    boolean getHasBlackSheep() {
        return getNumberOfSheep(SheepType.BLACK_SHEEP) == 1;
    }

    /**
     * Place the wolf on this tile.
     */
    public void addWolf() {
        if (hasWolf) {
            throw new GameInvariantViolationException("Wolf is already on tile");
        }
        hasWolf = true;
    }

    /**
     * Remove the wolf from this tile (in order to place it somewhere else).
     */
    public void removeWolf() {
        if (!hasWolf) {
            throw new GameInvariantViolationException("Wolf is not on tile");
        }
        hasWolf = false;
    }

    /**
     * @return true, if the wolf is on this tile, false otherwise
     */
    public boolean getHasWolf() {
        return hasWolf;
    }

    /**
     * Add a sheep (male, female or lamb according to type)
     */
    public void addSheep(Sheep sheep) {
        if (sheepSet.contains(sheep)) {
            throw new GameInvariantViolationException(
                    "Sheep is already on this tile");
        }
        sheepSet.add(sheep);
    }

    /**
     * Remove a sheep (male or female according to type)
     */
    public void removeSheep(Sheep sheep) {
        if (!sheepSet.contains(sheep)) {
            throw new GameInvariantViolationException(
                    "Sheep is not on this tile");
        }
        sheepSet.remove(sheep);
    }

    /**
     * Retrieve the number of sheep on this tile of a given type.
     * 
     * @param type
     *            a sheep type
     * @return the number
     */
    public int getNumberOfSheep(SheepType type) {
        int number = 0;

        for (Sheep s : sheepSet) {
            if (s.getType() == type) {
                number++;
            }
        }

        return number;
    }

    /**
     * Checks if this tile contains the given sheep.
     * 
     * @param sheep
     *            a sheep
     * @return true, if the sheep is currently on this tile, false otherwise
     */
    public boolean containsSheep(Sheep sheep) {
        return sheepSet.contains(sheep);
    }

    /**
     * Retrieves any sheep of the given type.
     * 
     * In case multiple sheep are present of the same type, it is undefined
     * which one is returned. One should not assume that calling this method
     * twice will return the same sheep.
     * 
     * If no sheep is present of that type, this method returns null.
     * 
     * @param type
     *            a sheep type
     * @return a sheep of the given type on this tile, or null
     */
    public Sheep getAnySheepOfType(SheepType type) {
        for (Sheep s : sheepSet) {
            if (s.getType() == type) {
                return s;
            }
        }

        return null;
    }

    /**
     * Retrieves a newly allocated array containing all the sheep on this tile.
     * 
     * @return an array of all the sheep
     */
    public Sheep[] getAllSheep() {
        Sheep[] allSheep = new Sheep[sheepSet.size()];
        sheepSet.toArray(allSheep);

        return allSheep;
    }

    /**
     * Grow all lambs, increasing their age.
     * 
     * Lambs that would become adult sheep are discarded.
     */
    public Collection<Lamb> growLambs() {
        List<Lamb> grownLambs = new ArrayList<Lamb>();

        for (Sheep s : sheepSet) {
            if (!(s instanceof Lamb)) {
                continue;
            }

            Lamb l = (Lamb) s;

            l.grow();
            if (l.getAge() == Lamb.GROWING_AGE) {
                grownLambs.add(l);
            }
        }

        for (Lamb l : grownLambs) {
            removeSheep(l);
        }

        return grownLambs;
    }

    /**
     * Get a random sheep.
     * 
     * This chooses a sheep at random from this tile, and returns its type. This
     * does not include the black sheep.
     * 
     * Note that this does not guaranteed that such a sheep exists, only that
     * the type is chosen with probability proportional to the relative number
     * of sheep.
     * 
     * @return the type of the randomly chosen sheep
     */
    public Sheep getRandomSheep(Random dice) {
        List<Sheep> sheepArray = new ArrayList<Sheep>();

        for (Sheep s : sheepSet) {
            if (s.getType() != SheepType.BLACK_SHEEP) {
                sheepArray.add(s);
            }
        }

        if (sheepArray.isEmpty()) {
            return null;
        } else {
            return sheepArray.get(dice.nextInt(sheepArray.size()));
        }
    }

    /**
     * Obtain the score value of this tile. This is calculated as the number of
     * sheep in the tile, taking into account that the black sheep is worth 2
     * points.
     * 
     * @return the score value of the tile.
     */
    public int getScoreValue() {
        int score = 0;
        for (Sheep s : sheepSet) {
            if (s.getType() == SheepType.BLACK_SHEEP) {
                score += 2;
            } else {
                score += 1;
            }
        }

        return score;
    }

    @Override
    public String toString() {
        return String
                .format("SheepTile of type %s, code %d", terrainType, code);
    }
}
