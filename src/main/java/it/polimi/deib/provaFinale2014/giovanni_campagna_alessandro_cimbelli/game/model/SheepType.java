package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

/**
 * The type of a sheep.
 * 
 * @author Giovanni Campagna
 */
public enum SheepType {
    /**
     * Female (Ewe): normal sheep, no special properties.
     * 
     * The name of the enum is historical.
     */
    FEMALE_SHEEP,
    /**
     * Male (Ram): normal sheep, can be mated with an ewe to produce a lamb.
     */
    RAM,
    /**
     * Black sheep: moves randomly at the beginning of the turn, worth double
     * score.
     */
    BLACK_SHEEP,
    /**
     * Lamb: created by mating an ewe and a ram, grows into an adult sheep after
     * two turns.
     */
    LAMB;
}
