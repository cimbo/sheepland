package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GamePhase;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.GameState;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.GameBoard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Sheep;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.PlayerIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.SheepTileIdentifier;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.AbstractSenderThread;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.IORunnable;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerSender;

import java.io.IOException;
import java.util.Map;

/**
 * This is the outgoing message queue for the server side.
 * 
 * All calls to ServerSender methods are queued and executed in worker thread at
 * a later time. The SenderThread might call back into ServerPlayer to signal IO
 * errors.
 * 
 * See the documentation in {@link AbstractSenderThread} for details.
 * 
 * @author Giovanni Campagna
 */
public class SenderThread extends AbstractSenderThread implements ServerSender {
    private final ServerPlayer player;
    private final ServerSender sender;

    /**
     * Construct a new sender thread for the given player and outgoing
     * connection.
     * 
     * The sender thread is strongly associated to the sender, so it must be
     * closed when the sender itself is closed.
     * 
     * @param player
     *            the player owning this thread
     * @param sender
     *            the sender wrapped by this queue
     */
    public SenderThread(ServerPlayer player, ServerSender sender) {
        super();

        this.player = player;
        this.sender = sender;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reportIOException(final IOException exception) {
        super.reportIOException(exception);
        player.reportIOWriteException(this);
    }

    /**
     * {@inheritDoc}
     */
    public void sendSetup(final RuleSet rules, final GameBoard board,
            final PlayerIdentifier self) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendSetup(rules, board, self);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendMoveAcknowledged(final Exception result) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendMoveAcknowledged(result);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendForceSynchronize(final RuleSet rules,
            final GameBoard newBoard, final PlayerIdentifier newSelf,
            final GameState newState) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendForceSynchronize(rules, newBoard, newSelf, newState);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendAdvanceNextTurn(final int turn, final GamePhase phase) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendAdvanceNextTurn(turn, phase);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendBlackSheepMoved(final SheepTileIdentifier destination) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendBlackSheepMoved(destination);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendWolfMoved(final SheepTileIdentifier destination,
            final SheepIdentifier eatenSheep) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendWolfMoved(destination, eatenSheep);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendLambsGrew(final SheepTileIdentifier where,
            final Sheep[] newAdultSheep) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendLambsGrew(where, newAdultSheep);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendDidExecuteMove(final Move move) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendDidExecuteMove(move);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendLoginAnswer(final boolean answer) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendLoginAnswer(answer);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendPing() {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendPing();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendEndGame(final Map<PlayerIdentifier, Integer> scores) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendEndGame(scores);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendPlayerLeft(final PlayerIdentifier who, final boolean clean) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendPlayerLeft(who, clean);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendPlayerJoined(final PlayerIdentifier who) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendPlayerJoined(who);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public void sendAbandonComplete() {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendAbandonComplete();
            }
        });
    }
}
