package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.RuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.TwoPlayersRuleSet;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.ServerGameEngine;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class holds the "lobby", the waiting area for new game.
 * 
 * It holds players as they connect, and constructs and starts the game when
 * ready.
 * 
 * This class is threadsafe.
 * 
 * @author Alessandro Cimbelli
 */
public class GameIncubator {
    private final long delay;
    private Timer timer;
    private Collection<ServerPlayer> array;

    /**
     * Construct a game incubator.
     */
    public GameIncubator(GameOptions options) {
        delay = options.getGameStartingTimeout() * 1000;
        array = new ArrayList<ServerPlayer>();
    }

    /**
     * This method add a player that wants start a new game into a queue
     * 
     * This method might potentially start a timer to begin the game at a later
     * time.
     * 
     * @param player
     *            the SeverPlayer that wants play
     */
    public synchronized void addServerPlayer(ServerPlayer player) {
        if (array.size() == RuleSet.getMaxNumberOfPlayers() - 1) {
            array.add(player);
            startGameNow();
        } else if (array.size() == 1) {
            array.add(player);
            timer = new Timer();
            timer.schedule(new Task(), delay);
        } else {
            array.add(player);
        }
    }

    // For testing
    synchronized boolean containsServerPlayer(ServerPlayer player) {
        return array.contains(player);
    }

    // For testing
    synchronized boolean isTimerRunning() {
        return timer != null;
    }

    // For testing
    synchronized void close() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        array.clear();
    }

    /**
     * Remove the player from the queue
     * 
     * If the queue is now too small to start a game, any pending timers are
     * cancelled.
     * 
     * Removing might have no effect in case the timer to start the game already
     * fired. In that case, the player is relably added to a new Game, and it's
     * the game responsability to handle the player's intention to leave.
     * 
     * @param player
     *            the player to remove
     */
    public synchronized void maybeRemoveServerPlayer(ServerPlayer player) {
        array.remove(player);

        if (array.size() < 2 && timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    // friendly for testing
    void startGameNow() {
        Task task = new Task();
        task.startGame();
    }

    /**
     * 
     * This method is called by the task to take the players which it must add
     * to the game and free the new "buffer of player" for a new game
     * 
     * @return
     */
    private synchronized Collection<ServerPlayer> takePlayers() {
        Collection<ServerPlayer> tmp = array;
        array = new ArrayList<ServerPlayer>();

        // Reset the timer
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        return tmp;
    }

    /**
     * This is an InnerClass that represents the task to do into the routine of
     * building new games
     * 
     * @author Alessandro Cimbelli
     * 
     */
    private class Task extends TimerTask {

        private Collection<ServerPlayer> players;

        /**
         * {@inheritDoc}
         */
        @Override
        public void run() {
            startGame();
        }

        /**
         * Start the game with the players currently in the incubator.
         */
        public void startGame() {
            players = takePlayers();

            if (playersAreEnough()) {
                createNewGame();
            } else {
                // one of the players disconnected while running the timer
                // and we raced with the timer, so now the BuildGame.array
                // is empty but we're not ready to run the game
                // readd the other players
                // (normally this only happens with 2 player games, but can
                // happen with 3 or 4 if 2 or 3 players disconnect at the same
                // time - obviously, being a race, it becomes increasingly
                // less likely)
                for (ServerPlayer player : players) {
                    addServerPlayer(player);
                }
                return;
            }
        }

        private void createNewGame() {
            try {
                ServerGameEngine gameEngine = new ServerGameEngine(
                        new MoveDelegate(players), chooseRules());
                new Game(gameEngine, players).start();
            } catch (RuleViolationException e) {
                Logger.getGlobal().log(
                        Level.SEVERE,
                        "Failed to setup the game engine due "
                                + "to a violation of the rules", e);
            }
        }

        /**
         * This method choose the RouleSet according to the number of players
         * which joined into the game
         * 
         * @return the opportune RuleSet
         */
        private RuleSet chooseRules() {
            RuleSet rules;
            if (players.size() == 2) {
                rules = new TwoPlayersRuleSet();
            } else {
                rules = new RuleSet();
            }

            rules.seal();
            return rules;
        }

        /**
         * This method verify if the number number of players is enough to start
         * the timer for the task
         * 
         * @return
         */
        private boolean playersAreEnough() {
            return players.size() >= 2;
        }
    }
}