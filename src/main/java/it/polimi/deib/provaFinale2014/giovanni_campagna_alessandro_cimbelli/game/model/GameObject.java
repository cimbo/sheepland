package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.serialization.GameObjectIdentifier;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * This is the base class of all objects that can be remotely identified.
 * 
 * The primary property of a game object is that it has a game object
 * identifier, which can be used to refer to "equivalent" game objects in
 * different boards (equivalent objects could be be the same player on the
 * client and on the server, or the same tile in two different games).
 * 
 * How a game object is retrieved from an identifier, or how a game object is
 * assigned an identifier is dependent on the specific game object subclass,
 * although in general it will be through a method on the game board or the map.
 * Please refer to the documentation for the game object identifiers for
 * details.
 * 
 * Because game objects are expected to be identifiable across serializations,
 * attempting to serialize a game object that does not have a defined ID will
 * fail.
 * 
 * @author giovanni
 * 
 */
public abstract class GameObject implements Serializable {
    private static final long serialVersionUID = 1L;

    private GameObjectIdentifier id;

    /**
     * Retrieve the game object identifier of this game object.
     * 
     * This method will throw if the identifier was not set already.
     * 
     * @return the identifier (which will never be null)
     */
    public GameObjectIdentifier getId() {
        if (id == null) {
            throw new IllegalStateException("Object does not have an id yet");
        }

        return id;
    }

    /**
     * Set the identifier on this game object (allowing future retrieval with
     * getId()).
     * 
     * Attempting to set the identifier twice, even to equal or identical
     * values, will throw an exception.
     * 
     * @param id
     *            the new identifier
     */
    public void setId(GameObjectIdentifier id) {
        if (this.id != null) {
            throw new IllegalStateException("Object already has an id");
        }
        if (id == null) {
            throw new IllegalArgumentException(
                    "The game object identifier cannot be null");
        }

        this.id = id;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        if (id == null) {
            throw new IllegalStateException(
                    "Before serializing a game object you must set an ID on it");
        }

        out.defaultWriteObject();
    }
}
