package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

/**
 * The interface to handle new connections from a ServerNetwork.
 * 
 * @author Giovanni Campagna
 */
public interface ServerConnectionHandler {
    /**
     * Handle a newly accepted connection.
     * 
     * @param connection
     *            the new connection
     */
    void handleNewConnection(ServerConnection connection);
}
