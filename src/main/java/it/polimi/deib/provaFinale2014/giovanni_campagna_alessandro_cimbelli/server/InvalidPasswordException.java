package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.server;

/**
 * Signal a failed login due to wrong password.
 * 
 * @author Giovanni Campagna
 */
public class InvalidPasswordException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Construct the exception with a default message.
     */
    public InvalidPasswordException() {
        super("Invalid password");
    }
}
