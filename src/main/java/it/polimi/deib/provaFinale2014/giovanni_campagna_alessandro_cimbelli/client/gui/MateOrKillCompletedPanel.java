package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * The overlay panel used for a successfull killing or mating. Shows the player
 * doing it, and the type of sheep affected.
 * 
 * @author Alessandro Cimbelli
 */
public class MateOrKillCompletedPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 527;
    private static final int Y_SIZE = 100;

    /**
     * Construct a new panel, with the given player icon, sheep icon and text.
     */
    public MateOrKillCompletedPanel(PlayerIcon pawn, SheepIcon lamb, String text) {
        setBackground(new Color(21, 170, 255));
        this.setSize(X_SIZE, Y_SIZE);
        setLayout(null);

        JLabel label = new JLabel(text);
        label.setSize(X_SIZE, Y_SIZE);
        label.setLocation(0, 0);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);
        label.setFont(new Font("Arial", Font.BOLD, 16));
        add(label);

        if (pawn != null) {
            this.add(pawn);
            pawn.setLocation((int) (getWidth() * 0.14),
                    (int) ((getHeight() - pawn.getHeight()) * 0.5));
        }

        if (lamb != null) {
            add(lamb);
            lamb.setLocation((int) (getWidth() * 0.70),
                    (int) ((getHeight() - lamb.getHeight()) * 0.5));
        }
    }
}
