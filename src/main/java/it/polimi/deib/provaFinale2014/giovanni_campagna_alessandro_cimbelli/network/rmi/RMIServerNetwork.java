package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.rmi;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerConnectionHandler;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ServerNetwork;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The RMI implementation of ServerNetwork
 * 
 * @author Alessandro Cimbelli
 */
public class RMIServerNetwork extends UnicastRemoteObject implements
        ServerNetwork, Server {
    private static final long serialVersionUID = 1L;

    private ServerConnectionHandler handler;
    private static Registry registry;

    /**
     * Construct a new RMIServerNetwork.
     */
    public RMIServerNetwork() throws RemoteException {
        super(0);
        synchronized (RMIServerNetwork.class) {
            if (registry == null) {
                registry = LocateRegistry.createRegistry(1099);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void setConnectionHandler(ServerConnectionHandler handler) {
        this.handler = handler;
    }

    /**
     * {@inheritDoc}
     */
    public void startListening() throws IOException {
        registry.rebind(Server.SERVER_NAME, this);

        Logger.getGlobal().info("Server listening on RMI");
    }

    /**
     * {@inheritDoc}
     */
    public void stopListening() {
        try {
            registry.unbind(Server.SERVER_NAME);
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE,
                    "Error unexporting Server RMI object", e);
        } catch (NotBoundException e) {
            Logger.getGlobal().log(Level.FINE,
                    "Error unexporting Server RMI object", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public Game connect(Client client) throws RemoteException {
        RMIServerConnection connection = new RMIServerConnection(client);

        handler.handleNewConnection(connection);

        return connection;
    }
}
