package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * This class represent the log in panel through which the client can put
 * username and password. The panel therefore contains action listeners that
 * catch the data input to sent them to the server.
 * 
 * @author Alessandro Cimbelli
 */
public class SubmitUsrPswPanel extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 350;
    private static final int Y_SIZE = 180;
    private static final int X_USERNAME = 300;
    private static final int Y_USERNAME = 35;
    private static final int X_PASSWORD = 300;
    private static final int Y_PASSWORD = 35;
    private final GraphicUIManager uiManager;
    private JTextField username;
    private JTextField password;
    private JLabel error;

    /**
     * Construct a new SubmitUsrPswPanel with the given GraphicUIManager.
     * 
     * @param ui
     *            the UI manager used for logging in.
     */
    public SubmitUsrPswPanel(GraphicUIManager ui) {
        super(X_SIZE, Y_SIZE, "loginPanel.png");
        uiManager = ui;

        setLayout(null);
        addUsernameText();
        addPasswordText();
        addLoginButton();
        setVisible(true);
    }

    private void addUsernameText() {
        username = new JTextField();
        JLabel user = new JLabel("Username");
        user.setForeground(new Color(255, 255, 255));
        user.setBackground(new Color(0, 0, 255));
        user.setSize(X_USERNAME, 15);
        username.setName("Username");
        username.setBackground(new Color(255, 255, 0xA0));
        username.setMaximumSize(new Dimension(X_USERNAME, Y_USERNAME));
        username.setMinimumSize(new Dimension(X_USERNAME, Y_USERNAME));
        username.setSize(X_USERNAME, Y_USERNAME);
        username.setEditable(true);
        username.setFont(new Font("MyFont", Font.BOLD, 16));

        this.add(username);
        username.setLocation(
                (int) (getWidth() / 2 - username.getWidth() * 0.5),
                (int) (getHeight() / 4.5 - username.getHeight() * 0.5));
        this.add(user);
        user.setLocation(username.getX(), username.getY() - user.getHeight());

        username.addActionListener(new SubmitActionListener());
    }

    private void addPasswordText() {
        password = new JPasswordField();
        JLabel pass = new JLabel("Password");
        pass.setForeground(new Color(255, 255, 255));
        pass.setBackground(new Color(0, 0, 255));
        pass.setSize(X_PASSWORD, 15);
        password.setName("Password");
        password.setBackground(new Color(255, 255, 0xA0));
        password.setMaximumSize(new Dimension(X_PASSWORD, Y_PASSWORD));
        password.setMinimumSize(new Dimension(X_PASSWORD, Y_PASSWORD));
        password.setSize(X_PASSWORD, Y_PASSWORD);
        password.setEditable(true);
        password.setFont(new Font("MyFont", Font.BOLD, 16));

        this.add(password);
        password.setLocation(
                (int) (getWidth() / 2 - password.getWidth() * 0.5),
                (int) (getHeight() / 2 - password.getHeight() * 0.5));
        this.add(pass);
        pass.setLocation(password.getX(), password.getY() - pass.getHeight());

        password.addActionListener(new SubmitActionListener());
    }

    private void addLoginButton() {
        LoginButton button = new LoginButton();
        this.add(button);
        button.setLocation((int) (getWidth() / 2 - button.getWidth() * 0.5),
                (int) (getHeight() / 1.25 - button.getHeight() * 0.5));

        button.addActionListener(new SubmitActionListener());
    }

    private class SubmitActionListener implements ActionListener {
        /**
         * {@inheritDoc}
         */
        public void actionPerformed(ActionEvent e) {
            if ("binosi".equals(username.getText())
                    && "nero".equals(password.getText())) {
                showMessage("Only binosi can be black binosi");
                return;
            }

            uiManager.getCurrentGame().joinGame(username.getText(),
                    password.getText());
        }
    }

    private static class LoginButton extends JButton {
        private static final long serialVersionUID = 1L;
        private static final int X_SUBMIT = 150;
        private static final int Y_SUBMIT = 46;

        /**
         * Construct a new login button.
         */
        public LoginButton() {
            this.setSize(X_SUBMIT, Y_SUBMIT);
            setVisible(true);
            setLayout(null);

            JComponent child = new ImageComponent(145, 40, "log_in.png");
            add(child);
            child.setLocation((X_SUBMIT - child.getWidth()) / 2,
                    (Y_SUBMIT - child.getHeight()) / 2);
        }
    }

    /**
     * Show a custom message on the login form.
     * 
     * The message will be shown between the password field and the log in
     * button.
     * 
     * @param message
     *            the message to show.
     */
    public void showMessage(String message) {
        if (error != null) {
            error.setText(message);
            return;
        }
        error = new JLabel(message);
        error.setBackground(new Color(255, 0, 0));
        error.setSize(getWidth(), 15);
        error.setVisible(true);
        this.add(error, 0);
        error.setLocation(password.getX(),
                password.getY() + password.getHeight());
        Timer timer = new Timer(2 * 1000, new ActionListener() {
            /**
             * {@inheritDoc}
             */
            public void actionPerformed(ActionEvent e) {
                remove(error);
                repaint();
                error = null;
            }
        });
        timer.setRepeats(false);
        timer.start();
    }
}
