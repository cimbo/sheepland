package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The base class for sender threads.
 * 
 * This class queues commands for writing in a background thread.
 * 
 * Note that the thread is not a daemon thread - you must close() it if you want
 * to shutdown the JVM cleanly.
 * 
 * @author Giovanni Campagna
 */
public abstract class AbstractSenderThread extends Thread {
    private final Queue<IORunnable> queue;
    private boolean running;

    /**
     * Construct a new AbstractSenderThread with an empty queue.
     */
    protected AbstractSenderThread() {
        queue = new LinkedList<IORunnable>();
        running = true;
    }

    /**
     * Report an error writing.
     * 
     * This method is protected to allow subclasses that wish to handle the
     * error directly (for example by informing the application logic of the
     * failure).
     * 
     * The default implementation will stop the thread.
     * 
     * @param exception
     *            the failure
     */
    protected void reportIOException(final IOException exception) {
        running = false;
    }

    private IORunnable getNextItem() {
        IORunnable r = queue.poll();
        while (running && r == null) {
            try {
                wait();
            } catch (InterruptedException e) {
                // do nothing, and go for the next iteration of the
                // loop
                //
                // (this comment courtesy of sonar)
            }

            r = queue.poll();
        }

        return r;
    }

    /**
     * Run the background sender thread
     */
    @Override
    public void run() {
        while (true) {
            try {
                IORunnable r;

                synchronized (this) {
                    r = getNextItem();
                    notifyAll();

                    if (!running) {
                        return;
                    }
                }
                if (r != null) {
                    r.run();
                }

            } catch (IOException e) {
                Logger.getGlobal().log(Level.INFO,
                        "IO exception while sending a command", e);

                reportIOException(e);
            } catch (Exception e) {
                Logger.getGlobal().log(Level.SEVERE,
                        "Unhandled exception while sending a command", e);
            }
        }
    }

    /**
     * Flush the sender queue, blocking until it is empty.
     * 
     * This method is not thread-safe wrt further additions from other threads.
     */
    public synchronized void flushQueue() {
        while (running && !queue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                Logger.getGlobal()
                        .log(Level.FINEST,
                                "Interrupted exception while flushing the sender queue",
                                e);
            }
        }
    }

    /**
     * Close the sender queue.
     * 
     * This method will not close the underlying connection.
     * 
     * Any pending messages are discarded.
     */
    public synchronized void close() {
        running = false;
        queue.clear();
        notify();
    }

    /**
     * Queue an item for later writing.
     * 
     * @param item
     *            the item to write
     */
    protected synchronized void safePut(IORunnable item) {
        queue.offer(item);
        notify();
    }
}
