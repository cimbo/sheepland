package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit;

/**
 * A collection of utilities for the sheepland animation framework.
 * 
 * @author Giovanni Campagna
 */
public class Util {
    private Util() {
        // utility class
    }

    /**
     * Linearly interpolate the given value between the two bounds.
     * 
     * @param from
     *            the lower bound
     * @param to
     *            the upper bound
     * @param progress
     *            the value to interpolate
     * @return the interpolated value
     */
    public static double interpolate(double from, double to, double progress) {
        return from * (1.0 - progress) + to * progress;
    }
}
