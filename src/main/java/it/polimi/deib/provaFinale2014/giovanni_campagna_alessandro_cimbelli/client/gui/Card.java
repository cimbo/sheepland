package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is useful to represent the graphical concept of a card or region
 * 
 * @author Alessandro Cimbelli
 * 
 */
public class Card extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 60;
    private static final int Y_SIZE = 60;
    private final Map<Integer, NumberOfCard> mapCardNumber;
    private NumberOfCard currentNumberCard;
    private boolean drawMask = false;
    private final TerrainType type;
    private int numberOfCard = 0;

    /**
     * Construct a new card of the given type.
     * 
     * @param type
     *            the type of card
     */
    public Card(TerrainType type) {
        super(X_SIZE, Y_SIZE, type.name().toLowerCase() + "Card.png");
        this.type = type;
        mapCardNumber = new HashMap<Integer, NumberOfCard>();
        fillMapCardNumber();
    }

    /**
     * Retrieve the type of this card
     * 
     * @return the card type
     */
    public TerrainType getType() {
        return type;
    }

    /**
     * Check a mask is drawn on top of this card.
     * 
     * @return true if a mask is drawn, false otherwise
     */
    public boolean getDrawMask() {
        return drawMask;
    }

    /**
     * Set the card to draw a mask.
     * 
     * Passing true will cause a semitrasparent layer to be drawn on top the
     * card, making it appear "disabled".
     * 
     * @param mask
     *            if the mask is to be drawn or not.
     */
    public void setDrawMask(boolean mask) {
        drawMask = mask;
        repaint();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (drawMask) {
            g.setColor(new Color(0f, 0f, 0f, 0.6f));
            g.fillRect(0, 0, X_SIZE, Y_SIZE);
        }
    }

    /**
     * Retrieve the number associated (overlaid) with this card icon.
     * 
     * @return the number
     */
    public int getNumberOfCard() {
        return numberOfCard;
    }

    private void fillMapCardNumber() {
        mapCardNumber.put(0, new NumberOfCard("0card.png"));
        mapCardNumber.put(1, new NumberOfCard("1card.png"));
        mapCardNumber.put(2, new NumberOfCard("2card.png"));
        mapCardNumber.put(3, new NumberOfCard("3card.png"));
        mapCardNumber.put(4, new NumberOfCard("4card.png"));
        mapCardNumber.put(5, new NumberOfCard("5card.png"));
    }

    /**
     * Change the number overlaid on this card to the given number.
     * 
     * @param number
     *            the number, must be between 0 and 5 inclusive.
     */
    public void setNumberOfCard(int number) {
        numberOfCard = number;

        Container p = getParent();
        if (currentNumberCard != null) {
            p.remove(currentNumberCard);
        }
        currentNumberCard = mapCardNumber.get(number);
        p.add(currentNumberCard, 0);
        currentNumberCard
                .setLocation((int) (getX() + getWidth() - currentNumberCard
                        .getWidth() * 0.5),
                        (int) (getY() + getHeight() - currentNumberCard
                                .getHeight() * 0.5));
    }
}
