package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

/**
 * The identifier of a command sent to the client in the command header.
 * 
 * @author Alessandro Cimbelli
 */
public enum ClientOperation {

    /**
     * There is a logic connection between the order of this Enum and
     * ServerSender methods
     * 
     * @author Alessandro Cimbelli
     */

    SETUP, MOVE_ACKNOWLEDGED, FORCE_SYNCHRONIZE, ADVANCE_NEXT_TURN, BLACK_SHEEP_MOVED, WOLF_MOVED, LAMBS_GREW, DID_EXECUTE_MOVE, LOGIN_ANSWER, PING, END_GAME, PLAYER_LEFT, PLAYER_JOINED, ABANDON_COMPLETE;

}
