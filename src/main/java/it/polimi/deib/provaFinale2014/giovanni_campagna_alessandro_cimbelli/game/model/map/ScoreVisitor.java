package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.map;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

/**
 * A graph visitor that collects the score values of each tile.
 * 
 * This can be considered an implementation detail of {@link GameMap}.
 * 
 * @author Giovanni Campagna
 */
class ScoreVisitor implements GraphVisitor {
    int[] scoreValues;

    /**
     * Construct a score visitor.
     */
    public ScoreVisitor() {
        scoreValues = new int[TerrainType.NUMBER_OF_TERRAINS];
    }

    /**
     * {@inheritDoc}
     */
    public void visit(SheepTile tile) {
        if (tile.getTerrainType() == TerrainType.SHEEPSBURG) {
            return;
        }

        scoreValues[tile.getTerrainType().ordinal()] += tile.getScoreValue();
    }

    /**
     * Retrieves the computed score values.
     * 
     * This method is only valid after visiting the graph.
     */
    public int[] getScoreValues() {
        return scoreValues;
    }
}
