package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * A generic animal icon with a number.
 * 
 * @author Alessandro Cimbelli
 */
public class AnimalIcon extends ImageComponent {
    private static final long serialVersionUID = 1L;

    private static final int X_SIZE = 24;
    private static final int Y_SIZE = 16;
    private NumberOfAnimal currentNumber;
    private final Map<Integer, NumberOfAnimal> mapNumberAnimal;

    /**
     * Construct and load the animal icon with the given path.
     * 
     * After construction, and before the first updateNumber() call, the number
     * is not visible.
     * 
     * @param path
     *            the image resource path
     */
    public AnimalIcon(String path) {
        super(X_SIZE, Y_SIZE, path);

        mapNumberAnimal = fillNumberAnimalMap();
        currentNumber = new NumberOfAnimal("1animal.png");
    }

    private Map<Integer, NumberOfAnimal> fillNumberAnimalMap() {
        Map<Integer, NumberOfAnimal> map = new HashMap<Integer, NumberOfAnimal>();

        for (int i = 1; i <= 21; i++) {
            map.put(i, new NumberOfAnimal(i + "animal.png"));
        }

        return map;
    }

    /**
     * Update the number on top of this icon.
     * 
     * @param number
     *            the number
     */
    public void updateNumber(int number) {
        remove(currentNumber);
        repaint();
        currentNumber = mapNumberAnimal.get(number);
        this.add(currentNumber);
        currentNumber.setLocation(
                (int) (getWidth() / 2 - currentNumber.getWidth() * 0.5),
                (int) (getHeight() / 2 - currentNumber.getHeight() * 0.5));
    }
}
