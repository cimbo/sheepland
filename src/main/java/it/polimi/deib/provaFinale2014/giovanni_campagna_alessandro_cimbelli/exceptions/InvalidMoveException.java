package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions;

/**
 * The superclass of all exceptions caused by bad game moves.
 * 
 * @author Giovanni Campagna
 * 
 */
public class InvalidMoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Construct a invalid move exception with the given message
     * 
     * @param message
     */
    public InvalidMoveException(String message) {
        super(message);
    }
}
