package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

/**
 * The icon of a fence.
 * 
 * @author Alessandro Cimbelli
 */
public class Fence extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 30;
    private static final int Y_SIZE = 30;

    /**
     * Construct a new fence, with the given image path.
     * 
     * @param path
     *            the path of the fence image
     */
    public Fence(String path) {
        super(X_SIZE, Y_SIZE, path);
    }

}
