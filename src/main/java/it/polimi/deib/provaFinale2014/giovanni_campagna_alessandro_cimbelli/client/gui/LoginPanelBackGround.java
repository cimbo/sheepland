package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.gui.toolkit.ImageComponent;

/**
 * The full login view.
 * 
 * @author Alessandro Cimbelli
 */
public class LoginPanelBackGround extends ImageComponent {
    private static final long serialVersionUID = 1L;
    private static final int X_SIZE = 647;
    private static final int Y_SIZE = 720;
    private SubmitUsrPswPanel loginTable;

    private final GraphicUIManager uiManager;

    /**
     * Construct a new login view, with the given UI manager (which is used to
     * actually log in)
     * 
     * @param ui
     *            the owner ui manager
     */
    public LoginPanelBackGround(GraphicUIManager ui) {
        super(X_SIZE, Y_SIZE, "login.png");
        uiManager = ui;

        setLayout(null);
        addUsrPswSubmit();
    }

    private void addUsrPswSubmit() {
        loginTable = new SubmitUsrPswPanel(uiManager);

        add(loginTable);
        loginTable.setLocation(
                (int) (getWidth() / 2 - loginTable.getWidth() * 0.5),
                (int) (getHeight() / 2 - loginTable.getHeight() * 0.5));

    }

    /**
     * Add a password error message.
     */
    public void addPasswordError() {
        showMessage("Wrong Password");
    }

    /**
     * Add a generic message.
     * 
     * @param message
     *            the message
     */
    public void showMessage(String message) {
        loginTable.showMessage(message);
    }
}
