package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.InvalidIdentifierException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.exceptions.RuleViolationException;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.Player;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainCard;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.model.TerrainType;

/**
 * The "buy" command, used for buying cards during the playing phases, and
 * during the market.
 * 
 * @author Giovanni Campagna
 */
class BuyCommand extends BaseCommand {
    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("buy <TERRAIN TYPE> [from <PLAYER>]: buy a terrain close to your shepherd or from the market");
        baseHelpTerrainType(ui);
    }

    private void parseAndDoBuyTerrain(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException,
            InvalidIdentifierException {
        if (ui.getChosenShepherd() == null) {
            ui.showMessage("As the farm owner, you would never engage with cards directly.");
            if (GameOptions.get().getShowHints()) {
                ui.showMessage("[Use the be command to choose a shepherd before buying a card]");
            }
            return;
        }

        TerrainCard card = ui.getCurrentGame().getGameBoard()
                .getCheapestTerrainCard(tokenizer.readTerrainType(), null);
        if (card == null) {
            ui.showMessage("It seems that all cards of this type are gone already.");
            return;
        }

        ui.getCurrentGame().buyTerrainCard(ui.getChosenShepherd(), card);
    }

    private void parseAndDoBuyMarket(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException {
        TerrainType type = tokenizer.readTerrainType();
        tokenizer.expect("from");

        String username = tokenizer.readAny();
        Player from = null;
        for (Player candidate : ui.getCurrentGame().getAllPlayers()) {
            if (candidate.getName().equals(username)) {
                from = candidate;
                break;
            }
        }
        if (from == null) {
            throw new InvalidInputException(username);
        }

        TerrainCard card = ui.getCurrentGame().getGameBoard()
                .getCheapestTerrainCard(type, from);
        if (card == null) {
            ui.showMessage("Enemy %s is not selling a card of this type.",
                    Representation.playerToString(from));
            return;
        }

        ui.getCurrentGame().buyMarketCard(card);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException, RuleViolationException,
            InvalidIdentifierException {
        if (ui.getCurrentGame().getGamePhase().isPlaying()) {
            parseAndDoBuyTerrain(tokenizer, ui);
        } else {
            parseAndDoBuyMarket(tokenizer, ui);
        }
    }
}
