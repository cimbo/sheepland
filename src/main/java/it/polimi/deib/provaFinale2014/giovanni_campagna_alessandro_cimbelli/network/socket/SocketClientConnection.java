package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.socket;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientConnection;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientReceiver;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientSender;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The socket implementation of ClientConnection
 * 
 * @author Alessandro Cimbelli
 */
public class SocketClientConnection implements ClientConnection, ClientSender {

    private final Socket socket;
    private final ObjectOutputStream socketOutputStream;
    private final InputStream socketInputStream;
    private Thread readerThread;

    /**
     * Construct a new SocketClientConnection, connected to the given address.
     * 
     * @param endpoint
     *            the server address
     * @throws IOException
     *             if connecting failed
     */
    public SocketClientConnection(SocketAddress endpoint) throws IOException {
        socket = new Socket();
        socket.connect(endpoint);
        socketInputStream = socket.getInputStream();
        socketOutputStream = new ObjectOutputStream(socket.getOutputStream());
        // flush immediately to unblock the reader thread on the other side
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public synchronized void close() {
        try {
            socketInputStream.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, "IO exception closing", e);
        }
        try {
            socketOutputStream.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, "IO exception closing", e);
        }
        try {
            socket.close();
        } catch (IOException e) {
            Logger.getGlobal().log(Level.FINE, "IO exception closing", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public ClientSender getClientSender() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public void setClientReceiver(ClientReceiver client) {
        // Initialize ReaderThread

        readerThread = new ClientReaderThread(this, client, socketInputStream);
        readerThread.start();
    }

    /**
     * {@inheritDoc}
     */
    public void sendAbandonGame() throws IOException {
        // Simple send
        presetCommand(ServerOperation.ABANDON_GAME);

        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendForceSynchronizeMe() throws IOException {
        // Simple send
        presetCommand(ServerOperation.FORCE_SYNCHRONIZE_ME);

        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendExecuteMove(Move move) throws IOException {
        // Initial send
        presetCommand(ServerOperation.EXECUTE_MOVE);

        // Send arguments
        socketOutputStream.writeObject(move);

        // Send the buffer's content
        socketOutputStream.flush();
    }

    /**
     * {@inheritDoc}
     */
    public void sendPong() throws IOException {
        // Simple send
        presetCommand(ServerOperation.PONG);

        socketOutputStream.flush();
    }

    private void presetCommand(ServerOperation operation) throws IOException {
        // Now i send to the server the informations according to the network
        // protocol

        // First 8 bit for the version of the protocol
        socketOutputStream.writeByte(Protocol.V1.getVersion());
        // Bits for future changes
        socketOutputStream.writeInt(0);
        // 32 Bits for the kind of the move
        socketOutputStream.writeInt(operation.ordinal());
    }

    /**
     * {@inheritDoc}
     */
    public void sendLoginToServer(String username, String password)
            throws IOException {
        // Simple send
        presetCommand(ServerOperation.LOGIN_TO_SERVER);

        socketOutputStream.writeObject(username);
        socketOutputStream.writeObject(password);

        // Send the buffer's content
        socketOutputStream.flush();
    }

}
