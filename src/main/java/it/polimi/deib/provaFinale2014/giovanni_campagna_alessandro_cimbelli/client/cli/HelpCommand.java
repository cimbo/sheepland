package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client.cli;

/**
 * The "help" command, shows the available commands and their syntax.
 * 
 * @author Giovanni Campagna
 * 
 */
class HelpCommand extends BaseCommand {
    private static void listCommands(CLIUIManager ui) {
        ui.showMessage("Available commands:");
        ui.skipLine();
        ui.showMessage("be - be a different shepherd");
        ui.showMessage("look - look around");
        ui.showMessage("jump - jump to a different road");
        ui.showMessage("move - move a sheep between two regions");
        ui.showMessage("mate - mate a female sheep and a ram on a region");
        ui.showMessage("kill - kill a sheep on a region");
        ui.showMessage("buy - buy a terrain");
        ui.showMessage("sell - sell a terrain");
        ui.showMessage("end - end your turn");
        ui.showMessage("set - change game options");
        ui.showMessage("quit - quit the game");

        if (GameOptions.get().getShowHints()) {
            ui.skipLine();
            ui.showMessage("[Use help <command> to get help about a specific command]");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void printHelp(CLIUIManager ui) {
        ui.showMessage("help <COMMAND>: receive help about a specific command");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void parseAndDo(Tokenizer tokenizer, CLIUIManager ui)
            throws InvalidInputException {
        if (tokenizer.hasNext()) {
            tokenizer.readCommand().printHelp(ui);
        } else {
            listCommands(ui);
        }
    }
}
