package it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.client;

import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.game.controller.Move;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.AbstractSenderThread;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.ClientSender;
import it.polimi.deib.provaFinale2014.giovanni_campagna_alessandro_cimbelli.network.IORunnable;

import java.awt.EventQueue;
import java.io.IOException;

/**
 * Implements a thread safe, asynchronous writer thread for the connection to
 * the server.
 * 
 * @author Giovanni Campagna
 * 
 */
public class NetworkSender extends AbstractSenderThread {
    private final Game game;
    private final ClientSender sender;

    /**
     * Instantiate a new NetworkSender, given the writing side of the server
     * connection as ClientSender.
     * 
     * @param s
     */
    public NetworkSender(Game g, ClientSender s) {
        game = g;
        sender = s;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reportIOException(final IOException exception) {
        super.reportIOException(exception);

        final NetworkSender self = this;
        EventQueue.invokeLater(new Runnable() {
            /**
             * {@inheritDoc}
             */
            public void run() {
                game.reportError(self, exception);
            }
        });
    }

    /**
     * Send a "abandon game" command to the server. See {@link ClientSender} for
     * a description of the arguments.
     * 
     * The command is queued for writing in a worker thread, and therefore this
     * method never blocks.
     */
    public void sendAbandonGame() {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendAbandonGame();
            }
        });
    }

    /**
     * Send a "force resync me" command to the server. See {@link ClientSender}
     * for a description of the arguments.
     * 
     * The command is queued for writing in a worker thread, and therefore this
     * method never blocks.
     */
    public void sendForceSynchronizeMe() {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendForceSynchronizeMe();
            }
        });
    }

    /**
     * Send a "execute move" command to the server. See {@link ClientSender} for
     * a description of the arguments.
     * 
     * The command is queued for writing in a worker thread, and therefore this
     * method never blocks.
     */
    public void sendExecuteMove(final Move move) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendExecuteMove(move);
            }
        });
    }

    /**
     * Send a "login to server" command to the server. See {@link ClientSender}
     * for a description of the arguments.
     * 
     * The command is queued for writing in a worker thread, and therefore this
     * method never blocks.
     */
    public void sendLoginToServer(final String username, final String password) {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendLoginToServer(username, password);
            }
        });
    }

    /**
     * Send a "pong" command to the server. See {@link ClientSender} for a
     * description of the arguments.
     * 
     * The command is queued for writing in a worker thread, and therefore this
     * method never blocks.
     */
    public void sendPong() {
        safePut(new IORunnable() {
            /**
             * {@inheritDoc}
             */
            public void run() throws IOException {
                sender.sendPong();
            }
        });
    }
}
