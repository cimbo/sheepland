all:
	mvn compile

test:
	mvn test

install:
	mvn package

clean:
	rm -fr target/
